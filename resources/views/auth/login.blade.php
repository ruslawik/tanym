@extends('front.layout')
@section('content')
<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
					<div class="notification _dn _hide">
						<span class="notification__text">
							{{ session('message') }}
						</span>
						<span class="notification__close"></span>
					</div>
				@endif
						<section class="login">
							<div class="login__tabs">
								<div data-tabs class="tabs-login">
									<div class="tabs-login__tabs">
										<nav data-tabs-titles class="tabs-login__navigation">
											<button type="button" class="tabs-login__subtitle _tab-active">{{__('front.login')}}</button>
											<button type="button" class="tabs-login__subtitle">{{__('front.register')}}</button>
											<button type="button" class="tabs-login__subtitle">{{__('front.pass_forget')}}?</button>
										</nav>
										<div data-tabs-body class="tabs-login__content">
											<div class="tabs-login__body">
												<div class="tabs-login__head">
													<h2 class="tabs-login__title">{{__('front.login')}}</h2>
												</div>
												<form method="POST" action="{{ route('login') }}" class="tabs-login__form form-tabs">
													@csrf
													<div class="form-tabs__line">
														<div class="form-tabs__title">Email</div>
														<input type="text" data-required="email" data-error="Введите правильный Email" placeholder="example@example.com" class="form-tabs__input" id="email" name="email" type="text" required autocomplete="email" autofocus value="{{ old('email', null) }}">
														@if($errors->has('email'))
								                            <div class="invalid-feedback">
								                                {{ $errors->first('email') }}
								                            </div>
                        								@endif
													</div>
													<div class="form-tabs__line">
														<div class="form-tabs__title">{{__('front.password')}}</div>
														<input name="password" type="password" data-required data-error="Это обязательное поле" placeholder="{{__('front.enter_pass')}}" class="form-tabs__input">
														<button type="button" class="form-tabs__viewpass" style="background-image: url(/front/img/icons/eye.svg);">
														</button>
														@if($errors->has('password'))
								                            <div class="invalid-feedback">
								                                {{ $errors->first('password') }}
								                            </div>
                        								@endif
													</div>
													<div class="form-tabs__line form-tabs__line_bd">&nbsp;
													</div>
													<div class="form-tabs__line">
														<button type="submit" class="form-tabs__button purple__button">{{__('front.login')}}</button>
													</div>
												</form>
											</div>
											<div class="tabs-login__body">
												<div class="tabs-login__head">
													<h2 class="tabs-login__title">{{__('front.register')}}</h2>
												</div>
												<form method="POST" action="{{ route('register') }}" class="tabs-login__form form-tabs">
													{{ csrf_field() }}
													<div class="settings-form__line">
														<div class="settings-form__subtitle">{{__('front.name')}}</div>
														<input type="text" placeholder="Санжар" data-error="Обязательное поле" name="name" class="settings-form__input">
														@if($errors->has('name'))
								                            <div class="invalid-feedback">
								                                {{ $errors->first('name') }}
								                            </div>
								                        @endif
													</div>
													<div class="settings-form__line">
														<div class="settings-form__subtitle">Email</div>
														<input type="text" placeholder="example@example.com" data-error="Обязательное поле" data-required="email" class="settings-form__input" name="email">
														@if($errors->has('email'))
								                            <div class="invalid-feedback">
								                                {{ $errors->first('email') }}
								                            </div>
								                        @endif
													</div>
													<div class="settings-form__line settings-form__line_bd"></div>
													<div class="settings-form__line password-settings">
														<div class="password-settings__line">
															<div class="settings-form__subtitle">{{__('front.password')}}</div>
														</div>
														<div class="password-settings__line">
															<input type="password" name="password" id="newPass1" placeholder="{{__('front.enter_pass')}}" data-required class="settings-form__input">
															<button type="button" class="settings-form__viewpass" style="background-image: url(/front/img/icons/eye.svg);"></button>
															@if($errors->has('password'))
									                            <div class="invalid-feedback">
									                                {{ $errors->first('password') }}
									                            </div>
									                        @endif
														</div>
														<div class="password-settings__line">
															<input type="password" name="password_confirmation" id="newPass2" placeholder="{{__('front.prove_pass')}}" data-required class="settings-form__input">
															<button type="button" class="settings-form__viewpass" style="background-image: url(/front/img/icons/eye.svg);"></button>
														</div>
														@if(isset($_GET['ref']) && is_numeric($_GET['ref']))
															<input type="hidden" name="ref" value="{{$_GET['ref']}}">
														@endif
													</div>
													<div class="settings-form__line settings-form__line_bd"></div>
													<div class="settings-form__line">
														<button class="settings-form__button purple__button">{{__('front.register')}}</button>
													</div>
												</form>
											</div>


											<div class="tabs-login__body">
												<div class="tabs-login__head">
													<h2 class="tabs-login__title">{{__('front.pass_forget')}}?</h2>
												</div>
												@if(session('message'))
								                    <div class="alert alert-info" role="alert">
								                        {{ session('message') }}
								                    </div>
                								@endif
												<form method="POST" action="/send-new-pass" class="tabs-login__form form-tabs">
													@csrf
													<div class="form-tabs__line">
														<div class="form-tabs__title">{{__('front.provide_email')}}</div>
														<input type="text" data-required="email" data-error="Введите правильный Email" placeholder="example@example.com" class="form-tabs__input" id="email" name="email" type="text" required autocomplete="email" autofocus value="{{ old('email', null) }}">
														@if($errors->has('email'))
								                            <div class="invalid-feedback">
								                                {{ $errors->first('email') }}
								                            </div>
                        								@endif
													</div>
													<div class="form-tabs__line form-tabs__line_bd">&nbsp;
													</div>
													<div class="form-tabs__line">
														<button type="submit" class="form-tabs__button purple__button">{{__('front.send_pass_email')}}</button>
													</div>
												</form>
											</div>

										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection