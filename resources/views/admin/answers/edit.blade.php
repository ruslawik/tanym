@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.answer.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.answers.update", [$answer->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="question_id">{{ trans('cruds.answer.fields.question') }}</label>
                <select class="form-control select2 {{ $errors->has('question') ? 'is-invalid' : '' }}" name="question_id" id="question_id">
                    @foreach($questions as $id => $entry)
                        <option value="{{ $id }}" {{ (old('question_id') ? old('question_id') : $answer->question->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('question'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.question_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="answer">{{ trans('cruds.answer.fields.answer') }}</label>
                <textarea class="form-control {{ $errors->has('answer') ? 'is-invalid' : '' }}" name="answer" id="answer">{{ old('answer', $answer->answer) }}</textarea>
                @if($errors->has('answer'))
                    <div class="invalid-feedback">
                        {{ $errors->first('answer') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.answer_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="answer_kz">{{ trans('cruds.answer.fields.answer_kz') }}</label>
                <textarea class="form-control {{ $errors->has('answer_kz') ? 'is-invalid' : '' }}" name="answer_kz" id="answer_kz">{{ old('answer_kz', $answer->answer_kz) }}</textarea>
                @if($errors->has('answer_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('answer_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.answer_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="answer_en">{{ trans('cruds.answer.fields.answer_en') }}</label>
                <textarea class="form-control {{ $errors->has('answer_en') ? 'is-invalid' : '' }}" name="answer_en" id="answer_en">{{ old('answer_en', $answer->answer_en) }}</textarea>
                @if($errors->has('answer_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('answer_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.answer_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="is_right">{{ trans('cruds.answer.fields.is_right') }}</label>
                <input class="form-control {{ $errors->has('is_right') ? 'is-invalid' : '' }}" type="number" name="is_right" id="is_right" value="{{ old('is_right', $answer->is_right) }}" step="1">
                @if($errors->has('is_right'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_right') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.is_right_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection