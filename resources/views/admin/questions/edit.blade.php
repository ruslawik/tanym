@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.question.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.questions.update", [$question->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="test_id">{{ trans('cruds.question.fields.test') }}</label>
                <select class="form-control select2 {{ $errors->has('test') ? 'is-invalid' : '' }}" name="test_id" id="test_id">
                    @foreach($tests as $id => $entry)
                        <option value="{{ $id }}" {{ (old('test_id') ? old('test_id') : $question->test->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('test'))
                    <div class="invalid-feedback">
                        {{ $errors->first('test') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.test_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="question">{{ trans('cruds.question.fields.question') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('question') ? 'is-invalid' : '' }}" name="question" id="question">{!! old('question', $question->question) !!}</textarea>
                @if($errors->has('question'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.question_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="question_kz">{{ trans('cruds.question.fields.question_kz') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('question_kz') ? 'is-invalid' : '' }}" name="question_kz" id="question_kz">{!! old('question_kz', $question->question_kz) !!}</textarea>
                @if($errors->has('question_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.question_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="question_en">{{ trans('cruds.question.fields.question_en') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('question_en') ? 'is-invalid' : '' }}" name="question_en" id="question_en">{!! old('question_en', $question->question_en) !!}</textarea>
                @if($errors->has('question_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.question_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="hint">{{ trans('cruds.question.fields.hint') }}</label>
                <input class="form-control {{ $errors->has('hint') ? 'is-invalid' : '' }}" type="text" name="hint" id="hint" value="{{ old('hint', $question->hint) }}">
                @if($errors->has('hint'))
                    <div class="invalid-feedback">
                        {{ $errors->first('hint') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.question.fields.hint_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.questions.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $question->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection