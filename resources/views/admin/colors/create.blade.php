@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.color.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.colors.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="html_color">{{ trans('cruds.color.fields.html_color') }}</label>
                <input class="form-control {{ $errors->has('html_color') ? 'is-invalid' : '' }}" type="text" name="html_color" id="html_color" value="{{ old('html_color', '') }}" required>
                @if($errors->has('html_color'))
                    <div class="invalid-feedback">
                        {{ $errors->first('html_color') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.color.fields.html_color_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection