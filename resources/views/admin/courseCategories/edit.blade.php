@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.courseCategory.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.course-categories.update", [$courseCategory->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.courseCategory.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $courseCategory->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseCategory.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_kz">{{ trans('cruds.courseCategory.fields.name_kz') }}</label>
                <input class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" type="text" name="name_kz" id="name_kz" value="{{ old('name_kz', $courseCategory->name_kz) }}">
                @if($errors->has('name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseCategory.fields.name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_en">{{ trans('cruds.courseCategory.fields.name_en') }}</label>
                <input class="form-control {{ $errors->has('name_en') ? 'is-invalid' : '' }}" type="text" name="name_en" id="name_en" value="{{ old('name_en', $courseCategory->name_en) }}">
                @if($errors->has('name_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseCategory.fields.name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection