@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.gameBackground.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.game-backgrounds.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.gameBackground.fields.id') }}
                        </th>
                        <td>
                            {{ $gameBackground->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameBackground.fields.name') }}
                        </th>
                        <td>
                            {{ $gameBackground->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameBackground.fields.image') }}
                        </th>
                        <td>
                            @if($gameBackground->image)
                                <a href="{{ $gameBackground->image->getUrl() }}" target="_blank" style="display: inline-block">
                                    <img src="{{ $gameBackground->image->getUrl('thumb') }}">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameBackground.fields.ritm_price') }}
                        </th>
                        <td>
                            {{ $gameBackground->ritm_price }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.game-backgrounds.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection