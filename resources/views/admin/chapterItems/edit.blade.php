@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.chapterItem.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.chapter-items.update", [$chapterItem->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="name">{{ trans('cruds.chapterItem.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $chapterItem->name) }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.chapterItem.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_kz">{{ trans('cruds.chapterItem.fields.name_kz') }}</label>
                <input class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" type="text" name="name_kz" id="name_kz" value="{{ old('name_kz', $chapterItem->name_kz) }}">
                @if($errors->has('name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.chapterItem.fields.name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_en">{{ trans('cruds.chapterItem.fields.name_en') }}</label>
                <input class="form-control {{ $errors->has('name_en') ? 'is-invalid' : '' }}" type="text" name="name_en" id="name_en" value="{{ old('name_en', $chapterItem->name_en) }}">
                @if($errors->has('name_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.chapterItem.fields.name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="element_id">{{ trans('cruds.chapterItem.fields.element') }}</label>
                <select class="form-control select2 {{ $errors->has('element') ? 'is-invalid' : '' }}" name="element_id" id="element_id">
                    @foreach($elements as $id => $entry)
                        <option value="{{ $id }}" {{ (old('element_id') ? old('element_id') : $chapterItem->element->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('element'))
                    <div class="invalid-feedback">
                        {{ $errors->first('element') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.chapterItem.fields.element_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection