@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.popupNews.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.popup-news.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.popupNews.fields.id') }}
                        </th>
                        <td>
                            {{ $popupNews->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.popupNews.fields.text') }}
                        </th>
                        <td>
                            {{ $popupNews->text }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.popupNews.fields.text_kz') }}
                        </th>
                        <td>
                            {{ $popupNews->text_kz }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.popupNews.fields.text_en') }}
                        </th>
                        <td>
                            {{ $popupNews->text_en }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.popup-news.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection