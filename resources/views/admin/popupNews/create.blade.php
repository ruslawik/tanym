@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.popupNews.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.popup-news.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="text">{{ trans('cruds.popupNews.fields.text') }}</label>
                <textarea class="form-control {{ $errors->has('text') ? 'is-invalid' : '' }}" name="text" id="text">{{ old('text') }}</textarea>
                @if($errors->has('text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.popupNews.fields.text_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="text_kz">{{ trans('cruds.popupNews.fields.text_kz') }}</label>
                <textarea class="form-control {{ $errors->has('text_kz') ? 'is-invalid' : '' }}" name="text_kz" id="text_kz">{{ old('text_kz') }}</textarea>
                @if($errors->has('text_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('text_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.popupNews.fields.text_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="text_en">{{ trans('cruds.popupNews.fields.text_en') }}</label>
                <textarea class="form-control {{ $errors->has('text_en') ? 'is-invalid' : '' }}" name="text_en" id="text_en">{{ old('text_en') }}</textarea>
                @if($errors->has('text_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('text_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.popupNews.fields.text_en_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection