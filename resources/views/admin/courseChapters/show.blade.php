@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.courseChapter.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.course-chapters.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.courseChapter.fields.id') }}
                        </th>
                        <td>
                            {{ $courseChapter->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseChapter.fields.name') }}
                        </th>
                        <td>
                            {{ $courseChapter->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseChapter.fields.name_kz') }}
                        </th>
                        <td>
                            {{ $courseChapter->name_kz }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseChapter.fields.name_en') }}
                        </th>
                        <td>
                            {{ $courseChapter->name_en }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseChapter.fields.course') }}
                        </th>
                        <td>
                            {{ $courseChapter->course->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.course-chapters.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection