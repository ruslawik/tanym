@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.courseChapter.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.course-chapters.update", [$courseChapter->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="name">{{ trans('cruds.courseChapter.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $courseChapter->name) }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseChapter.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_kz">{{ trans('cruds.courseChapter.fields.name_kz') }}</label>
                <input class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" type="text" name="name_kz" id="name_kz" value="{{ old('name_kz', $courseChapter->name_kz) }}">
                @if($errors->has('name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseChapter.fields.name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_en">{{ trans('cruds.courseChapter.fields.name_en') }}</label>
                <input class="form-control {{ $errors->has('name_en') ? 'is-invalid' : '' }}" type="text" name="name_en" id="name_en" value="{{ old('name_en', $courseChapter->name_en) }}">
                @if($errors->has('name_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseChapter.fields.name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="course_id">{{ trans('cruds.courseChapter.fields.course') }}</label>
                <select class="form-control select2 {{ $errors->has('course') ? 'is-invalid' : '' }}" name="course_id" id="course_id">
                    @foreach($courses as $id => $entry)
                        <option value="{{ $id }}" {{ (old('course_id') ? old('course_id') : $courseChapter->course->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('course'))
                    <div class="invalid-feedback">
                        {{ $errors->first('course') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courseChapter.fields.course_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection