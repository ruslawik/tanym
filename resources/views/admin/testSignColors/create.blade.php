@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.testSignColor.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.test-sign-colors.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="course_id">{{ trans('cruds.testSignColor.fields.course') }}</label>
                <select class="form-control select2 {{ $errors->has('course') ? 'is-invalid' : '' }}" name="course_id" id="course_id">
                    @foreach($courses as $id => $entry)
                        <option value="{{ $id }}" {{ old('course_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('course'))
                    <div class="invalid-feedback">
                        {{ $errors->first('course') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.testSignColor.fields.course_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="test_id">{{ trans('cruds.testSignColor.fields.test') }}</label>
                <select class="form-control select2 {{ $errors->has('test') ? 'is-invalid' : '' }}" name="test_id" id="test_id">
                    @foreach($tests as $id => $entry)
                        <option value="{{ $id }}" {{ old('test_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('test'))
                    <div class="invalid-feedback">
                        {{ $errors->first('test') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.testSignColor.fields.test_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="sign_id">{{ trans('cruds.testSignColor.fields.sign') }}</label>
                <select class="form-control select2 {{ $errors->has('sign') ? 'is-invalid' : '' }}" name="sign_id" id="sign_id">
                    @foreach($signs as $id => $entry)
                        <option value="{{ $id }}" {{ old('sign_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('sign'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sign') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.testSignColor.fields.sign_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="color_id">{{ trans('cruds.testSignColor.fields.color') }}</label>
                <select class="form-control select2 {{ $errors->has('color') ? 'is-invalid' : '' }}" name="color_id" id="color_id">
                    @foreach($colors as $id => $entry)
                        <option value="{{ $id }}" {{ old('color_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('color'))
                    <div class="invalid-feedback">
                        {{ $errors->first('color') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.testSignColor.fields.color_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_id">{{ trans('cruds.testSignColor.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $entry)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.testSignColor.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection