@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.test.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tests.update", [$test->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="chapter_item_id">{{ trans('cruds.test.fields.chapter_item') }}</label>
                <select class="form-control select2 {{ $errors->has('chapter_item') ? 'is-invalid' : '' }}" name="chapter_item_id" id="chapter_item_id">
                    @foreach($chapter_items as $id => $entry)
                        <option value="{{ $id }}" {{ (old('chapter_item_id') ? old('chapter_item_id') : $test->chapter_item->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('chapter_item'))
                    <div class="invalid-feedback">
                        {{ $errors->first('chapter_item') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.chapter_item_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name">{{ trans('cruds.test.fields.name') }}</label>
                <textarea class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" id="name">{{ old('name', $test->name) }}</textarea>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_kz">{{ trans('cruds.test.fields.name_kz') }}</label>
                <textarea class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" name="name_kz" id="name_kz">{{ old('name_kz', $test->name_kz) }}</textarea>
                @if($errors->has('name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_en">{{ trans('cruds.test.fields.name_en') }}</label>
                <textarea class="form-control {{ $errors->has('name_en') ? 'is-invalid' : '' }}" name="name_en" id="name_en">{{ old('name_en', $test->name_en) }}</textarea>
                @if($errors->has('name_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.test.fields.description') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{!! old('description', $test->description) !!}</textarea>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description_kz">{{ trans('cruds.test.fields.description_kz') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('description_kz') ? 'is-invalid' : '' }}" name="description_kz" id="description_kz">{!! old('description_kz', $test->description_kz) !!}</textarea>
                @if($errors->has('description_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.description_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description_en">{{ trans('cruds.test.fields.description_en') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('description_en') ? 'is-invalid' : '' }}" name="description_en" id="description_en">{!! old('description_en', $test->description_en) !!}</textarea>
                @if($errors->has('description_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.test.fields.description_en_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.tests.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $test->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection