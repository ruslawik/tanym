@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.file.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.files.update", [$file->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="chapter_item_id">{{ trans('cruds.file.fields.chapter_item') }}</label>
                <select class="form-control select2 {{ $errors->has('chapter_item') ? 'is-invalid' : '' }}" name="chapter_item_id" id="chapter_item_id">
                    @foreach($chapter_items as $id => $entry)
                        <option value="{{ $id }}" {{ (old('chapter_item_id') ? old('chapter_item_id') : $file->chapter_item->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('chapter_item'))
                    <div class="invalid-feedback">
                        {{ $errors->first('chapter_item') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.chapter_item_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="file_name">{{ trans('cruds.file.fields.file_name') }}</label>
                <textarea class="form-control {{ $errors->has('file_name') ? 'is-invalid' : '' }}" name="file_name" id="file_name">{{ old('file_name', $file->file_name) }}</textarea>
                @if($errors->has('file_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('file_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.file_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="uploaded_file">{{ trans('cruds.file.fields.uploaded_file') }}</label>
                <div class="needsclick dropzone {{ $errors->has('uploaded_file') ? 'is-invalid' : '' }}" id="uploaded_file-dropzone">
                </div>
                @if($errors->has('uploaded_file'))
                    <div class="invalid-feedback">
                        {{ $errors->first('uploaded_file') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.file.fields.uploaded_file_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.uploadedFileDropzone = {
    url: '{{ route('admin.files.storeMedia') }}',
    maxFilesize: 100, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 100
    },
    success: function (file, response) {
      $('form').find('input[name="uploaded_file"]').remove()
      $('form').append('<input type="hidden" name="uploaded_file" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="uploaded_file"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($file) && $file->uploaded_file)
      var file = {!! json_encode($file->uploaded_file) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="uploaded_file" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection