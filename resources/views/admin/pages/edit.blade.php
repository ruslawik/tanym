@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.page.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.pages.update", [$page->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="chapter_item_id">{{ trans('cruds.page.fields.chapter_item') }}</label>
                <select class="form-control select2 {{ $errors->has('chapter_item') ? 'is-invalid' : '' }}" name="chapter_item_id" id="chapter_item_id">
                    @foreach($chapter_items as $id => $entry)
                        <option value="{{ $id }}" {{ (old('chapter_item_id') ? old('chapter_item_id') : $page->chapter_item->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('chapter_item'))
                    <div class="invalid-feedback">
                        {{ $errors->first('chapter_item') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.chapter_item_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="page_name">{{ trans('cruds.page.fields.page_name') }}</label>
                <textarea class="form-control {{ $errors->has('page_name') ? 'is-invalid' : '' }}" name="page_name" id="page_name">{{ old('page_name', $page->page_name) }}</textarea>
                @if($errors->has('page_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('page_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.page_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="page_name_kz">{{ trans('cruds.page.fields.page_name_kz') }}</label>
                <textarea class="form-control {{ $errors->has('page_name_kz') ? 'is-invalid' : '' }}" name="page_name_kz" id="page_name_kz">{{ old('page_name_kz', $page->page_name_kz) }}</textarea>
                @if($errors->has('page_name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('page_name_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.page_name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="page_name_en">{{ trans('cruds.page.fields.page_name_en') }}</label>
                <textarea class="form-control {{ $errors->has('page_name_en') ? 'is-invalid' : '' }}" name="page_name_en" id="page_name_en">{{ old('page_name_en', $page->page_name_en) }}</textarea>
                @if($errors->has('page_name_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('page_name_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.page_name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="content">{{ trans('cruds.page.fields.content') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('content') ? 'is-invalid' : '' }}" name="content" id="content">{!! old('content', $page->content) !!}</textarea>
                @if($errors->has('content'))
                    <div class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.content_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="content_kz">{{ trans('cruds.page.fields.content_kz') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('content_kz') ? 'is-invalid' : '' }}" name="content_kz" id="content_kz">{!! old('content_kz', $page->content_kz) !!}</textarea>
                @if($errors->has('content_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('content_kz') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.content_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="content_en">{{ trans('cruds.page.fields.content_en') }}</label>
                <textarea class="form-control {{ $errors->has('content_en') ? 'is-invalid' : '' }}" name="content_en" id="content_en">{{ old('content_en', $page->content_en) }}</textarea>
                @if($errors->has('content_en'))
                    <div class="invalid-feedback">
                        {{ $errors->first('content_en') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.content_en_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.pages.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $page->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection