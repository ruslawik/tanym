@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.passedTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.passed-tests.update", [$passedTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="test_id">{{ trans('cruds.passedTest.fields.test') }}</label>
                <select class="form-control select2 {{ $errors->has('test') ? 'is-invalid' : '' }}" name="test_id" id="test_id">
                    @foreach($tests as $id => $entry)
                        <option value="{{ $id }}" {{ (old('test_id') ? old('test_id') : $passedTest->test->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('test'))
                    <div class="invalid-feedback">
                        {{ $errors->first('test') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.passedTest.fields.test_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="question_id">{{ trans('cruds.passedTest.fields.question') }}</label>
                <select class="form-control select2 {{ $errors->has('question') ? 'is-invalid' : '' }}" name="question_id" id="question_id">
                    @foreach($questions as $id => $entry)
                        <option value="{{ $id }}" {{ (old('question_id') ? old('question_id') : $passedTest->question->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('question'))
                    <div class="invalid-feedback">
                        {{ $errors->first('question') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.passedTest.fields.question_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="answer_id">{{ trans('cruds.passedTest.fields.answer') }}</label>
                <select class="form-control select2 {{ $errors->has('answer') ? 'is-invalid' : '' }}" name="answer_id" id="answer_id">
                    @foreach($answers as $id => $entry)
                        <option value="{{ $id }}" {{ (old('answer_id') ? old('answer_id') : $passedTest->answer->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('answer'))
                    <div class="invalid-feedback">
                        {{ $errors->first('answer') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.passedTest.fields.answer_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_id">{{ trans('cruds.passedTest.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $entry)
                        <option value="{{ $id }}" {{ (old('user_id') ? old('user_id') : $passedTest->user->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.passedTest.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection