@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.courseElement.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.course-elements.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.courseElement.fields.id') }}
                        </th>
                        <td>
                            {{ $courseElement->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseElement.fields.name') }}
                        </th>
                        <td>
                            {{ $courseElement->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseElement.fields.name_kz') }}
                        </th>
                        <td>
                            {{ $courseElement->name_kz }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseElement.fields.name_en') }}
                        </th>
                        <td>
                            {{ $courseElement->name_en }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.courseElement.fields.icon') }}
                        </th>
                        <td>
                            @if($courseElement->icon)
                                <a href="{{ $courseElement->icon->getUrl() }}" target="_blank" style="display: inline-block">
                                    <img src="{{ $courseElement->icon->getUrl('thumb') }}">
                                </a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.course-elements.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection