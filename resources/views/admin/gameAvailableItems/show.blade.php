@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.gameAvailableItem.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.game-available-items.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.gameAvailableItem.fields.id') }}
                        </th>
                        <td>
                            {{ $gameAvailableItem->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameAvailableItem.fields.item_type') }}
                        </th>
                        <td>
                            {{ $gameAvailableItem->item_type }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameAvailableItem.fields.element') }}
                        </th>
                        <td>
                            {{ $gameAvailableItem->element }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameAvailableItem.fields.user') }}
                        </th>
                        <td>
                            {{ $gameAvailableItem->user->email ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.gameAvailableItem.fields.installed') }}
                        </th>
                        <td>
                            {{ $gameAvailableItem->installed }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.game-available-items.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection