@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.gameAvailableItem.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.game-available-items.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="item_type">{{ trans('cruds.gameAvailableItem.fields.item_type') }}</label>
                <input class="form-control {{ $errors->has('item_type') ? 'is-invalid' : '' }}" type="text" name="item_type" id="item_type" value="{{ old('item_type', '') }}">
                @if($errors->has('item_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('item_type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.gameAvailableItem.fields.item_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="element">{{ trans('cruds.gameAvailableItem.fields.element') }}</label>
                <input class="form-control {{ $errors->has('element') ? 'is-invalid' : '' }}" type="text" name="element" id="element" value="{{ old('element', '') }}">
                @if($errors->has('element'))
                    <div class="invalid-feedback">
                        {{ $errors->first('element') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.gameAvailableItem.fields.element_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_id">{{ trans('cruds.gameAvailableItem.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $entry)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.gameAvailableItem.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="installed">{{ trans('cruds.gameAvailableItem.fields.installed') }}</label>
                <input class="form-control {{ $errors->has('installed') ? 'is-invalid' : '' }}" type="number" name="installed" id="installed" value="{{ old('installed', '') }}" step="1">
                @if($errors->has('installed'))
                    <div class="invalid-feedback">
                        {{ $errors->first('installed') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.gameAvailableItem.fields.installed_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection