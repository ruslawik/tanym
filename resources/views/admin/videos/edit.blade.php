@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.video.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.videos.update", [$video->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="chapter_item_id">{{ trans('cruds.video.fields.chapter_item') }}</label>
                <select class="form-control select2 {{ $errors->has('chapter_item') ? 'is-invalid' : '' }}" name="chapter_item_id" id="chapter_item_id">
                    @foreach($chapter_items as $id => $entry)
                        <option value="{{ $id }}" {{ (old('chapter_item_id') ? old('chapter_item_id') : $video->chapter_item->id ?? '') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('chapter_item'))
                    <div class="invalid-feedback">
                        {{ $errors->first('chapter_item') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.video.fields.chapter_item_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="video_code">{{ trans('cruds.video.fields.video_code') }}</label>
                <textarea class="form-control {{ $errors->has('video_code') ? 'is-invalid' : '' }}" name="video_code" id="video_code">{{ old('video_code', $video->video_code) }}</textarea>
                @if($errors->has('video_code'))
                    <div class="invalid-feedback">
                        {{ $errors->first('video_code') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.video.fields.video_code_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="video_description">{{ trans('cruds.video.fields.video_description') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('video_description') ? 'is-invalid' : '' }}" name="video_description" id="video_description">{!! old('video_description', $video->video_description) !!}</textarea>
                @if($errors->has('video_description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('video_description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.video.fields.video_description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.videos.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $video->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection