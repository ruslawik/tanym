@if ($paginator->hasPages())
    <div class="output-search__btns test-test__btns">
        @if ($paginator->onFirstPage())
            <div class="output-search__btn output-search__btn_dis purple__button">Пред.</div>
        @else
            <a href="{{ $paginator->previousPageUrl() }}"> <div class="output-search__btn output-search__btn purple__button">Пред.</div></a>
        @endif

        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}"><div class="output-search__btn output-search__btn purple__button">След.</div></a>
        @else
            <div class="output-search__btn output-search__btn_dis purple__button">След.</div>
        @endif
    </div>
@endif
