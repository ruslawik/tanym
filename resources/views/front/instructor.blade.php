@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="profile">

							<div class="section__head">
								<h2 class="section__title">{{__('front.instructor_profile')}}</h2>

							</div>

							<div class="profile__body">
								<div class="profile__user user-profile">
									<div class="user-profile__ava">
										<picture><img src="{{$instructor->avatar->url}}" alt="Avatar"></picture>
									</div>
									<div class="user-profile__name">{{$instructor->name}}</div>
									@if(app()->getLocale()=="ru")
										<div class="user-profile__profession">{{$instructor->regalia}}</div>
									@else
										<div class="user-profile__profession">{{$instructor->regalia_kz}}</div>
									@endif

									@if(app()->getLocale()=="ru")
										{!!$instructor->instructor_description!!}
									@else
										{!!$instructor->instructor_description_kz!!}
									@endif

								</div>
							</div>
						</section>
						<section class="reccomend">
							<div class="reccomend__head">
								<h2 class="reccomend__title">{{__('front.instructor_courses')}}</h2>
							</div>
							<div class="reccomend__body">
								<div class="reccomend__items">
											@foreach($courses as $course)
											<div class="reccomend__item item-reccomend undefined">
												<a href="/public/course/{{$course->id}}">
												<div class="item-reccomend__image">
													<picture><img height=200px src="{{$course->image->url}}" alt="item-reccomend__image" class="item-reccomend__img"></picture>
													@if($course->draft == 1)
														<div class="item-reccomend__popular undefined"> {{__('front.draft')}}</div>
													@endif
												</div>
												</a>
												<br>
												<a class="item-reccomend__title" href="/public/course/{{$course->id}}"><h3>{{$course->name}}</h3></a>
												<br>
												<a href="" class="item-reccomend__user user-item-reccomend">
													<div class="user-item-reccomend__avatar">
														<picture><img src="{{$course->author->avatar->thumbnail}}" alt="Avatar"></picture>
													</div>
													<div class="user-item-reccomend__name">{{$course->author->name}}</div>
												</a>
												<div class="item-reccomend__actions">
													<div class="item-reccomend__rating">
														<div class="rating rating_set">
															<div class="rating__body">
																<div class="rating__active"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach
										</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection