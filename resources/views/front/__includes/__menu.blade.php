<ul class="menu__list">
	<li class="menu__item"><a href="/" class="menu__link"> <span class="_icon-header_home">{{__('front.main')}}</span> </a></li>
	<li class="menu__item"><a href="/search" class="menu__link  "> <span class="_icon-header_search">{{__('front.search')}}</span> </a></li>
	@if(Auth::check())
		<li class="menu__item"><a href="/cabinet" class="menu__link ">
			<img src="/front/icons/user-circle.svg" width=20 style="margin-right: 10px;">
		{{__('front.my_profile')}}</a></li>
		@if(Auth::user()->game_on == 1)
			<li class="menu__item"><a href="/base" class="menu__link">
				<img src="/front/icons/rocket.svg" width=20 style="margin-right: 10px;">{{__('front.my_base')}}</a>
			</li>
			<li class="menu__item"><a href="/wallet" class="menu__link">
				<img src="/front/icons/money.png" width=17 style="margin-right: 10px;">{{__('front.my_wallet')}}</a>
			</li>
		@endif
		@if(Auth::user()->roles->contains(3) || Auth::user()->roles->contains(1))
			<li class="menu__item"><a href="/mycourses" class="menu__link">
				<img src="/front/icons/book-open.svg" width=20 style="margin-right: 10px;">{{__('front.my_courses')}}</a>
			</li>											
		@endif
	@endif
	@if(!Auth::check())
		<li class="menu__item"><a href="/login" class="menu__link"><span class="_icon-header_cabinet">{{__('front.login_register')}}</span></a></li>
		<li class="menu__item"><a href="/base-example" class="menu__link">
				<img src="/front/icons/rocket.svg" width=20 style="margin-right: 10px;">{{__('front.my_base')}}</a></li>
	@endif
	<li class="menu__item"><a href="/rating" class="menu__link">
		<img src="/front/icons/crown.svg" width=25 style="margin-right: 10px;"> {{__('front.rating')}}</a>
	</li>
	<li class="menu__item"><a href="/news" class="menu__link">
				<img src="/front/icons/news.svg" width=23 style="margin-right: 10px;"> {{__('front.news')}}</a></li>
</ul>