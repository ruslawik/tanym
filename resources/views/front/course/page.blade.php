@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="textimg">
							<div class="textimg__body">
								<div class="textimg__info">
									<h4 class="section__title">{{$chapter[0]->name}} - {{$element[0]->name}}</h4>
									<br>
									{!!$page[0]->content!!}
								</div>

								@include('front.course.__includes.__side_menu')
								<div class="list__close"></div>
								<span class="list__button">{{__('front.soder')}}</span>
							</div>

						</section>
					</div>
				</div>
			</div>
		</main>
@endsection