							<div class="addchapter__list addchapter__list_read">
									<div class="list-addchapter">
										<div class="list-addchapter__title">{{__('front.soder')}}</div>
										<div class="list-addchapter__list">
											<div data-spollers class="spollers">
												@foreach($course->chapters as $chapter)
												<div class="spollers__item">
													<button type="button" data-spoller class="spollers__title ">{{$chapter->name}}</button>
													<div class="spollers__body body-spoller">
														@foreach($chapter->items as $item)
															<a href="/course/{{$course->id}}/chapter/{{$chapter->id}}/element/{{$item->id}}" class="body-spoller__content">
																<span style="margin-right: 8px;"><img src="{{$item->element->icon->url}}" width=20></span>
																<span class="body-spoller__text">{{$item->name}}</span>
																<div class="body-spoller__span"></div>
															</a>
														@endforeach
													</div>
												</div>
												@endforeach
											</div>
										</div>
										<div class="list-addchapter__author author-addchapter">
											<div class="author-addchapter__title">Автор</div>
											<div class="author-addchapter__body" style="align-items: flex-start !important;">
												<div class="author-addchapter__img">
													<img src="{{$course->author->avatar->url}}" alt="avatar">
												</div>
												<div class="author-addchapter__content">
													<div class="author-addchapter__name">{{$course->author->name}} {{$course->author->surname}}</div>
													<div class="author-addchapter__text">{{$course->author->regalia}}</div>
													<a href="/instructor/{{$course->author->id}}" class="author-addchapter__link">{{__('front.all_courses')}}</a>
												</div>
											</div>
										</div>
									</div>
								</div>