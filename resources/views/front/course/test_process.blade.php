@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="test">
							<div class="test__body">
								<div class="test__info info-test">

									<div class="section__head">
										<h4 class="section__title">{{$chapter[0]->name}} - {{$element[0]->name}}</h4>
									</div>
									<div class="info-test__body">
										@if($auth == 0)
											<b>{{__('front.test_auth')}}</b>
										@else
										<form id="test_form" action="/test/{{$test[0]->id}}/finish" method="POST" class="info-test__test test-test">
										<input type="hidden" id="user_ritms" value="{{Auth::user()->ritms}}">
										<input type="hidden" name="course_id" value="{{$course->id}}">
										@csrf
										@php $i=1 @endphp
										@foreach($questions as $question)
										<span id="question_block_{{$i}}" @if($i!=1) style="display:none;" @endif>
											<div class="test-test__head head-test">
												<div class="head-test__question">
													{{__('front.quest')}} <span class="head-test__from">{{$i}}</span>/<span class="head-test__to">{{$total_questions}}</span>
												</div>
												@if(Auth::user()->game_on == 1)
													<a href="#" onClick="giveHint({{$question->id}}, {{$question->total_answers()}});" class="head-test__clue">{{__('front.test_hint')}} 50/50 (10 ртм.)</a>
												@endif
											</div>
											<div class="test-test__body">
												<div class="test-test__line">
													<span class="test-test__subtitle">{!!$question->question!!}</span>
												</div>
												<div class="test-test__line test-test__line_bd"></div>
												@php $k=0 @endphp
												@foreach($question->answers as $answer)
												<div class="test-test__line" @if($answer->is_right != 1) id="c_{{$question->id}}_{{$k}}" @endif>
													@php $k++ @endphp
													<input type="radio" name="q_{{$question->id}}" id="answer_{{$answer->id}}" value="{{$answer->id}}" class="test-test__input">
													<label for="answer_{{$answer->id}}" class="test-test__label">
														<span class="test-test__span">
															{!!$answer->proceed()!!}
														</span>
													</label>
												</div>
												@endforeach
											</div>
											<div class="output-search__btns test-test__btns">
												@if($i==1)
													<button type="button" class="output-search__btn output-search__btn_dis purple__button">{{__('front.prev')}}</button>
												@else
													<button type="button" onClick="show_question_block({{$i-1}}, {{$i}});" class="output-search__btn output-search__btn purple__button">{{__('front.prev')}}</button>
												@endif

												@if($i+1 > $total_questions)
													<button type="button" onClick="submit_form();" class="output-search__btn output-search__btn purple__button">{{__('front.bitiru')}}</button>
												@else
													<button onClick="show_question_block({{$i+1}}, {{$i}});" type="button" class="output-search__btn output-search__btn purple__button">{{__('front.next')}}</button>
												@endif
											</div>
											@php $i++ @endphp
											</span>
											@endforeach
										</form>
										@endif
									</div>
								</div>

								@include('front.course.__includes.__side_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection

@section('js')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script>

		function giveHint(question_id, total_answers){

			total_answers /= 2;
			var user_ritms = $("#user_ritms").val();

			if(user_ritms >= 10){

				for(var i=0; i<=total_answers; i++){
					$("#c_"+question_id+"_"+i).remove();
				}

				$.ajax(
				  '/give-hint',
				  {
				      success: function(data) {
				        $("#user_ritms").val(data);
				      },
				      error: function(e) {
				        
				      }
				   }
				);
			}else{
				alert('Недостаточно ритмов! Ваши ритмы: '+user_ritms);
			}

		}

		function show_question_block(next_question_block_id, now_question_block_id){
			$("#question_block_"+now_question_block_id).toggle();
			$("#question_block_"+next_question_block_id).toggle();
		}

		function submit_form(){
			var check = true;
        	$("input:radio").each(function(){
            	var name = $(this).attr("name");
            	if($("input:radio[name="+name+"]:checked").length == 0){
                	check = false;
            	}
        	});
        	if(check == false){
        		alert("Вы не ответили на все вопросы, пожалуйста, сделайте это.");
        	}else{
        		$("#test_form").submit();
        	}
		}
	</script>
@endsection