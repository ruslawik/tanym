@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="test">
							<div class="test__body">
								<div class="test__info info-test">

									<div class="section__head">
										<h4 class="section__title">{{$chapter[0]->name}} - {{$element[0]->name}}</h4>

									</div>
									<div class="info-test__body">
										@if(strlen($test[0]->description) > 10)
										<div data-showmore="size" class="info-uploaded__showmore showmore-uploaded">
												<h3 class="info-uploaded__subtitle">{{__('front.description')}}</h3>
												{!!$test[0]->description!!}
											<button hidden data-showmore-button type="button" class="showmore-uploaded__more">
												<span>{{__('front.show_more')}}</span>
												<span>{{__('front.hide')}}</span>
											</button>
										</div>
										@endif
										<a href="/course/{{$course->id}}/chapter/{{$chapter[0]->id}}/element/{{$element[0]->id}}/test/{{$test[0]->id}}">
											<button class="output-search__btn output-search__btn purple__button">{{__('front.start_test')}}!</button></a>
									</div>
								</div>

								@include('front.course.__includes.__side_menu')
								<div class="list__close"></div>
								<span class="list__button">{{__('front.soder')}}</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection