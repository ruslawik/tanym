@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
							<div class="uploaded__body">
								<div class="uploaded__info info-uploaded">

									<div class="section__head">
										<h4 class="section__title">{{$chapter[0]->name}} - {{$element[0]->name}}</h4>
									</div>
									<div class="info-uploaded__body">
										<div>
											<div class="info-uploaded__subtitle">{{__('front.uploaded_files')}}</div>		
												@php $i = 0 @endphp
												@foreach($files as $file)
												<a target="_blank" style="margin-top: 30px;" href="{{$file->uploaded_file->getUrl()}}" class="files-uploaded__line">
													<span class="files-uploaded__icon _icon-file"></span>
													<span class="files-uploaded__name">
														{{$file->file_name}}
													</span>
												</a>
												@php  $i++ @endphp
												@endforeach
												@php 
													if($i == 0)
														print("К сожалению, не загружен ни один файл");
												@endphp
										</div>
									</div>
								</div>

								@include('front.course.__includes.__side_menu')
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection