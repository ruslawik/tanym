@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="videolesson">
							<div class="videolesson__body">
								<div class="videolesson__info info-videolesson">

									<div class="section__head">
										<h4 class="section__title">{{$chapter[0]->name}} - {{$element[0]->name}}</h4>
									</div>
									<div class="info-videolesson__body">
										{!!$video[0]->video_code!!}
										<br><br>
										<div data-showmore="size" class="info-uploaded__showmore showmore-uploaded">
											<div data-showmore-content="200" class="showmore-uploaded__content">
												<h3 class="info-uploaded__subtitle" style="margin-bottom: 20px;">{{__('front.description')}}</h3>
												{!!$video[0]->video_description!!}
											</div>
											<button hidden data-showmore-button type="button" class="showmore-uploaded__more">
												<span>{{__('front.show_more')}}</span>
												<span>{{__('front.hide')}}</span>
											</button>
										</div>
									</div>
								</div>

								@include('front.course.__includes.__side_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection