@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
							<div class="videolesson__body">
								<div class="videolesson__info info-videolesson">
									<div class="section__head">
										<h2 class="section__title">{{$course->name}}</h2>

									</div>
									<div class="info-videolesson__body">

										<div data-showmore="size" class="info-uploaded__showmore showmore-uploaded">
												<h3 class="info-uploaded__subtitle">{{__('front.welcome_to_course')}}</h3>
												{{__('front.start_learning')}}
												<br><br>
												<h3 class="info-uploaded__subtitle">{{__('front.description')}}:</h3>
												{!!$course->description!!}
											<button hidden data-showmore-button type="button" class="showmore-uploaded__more">
												<span>{{__('front.show_more')}}</span>
												<span>{{__('front.hide')}}</span>
											</button>
										</div>
									</div>
								</div>

								@include('front.course.__includes.__side_menu')
								<div class="list__close"></div>
								<span class="list__button">{{__('front.soder')}}</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection