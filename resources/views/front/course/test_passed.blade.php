@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="test">
							<div class="test__body">
								<div class="test__info info-test">

									<div class="section__head">
										<h4 class="section__title">{{$chapter[0]->name}} - {{$element[0]->name}}</h4>
									</div>
									@if(strlen($test[0]->description) > 10)
									<h3 class="info-uploaded__subtitle">{{__('front.description')}}</h3>
												{!!$test[0]->description!!}
									@endif
									<br><br>
									<div class="final-test__body">
												<div class="final-test__line">
													<span class="final-test__subtitle final-test__subtitle_small">{{__('front.test_passed')}}!</span>
												</div>
												@if(strlen($test[0]->cert_id) >= 1 && ($right_count/$total_questions)*100 >= $test[0]->bally_to_cert)
													<a href="/get-cert/{{$test[0]->id}}"><button type="button" class="form-search__button purple__button">{{__('front.get_cert')}}</button></a>
												@endif

												@if(($right_count/$total_questions)*100 < $test[0]->bally_to_cert)
												<b>
													{{__('front.get_cert_error')}} - {{$test[0]->bally_to_cert}}.
													<br>
													{{__('front.your_score')}}:  {{round($right_count/$total_questions*100)}}
												</b>
												@endif

												<br><br>
												<div class="final-test__line final-test__line_bd"></div>
												<div class="final-test__line">
													<img src="/front/img/icons/test_final.svg" alt="test_final">
													<div class="final-test__text">
														Результат: Вы ответили правильно на <span class="final-test__from final-test__text_bold">{{$right_count}}</span> вопрос(-ов) из <span class="final-test__to final-test__text_bold">{{$passed_questions}}</span>!
													</div>
												</div>
										@foreach($given_answers as $given_answer)
											<b>Вопрос:</b> {!!$given_answer->question->question!!}
											<br>
											<font color="green"><b>Правильный ответ: </b></font><br>{!!$given_answer->right_answer->proceed()!!}
											<br><br>
											<font color="@if($given_answer->right_answer_id == $given_answer->answer_id) green @else red @endif"><b>Ваш ответ </b></font><br>{!!$given_answer->answer->proceed()!!}
											<br><br>
											<div class="final-test__line final-test__line_bd"></div>
										@endforeach
									</div>
								</div>

								@include('front.course.__includes.__side_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection

@section('js')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script>

	</script>
@endsection