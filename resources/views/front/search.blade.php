@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="search">

							<div class="section__head">
								<h2 class="section__title">{{__('front.search')}}</h2>

							</div>
							<form action="/search" method="GET" class="search__form form-search">
								<div class="form-search__line">
									<div class="form-search__title">{{__('front.enter_a_name')}}:</div>
									<div class="form-search__name">
										<input type="text" name="q" data-error="Обязательное поле" placeholder="{{__('front.name_or_part')}}" class="form-search__input" value="{{$q}}">
										<button type="submit" class="form-search__button purple__button">{{__('front.search')}}</button>
									</div>
								</div>
								<div class="form-search__line form-search__line_bd"></div>
								<div class="form-search__line">
									<div class="form-search__sels">
										<div class="form-search__sel">
											<div class="form-search__title">{{__('front.predmet')}}</div>
											<select data-scroll="200" name="category" class="form-search__select">
												<option value="all">{{__('front.all')}}</option>
												@foreach($categories as $category)
													<option @if($category->id == $selected_category_id)  selected @endif value="{{$category->id}}">{{$category->name}}</option>
												@endforeach
											</select>
										</div>
										<div class="form-search__sel">
											<div class="form-search__title">{{__('front.level')}}</div>
											<select data-scroll="200" name="class" class="form-search__select">
												<option value="all">{{__('front.all')}}</option>
												@foreach($classes as $class)
													<option @if($class->id == $selected_class_id)  selected @endif value="{{$class->id}}">{{$class->class}}</option>
												@endforeach
											</select>
										</div>
										<div class="form-search__sel">
											<!--
											<select data-scroll="200" name="form[]" class="info-addcourse__select">
												<option value="С видео">С видео</option>
												<option value="Без видео">Без видео</option>
											</select>!-->
										</div>
										<div class="form-search__sel">
											<!--
											<select data-scroll="200" name="form[]" class="info-addcourse__select">
												<option value="С тестами">С тестами</option>
												<option value="Без тестов">Без тестов</option>
											</select>
											!-->
										</div>
									</div>
								</div>
							</form>
							<div class="search__output output-search">
								<div class="output-search__head">
									<div class="output-search__result form-search__title" style="margin-left:20px;">{{__('front.search_results')}}:</div>
									<!--
									<div class="output-search__from">Показано 10 из 25 записей</div>
									!-->
								</div>
								<section class="reccomend" style="margin-top:-25px !important;">
									<div class="reccomend__head">
										<h2 class="reccomend__title"></h2>
									</div>
									<div class="reccomend__body">
										<div class="reccomend__items">
											@foreach($courses as $course)
											<div class="reccomend__item item-reccomend undefined">
												<a href="/public/course/{{$course->id}}">
												<div class="item-reccomend__image">
													<img height=200px src="{{$course->image->url}}" alt="item-reccomend__image" class="item-reccomend__img">
												</div>
												</a>
												<br>
												<a class="item-reccomend__title" href="/public/course/{{$course->id}}"><h3>{{$course->name}}</h3></a>
												<br>
												<a href="/instructor/{{$course->author->id}}" class="item-reccomend__user user-item-reccomend">
													<div class="user-item-reccomend__avatar">
														<img src="{{$course->author->avatar->thumbnail}}" alt="Avatar">
													</div>
													<div class="user-item-reccomend__name">{{$course->author->name}}</div>
												</a>
												<div class="item-reccomend__actions">
													<div class="item-reccomend__rating">
														<div class="rating rating_set">
															<div class="rating__body">
																<div class="rating__active"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach
										</div>
									</div>
								</section>
								<!--
								<div class="output-search__btns">
									<div class="output-search__btn output-search__btn_dis purple__button">Пред.</div>
									<div class="output-search__btn output-search__btn purple__button">След.</div>
								</div>!-->
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection