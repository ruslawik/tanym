@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="login">
							<div class="login__tabs">
								<div data-tabs class="tabs-login">
									<div class="tabs-login__tabs">
										<nav data-tabs-titles class="tabs-login__navigation">
											<button type="button" class="tabs-login__subtitle _tab-active">Аккаунт</button>
											<button type="button" class="tabs-login__subtitle">Геймификация</button>
											<form method="POST" action="/logout">
												@csrf
												<button type="submit" href="/logout" class="tabs-login__subtitle">{{__('front.exit')}}</button>
											</form>
										</nav>
										<div data-tabs-body class="tabs-login__content">
											<div class="tabs-login__body">
												<div class="tabs-login__head">
													<h2 class="tabs-login__title">{{__('front.my_profile')}}</h2>
												</div>
												<form action="/update-cabinet" method="POST" class="settings-form" enctype="multipart/form-data">
													@csrf
													<div class="settings-form__line">
														<div class="settings-form__title">Аккаунт</div>
														<div class="settings-form__text">{{__('front.change_profile')}}</div>
													</div>
													<div class="settings-form__line settings-form__line_bd"></div>
													@if(Auth::user()->avatar)
														<picture><img width=100 src="{{Auth::user()->avatar->preview}}" alt="avatar"></picture>
													@else
														<picture><img width=100 src="/front/img/user_icon.png" alt="avatar"></picture>
													@endif
													<input type="file" name="avatar" class="settings-form">
													<br><br>
													<div class="settings-form__line settings-form__line_bd"></div>
													<div class="settings-form__line">
														<div class="settings-form__subtitle">{{__('front.name')}}</div>
														<input type="text" class="settings-form__input" name="name" value="{{Auth::user()->name}}">
														@if(Auth::user()->roles->contains(3))
														<br><br>
														<div class="settings-form__subtitle">Регалии</div>
														<input placeholder="Бизнес-тренер, игротехник" type="text" class="settings-form__input" name="regalia" value="{{Auth::user()->regalia}}">
														<br><br>
														<div class="settings-form__subtitle">Регалии</div>
														<textarea placeholder="Я занимаюсь неформальным образованием с 2002 года, то есть уже 17 лет. Работу свою люблю и, кажется, это у нас взаимно. Я горжусь тем, что никогда не искала работу, всегда она искала меня и всегда находила. Моя специализация - это игровые решения для бизнеса, то есть бизнес-игры и геймификация бизнес-процессов." type="text" class="settings-form__input" name="instructor_description">{{Auth::user()->instructor_description}}</textarea>
														@endif
													</div>
													<div class="settings-form__line settings-form__line_bd"></div>
													<div class="settings-form__line password-settings">
														<div class="password-settings__line">
															<div class="settings-form__subtitle">{{__('front.password')}}</div>
														</div>
														<div class="password-settings__line">
															<input type="password" id="new" placeholder="{{__('front.new_password')}}" class="settings-form__input" name="password">
															<button type="button" class="settings-form__viewpass" style="background-image: url(/front/img/icons/eye.svg);"></button>
														</div>
													</div>
													<div class="settings-form__line settings-form__line_bd"></div>
													<div class="settings-form__line">
														<button class="settings-form__button purple__button">{{__('front.save_profile')}}</button>
													</div>
												</form>
											</div>
											<div class="tabs-login__body">
												<div class="tabs-login__head">
													<h2 class="tabs-login__title">{{__('front.game_settings')}}</h2>
												</div>
												<div class="settings-game">
													<div class="settings-game__line">
														<div class="settings-game__subtitle">
															@if(Auth::user()->game_on == 1)
																{{__('front.game_on')}}
															@else
																{{__('front.game_off')}}
															@endif
														</div>
														<div class="settings-game__text">{{__('front.set_up_base')}}</div>
													</div>
													<div class="settings-game__line settings-game__line_bd"></div>
													<div class="settings-game__line">
														@if(Auth::user()->game_on == 1)
															<a href="/game/0" class="settings-game__button purple__button">{{__('front.off_game')}}</a>
														@else
															<a href="/game/1" class="settings-game__button purple__button">{{__('front.on_game')}}</a>
														@endif
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection