@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="addcourse">

							<div class="section__head">
								<h2 class="section__title">Добавление курса</h2>

							</div>
							<form action="/add-course-store" method="POST" enctype="multipart/form-data" class="addcourse__body">
								@csrf
								<div class="addcourse__info info-addcourse">

									<div class="info-addcourse__line info-addcourse__line_head">
										<div class="info-addcourse__title">Название <a href="#" class="info-addcourse__link "></a></div>
										<div class="info-addcourse__text">Вы сможете поменять всю информацию позже</div>
									</div>
									<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__subtitle">Наименование</div>
										<input type="text" name="name" data-required data-error="Обязательное поле" placeholder="Наименование" class="info-addcourse__input _input">
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__subtitle">Обложка курса</div>
										<input type="file" name="image" data-required data-error="Обязательное поле" placeholder="Обложка курса" class="info-addcourse__input _input">
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__sels">
											<div class="info-addcourse__sel">
												<div class="info-addcourse__subtitle">Категория</div>
												<select data-scroll="200" name="category" class="info-addcourse__select">
													@foreach($categories as $category)
														<option value="{{$category->id}}">{{$category->name}}</option>
													@endforeach
												</select>
											</div>
											<div class="info-addcourse__sel">
												<div class="info-addcourse__subtitle">Класс</div>
												<select data-scroll="200" name="class" class="info-addcourse__select">
													@foreach($classes as $class)
														<option value="{{$class->id}}">{{$class->class}}</option>
													@endforeach
												</select>
											</div>
											<div class="info-addcourse__sel">
												<div class="info-addcourse__subtitle">Сложность</div>
												<select data-scroll="200" name="diffiulty" class="info-addcourse__select">
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
											<div class="info-addcourse__sel">
												<br>
												Черновик (не показывать всем)
												<input type="checkbox" name="draft" value="1" checked="checked" class="info-addcourse__select">
											</div>
										</div>
									</div>
								</div>
								<div class="addcourse__create">
									<button type="submit" class="addcourse__link purple__button">Создать</button>
								</div>
							</form>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection