@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>

									</div>

									<div class="info-addchapter__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Создание раздела <a href="#" class="info-addcourse__link "></a></div>
											<div class="info-addcourse__text">Введите название раздела. В нем будут содержаться элементы курса.</div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<div class="info-addchapter__subtitle">Наименование</div>
										<form method="POST" action="/add-chapter-store" class="info-addchapter__line">
											@csrf
											<input type="hidden" name="course_id" value="{{$course[0]->id}}">
											<input type="text" name="name" data-required placeholder="Наименование" class="info-addchapter__input">
											<button type="submit" class="info-addchapter__button purple__button">Создать</button>
										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')

								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection