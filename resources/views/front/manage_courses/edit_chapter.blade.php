@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>

									</div>

									<div class="info-addchapter__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Редактирование раздела <a href="/course/{{$course[0]->id}}/delete-chapter/{{$chapter[0]->id}}" class="info-addcourse__link _icon-del_elem _red"></a></div>
											<div class="info-addcourse__text">Редактируйте раздел. В нем будут содержаться элементы курса. </div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<div class="info-addchapter__subtitle">Наименование</div>
										<form method="POST" action="/edit-chapter-update" class="info-addchapter__line">
											@csrf
											<input type="hidden" name="course_id" value="{{$course[0]->id}}">
											<input type="hidden" name="chapter_id" value="{{$chapter[0]->id}}">
											<input type="text" name="name" data-required placeholder="Наименование" class="info-addchapter__input" value="{{$chapter[0]->name}}">
											<button type="submit" class="info-addchapter__button purple__button">Сохранить</button>
										</form>
										<br>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<div class="info-addchapter__subtitle">Порядок элементов раздела</div>
										Можно не указывать. В таком случае элементы будут отсортированы по дате создания.
										<br><br>
										<form method="POST" action="/course/{{$course[0]->id}}/chapter-elements-order-update/{{$chapter[0]->id}}">
											@csrf
											@foreach($chapter_items as $item)
												<div class="info-addchapter__subtitle">{{$item->name}}</div>
												<input type="text" name="item_{{$item->id}}" data-required placeholder="0" class="info-addchapter__input" value="{{$item->item_order}}">
											@endforeach
											<br><br>
											<button type="submit" class="info-addchapter__button purple__button">Сохранить</button>
										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')

								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection