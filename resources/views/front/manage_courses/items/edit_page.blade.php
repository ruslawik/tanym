@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>
									</div>

									<div class="info-addvideo__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Редактирование элемента «{{$chapter_item->name}}» <a href="/course/{{$course[0]->id}}/delete-element/{{$chapter_item->id}}" class="info-addcourse__link _icon-del_elem _red"></a></div>
											<div class="info-addcourse__text">Введите указанные данные для элемента. Вы всегда можете их отредактировать.</div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<form action="/course/{{$course[0]->id}}/edit-page-update/{{$chapter_item->id}}" method="POST" class="info-addfile__form form-addfile">
											@csrf
											<input type="hidden" name="page_id" value="{{$page[0]->id}}">
											<div class="form-addfile__line">
												<div class="form-addfile__title">Наименование</div>
												<div class="form-addfile__name">
													<input type="text" name="name" value="{{$chapter_item->name}}" placeholder="Введите название" data-required class="form-addtextimage__input">
												</div>
											</div>

											<div class="form-addfile__line">
												<div class="form-addfile__title">Визуальный редактор страницы</div>
												<textarea class="form-control" id="content" name="content">{{$page[0]->content}}</textarea>
											</div>
											<button type="submit" class="form-addtextimage__button purple__button">
														Сохранить
													</button>
										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>

						</section>
					</div>
				</div>
			</div>
		</main>
@endsection

@section('js')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="https://cdn.ckeditor.com/4.17.2/standard/ckeditor.js"></script>
	<script>
	$( document ).ready(function() {
    	CKEDITOR.replace( 'content', {
        	filebrowserUploadUrl: "{{route('upload-ckeditor-media', ['_token' => csrf_token() ])}}",
        	filebrowserUploadMethod: 'form'
    	});
	});
	</script>
@endsection