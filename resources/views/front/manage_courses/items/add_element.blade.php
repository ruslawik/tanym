@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>
									</div>

									<div class="info-addelem__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Создание элемента в раздел «{{$chapter[0]->name}}» <a href="#" class="info-addcourse__link "></a></div>
											<div class="info-addcourse__text">Выберите элемент для создания. Вы всегда сможете удалить его.</div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<form action="/add-element-store/{{$course[0]->id}}/{{$chapter[0]->id}}" method="POST" class="info-addelem__form form-addelem">
											@csrf
											<div class="form-addelem__line">
												<div class="form-addelem__title">Элементы раздела</div>
												<div class="form-addelem__radios radios-addelem">
													@foreach($item_types as $item_type)
													<div class="radios-addelem__line">
														<input checked name="radios-addelem__radio" type="radio" id="element_{{$item_type->id}}" class="radios-addelem__radio" value="{{$item_type->id}}">
														<label for="element_{{$item_type->id}}" class="radios-addelem__label">
															<span class="radios-addelem__text">{{$item_type->name}}</span>
														</label>
													</div>
													@endforeach
												</div>
											</div>
											<div class="form-addelem__line form-addelem__line_bd"></div>
											<div class="form-addelem__line">
												<div class="form-addelem__title">Наименование</div>
												<div class="form-addelem__name">
													<input type="text" placeholder="Наименование" data-required class="form-addelem__input" name="name">
													<button class="form-addelem__button purple__button">Создать</button>
												</div>
											</div>

										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection