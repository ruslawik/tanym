@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>
									</div>

									<div class="info-addvideo__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Редактирование видеоурока «{{$chapter_item->name}}» <a href="/course/{{$course[0]->id}}/delete-element/{{$chapter_item->id}}" class="info-addcourse__link _icon-del_elem _red"></a></div>
											<div class="info-addcourse__text">Введите указанные данные для элемента. Вы всегда можете их отредактировать.</div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<form action="/course/{{$course[0]->id}}/edit-video-update/{{$chapter_item->id}}" method="POST" class="info-addvideo__form form-addvideo">
											@csrf
											<input type="hidden" name="video_id" value="{{$video[0]->id}}">
											<div class="form-addvideo__line">
												<div class="form-addvideo__title">Введите код видео (Youtube, Vimeo и др.)</div>
												<textarea name="video_code" id="" placeholder="Код видео" data-required class="form-addvideo__textarea _input">{{$video[0]->video_code}}</textarea>
											</div>
											<div class="form-addvideo__line">
												<div class="form-addvideo__title">Введите описание к видео</div>
												<textarea name="video_description" id="" placeholder="Описание видео" data-required class="form-addvideo__textarea form-addvideo__textarea_small _input">{{$video[0]->video_description}}</textarea>
											</div>
											<div class="form-addvideo__line form-addvideo__line_bd">
											</div>
											<div class="form-addvideo__line">
												<div class="form-addvideo__title">Название</div>
												<div class="form-addvideo__name">
													<input type="text" placeholder="Наименование" data-required class="form-addvideo__input" name="name" value="{{$chapter_item->name}}">
													<button class="form-addvideo__button purple__button">Сохранить</button>
												</div>
											</div>
										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>

						</section>
					</div>
				</div>
			</div>
		</main>
@endsection