@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>
									</div>

									<div class="info-addtest__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Редактирование элемента «{{$chapter_item->name}}» <a href="/course/{{$course[0]->id}}/delete-element/{{$chapter_item->id}}" class="info-addcourse__link _icon-del_elem _red"></a></div>
											<div class="info-addcourse__text">Введите указанные данные для теста. Вы всегда можете их отредактировать.</div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<form action="/course/{{$course[0]->id}}/change-elem-name-update/{{$chapter_item->id}}" method="POST" class="info-addfile__form form-addfile">
												@csrf
												<div class="form-addfile__line">
												<div class="form-addfile__title">Изменить название</div>
												<div class="form-addfile__name">
													<input type="text" placeholder="Введите название" data-required class="form-addfile__input" name="chapter_item_name" value="{{$chapter_item->name}}">
												</div>
												<br>
												<div class="form-addfile__title">Краткое описание теста</div>
												<textarea name="test_description" class="form-addfile__input">{{$test[0]->description}}</textarea>
												<br><br>
												<div class="form-addfile__title">ID сертификата в системе certificates.caravanofknowledge.com (оставьте пустым, если сертификат не требуется)</div>
												<div class="form-addfile__name">
													<input type="text" placeholder="0000" class="form-addfile__input" name="cert_id" value="{{$test[0]->cert_id}}">
												</div>
												<br><br>
												<div class="form-addfile__title">Минимальные баллы для получения сертификата (от 0 до 100)</div>
												<div class="form-addfile__name">
													<input type="text" placeholder="60" class="form-addfile__input" name="bally_to_cert" value="{{$test[0]->bally_to_cert}}">
												</div>
												<br>
												<br>
												<button type="submit" class="form-addfile__button purple__button">
														<input type="submit" class="form-addfile__file">
														Сохранить
													</button>
												</div>
											</form>
											<br><br>
										@php $i = 1 @endphp
										@foreach($questions as $question)
										<a id="q{{$question->id}}"></a>
										<form action="/course/{{$course[0]->id}}/edit-question-update/{{$question->id}}" method="POST" class="info-addtest__form form-addtest">
											@csrf
											<div class="form-addtest__line__bd"></div>
											<div class="info-addtest__questions">
												<div class="info-addtest__question question-addtest">
													<div class="question-addtest__title"><span style="cursor: pointer;" onClick="toogle_quest({{$question->id}});"> - Вопрос #{{$i}}</span><a href="/course/{{$course[0]->id}}/delete-question/{{$question->id}}" class="question-addtest__del _icon-del_elem _red"></a></div>
													<span id="quest_{{$question->id}}" @if(isset($_GET['qid']) && $_GET['qid'] == $question->id) @else style="display: none;" @endif>
													<textarea name="question_{{$question->id}}" id="question_ckeditor_{{$question->id}}" placeholder="Впишите сюда текст вопроса" data-required class="question-addtest__name _input">{{$question->question}}</textarea>
													<br>
													<div class="question-addtest__answers">
														@php $j = 1 @endphp
														@foreach($question->answers as $answer)
														<div class="question-addtest__answer answer-addtest">
															<div class="answer-addtest__title">Ответ #{{$j}}</div>
															<div class="answer-addtest__line">
																<input @if($answer->is_right == 1) checked @endif type="radio" name="question_right_answer_{{$question->id}}" class="answer-addtest__radio" id="question_answer_{{$question->id}}_{{$answer->id}}" value="{{$answer->id}}">
																<label for="question_answer_{{$question->id}}_{{$answer->id}}" class="answer-addtest__label">
																	<input type="hidden" name="q_{{$question->id}}_answer_{{$j}}" value="{{$answer->id}}">
																	<input type="text" name="answer_{{$answer->id}}" placeholder="Ответ" class="answer-addtest__input" value="{{$answer->answer}}">
																</label>
																<a href="/course/{{$course[0]->id}}/question/{{$question->id}}/delete-answer/{{$answer->id}}" class="answer-addtest__del _icon-del_elem _red"></a>
															</div>
														</div>
														@php $j++ @endphp
														@endforeach
														<input type="hidden" name="total_answers_{{$question->id}}" value="{{$j}}">
													</div>
													<a href="/course/{{$course[0]->id}}/question/{{$question->id}}/add-answer-store">
													<div class="info-addtest__btns">
												<div class="info-addtest__link ">
														<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
															<path d="M22 11C22 13.9174 20.8411 16.7153 18.7782 18.7782C16.7153 20.8411 13.9174 22 11 22C8.08262 22 5.28473 20.8411 3.22183 18.7782C1.15893 16.7153 0 13.9174 0 11C0 8.08262 1.15893 5.28473 3.22183 3.22183C5.28473 1.15893 8.08262 0 11 0C13.9174 0 16.7153 1.15893 18.7782 3.22183C20.8411 5.28473 22 8.08262 22 11ZM11.6875 6.1875C11.6875 6.00516 11.6151 5.8303 11.4861 5.70136C11.3572 5.57243 11.1823 5.5 11 5.5C10.8177 5.5 10.6428 5.57243 10.5139 5.70136C10.3849 5.8303 10.3125 6.00516 10.3125 6.1875V10.3125H6.1875C6.00516 10.3125 5.8303 10.3849 5.70136 10.5139C5.57243 10.6428 5.5 10.8177 5.5 11C5.5 11.1823 5.57243 11.3572 5.70136 11.4861C5.8303 11.6151 6.00516 11.6875 6.1875 11.6875H10.3125V15.8125C10.3125 15.9948 10.3849 16.1697 10.5139 16.2986C10.6428 16.4276 10.8177 16.5 11 16.5C11.1823 16.5 11.3572 16.4276 11.4861 16.2986C11.6151 16.1697 11.6875 15.9948 11.6875 15.8125V11.6875H15.8125C15.9948 11.6875 16.1697 11.6151 16.2986 11.4861C16.4276 11.3572 16.5 11.1823 16.5 11C16.5 10.8177 16.4276 10.6428 16.2986 10.5139C16.1697 10.3849 15.9948 10.3125 15.8125 10.3125H11.6875V6.1875Z" fill="#82C43C" />
														</svg>
														Добавить ответ
													</div>
												</div>
												</a>
												<div class="info-addtest__btns">
												<div class="info-addtest__link ">
													<button type="submit" class="form-addfile__button purple__button">
														<input type="submit" class="form-addfile__file">
														Сохранить вопрос #{{$i}}
												</button>
											</div>
										</div>
												</div>
												</span>
											</div>
										</form>
										@php $i++ @endphp
										@endforeach
										<a href="/course/{{$course[0]->id}}/test/{{$test[0]->id}}/add-question-store">
										<div class="info-addtest__btns">
												<div class="info-addtest__link ">
													<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M22 11C22 13.9174 20.8411 16.7153 18.7782 18.7782C16.7153 20.8411 13.9174 22 11 22C8.08262 22 5.28473 20.8411 3.22183 18.7782C1.15893 16.7153 0 13.9174 0 11C0 8.08262 1.15893 5.28473 3.22183 3.22183C5.28473 1.15893 8.08262 0 11 0C13.9174 0 16.7153 1.15893 18.7782 3.22183C20.8411 5.28473 22 8.08262 22 11ZM11.6875 6.1875C11.6875 6.00516 11.6151 5.8303 11.4861 5.70136C11.3572 5.57243 11.1823 5.5 11 5.5C10.8177 5.5 10.6428 5.57243 10.5139 5.70136C10.3849 5.8303 10.3125 6.00516 10.3125 6.1875V10.3125H6.1875C6.00516 10.3125 5.8303 10.3849 5.70136 10.5139C5.57243 10.6428 5.5 10.8177 5.5 11C5.5 11.1823 5.57243 11.3572 5.70136 11.4861C5.8303 11.6151 6.00516 11.6875 6.1875 11.6875H10.3125V15.8125C10.3125 15.9948 10.3849 16.1697 10.5139 16.2986C10.6428 16.4276 10.8177 16.5 11 16.5C11.1823 16.5 11.3572 16.4276 11.4861 16.2986C11.6151 16.1697 11.6875 15.9948 11.6875 15.8125V11.6875H15.8125C15.9948 11.6875 16.1697 11.6151 16.2986 11.4861C16.4276 11.3572 16.5 11.1823 16.5 11C16.5 10.8177 16.4276 10.6428 16.2986 10.5139C16.1697 10.3849 15.9948 10.3125 15.8125 10.3125H11.6875V6.1875Z" fill="#82C43C" />
													</svg>
													Добавить вопрос
												</div>
											</div>
											</a>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')

								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection

@section('js')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script>
		function toogle_quest(quest_id){
			$("#quest_"+quest_id).toggle();
		}
	</script>
	<script src="https://cdn.ckeditor.com/4.17.2/standard/ckeditor.js"></script>
	<script>
	$( document ).ready(function() {
		@foreach($questions as $question)
	    	CKEDITOR.replace( 'question_ckeditor_'+{{$question->id}}, {
	        	filebrowserUploadUrl: "{{route('upload-ckeditor-media', ['_token' => csrf_token() ])}}",
	        	filebrowserUploadMethod: 'form'
	    	});
	    @endforeach
	});
	</script>
@endsection
