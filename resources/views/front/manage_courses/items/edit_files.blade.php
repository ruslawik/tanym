@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>
									</div>

									<div class="info-addfile__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Редактирование элемента «{{$chapter_item->name}}» <a href="/course/{{$course[0]->id}}/delete-element/{{$chapter_item->id}}" class="info-addcourse__link _icon-del_elem _red"></a></div>
											<div class="info-addcourse__text">Вы можете отредактировать название страницы</div>
											<br>
											<form action="/course/{{$course[0]->id}}/change-elem-name-update/{{$chapter_item->id}}" method="POST" class="info-addfile__form form-addfile">
												@csrf
												<div class="form-addfile__line">
												<div class="form-addfile__title">Изменить название</div>
												<div class="form-addfile__name">
													<input type="text" placeholder="Введите название" data-required class="form-addfile__input" name="chapter_item_name" value="{{$chapter_item->name}}">
													<button type="submit" class="form-addfile__button purple__button">
														<input type="submit" class="form-addfile__file">
														Сохранить
													</button>
												</div>
												</div>
											</form>

										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<div class="info-addcourse__text">Загрузите файлы. Вы всегда сможете их удалить.</div><br>
										<form action="/course/{{$course[0]->id}}/add-file-store/{{$chapter_item->id}}" method="POST" class="info-addfile__form form-addfile" enctype="multipart/form-data">
											@csrf
											<div class="form-addfile__line">
												<div class="form-addfile__title">Загрузите файл</div>
												<div class="form-addfile__name">
													<input type="file" data-required class="form-addfile__input" name="uploaded_file">
													<input type="text" placeholder="Введите название" data-required class="form-addfile__input" name="file_client_name">
													<button type="submit" class="form-addfile__button purple__button">
														<input type="submit" class="form-addfile__file">
														Добавить
													</button>
												</div>
											</div>
											<div class="form-addfile__line form-addfile__line_bd"></div>
											<div class="form-addfile__line formPreview" id="formPreview">
												@foreach($files as $file)
												<table>
													<tr>
														<td>
															<a href="{{$file->uploaded_file->getUrl()}}" class="formPreview__line">
																<span class="formPreview__icon _icon-file"></span>
																<div class="formPreview__text">
																	{{$file->file_name}}
																</div>
															</a>
														</td>
														<td>
															<a href="/course/{{$course[0]->id}}/delete-file/{{$file->id}}" class="info-addcourse__link _icon-del_elem _red"></a>
														</td>
													</tr>
												</table>
												@endforeach
											</div>
										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>

						</section>
					</div>
				</div>
			</div>
		</main>
@endsection