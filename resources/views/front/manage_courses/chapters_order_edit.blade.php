@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>

									</div>

									<div class="info-addchapter__body">

										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Редактирование содержания</div>
											<div class="info-addcourse__text">Редактируйте порядок разделов. </div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">&nbsp;
										</div>
										<div class="info-addchapter__subtitle">Порядок разделов</div>
										Можно не указывать. В таком случае разделы будут отсортированы по дате создания.
										<br><br>
										<form method="POST" action="/course/{{$course[0]->id}}/chapter-order-edit-update">
											@csrf
											@foreach($chapters as $chapter)
												<div class="info-addchapter__subtitle">{{$chapter->name}}</div>
												<input type="text" name="chapter_{{$chapter->id}}" data-required placeholder="0" class="info-addchapter__input" value="{{$chapter->chapter_order}}">
											@endforeach
											<br><br>
											<button type="submit" class="info-addchapter__button purple__button">Сохранить</button>
										</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')

								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection