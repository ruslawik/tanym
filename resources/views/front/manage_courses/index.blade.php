@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="authorcourses">
							<div class="authorcourses__head">
								<h2 class="authorcourses__title">Мои курсы</h2>
								<div class="authorcourses__add">
									<a href="/add-course" class="authorcourses__link purple__button">Добавить</a>
								</div>
							</div>
							<div class="authorcourses__body">
								<section class="reccomend">
									<div class="reccomend__head">
										<h2 class="reccomend__title"> </h2>
									</div>
									<div class="reccomend__body">
										<div class="reccomend__items">
											@foreach($my_courses as $course)
											<div class="reccomend__item item-reccomend undefined">
												<div class="item-reccomend__image">
													<picture><img height=200px src="{{$course->image->url}}" alt="item-reccomend__image" class="item-reccomend__img"></picture>
													@if($course->draft == 1)
														<div class="item-reccomend__popular undefined"> Черновик</div>
													@endif
												</div>
												<h3 class="item-reccomend__title">{{$course->name}}</h3>
												<a href="" class="item-reccomend__user user-item-reccomend">
													<div class="user-item-reccomend__avatar">
														<picture><img src="{{$course->author->avatar->thumbnail}}" alt="Avatar"></picture>
													</div>
													<div class="user-item-reccomend__name">{{$course->author->name}}</div>
												</a>
												<div class="item-reccomend__actions">
													<div class="item-reccomend__rating">
														<div class="rating rating_set">
															<div class="rating__body">
																<div class="rating__active"></div>
																<div class="rating__items">
																	@for($i=0; $i<$course->diffiulty; $i++)
																		<img src="/front/img/icons/rating_star.svg" alt="rating_star">
																	@endfor
																</div>
															</div>
															<div class="rating__diff">Сложность&nbsp;<span class="rating__value">{{$course->diffiulty}}</span>&nbsp;из 5</div>
														</div>
													</div>
													<a href="/edit-course/{{$course->id}}" class="item-reccomend__link purple__button">Ред.</a>
												</div>
											</div>
											@endforeach
										</div>
									</div>
								</section>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection