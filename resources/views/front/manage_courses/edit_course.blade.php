@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
						<section class="addchapter">
							<div class="addchapter__body">
								<div class="addchapter__info info-addchapter">

									<div class="section__head">
										<h2 class="section__title">{{$course[0]->name}}</h2>

									</div>
									<div class="info-addchapter__body">
										<div class="info-addcourse__line info-addcourse__line_head">
											<div class="info-addcourse__title">Действия с курсом <a href="#" class="info-addcourse__link "></a></div>
											<div class="info-addcourse__text">Вы можете добавлять новые разделы и элементы курса, а также изменить информацию о курсе ниже</div>
										</div>
										<div class="info-addcourse__line info-addcourse__line_bd ">
										</div>
								<form action="/edit-course-update" method="POST" enctype="multipart/form-data">
									@csrf
									<input type="hidden" value="{{$course[0]->id}}" name="course_id">
									<div class="info-addcourse__line">
										<div class="info-addcourse__subtitle">Наименование</div>
										<input type="text" name="name" data-required data-error="Обязательное поле" placeholder="Наименование" class="info-addcourse__input _input" value="{{$course[0]->name}}">
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__subtitle">Описание</div>
										<textarea name="description" placeholder="Описание" class="info-addcourse__input _input">{{$course[0]->description}}</textarea>
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__subtitle">Обложка курса</div>
										<img src="{{$course[0]->image->preview}}" width=100>
										<br><br>
										<input type="file" name="image" data-error="Обязательное поле" placeholder="Обложка курса" class="info-addcourse__input _input">
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__sels">
											<div class="info-addcourse__sel">
												<div class="info-addcourse__subtitle">Категория</div>
												<select data-scroll="200" name="category" class="info-addcourse__select">
													@foreach($categories as $category)
														<option value="{{$category->id}}" @if($course[0]->categories->contains($category->id)) selected @endif>{{$category->name}}</option>
													@endforeach
												</select>
											</div>
											<div class="info-addcourse__sel">
												<div class="info-addcourse__subtitle">Класс</div>
												<select data-scroll="200" name="class" class="info-addcourse__select">
													@foreach($classes as $class)
														<option value="{{$class->id}}" @if($course[0]->class_id == $class->id)) selected @endif>{{$class->class}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="info-addcourse__line">
										<div class="info-addcourse__sels">
											<div class="info-addcourse__sel">
												<div class="info-addcourse__subtitle">Сложность</div>
												<select data-scroll="200" name="diffiulty" class="info-addcourse__select">
													@for($i=1; $i<6; $i++)
														<option @if($course[0]->diffiulty == $i) selected @endif value="{{$i}}">{{$i}}</option>
													@endfor
												</select>
											</div>
											<div class="info-addcourse__sel">
												<br>
												Черновик (не показывать всем)
												<input type="checkbox" name="draft" value="1" @if ($course[0]->draft == 1) checked="checked" @endif class="info-addcourse__select">
											</div>
										</div>
									</div>
								<div class="addcourse__create">
									<button type="submit" class="addcourse__link purple__button">Сохранить</button>
								</div>
							</form>
									</div>
								</div>

								@include('front.manage_courses.__includes.__edit_course_chapter_menu')
								
								<div class="list__close"></div>
								<span class="list__button">Содержание</span>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection