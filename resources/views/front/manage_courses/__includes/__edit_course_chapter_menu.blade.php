
								<div class="addchapter__list">
									<div class="list-addchapter">
										<div class="list-addchapter__title">&nbsp; Содержание<a href="/course/{{$course[0]->id}}/chapter-order-edit"><div class="body-spoller__pencil _icon-pencil" style="float:left;"></div></a></div>
										<div class="list-addchapter__list">
											<div data-spollers class="spollers">
												@foreach($course[0]->chapters as $chapter)
												<div class="spollers__item">
													<button type="button" data-spoller class="spollers__title">{{$chapter->name}}</button>
													<div class="spollers__body body-spoller">
														<center>
														<a href="/edit-chapter/{{$course[0]->id}}/{{$chapter->id}}" class="purple__button">
															Редактировать раздел
														</a>
													</center><br>
													@foreach($chapter->items as $item)
														<a href="/course/{{$course[0]->id}}/edit-element/{{$item->id}}" class="body-spoller__content">
															<span style="margin-right: 8px;"><img src="{{$item->element->icon->url}}" width=20></span>
															<span class="body-spoller__text">{{$item->name}}</span>
															<div class="body-spoller__pencil _icon-pencil"></div>
														</a>
													@endforeach
														<a href="/add-element/{{$course[0]->id}}/{{$chapter->id}}" class="body-spoller__add">
															<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
																<path d="M22 11C22 13.9174 20.8411 16.7153 18.7782 18.7782C16.7153 20.8411 13.9174 22 11 22C8.08262 22 5.28473 20.8411 3.22183 18.7782C1.15893 16.7153 0 13.9174 0 11C0 8.08262 1.15893 5.28473 3.22183 3.22183C5.28473 1.15893 8.08262 0 11 0C13.9174 0 16.7153 1.15893 18.7782 3.22183C20.8411 5.28473 22 8.08262 22 11ZM11.6875 6.1875C11.6875 6.00516 11.6151 5.8303 11.4861 5.70136C11.3572 5.57243 11.1823 5.5 11 5.5C10.8177 5.5 10.6428 5.57243 10.5139 5.70136C10.3849 5.8303 10.3125 6.00516 10.3125 6.1875V10.3125H6.1875C6.00516 10.3125 5.8303 10.3849 5.70136 10.5139C5.57243 10.6428 5.5 10.8177 5.5 11C5.5 11.1823 5.57243 11.3572 5.70136 11.4861C5.8303 11.6151 6.00516 11.6875 6.1875 11.6875H10.3125V15.8125C10.3125 15.9948 10.3849 16.1697 10.5139 16.2986C10.6428 16.4276 10.8177 16.5 11 16.5C11.1823 16.5 11.3572 16.4276 11.4861 16.2986C11.6151 16.1697 11.6875 15.9948 11.6875 15.8125V11.6875H15.8125C15.9948 11.6875 16.1697 11.6151 16.2986 11.4861C16.4276 11.3572 16.5 11.1823 16.5 11C16.5 10.8177 16.4276 10.6428 16.2986 10.5139C16.1697 10.3849 15.9948 10.3125 15.8125 10.3125H11.6875V6.1875Z" fill="#82C43C" />
															</svg>
															Добавить элемент
														</a>
													</div>
												</div>
												@endforeach
												<div class="body-spoller__button">
													<a href="/add-chapter/{{$course[0]->id}}" class="purple__button">
														Добавить раздел
													</a>
												</div>
											</div>
										</div>
										<div class="list-addchapter__author author-addchapter">
											<div class="author-addchapter__title">Автор</div>
											<div class="author-addchapter__body">
												<div class="author-addchapter__img">
													<picture><img src="{{$course[0]->author->avatar->preview}}" alt="avatar"></picture>
												</div>
												<div class="author-addchapter__content">
													<div class="author-addchapter__name">{{$course[0]->author->name}} {{$course[0]->author->surname}}</div>
													<div class="author-addchapter__text">{{$course[0]->author->regalia}}</div>
													<a href="/mycourses" class="author-addchapter__link">Все курсы</a>
												</div>
											</div>
										</div>
									</div>
								</div>