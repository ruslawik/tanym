@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="hero">
							<h1 class="hero__title">{{__('front.site_title_h1')}}</h1>
							<span class="hero__subtitle">{{__('front.build_your_future')}}</span>
							<a href="/search" class="hero__link">{{__('front.go_to_search')}}</a>
						</section>
						@if(Auth::check())
						<section class="columns">
							<div class="columns__levels levels">
								<div class="reccomend__head">
								<h2 class="reccomend__title">{{__('front.new_added_courses')}}</h2>
							</div>
							<div class="reccomend__body">
								<div class="reccomend__items">
											@foreach($courses as $course)
											<div class="reccomend__item item-reccomend undefined">
												<a href="/public/course/{{$course->id}}">
												<div class="item-reccomend__image">
													<picture><img height=200px src="{{$course->image->url}}" alt="item-reccomend__image" class="item-reccomend__img"></picture>
													@if($course->draft == 1)
														<div class="item-reccomend__popular undefined"> {{__('front.draft')}}</div>
													@endif
												</div>
												</a>
												<br>
												<a class="item-reccomend__title" href="/public/course/{{$course->id}}"><h3>{{$course->name}}</h3></a>
												<br>
												<a href="/instructor/{{$course->author->id}}" class="item-reccomend__user user-item-reccomend">
													<div class="user-item-reccomend__avatar">
														<img src="{{$course->author->avatar->thumbnail}}" alt="Avatar">
													</div>
													<div class="user-item-reccomend__name">Автор:  {{$course->author->name}}</div>
												</a>
												<div class="item-reccomend__actions">
													<div class="item-reccomend__rating">
														<!--
														<div class="rating rating_set">
															<div class="rating__body">
																<div class="rating__active"></div>
																<div class="rating__items">
																	@for($i=0; $i<$course->diffiulty; $i++)
																		<img src="/front/img/icons/rating_star.svg" alt="rating_star">
																	@endfor
																</div>
															</div>
															<div class="rating__diff">Сложность&nbsp;<span class="rating__value">{{$course->diffiulty}}</span>&nbsp;из 5</div>
														</div>
														!-->
													</div>
												</div>
											</div>
											@endforeach
										</div>
							</div>
							</div>
							@if(Auth::check())
							<div class="columns__progress progress">
								<div class="progress__chart">
									<canvas id="progressChart" data-data="@for($i=0; $i<$total_cats; $i++) {{$cat_progresses[$i]['progress']}} @if($i+1 != $total_cats) , @endif @endfor" data-bg="@foreach($cat_progresses as $progress) {{$progress['color']}}, @endforeach" width="310" height="500"></canvas>
								</div>
								<div class="progress__labels">
									@foreach($cat_progresses as $progress)
										<div class="progress__label">
											<div class="progress__sqare"></div>
											<div class="progress__name">{{$progress['name']}}</div>
										</div>
									@endforeach
								</div>
							</div>
							@endif
						</section>
						@else
						<section class="reccomend">
							<div class="columns__levels levels">
								<div class="reccomend__head">
								<h2 class="reccomend__title">{{__('front.new_courses')}}</h2>
							</div>
							<div class="reccomend__body">
								<div class="reccomend__items">
											@foreach($courses_rec as $course)
											<div class="reccomend__item item-reccomend undefined">
												<a href="/public/course/{{$course->id}}">
												<div class="item-reccomend__image">
													<picture><img height=200px src="{{$course->image->url}}" alt="item-reccomend__image" class="item-reccomend__img"></picture>
													@if($course->draft == 1)
														<div class="item-reccomend__popular undefined"> {{__('front.draft')}}</div>
													@endif
												</div>
												</a>
												<br>
												<a class="item-reccomend__title" href="/public/course/{{$course->id}}"><h3>{{$course->name}}</h3></a>
												<br>
												<a href="/instructor/{{$course->author->id}}" class="item-reccomend__user user-item-reccomend">
													<div class="user-item-reccomend__avatar">
														<picture><img src="{{$course->author->avatar->thumbnail}}" alt="Avatar"></picture>
													</div>
													<div class="user-item-reccomend__name">Автор: {{$course->author->name}}</div>
												</a>
												<div class="item-reccomend__actions">
													<!--
													<div class="item-reccomend__rating">
														<div class="rating rating_set">
															<div class="rating__body">
																<div class="rating__active"></div>
																<div class="rating__items">
																	@for($i=0; $i<$course->diffiulty; $i++)
																		<img src="/front/img/icons/rating_star.svg" alt="rating_star">
																	@endfor
																</div>
															</div>
															<div class="rating__diff">Сложность&nbsp;<span class="rating__value">{{$course->diffiulty}}</span>&nbsp;из 5</div>
														</div>
													</div>
													!-->
												</div>
											</div>
											@endforeach
										</div>
							</div>
						</section>
						@endif
						<footer class="footer">
							<div class="footer__container">
							</div>
						</footer>
					</div>
				</div>
			</div>
		</main>
@endsection

@section('js')
@if(Auth::check())
	<script>
		const ctx = document.getElementById("progressChart");
            if (ctx) {
                const ctxData = ctx.dataset.data.split(",");
                const ctxBgs = ctx.dataset.bg.split(", ");
                new Chart(ctx, {
                    type: "polarArea",
                    data: {
                        labels: [ @foreach($cat_progresses as $progress) "{{$progress['name']}}", @endforeach],
                        datasets: [ {
                            data: ctxData,
                            borderWidth: 1,
                            backgroundColor: ctxBgs
                        } ]
                    },
                    options: {
                    	scale: {
            				min: 0,
            				max: 100,
        				},
                        responsive: true,
                        plugins: {
                            legend: {
                                position: "bottom",
                                display: false
                            }
                        }
                    }
                });
                const sqares = document.querySelectorAll(".progress__sqare");
                for (let i = 0; i < sqares.length; i++) sqares[i].style = `background-color: ${ctxBgs[i]}`;
            }
	</script>
@endif
@endsection