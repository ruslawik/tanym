@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="news">

							<div class="section__head">
								<h2 class="section__title">{{__('front.news')}}</h2>

							</div>
							<div class="news__body">
								@foreach($posts as $post)
								<div class="news__item item-news">
									<div class="item-news__image">
										<img src="{{$post->photo->url}}">
									</div>
									<h4 class="item-news__title">{{$post->name}}</h4>
									<a href="/news/{{$post->id}}" class="item-news__link purple__button">{{__('front.read')}}</a>
								</div>
								@endforeach
							</div>
							{{$posts->links('vendor.pagination.default')}}
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection