@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="textimg">
							<div class="textimg__body">
								<div class="textimg__info">
									<h4 class="section__title">{{$post[0]->name}}</h4>
									<br>
									{!!$post[0]->text!!}
								</div>
							</div>

						</section>
					</div>
				</div>
			</div>
		</main>
@endsection