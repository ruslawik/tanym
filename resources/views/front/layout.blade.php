<!DOCTYPE html>
<html lang="ru">

<head>
	<title>Главная</title>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<!-- <style>body{opacity: 0;}</style> -->
	<link rel="stylesheet" href="/front/css/style.css?_v=20220207132502">
	<link rel="shortcut icon" href="favicon.ico">
	<!-- <meta name="robots" content="noindex, nofollow"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<div class="wrapper">
		<header class="header">
			<div class="header__container">
				<div class="header__menu menu">
					<div class="menu__container">
						<nav class="menu__body">
							@include('front.__includes.__menu')
						</nav>
					</div>
				</div>
				<button type="button" class="menu__icon icon-menu"><span></span></button>
				<a href="/" class="header__logo">
					<picture><source srcset="/front/img/logo.webp" type="image/webp"><img src="/front/img/logo.png" alt="logo"></picture>
				</a>
				<div class="header__actions">
					<a href="/local/ru" class="header__profile profile-header">
						РУС
					</a>
					<a href="/local/kz" class="header__profile profile-header">
						ҚАЗ
					</a>
					<a href="/kaz" class="header__profile profile-header">
						&nbsp;<br>&nbsp;
					</a><a href="/kaz" class="header__profile profile-header">
						&nbsp;<br>&nbsp;
					</a>
					<a href="#" class="header__profile profile-header">
						@if(Auth::check())
							<div class="profile-header__avatar">
								@if(Auth::user()->avatar)
									<picture><img width=50 src="{{Auth::user()->avatar->preview}}" alt="avatar"></picture>
								@else
									<picture><img width=50 src="/front/img/user_icon.png" alt="avatar"></picture>
								@endif
							</div>
							<div class="profile-header__info" onClick="document.location.href='/cabinet'">
								<div class="profile-header__name">{{Auth::user()->name}}</div>
								<div class="profile-header__mail">{{Auth::user()->email}}</div>
							</div>
						@else
							<div class="profile-header__info" onClick="document.location.href='/login'">
								<div class="profile-header__name">{{__('front.login')}}</div>
								<div class="profile-header__mail">{{__('front.register')}}</div>
							</div>
						@endif
					</a>
				</div>
			</div>
		</header>
		@yield('content')
	</div>
	<!-- 
<div id="popup" aria-hidden="true" class="popup">
	<div class="popup__wrapper">
		<div class="popup__content">
			<button data-close type="button" class="popup__close">Закрыть</button>
			<div class="popup__text">
			</div>
		</div>
	</div>
</div>
 -->
	<script src="https://cdn.jsdelivr.net/npm/chart.js?_v=20220207132502"></script>
	<script src="/front/js/app.js?_v=2022020713252"></script>
	@yield('js')
</body>

</html>