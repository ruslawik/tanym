@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						@if(session('message'))
					<div class="notification _dn _hide">
						<span class="notification__text">
							{{ session('message') }}
						</span>
						<span class="notification__close"></span>
					</div>
				@endif
						<section class="mywallet">

							<div class="section__head">
								<h2 class="section__title">{{__('front.my_wallet')}}</h2>

							</div>
							<div class="mywallet__body">
								<div class="mywallet__line">
									<div class="mywallet__text">{{__('front.whats_ritms')}}</div>
								</div>
								<div class="mywallet__line mywallet__line_bd"></div>
								<div class="mywallet__line">
									<form action="/give-ritms" method="POST" class="mywallet__form form-mywallet">
										@csrf()
										<div class="form-mywallet__line">
											<div class="form-mywallet__title">
												<span class="form-mywallet__text">{{__('front.your_ritms')}}:</span>
												<img class="form-mywallet__image" src="/front/img/icons/coins.svg" alt="coins">
												<span class="form-mywallet__number">{{Auth::user()->ritms}}</span>
											</div>
										</div>
										<div class="form-mywallet__line">
											<input data-required placeholder="{{__('front.ritm_amount')}}" type="number" class="form-mywallet__input" name="ritms_to_give">
											<input data-required="email" placeholder="Email" type="text" class="form-mywallet__input" name="email_whom_give">
											<button type="submit" class="form-mywallet__button purple__button">{{__('front.give')}}</button>
										</div>
									</form>
								</div>
								<div class="mywallet__line mywallet__line_bd"></div>
								<div class="mywallet__line">
									<div class="mywallet__text">{{__('front.refer_link')}}: <br>
										<a href="#" class="mywallet__link">https://tanym.caravanofknowledge.com/login?ref={{Auth::user()->id}}</a>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
@endsection