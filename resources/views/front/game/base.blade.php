<!DOCTYPE html>
<html lang="ru">

<head>
	<title>Моя база</title>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<!-- <style>body{opacity: 0;}</style> -->
	<link rel="stylesheet" href="/front/css/style.css?_v=20220207132502">
	<link rel="shortcut icon" href="favicon.ico">
	<!-- <meta name="robots" content="noindex, nofollow"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<form action="/game/save-background" method="POST">
		@csrf
		<div class="wrapper">
			<header class="header header_base">
				<div class="header__container">
					<a href="/" class="header__logo">
						<picture><source srcset="/front/img/logo.webp" type="image/webp"><img src="/front/img/logo.png" alt="logo"></picture>
					</a>
					<div class="header__base base-header">
						<span class="base-header__span">{{__('front.my_base')}}</span>
						<button type="button" class="base-header__link base-header__link_redact purple__button " style="background-color: ">{{__('front.edit_base')}}</button>
						<button type="submit" class="base-header__link base-header__link_save purple__button _dn">{{__('front.save')}}</button>
						<a href="#" class="base-header__link base-header__link_back purple__button _dn"></a>
					</div>
					<div class="header__actions">
						<a href="/local/ru" class="header__profile profile-header">
						РУС
					</a>
					<a href="/local/kz" class="header__profile profile-header">
						ҚАЗ
					</a>
					<a href="/kaz" class="header__profile profile-header">
						&nbsp;<br>&nbsp;
					</a><a href="/kaz" class="header__profile profile-header">
						&nbsp;<br>&nbsp;
					</a>
					<a href="#" class="header__profile profile-header">
						@if(Auth::check())
							<div class="profile-header__avatar">
								@if(Auth::user()->avatar)
									<picture><img width=50 src="{{Auth::user()->avatar->preview}}" alt="avatar"></picture>
								@else
									<picture><img width=50 src="/front/img/user_icon.png" alt="avatar"></picture>
								@endif
							</div>
							<div class="profile-header__info" onClick="document.location.href='/cabinet'">
								<div class="profile-header__name">{{Auth::user()->name}}</div>
								<div class="profile-header__mail">{{Auth::user()->email}}</div>
							</div>
						@else
							<div class="profile-header__info" onClick="document.location.href='/login'">
								<div class="profile-header__name">{{__('front.login')}}</div>
								<div class="profile-header__mail">{{__('front.register')}}</div>
							</div>
						@endif
					</a>
				</div>
				</div>
			</header>
			<main class="page">
				<div class="page__wrapper page__wrapper_base">
					<section class="base {{Auth::user()->base_background}}">
						<div class="base__container">
							@if(session('message'))
					<div class="notification _dn _hide">
						<span class="notification__text">
							{{ session('message') }}
						</span>
						<span class="notification__close"></span>
					</div>
				@endif
							<div class="base__head">
								<a href="#" class="base__status base__item">
									<spam class="base__purple">{{__('front.my_status')}}: </spam>
									<span class="base__orange">Космо-рекрут</span>
								</a>
								<a href="/why-here" class="base__purple base__item">{{__('front.why_here')}}?</a>
							</div>
							<span class="base_planet base__pluto">
								<picture><source srcset="/front/img/base/base__mars_planet1.webp" type="image/webp"><img src="/front/img/base/base__mars_planet1.png" alt="planet"></picture>
							</span>
							<span class="base_planet base__earth">
								<picture><source srcset="/front/img/base/base__mars_planet2.webp" type="image/webp"><img src="/front/img/base/base__mars_planet2.png" alt="planet"></picture>
							</span>
							<span class="base_planet base__saturn">
								<picture><source srcset="/front/img/base/base__mars_planet3.webp" type="image/webp"><img src="/front/img/base/base__mars_planet3.png" alt="planet"></picture>
							</span>
							<div class="base__stantions">
								<a href="/station/{{$stations[0]->id}}" class="base__stantion base__stantion_1">
									<div class="base__image">
										<img src="{{$stations[0]->get_item_info->image->url}}" alt="base_physic">
									</div>
									<div class="base__text">
										<div class="base__name">{{$stations[0]->get_item_info->name}} - {{$stations[0]->get_category->name}}</div>
										<div class="base__progress">
											<div class="base_range">
												<span class="base_range_span" style="width:calc(65 / 100 * 100%)"></span>
											</div>
											<span class="base_percent">{{\App\Models\CourseCategory::progress($stations[0]->get_category->id, Auth::user()->id)}}%</span>
										</div>
									</div>
								</a>
								<a href="/station/{{$stations[1]->id}}" class="base__stantion base__stantion_2">
									<div class="base__image">
										<img src="{{$stations[1]->get_item_info->image->url}}" alt="base_physic">
									</div>
									<div class="base__text">
										<div class="base__name">{{$stations[1]->get_item_info->name}} - {{$stations[1]->get_category->name}}</div>
										<div class="base__progress">
											<div class="base_range">
												<span class="base_range_span" style="width:calc(65 / 100 * 100%)"></span>
											</div>
											<span class="base_percent">{{\App\Models\CourseCategory::progress($stations[1]->get_category->id, Auth::user()->id)}}%</span>
										</div>
									</div>
								</a>
								<a href="/station/{{$stations[2]->id}}" class="base__stantion base__stantion_3">
									<div class="base__image">
										<img src="{{$stations[2]->get_item_info->image->url}}" alt="base_physic">
									</div>
									<div class="base__text">
										<div class="base__name">{{$stations[2]->get_item_info->name}} - {{$stations[2]->get_category->name}}</div>
										<div class="base__progress">
											<div class="base_range">
												<span class="base_range_span" style="width:calc(65 / 100 * 100%)"></span>
											</div>
											<span class="base_percent">{{\App\Models\CourseCategory::progress($stations[2]->get_category->id, Auth::user()->id)}}%</span>
										</div>
									</div>
								</a>
								<a href="/station/{{$stations[3]->id}}" class="base__stantion base__stantion_4">
									<div class="base__image">
										<img src="{{$stations[3]->get_item_info->image->url}}" alt="base_physic">
									</div>
									<div class="base__text">
										<div class="base__name">{{$stations[3]->get_item_info->name}} - {{$stations[3]->get_category->name}}</div>
										<div class="base__progress">
											<div class="base_range">
												<span class="base_range_span" style="width:calc(65 / 100 * 100%)"></span>
											</div>
											<span class="base_percent">{{\App\Models\CourseCategory::progress($stations[3]->get_category->id, Auth::user()->id)}}%</span>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="base__settings settings-base">
							<div class="settings-base__container">
								<div class="settings-base__title">{{__('front.select_background')}}:</div>
								<div class="settings-base__radios">
									@foreach($backgrounds as $background)
										@php $can_buy = "" @endphp
										@php if($background->ritm_price > Auth::user()->ritms)
											$can_buy = "no"
										@endphp 
										<input checked type="radio" name="bg" value="base_{{$background->name}}" id="radio__{{$background->name}}" class="settings-base__radio">
										<label for="radio__{{$background->name}}" class="settings-base__label settings-base__label_{{$can_buy	}}	 settings-base__label_{{$background->name}}">
											<div class="settings-base__img"></div>
											@if(app()->getLocale()=="ru")
											<span class="settings-base__span">Ритмов: {{$background->ritm_price}}</span>
											@else
											<span class="settings-base__span"> {{$background->ritm_price}} ритм</span>
											@endif
										</label>
									@endforeach
								</div>
							</div>
						</div>
					</section>
				</div>
			</main>
		</div>
	</form>
	<!-- 
<div id="popup" aria-hidden="true" class="popup">
	<div class="popup__wrapper">
		<div class="popup__content">
			<button data-close type="button" class="popup__close">Закрыть</button>
			<div class="popup__text">
			</div>
		</div>
	</div>
</div>
 -->
	<script src="https://cdn.jsdelivr.net/npm/chart.js?_v=20220207132502"></script>

	<script src="/front/js/app.js?_v=20220207132502"></script>
</body>

</html>