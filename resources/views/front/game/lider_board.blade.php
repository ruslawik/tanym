@extends('front.layout')
@section('content')
		<main class="page">
			<div class="page__container">
				<div class="page__wrapper">
					<div class="page__containerr">
						<section class="ritmrating">

							<div class="section__head">
								<h2 class="section__title">{{__('front.rating_ritmov')}}</h2>

							</div>
							<div class="ritmrating__body">
								<div class="ritmrating__line">
									<div class="ritmrating__title">{{__('front.all_gamers')}}</div>
									<div class="ritmrating__text">{{__('front.rating_text_1')}}  <br>
										 {{__('front.rating_text_2')}}<br>{{__('front.rating_text_3')}}
										</div>
								</div>
								<div class="ritmrating__line ritmrating__line_bd"></div>
								<div class="ritmrating__line">
									<div class="ritmrating__table table-ritmrating">
										<div class="table-ritmrating__line">
											<div class="table-ritmrating__item">
												<div class="table-ritmrating__title">{{__('front.name')}}</div>
											</div>
											<div class="table-ritmrating__item">
												<div class="table-ritmrating__title">{{__('front.ritms')}}</div>
											</div>
											<div class="table-ritmrating__item">
												<div class="table-ritmrating__title">{{__('front.position')}}</div>
											</div>
											<div class="table-ritmrating__item">
												<div class="table-ritmrating__title">{{__('front.base_koru')}}</div>
											</div>
										</div>
										<?php $k = 1; ?>
										@foreach($users as $user)
										<div class="table-ritmrating__line">
											<div class="table-ritmrating__item">{{$user->name}}</div>
											<div class="table-ritmrating__item">{{$user->total_got_ritms}}</div>
											<div class="table-ritmrating__item">
												@if($k == 1)
													<img src="/front/img/icons/crown.svg" alt="crown">
												@endif
												{{$k}}
												<?php $k++; ?>
											</div>
											<div class="table-ritmrating__item">
												<a href="/user-base/{{$user->id}}" class="table-ritmrating__link">{{__('front.koru')}}</a>
											</div>
										</div>
										@endforeach
									</div>
								</div>
							</div>
						</section>
						<?php
							$load_more_limit = $limit+100;
						?>
					</div>
					<center>{{$users->links('vendor.pagination.default')}}</center>
					<br>
					<br>
					<br>
				</div>
			</div>
		</main>
@endsection