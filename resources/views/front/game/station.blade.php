<!DOCTYPE html>
<html lang="ru">

<head>
	<title>Станция</title>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<!-- <style>body{opacity: 0;}</style> -->
	<link rel="stylesheet" href="/front/css/style.css?_v=20220207132502">
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- <meta name="robots" content="noindex, nofollow"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<form action="/station/update/{{$item[0]->id}}" method="POST">
		@csrf
		<input type="hidden" name="course_category_id" value="{{$item[0]->get_category->id}}">
		<input type="hidden" name="selected_station_id" id="selected_station_id" value="{{$item[0]->station_id}}">
		<div class="wrapper wrapper__settingsbase">
			<header class="header header_base">
				<div class="header__container">
					<a href="/" class="header__logo">
						<picture><source srcset="/front/img/logo.webp" type="image/webp"><img src="/front/img/logo.png" alt="logo"></picture>
					</a>
					<div class="header__base base-header">
						<span class="base-header__span">«{{$item[0]->get_category->name}}»</span>
						<button type="button" class="base-header__link base-header__link_redact purple__button " style="background-color: ">{{__('front.edit_base')}}</button>
						<button type="submit" class="base-header__link base-header__link_save purple__button _dn">{{__('front.save')}}</button>
						<a href="/base" class="base-header__link base-header__link_back purple__button ">{{__('front.back_to_base')}}</a>
					</div>
					<div class="header__actions">
						<a href="/local/ru" class="header__profile profile-header">
						РУС
					</a>
					<a href="/local/kz" class="header__profile profile-header">
						ҚАЗ
					</a>
					<a href="/kaz" class="header__profile profile-header">
						&nbsp;<br>&nbsp;
					</a><a href="/kaz" class="header__profile profile-header">
						&nbsp;<br>&nbsp;
					</a>
					<a href="#" class="header__profile profile-header">
						@if(Auth::check())
							<div class="profile-header__avatar">
								@if(Auth::user()->avatar)
									<picture><img width=50 src="{{Auth::user()->avatar->preview}}" alt="avatar"></picture>
								@else
									<picture><img width=50 src="/front/img/user_icon.png" alt="avatar"></picture>
								@endif
							</div>
							<div class="profile-header__info" onClick="document.location.href='/cabinet'">
								<div class="profile-header__name">{{Auth::user()->name}}</div>
								<div class="profile-header__mail">{{Auth::user()->email}}</div>
							</div>
						@else
							<div class="profile-header__info" onClick="document.location.href='/login'">
								<div class="profile-header__name">{{__('front.login')}}</div>
								<div class="profile-header__mail">{{__('front.register')}}</div>
							</div>
						@endif
					</a>
				</div>
				</div>
			</header>
			<main class="page">
				<div class="page__wrapper page__wrapper_base">
					@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
					<section class="settingsbase">
						<div class="settingsbase__container"  style="min-height: 700px !important; ">
							<div class="settingsbase__body">
								<div class="settingsbase__stantion stantion-settingsbase">
									<div class="stantion-settingsbase__popup _dn" data-popup="#stantion-settings"></div>
									<div class="stantion-settingsbase__img">
										<img src="{{$item[0]->get_item_info->image->url}}" class="stantion-settingsbase__image" alt="stantion-settingsbase__image">
									</div>
									<div class="base__text">
										<div class="base__name">{{$item[0]->get_category->name}}</div>
										<div class="base__progress">
											<div class="base_range">
												<span class="base_range_span"></span>
											</div>
											<span class="base_percent">{{\App\Models\CourseCategory::progress($item[0]->get_category->id, Auth::user()->id)}}%</span>
										</div>
									</div>
								</div>
								<div class="settingsbase__modules">
									<input type="hidden" id="changed_test_signs" name="changed_test_signs" value="">
									<input type="hidden" id="changed_test_colors" name="changed_test_colors" value="">
									<table width=30%><tr><td><img style="width:50px;" src="/front/img/icons/coins.svg" alt="coins"></td><td>&nbsp;<h3>{{__('front.your_ritms')}}: {{Auth::user()->ritms}}</h3></td></tr></table>
									@php $k = 0; @endphp
									@foreach($levels as $level)
										@php $k++; @endphp
										<div class="settingsbase__module module-settingsbase">
											<div class="module-settingsbase__line">
												<div class="base__progress">
													<div class="base_range">
														<span class="base_range_span"></span>
													</div>
					<span class="base_percent">{{\App\Models\CourseClass::progress($level, Auth::user()->id)}}%</span>
												</div>
												@if(app()->getLocale() == "ru")
												<div class="module-settingsbase__title">Уровень {{$level}}</div>
												@else
												<div class="module-settingsbase__title">{{$level}} деңгей</div>
												@endif
											</div>
											<div class="module-settingsbase__line module-settingsbase__line_bd"></div>
											@foreach($courses as $course)
											@if($course->class->class == $level)
											<div class="module-settingsbase__line module-settingsbase__items items-settingsbase">
												<div class="module-settingsbase__title"> {{$course->name}}</div>
												<div class="items-settingsbase__body">
													@foreach($course->passed_tests_with_sign_and_color as $test)
													<div class="items-settingsbase__item">
														<input type="hidden" class="item_id" value="{{$test->id}}">
														<div class="items-settingsbase__icon">
															<img src="{{$test->sign->image->url}}" alt="items-settingsbase__icon">
														</div>
														<div class="items-settingsbase__name" style="background-color: #{{$test->color->html_color}};">{{$test->test->chapter_item->name}} ({{$test->test->progress(Auth::user()->id)}}%)</div>
													</div>
													<input type="hidden" id="test_sign_{{$test->id}}" name="test_sign_{{$test->id}}"  value="{{$test->sign->id}}">
													<input type="hidden" id="test_color_{{$test->id}}" name="test_color_{{$test->id}}"  value="{{$test->color->id}}">
													@endforeach
												</div>
											</div>
											@endif
											@endforeach
										</div>
									@endforeach
									@if ($k == 0)
										<div class="settingsbase__module module-settingsbase">
											<div class="module-settingsbase__line">
												<div class="base__progress">
													<div class="base_range">
														<span class="base_range_span"></span>
													</div>
												</div>
												<div class="module-settingsbase__title">{{__('front.need_test_1')}}<br><br>{{__('front.need_test_2')}}</div>
											</div>
											<div class="module-settingsbase__line module-settingsbase__line_bd"></div>
										</div>										
									@endif
								</div>
							</div>
						</div>
					</section>
				</div>
			</main>
		</div>
	</form>

	<div id="stantion-settings" aria-hidden="true" class="popup">
		<div class="popup__wrapper">
			<div class="popup__content">
				<button data-close type="button" class="popup__close">Закрыть</button>
				<div class="popup__text">
					<div class="settings__stantion stantion-settings ">
						<div class="stantion-settings__title">Выбрать вид станции:</div>
						<div class="stantion-settings__body">
							@foreach($all_stations as $station)
								@php $can_buy = "" @endphp
								@php if($station->ritm_price > Auth::user()->ritms && $station->is_installed_on_current_user() == false)
									$can_buy = "no"
								@endphp 
								<div class="stantion-settings__item stantion-settings__item_{{$can_buy}}">
									<input type="radio" name="station_visual" id="station_{{$station->id}}" value="{{$station->id}}***{{$station->image->url}}" class="stantion-settings__radio">
									<label for="station_{{$station->id}}" class="stantion-settings__label label-stantion-settings">
										<div class="label-stantion-settings__img">
											<img src="{{$station->image->url}}" alt="base__image">
										</div>
										@if($station->is_installed_on_current_user())
											<span class="label-stantion-settings__text">Установлен!</span>
										@else
											<span class="label-stantion-settings__text">{{$station->name}} - {{$station->ritm_price}} ртм.</span>
										@endif
									</label>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="module-settings" aria-hidden="true" class="popup">
		<div class="popup__wrapper">
			<div class="popup__content">
				<button data-close type="button" class="popup__close">Закрыть</button>
				<div class="popup__text">
					<div class="module-settings__title">Выбрать значок модуля и цвет:</div>
					<div class="module-settings__body">
						<div class="module-settings__icons icons-module-settings">
							@foreach($all_signs as $sign)
								<div class="icons-module-settings__item">
									<input type="radio" name="icons-module-settings" id="sign_{{$sign->id}}" value="{{$sign->image->url}}***{{$sign->id}}" class="icons-module-settings__radio">
									<label for="sign_{{$sign->id}}" class="icons-module-settings__label">
										<img src="{{$sign->image->url}}" alt="icon">
									</label>
									<br>
									<b>{{$sign->price}}</b> ртм.
								</div>
							@endforeach
						</div>
						<div class="module-settings__color color-module-settings">
							@foreach($all_colors as $color)
							<div class="color-module-settings__item">
								<input type="radio" name="color-module-settings" id="{{$color->html_color}}" value="{{$color->html_color}}***{{$color->id}}" class="color-module-settings__radio">
								<label for="{{$color->html_color}}" class="color-module-settings__label"></label>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://cdn.jsdelivr.net/npm/chart.js?_v=20220207132502"></script>

	<script src="/front/js/app.js?_v=20220207132502"></script>
</body>

</html>