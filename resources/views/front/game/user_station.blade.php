<!DOCTYPE html>
<html lang="ru">

<head>
	<title>Станция «{{$item[0]->get_item_info->name}}»</title>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<!-- <style>body{opacity: 0;}</style> -->
	<link rel="stylesheet" href="/front/css/style.css?_v=20220207132502">
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- <meta name="robots" content="noindex, nofollow"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<form action="/station/update/{{$item[0]->id}}" method="POST">
		@csrf
		<input type="hidden" name="course_category_id" value="{{$item[0]->get_category->id}}">
		<input type="hidden" name="selected_station_id" id="selected_station_id" value="{{$item[0]->id}}">
		<div class="wrapper wrapper__settingsbase">
			<header class="header header_base">
				<div class="header__container">
					<a href="/" class="header__logo">
						<picture><source srcset="/front/img/logo.webp" type="image/webp"><img src="/front/img/logo.png" alt="logo"></picture>
					</a>
					<div class="header__base base-header">
						<span class="base-header__span">Станция «{{$item[0]->get_item_info->name}}»</span>
						<span class="_dn">
							<button type="button" class="base-header__link base-header__link_redact purple__button " style="background-color: ">Редактировать</button>
							<button type="submit" class="base-header__link base-header__link_save purple__button _dn">Сохранить</button>
						</span>
						<a href="/user-base/{{$user[0]->id}}" class="base-header__link base-header__link_back purple__button ">Назад на базу</a>
					</div>
					<div class="header__actions">
					<a href="#" class="header__profile profile-header">
						@if(Auth::check())
							<div class="profile-header__avatar">
								@if(Auth::user()->avatar)
									<picture><img width=50 src="{{Auth::user()->avatar->preview}}" alt="avatar"></picture>
								@else
									<picture><img width=50 src="/front/img/user_icon.png" alt="avatar"></picture>
								@endif
							</div>
							<div class="profile-header__info" onClick="document.location.href='/cabinet'">
								<div class="profile-header__name">{{Auth::user()->name}}</div>
								<div class="profile-header__mail">{{Auth::user()->email}}</div>
							</div>
						@else
							<div class="profile-header__info" onClick="document.location.href='/login'">
								<div class="profile-header__name">Войти</div>
								<div class="profile-header__mail">Зарегистрироваться</div>
							</div>
						@endif
					</a>
				</div>
				</div>
			</header>
			<main class="page">
				<div class="page__wrapper page__wrapper_base">
					@if(session('message'))
							<div class="notification _dn _hide">
								<span class="notification__text">
									{{ session('message') }}
								</span>
								<span class="notification__close"></span>
							</div>
						@endif
					<section class="settingsbase">
						<div class="settingsbase__container"  style="min-height: 700px !important; ">
							<div class="settingsbase__body">
								<div class="settingsbase__stantion stantion-settingsbase">
									<div class="stantion-settingsbase__popup _dn" data-popup="#stantion-settings"></div>
									<div class="stantion-settingsbase__img">
										<img src="{{$item[0]->get_item_info->image->url}}" class="stantion-settingsbase__image" alt="stantion-settingsbase__image">
									</div>
									<div class="base__text">
										<div class="base__name">{{$item[0]->get_category->name}}</div>
										<div class="base__progress">
											<div class="base_range">
												<span class="base_range_span"></span>
											</div>
											<span class="base_percent">{{\App\Models\CourseCategory::progress($item[0]->get_category->id, $user[0]->id)}}%</span>
										</div>
									</div>
								</div>
								<div class="settingsbase__modules">
									<input type="hidden" id="changed_test_signs" name="changed_test_signs" value="">
									<input type="hidden" id="changed_test_colors" name="changed_test_colors" value="">

									@php $k = 0; @endphp
									@foreach($levels as $level)
										@php $k++; @endphp
										<div class="settingsbase__module module-settingsbase">
											<div class="module-settingsbase__line">
												<div class="base__progress">
													<div class="base_range">
														<span class="base_range_span"></span>
													</div>
					<span class="base_percent">{{\App\Models\CourseClass::progress($level, $user[0]->id)}}%</span>
												</div>
												<div class="module-settingsbase__title">Уровень {{$level}}</div>
											</div>
											<div class="module-settingsbase__line module-settingsbase__line_bd"></div>
											@foreach($courses as $course)
											@if($course->class->class == $level)
											<div class="module-settingsbase__line module-settingsbase__items items-settingsbase">
												<div class="module-settingsbase__title"> {{$course->name}}</div>
												<div class="items-settingsbase__body">
													@foreach($course->tests_of_user($user[0]->id, $course->id) as $test)
													<div class="items-settingsbase__item">
														<input type="hidden" class="item_id" value="{{$test->id}}">
														<div class="items-settingsbase__icon">
															<img src="{{$test->sign->image->url}}" alt="items-settingsbase__icon">
														</div>
														<div class="items-settingsbase__name" style="background-color: #{{$test->color->html_color}};">{{$test->test->chapter_item->name}} ({{$test->test->progress($user[0]->id)}}%)</div>
													</div>
													<input type="hidden" id="test_sign_{{$test->id}}" name="test_sign_{{$test->id}}"  value="{{$test->sign->id}}">
													<input type="hidden" id="test_color_{{$test->id}}" name="test_color_{{$test->id}}"  value="{{$test->color->id}}">
													@endforeach
												</div>
											</div>
											@endif
											@endforeach
										</div>
									@endforeach
									@if ($k == 0)
										<div class="settingsbase__module module-settingsbase">
											<div class="module-settingsbase__line">
												<div class="base__progress">
													<div class="base_range">
														<span class="base_range_span"></span>
													</div>
												</div>
												<div class="module-settingsbase__title">Пользователь еще не прошел ни одного теста для редактирования станции</div>
											</div>
											<div class="module-settingsbase__line module-settingsbase__line_bd"></div>
										</div>										
									@endif
								</div>
							</div>
						</div>
					</section>
				</div>
			</main>
		</div>
	</form>

	<script src="https://cdn.jsdelivr.net/npm/chart.js?_v=20220207132502"></script>

	<script src="/front/js/app.js?_v=20220207132502"></script>
</body>

</html>