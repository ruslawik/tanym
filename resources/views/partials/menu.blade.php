<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/permissions*") ? "c-show" : "" }} {{ request()->is("admin/roles*") ? "c-show" : "" }} {{ request()->is("admin/users*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('catalog_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/course-elements*") ? "c-show" : "" }} {{ request()->is("admin/course-categories*") ? "c-show" : "" }} {{ request()->is("admin/course-classes*") ? "c-show" : "" }} {{ request()->is("admin/game-backgrounds*") ? "c-show" : "" }} {{ request()->is("admin/stations*") ? "c-show" : "" }} {{ request()->is("admin/signs*") ? "c-show" : "" }} {{ request()->is("admin/colors*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-th-list c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.catalog.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('course_element_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.course-elements.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/course-elements") || request()->is("admin/course-elements/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-book c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.courseElement.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('course_category_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.course-categories.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/course-categories") || request()->is("admin/course-categories/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-list-ol c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.courseCategory.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('course_class_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.course-classes.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/course-classes") || request()->is("admin/course-classes/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-level-up-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.courseClass.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('game_background_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.game-backgrounds.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/game-backgrounds") || request()->is("admin/game-backgrounds/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-images c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.gameBackground.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('station_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.stations.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/stations") || request()->is("admin/stations/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-image c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.station.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('sign_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.signs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/signs") || request()->is("admin/signs/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-shekel-sign c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.sign.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('color_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.colors.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/colors") || request()->is("admin/colors/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-palette c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.color.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('course_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/courses*") ? "c-show" : "" }} {{ request()->is("admin/course-chapters*") ? "c-show" : "" }} {{ request()->is("admin/chapter-items*") ? "c-show" : "" }} {{ request()->is("admin/videos*") ? "c-show" : "" }} {{ request()->is("admin/files*") ? "c-show" : "" }} {{ request()->is("admin/pages*") ? "c-show" : "" }} {{ request()->is("admin/tests*") ? "c-show" : "" }} {{ request()->is("admin/questions*") ? "c-show" : "" }} {{ request()->is("admin/answers*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-book-open c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.courseManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('course_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.courses.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/courses") || request()->is("admin/courses/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-align-justify c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.course.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('course_chapter_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.course-chapters.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/course-chapters") || request()->is("admin/course-chapters/*") ? "c-active" : "" }}">
                                <i class="fa-fw fab fa-amilia c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.courseChapter.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('chapter_item_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.chapter-items.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/chapter-items") || request()->is("admin/chapter-items/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-arrow-circle-right c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.chapterItem.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('video_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.videos.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/videos") || request()->is("admin/videos/*") ? "c-active" : "" }}">
                                <i class="fa-fw fab fa-youtube c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.video.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('file_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.files.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/files") || request()->is("admin/files/*") ? "c-active" : "" }}">
                                <i class="fa-fw far fa-copy c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.file.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('page_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.pages.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/pages") || request()->is("admin/pages/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-file-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.page.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('test_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.tests.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/tests") || request()->is("admin/tests/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-question c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.test.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('question_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.questions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/questions") || request()->is("admin/questions/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-question-circle c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.question.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('answer_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.answers.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/answers") || request()->is("admin/answers/*") ? "c-active" : "" }}">
                                <i class="fa-fw far fa-comment c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.answer.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('gamification_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/game-available-items*") ? "c-show" : "" }} {{ request()->is("admin/test-sign-colors*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-dice c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.gamificationManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('game_available_item_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.game-available-items.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/game-available-items") || request()->is("admin/game-available-items/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-trophy c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.gameAvailableItem.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('test_sign_color_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.test-sign-colors.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/test-sign-colors") || request()->is("admin/test-sign-colors/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-palette c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.testSignColor.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('news_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.news.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/news") || request()->is("admin/news/*") ? "c-active" : "" }}">
                    <i class="fa-fw far fa-newspaper c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.news.title') }}
                </a>
            </li>
        @endcan
        @can('popup_news_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.popup-news.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/popup-news") || request()->is("admin/popup-news/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-fire c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.popupNews.title') }}
                </a>
            </li>
        @endcan
        @can('passed_test_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.passed-tests.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/passed-tests") || request()->is("admin/passed-tests/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-vial c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.passedTest.title') }}
                </a>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'c-active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>