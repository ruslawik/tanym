<?php


Route::get('/local/{locale}', 'FrontController@setLang');

Route::get('/', 'FrontController@index');
Route::get('/search', 'FrontController@search');
Route::get('/public/course/{course_id}', 'CourseController@course');
Route::get('/course/{course_id}/chapter/{chapter_id}/element/{element_id}', 'CourseController@elementRouter');
//Прохождение теста, если не авторизован, напишем и не дадим пройти тест
Route::get('/course/{course_id}/chapter/{chapter_id}/element/{element_id}/test/{test_id}', 'TestController@getPass');

Route::get('/instructor/{instructor_id}', 'FrontController@instructor');

Route::get('/base-example', 'GameController@baseExample');
Route::get('/why-here', 'GameController@whyHere');
Route::get('/rating', 'GameController@rating');
Route::get('/user-base/{user_id}', 'GameController@userBase');
Route::get('/user-station/{game_selected_station}/{user_id}', 'GameController@userStation');

Route::get('/news', 'NewsController@index');
Route::get('/news/{post_id}', 'NewsController@post');

Route::post('/send-new-pass', 'CabinetController@sendNewPass');

Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function(){
    Route::get('/cabinet', 'CabinetController@index');
    Route::post('/update-cabinet', 'CabinetController@updateUserInfo');
    Route::get('/game/{on_off}', 'CabinetController@gameOnOff');

    //GAME RELATED ROUTES
    Route::get('/base', 'GameController@baseIndex');
    Route::post('/game/save-background', 'GameController@saveBackground');
    Route::get('/station/{game_available_item_id}', 'GameController@stationEdit');
    Route::post('/station/update/{current_available_item_id}', 'GameController@stationUpdate');
    Route::get('/give-hint', 'GameController@giveHint');
    Route::post('/give-ritms', 'GameController@giveRitms');

    Route::get('/wallet', 'GameController@wallet');

    Route::post('/test/{test_id}/finish', 'TestController@finishTest');

    Route::get('/get-cert/{test_id}', 'TestController@getCert');

});

Route::group(['middleware' => ['auth', 'is_instructor']], function(){

    Route::get('/mycourses', 'ManageCourseController@index');

    Route::get('/add-course', 'ManageCourseController@addCourse');
    Route::post('/add-course-store', 'ManageCourseController@addCourseStore');
    Route::get('/edit-course/{course_id}', 'ManageCourseController@editCourse');
    Route::post('/edit-course-update', 'ManageCourseController@editCourseUpdate');
    Route::get('/add-chapter/{course_id}', 'ManageCourseController@addChapter');
    Route::post('/add-chapter-store', 'ManageCourseController@addChapterStore');
    Route::get('/edit-chapter/{course_id}/{chapter_id}', 'ManageCourseController@editChapter');
    Route::post('/edit-chapter-update', 'ManageCourseController@editChapterUpdate');
    Route::get('/course/{course_id}/delete-chapter/{chapter_id}', 'ManageCourseController@deleteChapter');
    Route::post('/course/{course_id}/chapter-elements-order-update/{chapter_id}', 'ManageCourseController@chapterElementsOrderUpdate');
    Route::get('/course/{course_id}/chapter-order-edit', 'ManageCourseController@chapterOrderEdit');
    Route::post('/course/{course_id}/chapter-order-edit-update', 'ManageCourseController@chapterOrderEditUpdate');

    Route::get('/add-element/{course_id}/{chapter_id}', 'ManageChapterItemsController@addElement');
    Route::post('/add-element-store/{course_id}/{chapter_id}', 'ManageChapterItemsController@addElementStore');
    Route::get('/edit-element/{element_id}/{item_id}', 'ManageChapterItemsController@editElement');

    Route::get('/course/{course_id}/edit-element/{element_id}', 'ManageChapterItemsController@editElement');
    Route::post('/course/{course_id}/change-elem-name-update/{element_id}', 'ManageChapterItemsController@changeElemName');
    Route::get('/course/{course_id}/delete-element/{element_id}', 'ManageChapterItemsController@deleteElement');

    Route::post('/course/{course_id}/edit-video-update/{chapter_item_id}', 'ManageChapterItemsController@editVideoUpdate');
    Route::post('/course/{course_id}/add-file-store/{chapter_item_id}', 'ManageChapterItemsController@addFileStore');
    Route::get('/course/{course_id}/delete-file/{file_id}', 'ManageChapterItemsController@deleteFile');
    Route::post('/course/{course_id}/edit-page-update/{chapter_item_id}', 'ManageChapterItemsController@editPageUpdate');
    Route::post('/upload-page-media/ckeditor', 'ManageChapterItemsController@uploadCkeditorMedia')->name('upload-ckeditor-media');

    Route::get('/course/{course_id}/test/{test_id}/add-question-store', 'ManageChapterItemsController@addQuestionStore');
    Route::get('/course/{course_id}/delete-question/{question_id}', 'ManageChapterItemsController@deleteQuestion');
    Route::get('/course/{course_id}/question/{question_id}/add-answer-store', 'ManageChapterItemsController@addAnswerStore');
    Route::get('/course/{course_id}/question/{question_id}/delete-answer/{answer_id}', 'ManageChapterItemsController@deleteAnswer');
    Route::post('/course/{course}/edit-question-update/{question_id}', 'ManageChapterItemsController@editQuestionUpdate');

});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::post('users/ckmedia', 'UsersController@storeCKEditorImages')->name('users.storeCKEditorImages');
    Route::resource('users', 'UsersController');

    // Course Elements
    Route::delete('course-elements/destroy', 'CourseElementsController@massDestroy')->name('course-elements.massDestroy');
    Route::post('course-elements/media', 'CourseElementsController@storeMedia')->name('course-elements.storeMedia');
    Route::post('course-elements/ckmedia', 'CourseElementsController@storeCKEditorImages')->name('course-elements.storeCKEditorImages');
    Route::resource('course-elements', 'CourseElementsController');

    // Course Category
    Route::delete('course-categories/destroy', 'CourseCategoryController@massDestroy')->name('course-categories.massDestroy');
    Route::resource('course-categories', 'CourseCategoryController');

    // Course Class
    Route::delete('course-classes/destroy', 'CourseClassController@massDestroy')->name('course-classes.massDestroy');
    Route::resource('course-classes', 'CourseClassController');

    // Game Background
    Route::delete('game-backgrounds/destroy', 'GameBackgroundController@massDestroy')->name('game-backgrounds.massDestroy');
    Route::post('game-backgrounds/media', 'GameBackgroundController@storeMedia')->name('game-backgrounds.storeMedia');
    Route::post('game-backgrounds/ckmedia', 'GameBackgroundController@storeCKEditorImages')->name('game-backgrounds.storeCKEditorImages');
    Route::resource('game-backgrounds', 'GameBackgroundController');

    // Station
    Route::delete('stations/destroy', 'StationController@massDestroy')->name('stations.massDestroy');
    Route::post('stations/media', 'StationController@storeMedia')->name('stations.storeMedia');
    Route::post('stations/ckmedia', 'StationController@storeCKEditorImages')->name('stations.storeCKEditorImages');
    Route::resource('stations', 'StationController');

    // Sign
    Route::delete('signs/destroy', 'SignController@massDestroy')->name('signs.massDestroy');
    Route::post('signs/media', 'SignController@storeMedia')->name('signs.storeMedia');
    Route::post('signs/ckmedia', 'SignController@storeCKEditorImages')->name('signs.storeCKEditorImages');
    Route::resource('signs', 'SignController');

    // Color
    Route::delete('colors/destroy', 'ColorController@massDestroy')->name('colors.massDestroy');
    Route::resource('colors', 'ColorController');

    // Course
    Route::delete('courses/destroy', 'CourseController@massDestroy')->name('courses.massDestroy');
    Route::post('courses/media', 'CourseController@storeMedia')->name('courses.storeMedia');
    Route::post('courses/ckmedia', 'CourseController@storeCKEditorImages')->name('courses.storeCKEditorImages');
    Route::resource('courses', 'CourseController');

    // Course Chapter
    Route::delete('course-chapters/destroy', 'CourseChapterController@massDestroy')->name('course-chapters.massDestroy');
    Route::resource('course-chapters', 'CourseChapterController');

    // Chapter Item
    Route::delete('chapter-items/destroy', 'ChapterItemController@massDestroy')->name('chapter-items.massDestroy');
    Route::resource('chapter-items', 'ChapterItemController');

    // Video
    Route::delete('videos/destroy', 'VideoController@massDestroy')->name('videos.massDestroy');
    Route::post('videos/media', 'VideoController@storeMedia')->name('videos.storeMedia');
    Route::post('videos/ckmedia', 'VideoController@storeCKEditorImages')->name('videos.storeCKEditorImages');
    Route::resource('videos', 'VideoController');

    // File
    Route::delete('files/destroy', 'FileController@massDestroy')->name('files.massDestroy');
    Route::post('files/media', 'FileController@storeMedia')->name('files.storeMedia');
    Route::post('files/ckmedia', 'FileController@storeCKEditorImages')->name('files.storeCKEditorImages');
    Route::resource('files', 'FileController');

    // Page
    Route::delete('pages/destroy', 'PageController@massDestroy')->name('pages.massDestroy');
    Route::post('pages/media', 'PageController@storeMedia')->name('pages.storeMedia');
    Route::post('pages/ckmedia', 'PageController@storeCKEditorImages')->name('pages.storeCKEditorImages');
    Route::resource('pages', 'PageController');

    // Test
    Route::delete('tests/destroy', 'TestController@massDestroy')->name('tests.massDestroy');
    Route::post('tests/media', 'TestController@storeMedia')->name('tests.storeMedia');
    Route::post('tests/ckmedia', 'TestController@storeCKEditorImages')->name('tests.storeCKEditorImages');
    Route::resource('tests', 'TestController');

    // Question
    Route::delete('questions/destroy', 'QuestionController@massDestroy')->name('questions.massDestroy');
    Route::post('questions/media', 'QuestionController@storeMedia')->name('questions.storeMedia');
    Route::post('questions/ckmedia', 'QuestionController@storeCKEditorImages')->name('questions.storeCKEditorImages');
    Route::resource('questions', 'QuestionController');

    // Answer
    Route::delete('answers/destroy', 'AnswerController@massDestroy')->name('answers.massDestroy');
    Route::resource('answers', 'AnswerController');

    // News
    Route::delete('news/destroy', 'NewsController@massDestroy')->name('news.massDestroy');
    Route::post('news/media', 'NewsController@storeMedia')->name('news.storeMedia');
    Route::post('news/ckmedia', 'NewsController@storeCKEditorImages')->name('news.storeCKEditorImages');
    Route::resource('news', 'NewsController');

    // Popup News
    Route::delete('popup-news/destroy', 'PopupNewsController@massDestroy')->name('popup-news.massDestroy');
    Route::resource('popup-news', 'PopupNewsController');

    // Game Available Items
    Route::delete('game-available-items/destroy', 'GameAvailableItemsController@massDestroy')->name('game-available-items.massDestroy');
    Route::resource('game-available-items', 'GameAvailableItemsController');

    // Passed Test
    Route::delete('passed-tests/destroy', 'PassedTestController@massDestroy')->name('passed-tests.massDestroy');
    Route::resource('passed-tests', 'PassedTestController');

    // Test Sign Color
    Route::delete('test-sign-colors/destroy', 'TestSignColorController@massDestroy')->name('test-sign-colors.massDestroy');
    Route::resource('test-sign-colors', 'TestSignColorController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
