<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameBackgroundsTable extends Migration
{
    public function up()
    {
        Schema::create('game_backgrounds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('ritm_price');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
