<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTestSignColorsTable extends Migration
{
    public function up()
    {
        Schema::table('test_sign_colors', function (Blueprint $table) {
            $table->unsignedBigInteger('course_id')->nullable();
            $table->foreign('course_id', 'course_fk_5985691')->references('id')->on('courses');
            $table->unsignedBigInteger('test_id')->nullable();
            $table->foreign('test_id', 'test_fk_5985692')->references('id')->on('tests');
            $table->unsignedBigInteger('sign_id')->nullable();
            $table->foreign('sign_id', 'sign_fk_5985693')->references('id')->on('signs');
            $table->unsignedBigInteger('color_id')->nullable();
            $table->foreign('color_id', 'color_fk_5985694')->references('id')->on('colors');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_5985695')->references('id')->on('users');
        });
    }
}
