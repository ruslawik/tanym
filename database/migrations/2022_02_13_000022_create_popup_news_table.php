<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopupNewsTable extends Migration
{
    public function up()
    {
        Schema::create('popup_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('text')->nullable();
            $table->longText('text_kz')->nullable();
            $table->longText('text_en')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
