<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTestsTable extends Migration
{
    public function up()
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->unsignedBigInteger('chapter_item_id')->nullable();
            $table->foreign('chapter_item_id', 'chapter_item_fk_5981602')->references('id')->on('chapter_items');
        });
    }
}
