<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('video_code')->nullable();
            $table->longText('video_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
