<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCoursesTable extends Migration
{
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->unsignedBigInteger('author_id')->nullable();
            $table->foreign('author_id', 'author_fk_5981381')->references('id')->on('users');
            $table->unsignedBigInteger('class_id')->nullable();
            $table->foreign('class_id', 'class_fk_5981384')->references('id')->on('course_classes');
        });
    }
}
