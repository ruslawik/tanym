<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPassedTestsTable extends Migration
{
    public function up()
    {
        Schema::table('passed_tests', function (Blueprint $table) {
            $table->unsignedBigInteger('test_id')->nullable();
            $table->foreign('test_id', 'test_fk_5985673')->references('id')->on('tests');
            $table->unsignedBigInteger('question_id')->nullable();
            $table->foreign('question_id', 'question_fk_5985674')->references('id')->on('questions');
            $table->unsignedBigInteger('answer_id')->nullable();
            $table->foreign('answer_id', 'answer_fk_5985675')->references('id')->on('answers');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_5985676')->references('id')->on('users');
        });
    }
}
