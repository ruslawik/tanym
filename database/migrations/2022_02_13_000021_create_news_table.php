<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('name_kz')->nullable();
            $table->string('name_en')->nullable();
            $table->longText('text')->nullable();
            $table->longText('text_kz')->nullable();
            $table->longText('text_en')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
