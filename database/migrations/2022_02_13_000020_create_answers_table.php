<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('answer')->nullable();
            $table->longText('answer_kz')->nullable();
            $table->longText('answer_en')->nullable();
            $table->integer('is_right')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
