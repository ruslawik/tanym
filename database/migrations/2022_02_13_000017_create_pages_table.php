<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('page_name')->nullable();
            $table->longText('page_name_kz')->nullable();
            $table->longText('page_name_en')->nullable();
            $table->longText('content')->nullable();
            $table->longText('content_kz')->nullable();
            $table->longText('content_en')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
