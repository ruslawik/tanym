<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToChapterItemsTable extends Migration
{
    public function up()
    {
        Schema::table('chapter_items', function (Blueprint $table) {
            $table->unsignedBigInteger('element_id')->nullable();
            $table->foreign('element_id', 'element_fk_5981447')->references('id')->on('course_elements');
        });
    }
}
