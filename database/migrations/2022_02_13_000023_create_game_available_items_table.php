<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameAvailableItemsTable extends Migration
{
    public function up()
    {
        Schema::create('game_available_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_type')->nullable();
            $table->string('element')->nullable();
            $table->integer('installed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
