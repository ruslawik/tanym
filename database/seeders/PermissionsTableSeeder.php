<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'catalog_access',
            ],
            [
                'id'    => 18,
                'title' => 'course_element_create',
            ],
            [
                'id'    => 19,
                'title' => 'course_element_edit',
            ],
            [
                'id'    => 20,
                'title' => 'course_element_show',
            ],
            [
                'id'    => 21,
                'title' => 'course_element_delete',
            ],
            [
                'id'    => 22,
                'title' => 'course_element_access',
            ],
            [
                'id'    => 23,
                'title' => 'course_category_create',
            ],
            [
                'id'    => 24,
                'title' => 'course_category_edit',
            ],
            [
                'id'    => 25,
                'title' => 'course_category_show',
            ],
            [
                'id'    => 26,
                'title' => 'course_category_delete',
            ],
            [
                'id'    => 27,
                'title' => 'course_category_access',
            ],
            [
                'id'    => 28,
                'title' => 'course_class_create',
            ],
            [
                'id'    => 29,
                'title' => 'course_class_edit',
            ],
            [
                'id'    => 30,
                'title' => 'course_class_show',
            ],
            [
                'id'    => 31,
                'title' => 'course_class_delete',
            ],
            [
                'id'    => 32,
                'title' => 'course_class_access',
            ],
            [
                'id'    => 33,
                'title' => 'game_background_create',
            ],
            [
                'id'    => 34,
                'title' => 'game_background_edit',
            ],
            [
                'id'    => 35,
                'title' => 'game_background_show',
            ],
            [
                'id'    => 36,
                'title' => 'game_background_delete',
            ],
            [
                'id'    => 37,
                'title' => 'game_background_access',
            ],
            [
                'id'    => 38,
                'title' => 'station_create',
            ],
            [
                'id'    => 39,
                'title' => 'station_edit',
            ],
            [
                'id'    => 40,
                'title' => 'station_show',
            ],
            [
                'id'    => 41,
                'title' => 'station_delete',
            ],
            [
                'id'    => 42,
                'title' => 'station_access',
            ],
            [
                'id'    => 43,
                'title' => 'sign_create',
            ],
            [
                'id'    => 44,
                'title' => 'sign_edit',
            ],
            [
                'id'    => 45,
                'title' => 'sign_show',
            ],
            [
                'id'    => 46,
                'title' => 'sign_delete',
            ],
            [
                'id'    => 47,
                'title' => 'sign_access',
            ],
            [
                'id'    => 48,
                'title' => 'color_create',
            ],
            [
                'id'    => 49,
                'title' => 'color_edit',
            ],
            [
                'id'    => 50,
                'title' => 'color_show',
            ],
            [
                'id'    => 51,
                'title' => 'color_delete',
            ],
            [
                'id'    => 52,
                'title' => 'color_access',
            ],
            [
                'id'    => 53,
                'title' => 'course_management_access',
            ],
            [
                'id'    => 54,
                'title' => 'course_create',
            ],
            [
                'id'    => 55,
                'title' => 'course_edit',
            ],
            [
                'id'    => 56,
                'title' => 'course_show',
            ],
            [
                'id'    => 57,
                'title' => 'course_delete',
            ],
            [
                'id'    => 58,
                'title' => 'course_access',
            ],
            [
                'id'    => 59,
                'title' => 'course_chapter_create',
            ],
            [
                'id'    => 60,
                'title' => 'course_chapter_edit',
            ],
            [
                'id'    => 61,
                'title' => 'course_chapter_show',
            ],
            [
                'id'    => 62,
                'title' => 'course_chapter_delete',
            ],
            [
                'id'    => 63,
                'title' => 'course_chapter_access',
            ],
            [
                'id'    => 64,
                'title' => 'chapter_item_create',
            ],
            [
                'id'    => 65,
                'title' => 'chapter_item_edit',
            ],
            [
                'id'    => 66,
                'title' => 'chapter_item_show',
            ],
            [
                'id'    => 67,
                'title' => 'chapter_item_delete',
            ],
            [
                'id'    => 68,
                'title' => 'chapter_item_access',
            ],
            [
                'id'    => 69,
                'title' => 'video_create',
            ],
            [
                'id'    => 70,
                'title' => 'video_edit',
            ],
            [
                'id'    => 71,
                'title' => 'video_show',
            ],
            [
                'id'    => 72,
                'title' => 'video_delete',
            ],
            [
                'id'    => 73,
                'title' => 'video_access',
            ],
            [
                'id'    => 74,
                'title' => 'file_create',
            ],
            [
                'id'    => 75,
                'title' => 'file_edit',
            ],
            [
                'id'    => 76,
                'title' => 'file_show',
            ],
            [
                'id'    => 77,
                'title' => 'file_delete',
            ],
            [
                'id'    => 78,
                'title' => 'file_access',
            ],
            [
                'id'    => 79,
                'title' => 'page_create',
            ],
            [
                'id'    => 80,
                'title' => 'page_edit',
            ],
            [
                'id'    => 81,
                'title' => 'page_show',
            ],
            [
                'id'    => 82,
                'title' => 'page_delete',
            ],
            [
                'id'    => 83,
                'title' => 'page_access',
            ],
            [
                'id'    => 84,
                'title' => 'test_create',
            ],
            [
                'id'    => 85,
                'title' => 'test_edit',
            ],
            [
                'id'    => 86,
                'title' => 'test_show',
            ],
            [
                'id'    => 87,
                'title' => 'test_delete',
            ],
            [
                'id'    => 88,
                'title' => 'test_access',
            ],
            [
                'id'    => 89,
                'title' => 'question_create',
            ],
            [
                'id'    => 90,
                'title' => 'question_edit',
            ],
            [
                'id'    => 91,
                'title' => 'question_show',
            ],
            [
                'id'    => 92,
                'title' => 'question_delete',
            ],
            [
                'id'    => 93,
                'title' => 'question_access',
            ],
            [
                'id'    => 94,
                'title' => 'answer_create',
            ],
            [
                'id'    => 95,
                'title' => 'answer_edit',
            ],
            [
                'id'    => 96,
                'title' => 'answer_show',
            ],
            [
                'id'    => 97,
                'title' => 'answer_delete',
            ],
            [
                'id'    => 98,
                'title' => 'answer_access',
            ],
            [
                'id'    => 99,
                'title' => 'gamification_management_access',
            ],
            [
                'id'    => 100,
                'title' => 'news_create',
            ],
            [
                'id'    => 101,
                'title' => 'news_edit',
            ],
            [
                'id'    => 102,
                'title' => 'news_show',
            ],
            [
                'id'    => 103,
                'title' => 'news_delete',
            ],
            [
                'id'    => 104,
                'title' => 'news_access',
            ],
            [
                'id'    => 105,
                'title' => 'popup_news_create',
            ],
            [
                'id'    => 106,
                'title' => 'popup_news_edit',
            ],
            [
                'id'    => 107,
                'title' => 'popup_news_show',
            ],
            [
                'id'    => 108,
                'title' => 'popup_news_delete',
            ],
            [
                'id'    => 109,
                'title' => 'popup_news_access',
            ],
            [
                'id'    => 110,
                'title' => 'game_available_item_create',
            ],
            [
                'id'    => 111,
                'title' => 'game_available_item_edit',
            ],
            [
                'id'    => 112,
                'title' => 'game_available_item_show',
            ],
            [
                'id'    => 113,
                'title' => 'game_available_item_delete',
            ],
            [
                'id'    => 114,
                'title' => 'game_available_item_access',
            ],
            [
                'id'    => 115,
                'title' => 'passed_test_create',
            ],
            [
                'id'    => 116,
                'title' => 'passed_test_edit',
            ],
            [
                'id'    => 117,
                'title' => 'passed_test_show',
            ],
            [
                'id'    => 118,
                'title' => 'passed_test_delete',
            ],
            [
                'id'    => 119,
                'title' => 'passed_test_access',
            ],
            [
                'id'    => 120,
                'title' => 'test_sign_color_create',
            ],
            [
                'id'    => 121,
                'title' => 'test_sign_color_edit',
            ],
            [
                'id'    => 122,
                'title' => 'test_sign_color_show',
            ],
            [
                'id'    => 123,
                'title' => 'test_sign_color_delete',
            ],
            [
                'id'    => 124,
                'title' => 'test_sign_color_access',
            ],
            [
                'id'    => 125,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
