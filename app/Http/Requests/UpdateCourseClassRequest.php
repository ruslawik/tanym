<?php

namespace App\Http\Requests;

use App\Models\CourseClass;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateCourseClassRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('course_class_edit');
    }

    public function rules()
    {
        return [
            'class' => [
                'string',
                'required',
            ],
        ];
    }
}
