<?php

namespace App\Http\Requests;

use App\Models\GameBackground;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateGameBackgroundRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('game_background_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'image' => [
                'required',
            ],
            'ritm_price' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
