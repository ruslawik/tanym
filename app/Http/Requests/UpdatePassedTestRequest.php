<?php

namespace App\Http\Requests;

use App\Models\PassedTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePassedTestRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('passed_test_edit');
    }

    public function rules()
    {
        return [];
    }
}
