<?php

namespace App\Http\Requests;

use App\Models\CourseChapter;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCourseChapterRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('course_chapter_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
            'name_kz' => [
                'string',
                'nullable',
            ],
            'name_en' => [
                'string',
                'nullable',
            ],
        ];
    }
}
