<?php

namespace App\Http\Requests;

use App\Models\TestSignColor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreTestSignColorRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('test_sign_color_create');
    }

    public function rules()
    {
        return [];
    }
}
