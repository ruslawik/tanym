<?php

namespace App\Http\Requests;

use App\Models\GameAvailableItem;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateGameAvailableItemRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('game_available_item_edit');
    }

    public function rules()
    {
        return [
            'item_type' => [
                'string',
                'nullable',
            ],
            'element' => [
                'string',
                'nullable',
            ],
            'installed' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
