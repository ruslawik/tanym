<?php

namespace App\Http\Requests;

use App\Models\TestSignColor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTestSignColorRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('test_sign_color_edit');
    }

    public function rules()
    {
        return [];
    }
}
