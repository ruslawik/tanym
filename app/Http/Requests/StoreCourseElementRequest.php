<?php

namespace App\Http\Requests;

use App\Models\CourseElement;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCourseElementRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('course_element_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'name_kz' => [
                'string',
                'nullable',
            ],
            'name_en' => [
                'string',
                'nullable',
            ],
            'icon' => [
                'required',
            ],
        ];
    }
}
