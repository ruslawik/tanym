<?php

namespace App\Http\Requests;

use App\Models\CourseElement;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCourseElementRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('course_element_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:course_elements,id',
        ];
    }
}
