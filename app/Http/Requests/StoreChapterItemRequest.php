<?php

namespace App\Http\Requests;

use App\Models\ChapterItem;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreChapterItemRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('chapter_item_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
            'name_kz' => [
                'string',
                'nullable',
            ],
            'name_en' => [
                'string',
                'nullable',
            ],
        ];
    }
}
