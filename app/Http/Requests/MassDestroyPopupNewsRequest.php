<?php

namespace App\Http\Requests;

use App\Models\PopupNews;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyPopupNewsRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('popup_news_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:popup_news,id',
        ];
    }
}
