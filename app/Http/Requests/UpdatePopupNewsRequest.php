<?php

namespace App\Http\Requests;

use App\Models\PopupNews;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePopupNewsRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('popup_news_edit');
    }

    public function rules()
    {
        return [];
    }
}
