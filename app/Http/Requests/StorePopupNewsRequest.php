<?php

namespace App\Http\Requests;

use App\Models\PopupNews;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePopupNewsRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('popup_news_create');
    }

    public function rules()
    {
        return [];
    }
}
