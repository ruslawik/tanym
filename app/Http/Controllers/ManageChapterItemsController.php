<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseClass;
use App\Models\CourseChapter;
use App\Models\ChapterItem;
use App\Models\CourseElement;
use App\Models\Video;
use App\Models\Page;
use App\Models\Test;
use App\Models\File;
use App\Models\Question;
use App\Models\Answer;
use Auth;

class ManageChapterItemsController extends Controller
{

	public function editQuestionUpdate($course_id, $question_id, Request $r){
		$this->check_rights($course_id);

		$question_text = $r->input('question_'.$question_id);
		$total_answers = $r->input('total_answers_'.$question_id);
		$right_answer_id = $r->input('question_right_answer_'.$question_id);

		Question::where('id', $question_id)
				->update([
					"question" => $question_text
				]);
		for($i=1; $i<$total_answers; $i++){
			$answer_id = $r->input("q_".$question_id."_answer_".$i);
			$answer_text = $r->input("answer_".$answer_id);
			if($right_answer_id == $answer_id){
				$is_right = 1;
			}else{
				$is_right = 0;
			}
			Answer::where("id", $answer_id)
					->update([
						"answer" => $answer_text,
						"is_right" => $is_right
					]);
		}

		$url = back()->getTargetUrl();
		$url_clean = explode("?", $url);
		$redirect_url = $url_clean[0]."?qid=".$question_id."#q".$question_id;
		return redirect($redirect_url);
	}

	public function deleteAnswer($course_id, $question_id, $answer_id){
		$this->check_rights($course_id);

		Answer::where('id', $answer_id)->delete();

		$url = back()->getTargetUrl();
		$url_clean = explode("?", $url);
		$redirect_url = $url_clean[0]."?qid=".$question_id."#q".$question_id;
		return redirect($redirect_url);
	}

	public function addAnswerStore($course_id, $question_id){
		$this->check_rights($course_id);

		Answer::create(["answer" => "", "question_id" => $question_id,  "is_right" => 0]);

		$url = back()->getTargetUrl();
		$url_clean = explode("?", $url);
		$redirect_url = $url_clean[0]."?qid=".$question_id."#q".$question_id;
		return redirect($redirect_url);
	}

	public function deleteQuestion($course_id, $question_id){
		$this->check_rights($course_id);

		Question::where('id', $question_id)->delete();
		Answer::where('question_id', $question_id)->delete();

		return back();
	}

	public function addQuestionStore($course_id, $test_id){
		$this->check_rights($course_id);

		$question_id = Question::insertGetId(["question" => "", "test_id" => $test_id]);

		$url = back()->getTargetUrl();
		$url_clean = explode("?", $url);
		$redirect_url = $url_clean[0]."?qid=".$question_id."#q".$question_id;
		return redirect($redirect_url);
	}

	public function editTest($course_id, $element_id){

		$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['test'] = Test::where('chapter_item_id', $element_id)->get();
    	$ar['chapter_item'] = ChapterItem::find($element_id);
    	$ar['questions'] = Question::where('test_id', $ar['test'][0]->id)->get();

		return view("front.manage_courses.items.edit_test", $ar);
	}

	public function editPage($course_id, $element_id){

		$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['page'] = Page::where('chapter_item_id', $element_id)->get();
    	$ar['chapter_item'] = ChapterItem::find($element_id);

    	return view('front.manage_courses.items.edit_page', $ar);
	}

	public function editPageUpdate($course_id, $chapter_item_id, Request $r){
		$this->check_rights($course_id);

		ChapterItem::where('id', $chapter_item_id)->update(["name" => $r->input('name')]);
		Page::where('id', $r->input('page_id'))->update(["content" => $r->input("content")]);

		return back()->with('message', 'Страница успешно изменена!');
	}

	public function editFiles($course_id, $element_id){

    	$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['files'] = File::where('chapter_item_id', $element_id)->get();
    	$ar['chapter_item'] = ChapterItem::find($element_id);

    	return view('front.manage_courses.items.edit_files', $ar);
    }

    public function addFileStore($course_id, $chapter_item_id, Request $r){
    	$this->check_rights($course_id);

    	$file = new File();
    	$file->file_name = $r->input('file_client_name');
    	$file->chapter_item_id = $chapter_item_id;
    	$file->save();

    	$file->addMediaFromRequest('uploaded_file')->toMediaCollection('uploaded_file');

    	return back();
    }

    public function deleteFile($course_id, $file_id){
    	$this->check_rights($course_id);

    	$file = File::find($file_id);
    	$file->uploaded_file->delete();
    	$file->delete();
    	$file->save();

    	return back();
    }

    public function editVideo($course_id, $element_id){

    	$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['video'] = Video::where('chapter_item_id', $element_id)->get();
    	$ar['chapter_item'] = ChapterItem::find($element_id);

    	return view('front.manage_courses.items.edit_video', $ar);
    }

    public function editVideoUpdate($course_id, $chapter_item_id, Request $r){
    	$this->check_rights($course_id);

    	$video_id = $r->input('video_id');

    	ChapterItem::where('id', $chapter_item_id)->update(["name" => $r->input('name')]);
    	Video::where('id', $video_id)
    		->update([
    			"video_code" => $r->input('video_code'),
    			"video_description" => $r->input('video_description')
    		]);

    	return back()->with('message', 'Информация о видеоуроке успешно обновлена!');
    }

    public function changeElemName($course_id, $element_id, Request $r){
    	$this->check_rights($course_id);

    	ChapterItem::where('id', $element_id)->update(["name" => $r->input('chapter_item_name')]);

        if($r->has('test_description')){
            Test::where('chapter_item_id', $element_id)->update(["description" => $r->input('test_description')]);
        }
        if($r->has('cert_id')){
            Test::where('chapter_item_id', $element_id)->update(["cert_id" => $r->input('cert_id')]);
        }
        if($r->has('bally_to_cert')){
            Test::where('chapter_item_id', $element_id)->update(["bally_to_cert" => $r->input('bally_to_cert')]);
        }

    	return back();
    }

    public function editElement($course_id, $element_id){
    	$this->check_rights($course_id);

    	$element = ChapterItem::find($element_id);

    	switch ($element->element_id) {
    		case '1':
    			return $this->editVideo($course_id, $element_id);
    			break;
    		case '2':
    			return $this->editFiles($course_id, $element_id);
    			break;
    		case '3':
    			return $this->editPage($course_id, $element_id);
    			break;
    		case '4':
    			return $this->editTest($course_id, $element_id);
    			break;
    		default:
    			return redirect('/mycourses');
    			break;
    	}
    }

    public function deleteElement($course_id, $element_id){
    	$this->check_rights($course_id);

    	ChapterItem::where('id', $element_id)->delete();

    	return redirect('/edit-course/'.$course_id);
    }

    public function addElement($course_id, $chapter_id){

    	$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
    	$ar['item_types'] = CourseElement::all();

    	return view('front.manage_courses.items.add_element', $ar);
    }

    public function addElementStore($course_id, $chapter_id, Request $r){
    	$this->check_rights($course_id);

    	$element = new ChapterItem();
    	$element->name = $r->input('name');
    	$element->element_id = $r->input("radios-addelem__radio");
    	$element->course_chapter_id = $chapter_id;
    	$element->save();

    	//создать соответствующий элемент видео, тест, страница и перевести на страницу его редактирования
    	if($element->element_id == 1){
    		$video_id = Video::insertGetId(["video_code" => "", "video_description" => "", "chapter_item_id" => $element->id]);
    	}
    	if($element->element_id == 3){
    		$page_id = Page::insertGetId(["page_name" => "", "content" => "", "chapter_item_id" => $element->id]);
    	}
    	if($element->element_id == 4){
    		$test_id = Test::insertGetId(["name" => "", "description" => "", "chapter_item_id" => $element->id]);
    	}
    	return redirect('/course/'.$course_id.'/edit-element/'.$element->id);
    }

    public function check_rights($course_id){

    	$ar['course'] = Course::where('id', $course_id)
    								->where('author_id', Auth::user()->id)->get();
    	//Если пытается открыть не свой курс
    	if(count($ar['course']) <= 0){
    		return redirect('/mycourses');
    	}
    }

    public function uploadCkeditorMedia(Request $request){
    	if($request->hasFile('upload')) {
	        //get filename with extension
	        $filenamewithextension = $request->file('upload')->getClientOriginalName();
	   
	        //get filename without extension
	        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	   
	        //get file extension
	        $extension = $request->file('upload')->getClientOriginalExtension();
	   
	        //filename to store
	        $filenametostore = $filename.'_'.time().'.'.$extension;
	   		
	        //Upload File
	        $request->file('upload')->storeAs('public/ckeditor-uploads', $filenametostore);
	 
	        $CKEditorFuncNum = $request->input('CKEditorFuncNum');
	        $url = asset('storage/ckeditor-uploads/'.$filenametostore); 
	        $msg = 'Успешно загружено!'; 
	        $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
	          
	        // Render HTML output 
	        @header('Content-type: text/html; charset=utf-8'); 
	        echo $re;
    	}
    }
}
