<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseClass;
use App\Models\ChapterItem;
use App\Models\Video;
use App\Models\Page;
use App\Models\Test;
use App\Models\File;
use App\Models\Question;
use App\Models\Answer;
use App\Models\CourseChapter;
use Auth;
use App\Models\PassedTest;

class CourseController extends Controller
{
    public function course($course_id){

    	$ar['course'] = Course::where('draft', 0)->find($course_id);

    	
    	return view("front.course.course_index", $ar);
    }

    public function showTest($course_id, $chapter_id, $element_id){

        if(Auth::check()){
            $ar['auth'] = 1;
            $test = Test::where('chapter_item_id', $element_id)->get();
            $is_passed = PassedTest::where('test_id', $test[0]->id)->where('user_id', Auth::user()->id)->count();
            if($is_passed > 0){
                return redirect('/course/'.$course_id.'/chapter/'.$chapter_id.'/element/'.$element_id.
        '/test/'.$test[0]->id);
            }
        }

    	$ar['course'] = Course::where('draft', 0)->find($course_id);
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
    	$ar['element'] = ChapterItem::where('id', $element_id)->get();
    	$ar['test'] = Test::where('chapter_item_id', $element_id)->get();

    	return view("front.course.test", $ar);
    }

    public function showPage($course_id, $chapter_id, $element_id){

    	$ar['course'] = Course::where('draft', 0)->find($course_id);
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
    	$ar['element'] = ChapterItem::where('id', $element_id)->get();
    	$ar['page'] = Page::where('chapter_item_id', $element_id)->get();

    	return view("front.course.page", $ar);
    }

    public function showVideo($course_id, $chapter_id, $element_id){

    	$ar['course'] = Course::where('draft', 0)->find($course_id);
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
    	$ar['element'] = ChapterItem::where('id', $element_id)->get();
    	$ar['video'] = Video::where('chapter_item_id', $element_id)->get();

    	return view("front.course.video", $ar);
    }

    public function showFiles($course_id, $chapter_id, $element_id){

    	$ar['course'] = Course::where('draft', 0)->find($course_id);
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
    	$ar['element'] = ChapterItem::where('id', $element_id)->get();
    	$ar['files'] = File::where('chapter_item_id', $element_id)->get();

    	return view("front.course.files", $ar);
    }

    public function elementRouter($course_id, $chapter_id, $element_id){

    	$element = ChapterItem::find($element_id);

    	switch ($element->element_id) {
    		case '1':
    			return $this->showVideo($course_id, $chapter_id, $element_id);
    			break;
    		case '2':
    			return $this->showFiles($course_id, $chapter_id, $element_id);
    			break;
    		case '3':
    			return $this->showPage($course_id, $chapter_id, $element_id);
    			break;
    		case '4':
    			return $this->showTest($course_id, $chapter_id, $element_id);
    			break;
    		default:
    			return redirect('/mycourses');
    			break;
    	}
    }
}
