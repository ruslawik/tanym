<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\GameAvailableItem;
use App\Models\GameSelectedStation;
use Illuminate\Support\Facades\Hash;
use Auth;
use Mail;

class CabinetController extends Controller
{

    public function sendNewPass(Request $request){

        $new_pass = "tanym".rand(10000, 99999);
        $new_hash = Hash::make($new_pass);

        $user_data = User::where('email', $request->input('email'))->get();
        if(count($user_data) > 0)
        {
            User::where('email', $request->input('email'))->update(["password" => $new_hash]);

            $message_text = "Ваш новый пароль для tanym.caravanofknowledge.com: <b>".$new_pass."</b>";

            Mail::send(array(), array(), function ($message) use ($message_text, $request) {
                    $message->to($request->email)
                        ->subject("Успешная смена пароля на Tanym!")
                        ->setBody($message_text, 'text/html');
                });

            return back()->with(["message" =>  "Пароль успешно выслан Вам на указанную почту!"]);
        }else{
            return back()->with(["message" =>  "Такой аккаунт не найден"]);
        }
    }

    public function index(){

    	return view('front.cabinet');
    }

    public function gameOnOff ($on_off){

    	if($on_off == 1){
    		User::where("id", Auth::user()->id)->update(["game_on" => 1]);
    	}else{
			User::where("id", Auth::user()->id)->update(["game_on" => 0]);
    	}
    	return back()->with('message', "Настройки геймификации изменены");
    }

    public function updateUserInfo (Request $request){

    	$user = User::where('id', Auth::user()->id)->get();
    	$user = $user[0];

        if(Auth::user()->roles->contains(3)){
            $user->regalia = $request->input('regalia');
            $user->instructor_description = $request->input('instructor_description');
        }

        if ($request->file('avatar') != null) {
        	if($user->avatar != null){
            	$user->avatar->delete();
            }
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatar');
        }
        $user->name = $request->input('name');
        $user->save();

        if(strlen($request->input('password')) >= 1){
        	$new_pass = Hash::make($request->input('password'));
        	User::where('id', Auth::user()->id)->update(["password" => $new_pass]);
        }

        return back()->with('message', "Информация профиля изменена");
    }
}
