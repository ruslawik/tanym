<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    public function index(){

    	$ar['posts'] = News::paginate(6);

    	return view("front.news", $ar);
    }

    public function post($post_id){

    	$ar['post'] = News::where('id', $post_id)->get();

    	return view("front.post", $ar);
    }
}
