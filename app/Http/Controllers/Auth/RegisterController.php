<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\GameSelectedStation;
use App\Models\GameAvailableItem;
use File;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'game_on'  => 1,
            'ritms' => 100,
            'total_got_ritms' => 100,
            'password' => Hash::make($data['password']),
        ]);
        $stations = [
            [
                "user_id" => $user->id,
                "station_id" => 1,
                "course_category_id" => 1 
            ],
            [
                "user_id" => $user->id,
                "station_id" => 2,
                "course_category_id" => 2 
            ],
            [
                "user_id" => $user->id,
                "station_id" => 1,
                "course_category_id" => 3 
            ],
            [
                "user_id" => $user->id,
                "station_id" => 2,
                "course_category_id" => 4 
            ]
        ];

        $user_available_items = [
            [
                "item_type" => "station",
                "element" => 1,
                "installed" => 1,
                "user_id" => $user->id
            ],
            [
                "item_type" => "station",
                "element" => 2,
                "installed" => 1,
                "user_id" => $user->id
            ],
            [
                "item_type" => "station",
                "element" => 1,
                "installed" => 1,
                "user_id" => $user->id
            ],
            [
                "item_type" => "station",
                "element" => 2,
                "installed" => 1,
                "user_id" => $user->id
            ],
        ];
        GameAvailableItem::insert($user_available_items);
        GameSelectedStation::insert($stations);

        if(isset($_POST['ref'])){
            $ref_id = $_POST['ref'];
            $user_data = User::where('id', $ref_id)->get();
            User::where('id', $ref_id)->update([
                "ritms" => $user_data[0]->ritms + 20
            ]);
        }

        File::copy(public_path().'/front/img/user_icon.png', public_path().'/front/img/user_icon2.png');
        $user->addMedia(public_path().'/front/img/user_icon2.png')->toMediaCollection('avatar');

        return $user;
    }
}
