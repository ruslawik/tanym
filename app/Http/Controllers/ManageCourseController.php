<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseClass;
use App\Models\CourseChapter;
use App\Models\ChapterItem;
use Auth;

class ManageCourseController extends Controller
{
    public function index(){

    	$ar['my_courses'] = Course::where('author_id', Auth::user()->id)->get();

    	return view("front.manage_courses.index", $ar);
    }

    public function chapterOrderEditUpdate($course_id, Request $r){

        $chapters = CourseChapter::where('course_id', $course_id)->get();

        foreach ($chapters as $chapter) {
            $chapter_order_value = $r->input("chapter_".$chapter->id);
            CourseChapter::where('id', $chapter->id)->update(["chapter_order" => $chapter_order_value]);
        }

        return back();
    }

    public function chapterOrderEdit($course_id){
        $this->check_rights($course_id);

        $ar['course'] = Course::where('id', $course_id)
                                    ->where('author_id', Auth::user()->id)->get();
        $ar['chapters'] = CourseChapter::where('course_id', $course_id)->get();

        return view("front.manage_courses.chapters_order_edit", $ar);
    }

    public function chapterElementsOrderUpdate($course_id, $chapter_id, Request $r){

        $items = ChapterItem::where('course_chapter_id', $chapter_id)->get();

        foreach ($items as $item) {
            $chapter_item_order_value = $r->input('item_'.$item->id);
            ChapterItem::where('id', $item->id)->update(["item_order" => $chapter_item_order_value]);
        }

        return back();
    }

    public function deleteChapter($course_id, $chapter_id){
        $this->check_rights($course_id);

        CourseChapter::where('id', $chapter_id)->delete();

        return redirect("/edit-course/".$course_id);
    }

    public function editChapterUpdate(Request $r){
    	$this->check_rights($r->input('course_id'));

    	$chapter = CourseChapter::find($r->input('chapter_id'));
    	$chapter->name = $r->input('name');
    	$chapter->save();

    	return back();
    }

    public function editChapter($course_id, $chapter_id){
    	$this->check_rights($course_id);

    	$ar['course'] = Course::where('id', $course_id)->get();
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
        $ar['chapter_items'] = ChapterItem::where('course_chapter_id', $chapter_id)->orderBy('item_order')->get();

    	return view("front.manage_courses.edit_chapter", $ar);
    }

    public function addChapter($course_id){
    	$this->check_rights($course_id);

    	$ar['course'] = Course::where('id', $course_id)
    								->where('author_id', Auth::user()->id)->get();

    	return view("front.manage_courses.add_chapter", $ar);
    }

    public function addChapterStore(Request $r){

    	$chapter = new CourseChapter();
    	$chapter->name = $r->input('name');
    	$chapter->course_id = $r->input('course_id');
    	$chapter->save();

    	return back();
    }

    public function editCourse($course_id){

    	$ar['course'] = Course::where('id', $course_id)
    								->where('author_id', Auth::user()->id)->get();
    	//Если пытается открыть не свой курс
    	if(count($ar['course']) <= 0){
    		return redirect('/mycourses');
    	}
    	$ar['categories'] = CourseCategory::all();
    	$ar['classes'] = CourseClass::all();

    	return view("front.manage_courses.edit_course", $ar);
    }

    public function editCourseUpdate(Request $r){

    	$course = Course::find($r->input('course_id'));
    	$course->name = $r->input('name');
    	$course->class_id = $r->input('class');
    	$course->author_id = Auth::user()->id;
    	$course->diffiulty = $r->input('diffiulty');
        $course->description = $r->input('description');
    	if($r->has('draft')){
    		$course->draft = 1;
    	}else{
    		$course->draft = 0;
    	}
    	$course->save();

    	$course->categories()->sync($r->input('category', []));

    	if($r->hasFile('image')){
    		if ($course->image) {
                    $course->image->delete();
            }
        	$course->addMediaFromRequest('image')->toMediaCollection('image');
        	$course->save();
        }

    	return back()->with('message', 'Информация о курсе успешно обновлена!');
    }

    public function addCourse(){

    	$ar['categories'] = CourseCategory::all();
    	$ar['classes'] = CourseClass::all();

    	return view("front.manage_courses.add_course", $ar);
    }

    public function addCourseStore(Request $r){

    	$course = new Course();
    	$course->name = $r->input('name');
    	$course->class_id = $r->input('class');
    	$course->author_id = Auth::user()->id;
    	$course->diffiulty = $r->input('diffiulty');
    	if($r->has('draft')){
    		$course->draft = 1;
    	}else{
    		$course->draft = 0;
    	}
    	$course->save();

    	$course->categories()->sync($r->input('category', []));

        $course->addMediaFromRequest('image')->toMediaCollection('image');
        $course->save();

    	return redirect("/mycourses");
    }

    public function check_rights($course_id){

    	$ar['course'] = Course::where('id', $course_id)
    								->where('author_id', Auth::user()->id)->get();
    	//Если пытается открыть не свой курс
    	if(count($ar['course']) <= 0){
    		return redirect('/mycourses');
    	}
    }
}
