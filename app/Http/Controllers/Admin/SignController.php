<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySignRequest;
use App\Http\Requests\StoreSignRequest;
use App\Http\Requests\UpdateSignRequest;
use App\Models\Sign;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class SignController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('sign_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $signs = Sign::with(['media'])->get();

        return view('admin.signs.index', compact('signs'));
    }

    public function create()
    {
        abort_if(Gate::denies('sign_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.signs.create');
    }

    public function store(StoreSignRequest $request)
    {
        $sign = Sign::create($request->all());

        if ($request->input('image', false)) {
            $sign->addMedia(storage_path('tmp/uploads/' . basename($request->input('image'))))->toMediaCollection('image');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $sign->id]);
        }

        return redirect()->route('admin.signs.index');
    }

    public function edit(Sign $sign)
    {
        abort_if(Gate::denies('sign_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.signs.edit', compact('sign'));
    }

    public function update(UpdateSignRequest $request, Sign $sign)
    {
        $sign->update($request->all());

        if ($request->input('image', false)) {
            if (!$sign->image || $request->input('image') !== $sign->image->file_name) {
                if ($sign->image) {
                    $sign->image->delete();
                }
                $sign->addMedia(storage_path('tmp/uploads/' . basename($request->input('image'))))->toMediaCollection('image');
            }
        } elseif ($sign->image) {
            $sign->image->delete();
        }

        return redirect()->route('admin.signs.index');
    }

    public function show(Sign $sign)
    {
        abort_if(Gate::denies('sign_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.signs.show', compact('sign'));
    }

    public function destroy(Sign $sign)
    {
        abort_if(Gate::denies('sign_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sign->delete();

        return back();
    }

    public function massDestroy(MassDestroySignRequest $request)
    {
        Sign::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('sign_create') && Gate::denies('sign_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Sign();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
