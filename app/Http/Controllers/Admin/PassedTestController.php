<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPassedTestRequest;
use App\Http\Requests\StorePassedTestRequest;
use App\Http\Requests\UpdatePassedTestRequest;
use App\Models\Answer;
use App\Models\PassedTest;
use App\Models\Question;
use App\Models\Test;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PassedTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('passed_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $passedTests = PassedTest::with(['test', 'question', 'answer', 'user'])->get();

        return view('admin.passedTests.index', compact('passedTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('passed_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tests = Test::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $questions = Question::pluck('hint', 'id')->prepend(trans('global.pleaseSelect'), '');

        $answers = Answer::pluck('answer', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.passedTests.create', compact('answers', 'questions', 'tests', 'users'));
    }

    public function store(StorePassedTestRequest $request)
    {
        $passedTest = PassedTest::create($request->all());

        return redirect()->route('admin.passed-tests.index');
    }

    public function edit(PassedTest $passedTest)
    {
        abort_if(Gate::denies('passed_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tests = Test::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $questions = Question::pluck('hint', 'id')->prepend(trans('global.pleaseSelect'), '');

        $answers = Answer::pluck('answer', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $passedTest->load('test', 'question', 'answer', 'user');

        return view('admin.passedTests.edit', compact('answers', 'passedTest', 'questions', 'tests', 'users'));
    }

    public function update(UpdatePassedTestRequest $request, PassedTest $passedTest)
    {
        $passedTest->update($request->all());

        return redirect()->route('admin.passed-tests.index');
    }

    public function show(PassedTest $passedTest)
    {
        abort_if(Gate::denies('passed_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $passedTest->load('test', 'question', 'answer', 'user');

        return view('admin.passedTests.show', compact('passedTest'));
    }

    public function destroy(PassedTest $passedTest)
    {
        abort_if(Gate::denies('passed_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $passedTest->delete();

        return back();
    }

    public function massDestroy(MassDestroyPassedTestRequest $request)
    {
        PassedTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
