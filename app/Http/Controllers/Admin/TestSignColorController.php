<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTestSignColorRequest;
use App\Http\Requests\StoreTestSignColorRequest;
use App\Http\Requests\UpdateTestSignColorRequest;
use App\Models\Color;
use App\Models\Course;
use App\Models\Sign;
use App\Models\Test;
use App\Models\TestSignColor;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TestSignColorController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('test_sign_color_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $testSignColors = TestSignColor::with(['course', 'test', 'sign', 'color', 'user'])->get();

        return view('admin.testSignColors.index', compact('testSignColors'));
    }

    public function create()
    {
        abort_if(Gate::denies('test_sign_color_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courses = Course::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $tests = Test::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $signs = Sign::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $colors = Color::pluck('html_color', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.testSignColors.create', compact('colors', 'courses', 'signs', 'tests', 'users'));
    }

    public function store(StoreTestSignColorRequest $request)
    {
        $testSignColor = TestSignColor::create($request->all());

        return redirect()->route('admin.test-sign-colors.index');
    }

    public function edit(TestSignColor $testSignColor)
    {
        abort_if(Gate::denies('test_sign_color_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courses = Course::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $tests = Test::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $signs = Sign::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $colors = Color::pluck('html_color', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $testSignColor->load('course', 'test', 'sign', 'color', 'user');

        return view('admin.testSignColors.edit', compact('colors', 'courses', 'signs', 'testSignColor', 'tests', 'users'));
    }

    public function update(UpdateTestSignColorRequest $request, TestSignColor $testSignColor)
    {
        $testSignColor->update($request->all());

        return redirect()->route('admin.test-sign-colors.index');
    }

    public function show(TestSignColor $testSignColor)
    {
        abort_if(Gate::denies('test_sign_color_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $testSignColor->load('course', 'test', 'sign', 'color', 'user');

        return view('admin.testSignColors.show', compact('testSignColor'));
    }

    public function destroy(TestSignColor $testSignColor)
    {
        abort_if(Gate::denies('test_sign_color_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $testSignColor->delete();

        return back();
    }

    public function massDestroy(MassDestroyTestSignColorRequest $request)
    {
        TestSignColor::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
