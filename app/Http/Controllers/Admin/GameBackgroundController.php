<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyGameBackgroundRequest;
use App\Http\Requests\StoreGameBackgroundRequest;
use App\Http\Requests\UpdateGameBackgroundRequest;
use App\Models\GameBackground;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class GameBackgroundController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('game_background_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $gameBackgrounds = GameBackground::with(['media'])->get();

        return view('admin.gameBackgrounds.index', compact('gameBackgrounds'));
    }

    public function create()
    {
        abort_if(Gate::denies('game_background_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gameBackgrounds.create');
    }

    public function store(StoreGameBackgroundRequest $request)
    {
        $gameBackground = GameBackground::create($request->all());

        if ($request->input('image', false)) {
            $gameBackground->addMedia(storage_path('tmp/uploads/' . basename($request->input('image'))))->toMediaCollection('image');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $gameBackground->id]);
        }

        return redirect()->route('admin.game-backgrounds.index');
    }

    public function edit(GameBackground $gameBackground)
    {
        abort_if(Gate::denies('game_background_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gameBackgrounds.edit', compact('gameBackground'));
    }

    public function update(UpdateGameBackgroundRequest $request, GameBackground $gameBackground)
    {
        $gameBackground->update($request->all());

        if ($request->input('image', false)) {
            if (!$gameBackground->image || $request->input('image') !== $gameBackground->image->file_name) {
                if ($gameBackground->image) {
                    $gameBackground->image->delete();
                }
                $gameBackground->addMedia(storage_path('tmp/uploads/' . basename($request->input('image'))))->toMediaCollection('image');
            }
        } elseif ($gameBackground->image) {
            $gameBackground->image->delete();
        }

        return redirect()->route('admin.game-backgrounds.index');
    }

    public function show(GameBackground $gameBackground)
    {
        abort_if(Gate::denies('game_background_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.gameBackgrounds.show', compact('gameBackground'));
    }

    public function destroy(GameBackground $gameBackground)
    {
        abort_if(Gate::denies('game_background_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $gameBackground->delete();

        return back();
    }

    public function massDestroy(MassDestroyGameBackgroundRequest $request)
    {
        GameBackground::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('game_background_create') && Gate::denies('game_background_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new GameBackground();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
