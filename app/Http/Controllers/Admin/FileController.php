<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFileRequest;
use App\Http\Requests\StoreFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Models\ChapterItem;
use App\Models\File;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class FileController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('file_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = File::with(['chapter_item'])->select(sprintf('%s.*', (new File())->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'file_show';
                $editGate = 'file_edit';
                $deleteGate = 'file_delete';
                $crudRoutePart = 'files';

                return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->addColumn('chapter_item_name', function ($row) {
                return $row->chapter_item ? $row->chapter_item->name : '';
            });

            $table->editColumn('file_name', function ($row) {
                return $row->file_name ? $row->file_name : '';
            });
            $table->editColumn('uploaded_file', function ($row) {
                return $row->uploaded_file ? '<a href="' . $row->uploaded_file->getUrl() . '" target="_blank">' . trans('global.downloadFile') . '</a>' : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'chapter_item', 'uploaded_file']);

            return $table->make(true);
        }

        $chapter_items = ChapterItem::get();

        return view('admin.files.index', compact('chapter_items'));
    }

    public function create()
    {
        abort_if(Gate::denies('file_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapter_items = ChapterItem::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.files.create', compact('chapter_items'));
    }

    public function store(StoreFileRequest $request)
    {
        $file = File::create($request->all());

        if ($request->input('uploaded_file', false)) {
            $file->addMedia(storage_path('tmp/uploads/' . basename($request->input('uploaded_file'))))->toMediaCollection('uploaded_file');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $file->id]);
        }

        return redirect()->route('admin.files.index');
    }

    public function edit(File $file)
    {
        abort_if(Gate::denies('file_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapter_items = ChapterItem::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $file->load('chapter_item');

        return view('admin.files.edit', compact('chapter_items', 'file'));
    }

    public function update(UpdateFileRequest $request, File $file)
    {
        $file->update($request->all());

        if ($request->input('uploaded_file', false)) {
            if (!$file->uploaded_file || $request->input('uploaded_file') !== $file->uploaded_file->file_name) {
                if ($file->uploaded_file) {
                    $file->uploaded_file->delete();
                }
                $file->addMedia(storage_path('tmp/uploads/' . basename($request->input('uploaded_file'))))->toMediaCollection('uploaded_file');
            }
        } elseif ($file->uploaded_file) {
            $file->uploaded_file->delete();
        }

        return redirect()->route('admin.files.index');
    }

    public function show(File $file)
    {
        abort_if(Gate::denies('file_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $file->load('chapter_item');

        return view('admin.files.show', compact('file'));
    }

    public function destroy(File $file)
    {
        abort_if(Gate::denies('file_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $file->delete();

        return back();
    }

    public function massDestroy(MassDestroyFileRequest $request)
    {
        File::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('file_create') && Gate::denies('file_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new File();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
