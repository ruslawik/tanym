<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCourseClassRequest;
use App\Http\Requests\StoreCourseClassRequest;
use App\Http\Requests\UpdateCourseClassRequest;
use App\Models\CourseClass;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CourseClassController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('course_class_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courseClasses = CourseClass::all();

        return view('admin.courseClasses.index', compact('courseClasses'));
    }

    public function create()
    {
        abort_if(Gate::denies('course_class_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courseClasses.create');
    }

    public function store(StoreCourseClassRequest $request)
    {
        $courseClass = CourseClass::create($request->all());

        return redirect()->route('admin.course-classes.index');
    }

    public function edit(CourseClass $courseClass)
    {
        abort_if(Gate::denies('course_class_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courseClasses.edit', compact('courseClass'));
    }

    public function update(UpdateCourseClassRequest $request, CourseClass $courseClass)
    {
        $courseClass->update($request->all());

        return redirect()->route('admin.course-classes.index');
    }

    public function show(CourseClass $courseClass)
    {
        abort_if(Gate::denies('course_class_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courseClasses.show', compact('courseClass'));
    }

    public function destroy(CourseClass $courseClass)
    {
        abort_if(Gate::denies('course_class_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courseClass->delete();

        return back();
    }

    public function massDestroy(MassDestroyCourseClassRequest $request)
    {
        CourseClass::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
