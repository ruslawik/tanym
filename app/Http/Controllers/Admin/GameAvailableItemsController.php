<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyGameAvailableItemRequest;
use App\Http\Requests\StoreGameAvailableItemRequest;
use App\Http\Requests\UpdateGameAvailableItemRequest;
use App\Models\GameAvailableItem;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class GameAvailableItemsController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('game_available_item_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = GameAvailableItem::with(['user'])->select(sprintf('%s.*', (new GameAvailableItem())->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'game_available_item_show';
                $editGate = 'game_available_item_edit';
                $deleteGate = 'game_available_item_delete';
                $crudRoutePart = 'game-available-items';

                return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('item_type', function ($row) {
                return $row->item_type ? $row->item_type : '';
            });
            $table->editColumn('element', function ($row) {
                return $row->element ? $row->element : '';
            });
            $table->addColumn('user_email', function ($row) {
                return $row->user ? $row->user->email : '';
            });

            $table->editColumn('installed', function ($row) {
                return $row->installed ? $row->installed : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'user']);

            return $table->make(true);
        }

        return view('admin.gameAvailableItems.index');
    }

    public function create()
    {
        abort_if(Gate::denies('game_available_item_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.gameAvailableItems.create', compact('users'));
    }

    public function store(StoreGameAvailableItemRequest $request)
    {
        $gameAvailableItem = GameAvailableItem::create($request->all());

        return redirect()->route('admin.game-available-items.index');
    }

    public function edit(GameAvailableItem $gameAvailableItem)
    {
        abort_if(Gate::denies('game_available_item_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $gameAvailableItem->load('user');

        return view('admin.gameAvailableItems.edit', compact('gameAvailableItem', 'users'));
    }

    public function update(UpdateGameAvailableItemRequest $request, GameAvailableItem $gameAvailableItem)
    {
        $gameAvailableItem->update($request->all());

        return redirect()->route('admin.game-available-items.index');
    }

    public function show(GameAvailableItem $gameAvailableItem)
    {
        abort_if(Gate::denies('game_available_item_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $gameAvailableItem->load('user');

        return view('admin.gameAvailableItems.show', compact('gameAvailableItem'));
    }

    public function destroy(GameAvailableItem $gameAvailableItem)
    {
        abort_if(Gate::denies('game_available_item_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $gameAvailableItem->delete();

        return back();
    }

    public function massDestroy(MassDestroyGameAvailableItemRequest $request)
    {
        GameAvailableItem::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
