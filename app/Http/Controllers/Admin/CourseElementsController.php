<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCourseElementRequest;
use App\Http\Requests\StoreCourseElementRequest;
use App\Http\Requests\UpdateCourseElementRequest;
use App\Models\CourseElement;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class CourseElementsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('course_element_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courseElements = CourseElement::with(['media'])->get();

        return view('admin.courseElements.index', compact('courseElements'));
    }

    public function create()
    {
        abort_if(Gate::denies('course_element_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courseElements.create');
    }

    public function store(StoreCourseElementRequest $request)
    {
        $courseElement = CourseElement::create($request->all());

        if ($request->input('icon', false)) {
            $courseElement->addMedia(storage_path('tmp/uploads/' . basename($request->input('icon'))))->toMediaCollection('icon');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $courseElement->id]);
        }

        return redirect()->route('admin.course-elements.index');
    }

    public function edit(CourseElement $courseElement)
    {
        abort_if(Gate::denies('course_element_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courseElements.edit', compact('courseElement'));
    }

    public function update(UpdateCourseElementRequest $request, CourseElement $courseElement)
    {
        $courseElement->update($request->all());

        if ($request->input('icon', false)) {
            if (!$courseElement->icon || $request->input('icon') !== $courseElement->icon->file_name) {
                if ($courseElement->icon) {
                    $courseElement->icon->delete();
                }
                $courseElement->addMedia(storage_path('tmp/uploads/' . basename($request->input('icon'))))->toMediaCollection('icon');
            }
        } elseif ($courseElement->icon) {
            $courseElement->icon->delete();
        }

        return redirect()->route('admin.course-elements.index');
    }

    public function show(CourseElement $courseElement)
    {
        abort_if(Gate::denies('course_element_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.courseElements.show', compact('courseElement'));
    }

    public function destroy(CourseElement $courseElement)
    {
        abort_if(Gate::denies('course_element_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courseElement->delete();

        return back();
    }

    public function massDestroy(MassDestroyCourseElementRequest $request)
    {
        CourseElement::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('course_element_create') && Gate::denies('course_element_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new CourseElement();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
