<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyChapterItemRequest;
use App\Http\Requests\StoreChapterItemRequest;
use App\Http\Requests\UpdateChapterItemRequest;
use App\Models\ChapterItem;
use App\Models\CourseElement;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ChapterItemController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('chapter_item_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = ChapterItem::with(['element'])->select(sprintf('%s.*', (new ChapterItem())->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'chapter_item_show';
                $editGate = 'chapter_item_edit';
                $deleteGate = 'chapter_item_delete';
                $crudRoutePart = 'chapter-items';

                return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : '';
            });
            $table->editColumn('name_kz', function ($row) {
                return $row->name_kz ? $row->name_kz : '';
            });
            $table->editColumn('name_en', function ($row) {
                return $row->name_en ? $row->name_en : '';
            });
            $table->addColumn('element_name', function ($row) {
                return $row->element ? $row->element->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'element']);

            return $table->make(true);
        }

        return view('admin.chapterItems.index');
    }

    public function create()
    {
        abort_if(Gate::denies('chapter_item_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $elements = CourseElement::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.chapterItems.create', compact('elements'));
    }

    public function store(StoreChapterItemRequest $request)
    {
        $chapterItem = ChapterItem::create($request->all());

        return redirect()->route('admin.chapter-items.index');
    }

    public function edit(ChapterItem $chapterItem)
    {
        abort_if(Gate::denies('chapter_item_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $elements = CourseElement::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $chapterItem->load('element');

        return view('admin.chapterItems.edit', compact('chapterItem', 'elements'));
    }

    public function update(UpdateChapterItemRequest $request, ChapterItem $chapterItem)
    {
        $chapterItem->update($request->all());

        return redirect()->route('admin.chapter-items.index');
    }

    public function show(ChapterItem $chapterItem)
    {
        abort_if(Gate::denies('chapter_item_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapterItem->load('element');

        return view('admin.chapterItems.show', compact('chapterItem'));
    }

    public function destroy(ChapterItem $chapterItem)
    {
        abort_if(Gate::denies('chapter_item_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chapterItem->delete();

        return back();
    }

    public function massDestroy(MassDestroyChapterItemRequest $request)
    {
        ChapterItem::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
