<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPopupNewsRequest;
use App\Http\Requests\StorePopupNewsRequest;
use App\Http\Requests\UpdatePopupNewsRequest;
use App\Models\PopupNews;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PopupNewsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('popup_news_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $popupNews = PopupNews::all();

        return view('admin.popupNews.index', compact('popupNews'));
    }

    public function create()
    {
        abort_if(Gate::denies('popup_news_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.popupNews.create');
    }

    public function store(StorePopupNewsRequest $request)
    {
        $popupNews = PopupNews::create($request->all());

        return redirect()->route('admin.popup-news.index');
    }

    public function edit(PopupNews $popupNews)
    {
        abort_if(Gate::denies('popup_news_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.popupNews.edit', compact('popupNews'));
    }

    public function update(UpdatePopupNewsRequest $request, PopupNews $popupNews)
    {
        $popupNews->update($request->all());

        return redirect()->route('admin.popup-news.index');
    }

    public function show(PopupNews $popupNews)
    {
        abort_if(Gate::denies('popup_news_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.popupNews.show', compact('popupNews'));
    }

    public function destroy(PopupNews $popupNews)
    {
        abort_if(Gate::denies('popup_news_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $popupNews->delete();

        return back();
    }

    public function massDestroy(MassDestroyPopupNewsRequest $request)
    {
        PopupNews::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
