<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GameBackground;
use App\Models\GameAvailableItem;
use App\Models\Station;
use App\Models\User;
use App\Models\Course;
use App\Models\GameSelectedStation;
use App\Models\Sign;
use App\Models\Color;
use App\Models\PassedTest;
use App\Models\Test;
use App\Models\TestSignColor;
use Auth;

class GameController extends Controller
{
    public function giveRitms(Request $r){

        $ritms_to_give = $r->input('ritms_to_give');
        $email_whom_give = $r->input('email_whom_give');

        if($ritms_to_give > Auth::user()->ritms){
            return back()->with(["message" => "У Вас меньше ритмов чем Вы хотите передать!"]);
        }

        $user = User::where('email', $email_whom_give)->get();

        if(count($user) < 1){
            return back()->with(["message" => "Адресат не найден. Проверьте правильность почты."]);
        }

        User::where('email', $email_whom_give)
                ->update([
                    "ritms" => $user[0]->ritms + $ritms_to_give
                ]);

        $user2 = Auth::user();
        $user2->ritms = $user2->ritms - $ritms_to_give;
        $user2->save();

        return back()->with(["message" => "Ритмы успешно отправлены адресату - ".$email_whom_give]);
    }


    public function giveHint(){

        $user = Auth::user();
        $user->ritms = $user->ritms - 10;
        $user->save();

        return $user->ritms;
    }

    public function userStation($game_selected_station, $user_id){

        $uniq_test = array();

        $ar['user'] = User::where('id', $user_id)->get();

        $ar['item'] = GameSelectedStation::where('id', $game_selected_station)->get();

        $test_answers = PassedTest::where("user_id", $user_id)->get();

        $passed_tests_id = array();

        foreach ($test_answers as $test_answer) {
            if(isset($uniq_test[$test_answer->test_id])){

            }else{
                $passed_tests_id[] = $test_answer->test_id;
                $uniq_test[$test_answer->test_id] = 1;
            }
        }

        $passed_tests = Test::whereIn('id', $passed_tests_id)->get();

        $courses_id = array();

        foreach ($passed_tests as $passed_test) {
            $courses_id[] = $passed_test->chapter_item->course_chapter->course->id;
        }

        $cat_id =  $ar['item'][0]->course_category_id;

        $courses = Course::whereIn('id', $courses_id)->whereHas('categories', function($query) use($cat_id) {
            $query->where('course_category_id', $cat_id);
        })->orderBy('class_id')->get();

        $levels = array();
        $level_model = array();

        foreach ($courses as $course) {
            if(!in_array($course->class->class, $levels)){
                $levels[] = $course->class->class;
                $level_model[] = $course->class;
            }
        }
        $ar['levels'] =  $levels;
        $ar['courses'] = $courses;
        $ar['passed_tests'] = $passed_tests;

        return view("front.game.user_station", $ar);
    }

    public function userBase($user_id){

        $ar['user'] = User::where('id', $user_id)->get();

        $ar['stations'] = GameSelectedStation::where('user_id', $user_id)->get();

        if($ar['user'][0]->game_on == 1)
            return view("front.game.user_base", $ar);
        else return "Пользователь отключил просмотр его базы.";
    }

    public function rating(Request $r){

        if($r->has('limit')){
            $limit = $r->input('limit');
        }else{  
            $limit = 100;
        }

        $ar['users'] = User::where("game_on", 1)->where('total_got_ritms', '>', 0)->orderBy('total_got_ritms', 'DESC')->paginate(50);
        $ar['limit'] = $limit;

        return view("front.game.lider_board", $ar);
    }

    public function stationEdit($game_selected_station){

        $uniq_test = array();

        $ar['item'] = GameSelectedStation::where('id', $game_selected_station)->get();
        $ar['all_stations'] = Station::all();
        $ar['all_signs'] = Sign::all();
        $ar['all_colors'] = Color::all();
        $test_answers = PassedTest::where("user_id", Auth::user()->id)->get();

        $passed_tests_id = array();

        foreach ($test_answers as $test_answer) {
            if(isset($uniq_test[$test_answer->test_id])){

            }else{
                $passed_tests_id[] = $test_answer->test_id;
                $uniq_test[$test_answer->test_id] = 1;
            }
        }

        $passed_tests = Test::whereIn('id', $passed_tests_id)->get();

        $courses_id = array();

        foreach ($passed_tests as $passed_test) {
            $courses_id[] = $passed_test->chapter_item->course_chapter->course->id;
        }

        $cat_id =  $ar['item'][0]->course_category_id;

        $courses = Course::whereIn('id', $courses_id)->whereHas('categories', function($query) use($cat_id) {
            $query->where('course_category_id', $cat_id);
        })->orderBy('class_id')->get();

        $levels = array();
        $level_model = array();

        foreach ($courses as $course) {
            if(!in_array($course->class->class, $levels)){
                $levels[] = $course->class->class;
                $level_model[] = $course->class;
            }
        }
        $ar['levels'] =  $levels;
        $ar['courses'] = $courses;
        $ar['passed_tests'] = $passed_tests;

        return view("front.game.station", $ar);
    }

    public function stationUpdate($current_selected_station, Request $r){

        $not_enough_ritms = 0;

        $chosen_station_id = $r->input("selected_station_id");

        $is_installed = GameAvailableItem::where('element', $chosen_station_id)->where('user_id', Auth::user()->id)->get();

        if(count($is_installed) > 0){

        }else{
            $selected_item_id = GameAvailableItem::insertGetId([
                                "item_type" => "station",
                                "element" => $chosen_station_id,
                                "installed" => 1,
                                "user_id" => Auth::user()->id
                            ]);
            $station_data = Station::where('id', $chosen_station_id)->get();
            User::where("id", Auth::user()->id)->update(["ritms" => Auth::user()->ritms - $station_data[0]->ritm_price]);
        }

        GameSelectedStation::where('id', $current_selected_station)
                            ->update([
                                "station_id" => $chosen_station_id
                            ]);

        $changed_signs_string = $r->input("changed_test_signs");
        $changed_signs_array = array_unique(explode(",", $changed_signs_string));

        $changed_colors_string = $r->input("changed_test_colors");
        $changed_colors_array = array_unique(explode(",", $changed_colors_string));

        foreach ($changed_signs_array as $changed_sign_test) {
            $sign_data = Sign::where('id', $r->input("test_sign_".$changed_sign_test))->get();
            if(count($sign_data) > 0){
                $user_data = User::where('id', Auth::user()->id)->get();
                if($user_data[0]->ritms >= $sign_data[0]->price){
                    TestSignColor::where('id', $changed_sign_test)
                                ->update([
                                    'sign_id' => $r->input("test_sign_".$changed_sign_test)
                                ]);
                    User::where("id", Auth::user()->id)->update(["ritms" => $user_data[0]->ritms - $sign_data[0]->price]);
                }else{
                    $not_enough_ritms = 1;
                }
            }
        }

        foreach ($changed_colors_array as $changed_color_test) {
            if(is_numeric($changed_color_test))
            {
                TestSignColor::where('id', $changed_color_test)
                        ->update([
                            'color_id' => $r->input("test_color_".$changed_color_test)
                        ]);
            }
        }

        if($not_enough_ritms == 0)
            return back()->with(["message" => "Преобразования успешно произведены!"]);
        else
            return back()->with(["message" => "Недостаточно ритмов для всех преобразований!"]);

    }

    public function wallet(){

        return view("front.game.wallet");
    }

    public function baseIndex(){

    	$ar['backgrounds'] = GameBackground::all();
        $ar['stations'] = GameSelectedStation::where('user_id', Auth::user()->id)->get();

    	return view("front.game.base", $ar);
    }

    public function baseExample(){

        $ar['backgrounds'] = GameBackground::all();

        return view("front.game.base_example", $ar);
    }

    public function whyHere(){

    	return view("front.game.whyhere");
    }

    public function saveBackground(Request $r){

    	$background = $r->input("bg");

        $background_name = preg_replace("/base_/", "", $background);
        $background_data = GameBackground::where('name', $background_name)->get();


        if($background_data[0]->ritm_price <= Auth::user()->ritms){
            $user = Auth::user();
            $user->ritms = $user->ritms - $background_data[0]->ritm_price;
            $user->save();

        	User::where('id', Auth::user()->id)->update(["base_background" => $background]);
            return back();
        }else{
            return back()->with(["message" => "Недостаточно ритмов!"]);
        }

    }
}
