<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseClass;
use App\Models\ChapterItem;
use App\Models\Video;
use App\Models\Page;
use App\Models\Test;
use App\Models\File;
use App\Models\Question;
use App\Models\Answer;
use App\Models\CourseChapter;
use App\Models\PassedTest;
use App\Models\TestSignColor;
use App\Models\Sign;
use App\Models\Color;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Http;

class TestController extends Controller
{

	public function getCert($test_id){

		$test = Test::where('id', $test_id)->get();
		$email = Auth::user()->email;

		$response = Http::get('https://certificates.caravanofknowledge.com/check-people/'.$email.'/'.$test[0]->cert_id);

		if($response == "No"){
			$add_person_response = Http::get('https://certificates.caravanofknowledge.com/add-people/'.$email.'/'.Auth::user()->name.'/'.$test[0]->cert_id.'/jadkadbajhbdh346tsfbjks886868ikjjhs');
			if($add_person_response == "success"){

				$filename = 'cert.pdf';
				$tempfile = tempnam(sys_get_temp_dir(), $filename);
				copy('https://certificates.caravanofknowledge.com/download-api/'.$email.'/'.$test[0]->cert_id.'/jadkadbajhbdh346tsfbjks886868ikjjhs', $tempfile);

				return response()->download($tempfile, $filename);

			}
		}else{
			
			$filename = 'cert.pdf';
			$tempfile = tempnam(sys_get_temp_dir(), $filename);
			copy('https://certificates.caravanofknowledge.com/download-api/'.$email.'/'.$test[0]->cert_id.'/jadkadbajhbdh346tsfbjks886868ikjjhs', $tempfile);

			return response()->download($tempfile, $filename);

		}
	}

	public function finishTest($test_id, Request $r){

		$questions = Question::where('test_id', $test_id)->get();
		$total_questions = Question::where('test_id', $test_id)->count();
		$right_answered_questions = 0;
		foreach ($questions as $question) {
			$q_id = $question->id;
			$a_id = $r->input('q_'.$q_id);
			$right_a_id_query = Answer::where('question_id', $q_id)->where('is_right', 1)->get();
			$right_a_id = $right_a_id_query[0]->id;
			$user_id = Auth::user()->id;

			$user_answer = new PassedTest();
			$user_answer->test_id = $test_id;
			$user_answer->question_id = $q_id;
			$user_answer->answer_id = $a_id;
			$user_answer->right_answer_id = $right_a_id;
			$user_answer->user_id = $user_id;
			$user_answer->save();

			if($right_a_id == $a_id){
				$right_answered_questions++;
			}
		}

		$game_test_sign_color = new TestSignColor();
		$game_test_sign_color->course_id = $r->input("course_id");
		$game_test_sign_color->test_id = $test_id;
		$game_test_sign_color->sign_id = Sign::inRandomOrder()->first()->id;
		$game_test_sign_color->color_id = Color::inRandomOrder()->first()->id;
		$game_test_sign_color->user_id = Auth::user()->id;
		$game_test_sign_color->save();


		User::where('id', Auth::user()->id)->update([
				"ritms" => Auth::user()->ritms + round(($right_answered_questions/$total_questions)*100),
				"total_got_ritms" => Auth::user()->total_got_ritms + round(($right_answered_questions/$total_questions)*100)
			]);

		return back();
	}

    public function getPass($course_id, $chapter_id, $element_id, $test_id){

    	$ar['passed_questions'] = 0;

    	if(Auth::check()){
    		$ar['auth'] = 1;
    		$ar['passed_questions'] = PassedTest::where('test_id', $test_id)->where('user_id', Auth::user()->id)->count();
    		$ar['given_answers'] = PassedTest::where('test_id', $test_id)->where('user_id', Auth::user()->id)->get();
    		$ar['right_count'] = PassedTest::where('test_id', $test_id)->where('user_id', Auth::user()->id)->whereRaw('right_answer_id = answer_id')->count();
    	}else{
    		$ar['auth'] = 0;
    	}

    	$ar['course'] = Course::where('draft', 0)->find($course_id);
    	$ar['chapter'] = CourseChapter::where('id', $chapter_id)->get();
    	$ar['element'] = ChapterItem::where('id', $element_id)->get();
    	$ar['test'] = Test::where('chapter_item_id', $element_id)->get();
    	$ar['questions'] = Question::where('test_id', $test_id)->get();
    	$ar['total_questions'] = Question::where('test_id', $test_id)->count();

    	if($ar['passed_questions'] > 0){
    		return view("front.course.test_passed", $ar);
    	}else{
			return view("front.course.test_process", $ar);
		}
    }

}
