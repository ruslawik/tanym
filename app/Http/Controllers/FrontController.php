<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseClass;
use App\Models\User;
use App\Models\TestSignColor;
use Auth;
use App\Models\PassedTest;

class FrontController extends Controller
{

    public function setLang($locale){
        app()->setlocale($locale);
        session()->put('language', $locale);
        return redirect()->back();
    }

    public function index (){

        $ar['courses'] = Course::where('draft', 0)->orderBy('id', 'desc')->limit(4)->get();
        $ar['courses_rec'] = Course::where('draft', 0)->orderBy('id', 'desc')->limit(6)->get();
        

        if(Auth::check()){
            $colors = array("rgb(117,159,199)", "rgb(175,151,205)", "rgb(208,208,125)", "rgb(215,117,111)", "rgb(184,194,0)", "rgb(184,97,128)",  "rgb(110,168,128)", "rgb(110,168,195)", "rgb(24,43,195)",  "rgb(233,225,117)",  "rgb(10,240,117)");

            $cat_progresses = array();
            $k=0;
            $color_id = 0;

            $categories = CourseCategory::all();
            foreach($categories as $category){
                //данные для паутинки
                $courses = Course::whereHas('categories', function($query) use($category) {
                    $query->where('course_category_id', $category->id);
                })->get();

                $tests_id = array();
                foreach ($courses as $course) {
                    $tests = TestSignColor::where('course_id', $course->id)->where('user_id', Auth::user()->id)->get();
                    if(count($tests) > 0){
                        foreach ($tests as $test) {
                            $tests_id[] = $test->test_id;
                        }
                    }
                }
                $test_results = array();
                foreach ($tests_id as $test_id) {
                    $test_answers_total = PassedTest::where('test_id', $test_id)->where('user_id', Auth::user()->id)->count();
                    $test_answers_right = PassedTest::where('test_id', $test_id)->where('user_id', Auth::user()->id)->whereRaw('right_answer_id = answer_id')->count();
                    if($test_answers_total != 0){
                        $test_results[] = round(($test_answers_right/$test_answers_total)*100);
                    }
                }


                if(count($test_results) != 0){
                    $cat_progress = array_sum($test_results)/count($test_results);
                }else{
                     $cat_progress = 10;
                }

                $cat_progresses[$k]['name'] = $category->name;
                $cat_progresses[$k]['progress'] = $cat_progress;
                if($color_id < count($colors))
                    $cat_progresses[$k]['color'] = $colors[$color_id];
                else{ 
                    $color_id = 0;
                    $cat_progresses[$k]['color'] = $colors[$color_id];
                }
                $color_id++;
                $k++;
            }

            $ar['cat_progresses'] = $cat_progresses;
            $ar['total_cats'] = $k;
        }

    	return view("front.index", $ar);
    }

    public function instructor($instructor_id){

        $ar['instructor'] = User::find($instructor_id);
        $ar['courses'] = Course::where('draft', 0)->where('author_id', $instructor_id)->orderBy('id', 'desc')->get();

        return view("front.instructor", $ar);
    }

    public function search(Request $r){

    	$search_query = Course::query();

    	if($r->has('q') && strlen($r->input('q')) > 1){
    		$ar['q'] = $r->input('q');
    			$search_query->where('name', 'LIKE', '%'.$ar['q'].'%');
    	}else{
    		$ar['q'] = "";
    	}

    	if($r->has('class')){
    		$ar['selected_class_id'] = $r->input('class');
    		if($r->input('class') != "all")
    			$search_query->where('class_id', $ar['selected_class_id']);
    	}else{
    		$ar['selected_class_id'] = "";
    	}

    	if($r->has('category')){
    		$ar['selected_category_id'] = $r->input('category');
    		$selected_category_id = $ar['selected_category_id'];
    		if($r->input('category')!="all")
    			$search_query->whereHas('categories', function($q) use ($selected_category_id){
    				$q->where('course_category_id', $selected_category_id);
    			});
    	}else{
    		$ar['selected_category_id'] = "";
    	}

    	$ar['courses'] = $search_query->where('draft', 0)->get();

    	$ar['categories'] = CourseCategory::all();
    	$ar['classes'] = CourseClass::all();

    	return view("front.search", $ar);
    }
    
}
