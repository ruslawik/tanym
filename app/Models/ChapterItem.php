<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChapterItem extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'chapter_items';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'name_kz',
        'name_en',
        'item_order',
        'element_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function element()
    {
        return $this->belongsTo(CourseElement::class, 'element_id');
    }

    public function course_chapter(){
        return $this->belongsTo(CourseChapter::class, 'course_chapter_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
