<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Station;
use App\Models\CourseCategory;

class GameSelectedStation extends Model
{
    public $table = 'game_selected_stations';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'station_id',
        'user_id',
        'course_category_id',
        'created_at',
        'updated_at',
    ];

    public function get_progress(){
        return rand(0, 100);
    }

    public function get_item_info(){
        return $this->hasOne(Station::class, 'id', 'station_id');
    }

    public function get_category(){
        return $this->hasOne(CourseCategory::class, 'id', 'course_category_id');
    }
}
