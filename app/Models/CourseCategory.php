<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class CourseCategory extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'course_categories';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'name_kz',
        'name_en',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function progress($category_id, $user_id){
        $courses = Course::whereHas('categories', function($query) use($category_id) {
            $query->where('course_category_id', $category_id);
        })->get();

        $tests_id = array();
        foreach ($courses as $course) {
            $tests = TestSignColor::where('course_id', $course->id)->where('user_id', $user_id)->get();
            if(count($tests) > 0){
                foreach ($tests as $test) {
                    $tests_id[] = $test->test_id;
                }
            }
        }
        $test_results = array();
        foreach ($tests_id as $test_id) {
            $test_answers_total = PassedTest::where('test_id', $test_id)->where('user_id', $user_id)->count();
            $test_answers_right = PassedTest::where('test_id', $test_id)->where('user_id', $user_id)->whereRaw('right_answer_id = answer_id')->count();
            if($test_answers_total != 0){
                $test_results[] = round(($test_answers_right/$test_answers_total)*100);
            }
        }
        if(count($test_results) != 0){
            return round(array_sum($test_results)/count($test_results));
        }else{
            return 1;
        }
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
