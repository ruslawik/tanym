<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Auth;

/**
 * @internal
 * @coversNothing
 */
class Test extends Model implements HasMedia
{
    use SoftDeletes;
    use InteractsWithMedia;
    use HasFactory;

    public $table = 'tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'chapter_item_id',
        'name',
        'name_kz',
        'name_en',
        'description',
        'description_kz',
        'description_en',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function progress($user_id){
        $test_answers_total = PassedTest::where('test_id', $this->id)->where('user_id', $user_id)->count();
        $test_answers_right = PassedTest::where('test_id', $this->id)->where('user_id', $user_id)->whereRaw('right_answer_id = answer_id')->count();
        if($test_answers_total > 0){
            return round(($test_answers_right/$test_answers_total)*100);
        }
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function chapter_item()
    {
        return $this->belongsTo(ChapterItem::class, 'chapter_item_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
