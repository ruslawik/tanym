<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class CourseClass extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'course_classes';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'class',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function progress($class, $user_id){
        $class_q = CourseClass::where('class', $class)->get();
        $courses = Course::where("class_id", $class_q[0]->id)->get();
        $tests_id = array();
        foreach ($courses as $course) {
            $tests = TestSignColor::where('course_id', $course->id)->where('user_id', $user_id)->get();
            if(count($tests) > 0){
                foreach ($tests as $test) {
                    $tests_id[] = $test->test_id;
                }
            }
        }
        $test_results = array();
        foreach ($tests_id as $test_id) {
            $test_answers_total = PassedTest::where('test_id', $test_id)->where('user_id', $user_id)->count();
            $test_answers_right = PassedTest::where('test_id', $test_id)->where('user_id', $user_id)->whereRaw('right_answer_id = answer_id')->count();
            if($test_answers_total != 0)
            $test_results[] = round(($test_answers_right/$test_answers_total)*100);
        }
        return round(array_sum($test_results)/count($test_results));
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
