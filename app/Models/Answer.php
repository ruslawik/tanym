<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'answers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'question_id',
        'answer',
        'answer_kz',
        'answer_en',
        'is_right',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function proceed(){
        preg_match("/\*\*\*(.*?)\*\*\*/", $this->answer, $img_link);
        if(isset($img_link[1])){
            $this->answer = preg_replace("/\*\*\*(.*?)\*\*\*/", "<img src='".$img_link[1]."'>", $this->answer);
        }
        return $this->answer;
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
