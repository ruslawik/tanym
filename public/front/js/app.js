(() => {
    var __webpack_modules__ = {
        1807: module => {
            var canUseDOM = !!("undefined" !== typeof window && window.document && window.document.createElement);
            module.exports = canUseDOM;
        },
        1296: (module, __unused_webpack_exports, __webpack_require__) => {
            var FUNC_ERROR_TEXT = "Expected a function";
            var NAN = 0 / 0;
            var symbolTag = "[object Symbol]";
            var reTrim = /^\s+|\s+$/g;
            var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
            var reIsBinary = /^0b[01]+$/i;
            var reIsOctal = /^0o[0-7]+$/i;
            var freeParseInt = parseInt;
            var freeGlobal = "object" == typeof __webpack_require__.g && __webpack_require__.g && __webpack_require__.g.Object === Object && __webpack_require__.g;
            var freeSelf = "object" == typeof self && self && self.Object === Object && self;
            var root = freeGlobal || freeSelf || Function("return this")();
            var objectProto = Object.prototype;
            var objectToString = objectProto.toString;
            var nativeMax = Math.max, nativeMin = Math.min;
            var now = function() {
                return root.Date.now();
            };
            function debounce(func, wait, options) {
                var lastArgs, lastThis, maxWait, result, timerId, lastCallTime, lastInvokeTime = 0, leading = false, maxing = false, trailing = true;
                if ("function" != typeof func) throw new TypeError(FUNC_ERROR_TEXT);
                wait = toNumber(wait) || 0;
                if (isObject(options)) {
                    leading = !!options.leading;
                    maxing = "maxWait" in options;
                    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
                    trailing = "trailing" in options ? !!options.trailing : trailing;
                }
                function invokeFunc(time) {
                    var args = lastArgs, thisArg = lastThis;
                    lastArgs = lastThis = void 0;
                    lastInvokeTime = time;
                    result = func.apply(thisArg, args);
                    return result;
                }
                function leadingEdge(time) {
                    lastInvokeTime = time;
                    timerId = setTimeout(timerExpired, wait);
                    return leading ? invokeFunc(time) : result;
                }
                function remainingWait(time) {
                    var timeSinceLastCall = time - lastCallTime, timeSinceLastInvoke = time - lastInvokeTime, result = wait - timeSinceLastCall;
                    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
                }
                function shouldInvoke(time) {
                    var timeSinceLastCall = time - lastCallTime, timeSinceLastInvoke = time - lastInvokeTime;
                    return void 0 === lastCallTime || timeSinceLastCall >= wait || timeSinceLastCall < 0 || maxing && timeSinceLastInvoke >= maxWait;
                }
                function timerExpired() {
                    var time = now();
                    if (shouldInvoke(time)) return trailingEdge(time);
                    timerId = setTimeout(timerExpired, remainingWait(time));
                }
                function trailingEdge(time) {
                    timerId = void 0;
                    if (trailing && lastArgs) return invokeFunc(time);
                    lastArgs = lastThis = void 0;
                    return result;
                }
                function cancel() {
                    if (void 0 !== timerId) clearTimeout(timerId);
                    lastInvokeTime = 0;
                    lastArgs = lastCallTime = lastThis = timerId = void 0;
                }
                function flush() {
                    return void 0 === timerId ? result : trailingEdge(now());
                }
                function debounced() {
                    var time = now(), isInvoking = shouldInvoke(time);
                    lastArgs = arguments;
                    lastThis = this;
                    lastCallTime = time;
                    if (isInvoking) {
                        if (void 0 === timerId) return leadingEdge(lastCallTime);
                        if (maxing) {
                            timerId = setTimeout(timerExpired, wait);
                            return invokeFunc(lastCallTime);
                        }
                    }
                    if (void 0 === timerId) timerId = setTimeout(timerExpired, wait);
                    return result;
                }
                debounced.cancel = cancel;
                debounced.flush = flush;
                return debounced;
            }
            function isObject(value) {
                var type = typeof value;
                return !!value && ("object" == type || "function" == type);
            }
            function isObjectLike(value) {
                return !!value && "object" == typeof value;
            }
            function isSymbol(value) {
                return "symbol" == typeof value || isObjectLike(value) && objectToString.call(value) == symbolTag;
            }
            function toNumber(value) {
                if ("number" == typeof value) return value;
                if (isSymbol(value)) return NAN;
                if (isObject(value)) {
                    var other = "function" == typeof value.valueOf ? value.valueOf() : value;
                    value = isObject(other) ? other + "" : other;
                }
                if ("string" != typeof value) return 0 === value ? value : +value;
                value = value.replace(reTrim, "");
                var isBinary = reIsBinary.test(value);
                return isBinary || reIsOctal.test(value) ? freeParseInt(value.slice(2), isBinary ? 2 : 8) : reIsBadHex.test(value) ? NAN : +value;
            }
            module.exports = debounce;
        },
        773: (module, __unused_webpack_exports, __webpack_require__) => {
            var FUNC_ERROR_TEXT = "Expected a function";
            var HASH_UNDEFINED = "__lodash_hash_undefined__";
            var funcTag = "[object Function]", genTag = "[object GeneratorFunction]";
            var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
            var reIsHostCtor = /^\[object .+?Constructor\]$/;
            var freeGlobal = "object" == typeof __webpack_require__.g && __webpack_require__.g && __webpack_require__.g.Object === Object && __webpack_require__.g;
            var freeSelf = "object" == typeof self && self && self.Object === Object && self;
            var root = freeGlobal || freeSelf || Function("return this")();
            function getValue(object, key) {
                return null == object ? void 0 : object[key];
            }
            function isHostObject(value) {
                var result = false;
                if (null != value && "function" != typeof value.toString) try {
                    result = !!(value + "");
                } catch (e) {}
                return result;
            }
            var arrayProto = Array.prototype, funcProto = Function.prototype, objectProto = Object.prototype;
            var coreJsData = root["__core-js_shared__"];
            var maskSrcKey = function() {
                var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || "");
                return uid ? "Symbol(src)_1." + uid : "";
            }();
            var funcToString = funcProto.toString;
            var hasOwnProperty = objectProto.hasOwnProperty;
            var objectToString = objectProto.toString;
            var reIsNative = RegExp("^" + funcToString.call(hasOwnProperty).replace(reRegExpChar, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
            var splice = arrayProto.splice;
            var Map = getNative(root, "Map"), nativeCreate = getNative(Object, "create");
            function Hash(entries) {
                var index = -1, length = entries ? entries.length : 0;
                this.clear();
                while (++index < length) {
                    var entry = entries[index];
                    this.set(entry[0], entry[1]);
                }
            }
            function hashClear() {
                this.__data__ = nativeCreate ? nativeCreate(null) : {};
            }
            function hashDelete(key) {
                return this.has(key) && delete this.__data__[key];
            }
            function hashGet(key) {
                var data = this.__data__;
                if (nativeCreate) {
                    var result = data[key];
                    return result === HASH_UNDEFINED ? void 0 : result;
                }
                return hasOwnProperty.call(data, key) ? data[key] : void 0;
            }
            function hashHas(key) {
                var data = this.__data__;
                return nativeCreate ? void 0 !== data[key] : hasOwnProperty.call(data, key);
            }
            function hashSet(key, value) {
                var data = this.__data__;
                data[key] = nativeCreate && void 0 === value ? HASH_UNDEFINED : value;
                return this;
            }
            Hash.prototype.clear = hashClear;
            Hash.prototype["delete"] = hashDelete;
            Hash.prototype.get = hashGet;
            Hash.prototype.has = hashHas;
            Hash.prototype.set = hashSet;
            function ListCache(entries) {
                var index = -1, length = entries ? entries.length : 0;
                this.clear();
                while (++index < length) {
                    var entry = entries[index];
                    this.set(entry[0], entry[1]);
                }
            }
            function listCacheClear() {
                this.__data__ = [];
            }
            function listCacheDelete(key) {
                var data = this.__data__, index = assocIndexOf(data, key);
                if (index < 0) return false;
                var lastIndex = data.length - 1;
                if (index == lastIndex) data.pop(); else splice.call(data, index, 1);
                return true;
            }
            function listCacheGet(key) {
                var data = this.__data__, index = assocIndexOf(data, key);
                return index < 0 ? void 0 : data[index][1];
            }
            function listCacheHas(key) {
                return assocIndexOf(this.__data__, key) > -1;
            }
            function listCacheSet(key, value) {
                var data = this.__data__, index = assocIndexOf(data, key);
                if (index < 0) data.push([ key, value ]); else data[index][1] = value;
                return this;
            }
            ListCache.prototype.clear = listCacheClear;
            ListCache.prototype["delete"] = listCacheDelete;
            ListCache.prototype.get = listCacheGet;
            ListCache.prototype.has = listCacheHas;
            ListCache.prototype.set = listCacheSet;
            function MapCache(entries) {
                var index = -1, length = entries ? entries.length : 0;
                this.clear();
                while (++index < length) {
                    var entry = entries[index];
                    this.set(entry[0], entry[1]);
                }
            }
            function mapCacheClear() {
                this.__data__ = {
                    hash: new Hash,
                    map: new (Map || ListCache),
                    string: new Hash
                };
            }
            function mapCacheDelete(key) {
                return getMapData(this, key)["delete"](key);
            }
            function mapCacheGet(key) {
                return getMapData(this, key).get(key);
            }
            function mapCacheHas(key) {
                return getMapData(this, key).has(key);
            }
            function mapCacheSet(key, value) {
                getMapData(this, key).set(key, value);
                return this;
            }
            MapCache.prototype.clear = mapCacheClear;
            MapCache.prototype["delete"] = mapCacheDelete;
            MapCache.prototype.get = mapCacheGet;
            MapCache.prototype.has = mapCacheHas;
            MapCache.prototype.set = mapCacheSet;
            function assocIndexOf(array, key) {
                var length = array.length;
                while (length--) if (eq(array[length][0], key)) return length;
                return -1;
            }
            function baseIsNative(value) {
                if (!isObject(value) || isMasked(value)) return false;
                var pattern = isFunction(value) || isHostObject(value) ? reIsNative : reIsHostCtor;
                return pattern.test(toSource(value));
            }
            function getMapData(map, key) {
                var data = map.__data__;
                return isKeyable(key) ? data["string" == typeof key ? "string" : "hash"] : data.map;
            }
            function getNative(object, key) {
                var value = getValue(object, key);
                return baseIsNative(value) ? value : void 0;
            }
            function isKeyable(value) {
                var type = typeof value;
                return "string" == type || "number" == type || "symbol" == type || "boolean" == type ? "__proto__" !== value : null === value;
            }
            function isMasked(func) {
                return !!maskSrcKey && maskSrcKey in func;
            }
            function toSource(func) {
                if (null != func) {
                    try {
                        return funcToString.call(func);
                    } catch (e) {}
                    try {
                        return func + "";
                    } catch (e) {}
                }
                return "";
            }
            function memoize(func, resolver) {
                if ("function" != typeof func || resolver && "function" != typeof resolver) throw new TypeError(FUNC_ERROR_TEXT);
                var memoized = function() {
                    var args = arguments, key = resolver ? resolver.apply(this, args) : args[0], cache = memoized.cache;
                    if (cache.has(key)) return cache.get(key);
                    var result = func.apply(this, args);
                    memoized.cache = cache.set(key, result);
                    return result;
                };
                memoized.cache = new (memoize.Cache || MapCache);
                return memoized;
            }
            memoize.Cache = MapCache;
            function eq(value, other) {
                return value === other || value !== value && other !== other;
            }
            function isFunction(value) {
                var tag = isObject(value) ? objectToString.call(value) : "";
                return tag == funcTag || tag == genTag;
            }
            function isObject(value) {
                var type = typeof value;
                return !!value && ("object" == type || "function" == type);
            }
            module.exports = memoize;
        },
        3096: (module, __unused_webpack_exports, __webpack_require__) => {
            var FUNC_ERROR_TEXT = "Expected a function";
            var NAN = 0 / 0;
            var symbolTag = "[object Symbol]";
            var reTrim = /^\s+|\s+$/g;
            var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
            var reIsBinary = /^0b[01]+$/i;
            var reIsOctal = /^0o[0-7]+$/i;
            var freeParseInt = parseInt;
            var freeGlobal = "object" == typeof __webpack_require__.g && __webpack_require__.g && __webpack_require__.g.Object === Object && __webpack_require__.g;
            var freeSelf = "object" == typeof self && self && self.Object === Object && self;
            var root = freeGlobal || freeSelf || Function("return this")();
            var objectProto = Object.prototype;
            var objectToString = objectProto.toString;
            var nativeMax = Math.max, nativeMin = Math.min;
            var now = function() {
                return root.Date.now();
            };
            function debounce(func, wait, options) {
                var lastArgs, lastThis, maxWait, result, timerId, lastCallTime, lastInvokeTime = 0, leading = false, maxing = false, trailing = true;
                if ("function" != typeof func) throw new TypeError(FUNC_ERROR_TEXT);
                wait = toNumber(wait) || 0;
                if (isObject(options)) {
                    leading = !!options.leading;
                    maxing = "maxWait" in options;
                    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
                    trailing = "trailing" in options ? !!options.trailing : trailing;
                }
                function invokeFunc(time) {
                    var args = lastArgs, thisArg = lastThis;
                    lastArgs = lastThis = void 0;
                    lastInvokeTime = time;
                    result = func.apply(thisArg, args);
                    return result;
                }
                function leadingEdge(time) {
                    lastInvokeTime = time;
                    timerId = setTimeout(timerExpired, wait);
                    return leading ? invokeFunc(time) : result;
                }
                function remainingWait(time) {
                    var timeSinceLastCall = time - lastCallTime, timeSinceLastInvoke = time - lastInvokeTime, result = wait - timeSinceLastCall;
                    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
                }
                function shouldInvoke(time) {
                    var timeSinceLastCall = time - lastCallTime, timeSinceLastInvoke = time - lastInvokeTime;
                    return void 0 === lastCallTime || timeSinceLastCall >= wait || timeSinceLastCall < 0 || maxing && timeSinceLastInvoke >= maxWait;
                }
                function timerExpired() {
                    var time = now();
                    if (shouldInvoke(time)) return trailingEdge(time);
                    timerId = setTimeout(timerExpired, remainingWait(time));
                }
                function trailingEdge(time) {
                    timerId = void 0;
                    if (trailing && lastArgs) return invokeFunc(time);
                    lastArgs = lastThis = void 0;
                    return result;
                }
                function cancel() {
                    if (void 0 !== timerId) clearTimeout(timerId);
                    lastInvokeTime = 0;
                    lastArgs = lastCallTime = lastThis = timerId = void 0;
                }
                function flush() {
                    return void 0 === timerId ? result : trailingEdge(now());
                }
                function debounced() {
                    var time = now(), isInvoking = shouldInvoke(time);
                    lastArgs = arguments;
                    lastThis = this;
                    lastCallTime = time;
                    if (isInvoking) {
                        if (void 0 === timerId) return leadingEdge(lastCallTime);
                        if (maxing) {
                            timerId = setTimeout(timerExpired, wait);
                            return invokeFunc(lastCallTime);
                        }
                    }
                    if (void 0 === timerId) timerId = setTimeout(timerExpired, wait);
                    return result;
                }
                debounced.cancel = cancel;
                debounced.flush = flush;
                return debounced;
            }
            function throttle(func, wait, options) {
                var leading = true, trailing = true;
                if ("function" != typeof func) throw new TypeError(FUNC_ERROR_TEXT);
                if (isObject(options)) {
                    leading = "leading" in options ? !!options.leading : leading;
                    trailing = "trailing" in options ? !!options.trailing : trailing;
                }
                return debounce(func, wait, {
                    leading,
                    maxWait: wait,
                    trailing
                });
            }
            function isObject(value) {
                var type = typeof value;
                return !!value && ("object" == type || "function" == type);
            }
            function isObjectLike(value) {
                return !!value && "object" == typeof value;
            }
            function isSymbol(value) {
                return "symbol" == typeof value || isObjectLike(value) && objectToString.call(value) == symbolTag;
            }
            function toNumber(value) {
                if ("number" == typeof value) return value;
                if (isSymbol(value)) return NAN;
                if (isObject(value)) {
                    var other = "function" == typeof value.valueOf ? value.valueOf() : value;
                    value = isObject(other) ? other + "" : other;
                }
                if ("string" != typeof value) return 0 === value ? value : +value;
                value = value.replace(reTrim, "");
                var isBinary = reIsBinary.test(value);
                return isBinary || reIsOctal.test(value) ? freeParseInt(value.slice(2), isBinary ? 2 : 8) : reIsBadHex.test(value) ? NAN : +value;
            }
            module.exports = throttle;
        },
        5055: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isCallable = __webpack_require__(6282);
            var tryToString = __webpack_require__(180);
            var TypeError = global.TypeError;
            module.exports = function(argument) {
                if (isCallable(argument)) return argument;
                throw TypeError(tryToString(argument) + " is not a function");
            };
        },
        2004: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isCallable = __webpack_require__(6282);
            var String = global.String;
            var TypeError = global.TypeError;
            module.exports = function(argument) {
                if ("object" == typeof argument || isCallable(argument)) return argument;
                throw TypeError("Can't set " + String(argument) + " as a prototype");
            };
        },
        9256: (module, __unused_webpack_exports, __webpack_require__) => {
            var wellKnownSymbol = __webpack_require__(8149);
            var create = __webpack_require__(1525);
            var definePropertyModule = __webpack_require__(9168);
            var UNSCOPABLES = wellKnownSymbol("unscopables");
            var ArrayPrototype = Array.prototype;
            if (void 0 == ArrayPrototype[UNSCOPABLES]) definePropertyModule.f(ArrayPrototype, UNSCOPABLES, {
                configurable: true,
                value: create(null)
            });
            module.exports = function(key) {
                ArrayPrototype[UNSCOPABLES][key] = true;
            };
        },
        3615: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var charAt = __webpack_require__(7321).charAt;
            module.exports = function(S, index, unicode) {
                return index + (unicode ? charAt(S, index).length : 1);
            };
        },
        3046: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isPrototypeOf = __webpack_require__(1786);
            var TypeError = global.TypeError;
            module.exports = function(it, Prototype) {
                if (isPrototypeOf(Prototype, it)) return it;
                throw TypeError("Incorrect invocation");
            };
        },
        1474: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isObject = __webpack_require__(5896);
            var String = global.String;
            var TypeError = global.TypeError;
            module.exports = function(argument) {
                if (isObject(argument)) return argument;
                throw TypeError(String(argument) + " is not an object");
            };
        },
        8774: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            module.exports = fails((function() {
                if ("function" == typeof ArrayBuffer) {
                    var buffer = new ArrayBuffer(8);
                    if (Object.isExtensible(buffer)) Object.defineProperty(buffer, "a", {
                        value: 8
                    });
                }
            }));
        },
        1269: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $forEach = __webpack_require__(528).forEach;
            var arrayMethodIsStrict = __webpack_require__(1923);
            var STRICT_METHOD = arrayMethodIsStrict("forEach");
            module.exports = !STRICT_METHOD ? function forEach(callbackfn) {
                return $forEach(this, callbackfn, arguments.length > 1 ? arguments[1] : void 0);
            } : [].forEach;
        },
        5675: (module, __unused_webpack_exports, __webpack_require__) => {
            var toIndexedObject = __webpack_require__(3206);
            var toAbsoluteIndex = __webpack_require__(9623);
            var lengthOfArrayLike = __webpack_require__(1829);
            var createMethod = function(IS_INCLUDES) {
                return function($this, el, fromIndex) {
                    var O = toIndexedObject($this);
                    var length = lengthOfArrayLike(O);
                    var index = toAbsoluteIndex(fromIndex, length);
                    var value;
                    if (IS_INCLUDES && el != el) while (length > index) {
                        value = O[index++];
                        if (value != value) return true;
                    } else for (;length > index; index++) if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
                    return !IS_INCLUDES && -1;
                };
            };
            module.exports = {
                includes: createMethod(true),
                indexOf: createMethod(false)
            };
        },
        528: (module, __unused_webpack_exports, __webpack_require__) => {
            var bind = __webpack_require__(1098);
            var uncurryThis = __webpack_require__(1768);
            var IndexedObject = __webpack_require__(7530);
            var toObject = __webpack_require__(9473);
            var lengthOfArrayLike = __webpack_require__(1829);
            var arraySpeciesCreate = __webpack_require__(2768);
            var push = uncurryThis([].push);
            var createMethod = function(TYPE) {
                var IS_MAP = 1 == TYPE;
                var IS_FILTER = 2 == TYPE;
                var IS_SOME = 3 == TYPE;
                var IS_EVERY = 4 == TYPE;
                var IS_FIND_INDEX = 6 == TYPE;
                var IS_FILTER_REJECT = 7 == TYPE;
                var NO_HOLES = 5 == TYPE || IS_FIND_INDEX;
                return function($this, callbackfn, that, specificCreate) {
                    var O = toObject($this);
                    var self = IndexedObject(O);
                    var boundFunction = bind(callbackfn, that);
                    var length = lengthOfArrayLike(self);
                    var index = 0;
                    var create = specificCreate || arraySpeciesCreate;
                    var target = IS_MAP ? create($this, length) : IS_FILTER || IS_FILTER_REJECT ? create($this, 0) : void 0;
                    var value, result;
                    for (;length > index; index++) if (NO_HOLES || index in self) {
                        value = self[index];
                        result = boundFunction(value, index, O);
                        if (TYPE) if (IS_MAP) target[index] = result; else if (result) switch (TYPE) {
                          case 3:
                            return true;

                          case 5:
                            return value;

                          case 6:
                            return index;

                          case 2:
                            push(target, value);
                        } else switch (TYPE) {
                          case 4:
                            return false;

                          case 7:
                            push(target, value);
                        }
                    }
                    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
                };
            };
            module.exports = {
                forEach: createMethod(0),
                map: createMethod(1),
                filter: createMethod(2),
                some: createMethod(3),
                every: createMethod(4),
                find: createMethod(5),
                findIndex: createMethod(6),
                filterReject: createMethod(7)
            };
        },
        4820: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            var wellKnownSymbol = __webpack_require__(8149);
            var V8_VERSION = __webpack_require__(4324);
            var SPECIES = wellKnownSymbol("species");
            module.exports = function(METHOD_NAME) {
                return V8_VERSION >= 51 || !fails((function() {
                    var array = [];
                    var constructor = array.constructor = {};
                    constructor[SPECIES] = function() {
                        return {
                            foo: 1
                        };
                    };
                    return 1 !== array[METHOD_NAME](Boolean).foo;
                }));
            };
        },
        1923: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var fails = __webpack_require__(6183);
            module.exports = function(METHOD_NAME, argument) {
                var method = [][METHOD_NAME];
                return !!method && fails((function() {
                    method.call(null, argument || function() {
                        throw 1;
                    }, 1);
                }));
            };
        },
        6589: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var aCallable = __webpack_require__(5055);
            var toObject = __webpack_require__(9473);
            var IndexedObject = __webpack_require__(7530);
            var lengthOfArrayLike = __webpack_require__(1829);
            var TypeError = global.TypeError;
            var createMethod = function(IS_RIGHT) {
                return function(that, callbackfn, argumentsLength, memo) {
                    aCallable(callbackfn);
                    var O = toObject(that);
                    var self = IndexedObject(O);
                    var length = lengthOfArrayLike(O);
                    var index = IS_RIGHT ? length - 1 : 0;
                    var i = IS_RIGHT ? -1 : 1;
                    if (argumentsLength < 2) while (true) {
                        if (index in self) {
                            memo = self[index];
                            index += i;
                            break;
                        }
                        index += i;
                        if (IS_RIGHT ? index < 0 : length <= index) throw TypeError("Reduce of empty array with no initial value");
                    }
                    for (;IS_RIGHT ? index >= 0 : length > index; index += i) if (index in self) memo = callbackfn(memo, self[index], index, O);
                    return memo;
                };
            };
            module.exports = {
                left: createMethod(false),
                right: createMethod(true)
            };
        },
        4072: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var toAbsoluteIndex = __webpack_require__(9623);
            var lengthOfArrayLike = __webpack_require__(1829);
            var createProperty = __webpack_require__(2759);
            var Array = global.Array;
            var max = Math.max;
            module.exports = function(O, start, end) {
                var length = lengthOfArrayLike(O);
                var k = toAbsoluteIndex(start, length);
                var fin = toAbsoluteIndex(void 0 === end ? length : end, length);
                var result = Array(max(fin - k, 0));
                for (var n = 0; k < fin; k++, n++) createProperty(result, n, O[k]);
                result.length = n;
                return result;
            };
        },
        9882: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isArray = __webpack_require__(7931);
            var isConstructor = __webpack_require__(2240);
            var isObject = __webpack_require__(5896);
            var wellKnownSymbol = __webpack_require__(8149);
            var SPECIES = wellKnownSymbol("species");
            var Array = global.Array;
            module.exports = function(originalArray) {
                var C;
                if (isArray(originalArray)) {
                    C = originalArray.constructor;
                    if (isConstructor(C) && (C === Array || isArray(C.prototype))) C = void 0; else if (isObject(C)) {
                        C = C[SPECIES];
                        if (null === C) C = void 0;
                    }
                }
                return void 0 === C ? Array : C;
            };
        },
        2768: (module, __unused_webpack_exports, __webpack_require__) => {
            var arraySpeciesConstructor = __webpack_require__(9882);
            module.exports = function(originalArray, length) {
                return new (arraySpeciesConstructor(originalArray))(0 === length ? 0 : length);
            };
        },
        1751: (module, __unused_webpack_exports, __webpack_require__) => {
            var wellKnownSymbol = __webpack_require__(8149);
            var ITERATOR = wellKnownSymbol("iterator");
            var SAFE_CLOSING = false;
            try {
                var called = 0;
                var iteratorWithReturn = {
                    next: function() {
                        return {
                            done: !!called++
                        };
                    },
                    return: function() {
                        SAFE_CLOSING = true;
                    }
                };
                iteratorWithReturn[ITERATOR] = function() {
                    return this;
                };
                Array.from(iteratorWithReturn, (function() {
                    throw 2;
                }));
            } catch (error) {}
            module.exports = function(exec, SKIP_CLOSING) {
                if (!SKIP_CLOSING && !SAFE_CLOSING) return false;
                var ITERATION_SUPPORT = false;
                try {
                    var object = {};
                    object[ITERATOR] = function() {
                        return {
                            next: function() {
                                return {
                                    done: ITERATION_SUPPORT = true
                                };
                            }
                        };
                    };
                    exec(object);
                } catch (error) {}
                return ITERATION_SUPPORT;
            };
        },
        1510: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var toString = uncurryThis({}.toString);
            var stringSlice = uncurryThis("".slice);
            module.exports = function(it) {
                return stringSlice(toString(it), 8, -1);
            };
        },
        9225: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var TO_STRING_TAG_SUPPORT = __webpack_require__(4823);
            var isCallable = __webpack_require__(6282);
            var classofRaw = __webpack_require__(1510);
            var wellKnownSymbol = __webpack_require__(8149);
            var TO_STRING_TAG = wellKnownSymbol("toStringTag");
            var Object = global.Object;
            var CORRECT_ARGUMENTS = "Arguments" == classofRaw(function() {
                return arguments;
            }());
            var tryGet = function(it, key) {
                try {
                    return it[key];
                } catch (error) {}
            };
            module.exports = TO_STRING_TAG_SUPPORT ? classofRaw : function(it) {
                var O, tag, result;
                return void 0 === it ? "Undefined" : null === it ? "Null" : "string" == typeof (tag = tryGet(O = Object(it), TO_STRING_TAG)) ? tag : CORRECT_ARGUMENTS ? classofRaw(O) : "Object" == (result = classofRaw(O)) && isCallable(O.callee) ? "Arguments" : result;
            };
        },
        7790: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var uncurryThis = __webpack_require__(1768);
            var redefineAll = __webpack_require__(9573);
            var getWeakData = __webpack_require__(6582).getWeakData;
            var anObject = __webpack_require__(1474);
            var isObject = __webpack_require__(5896);
            var anInstance = __webpack_require__(3046);
            var iterate = __webpack_require__(1518);
            var ArrayIterationModule = __webpack_require__(528);
            var hasOwn = __webpack_require__(8281);
            var InternalStateModule = __webpack_require__(1030);
            var setInternalState = InternalStateModule.set;
            var internalStateGetterFor = InternalStateModule.getterFor;
            var find = ArrayIterationModule.find;
            var findIndex = ArrayIterationModule.findIndex;
            var splice = uncurryThis([].splice);
            var id = 0;
            var uncaughtFrozenStore = function(store) {
                return store.frozen || (store.frozen = new UncaughtFrozenStore);
            };
            var UncaughtFrozenStore = function() {
                this.entries = [];
            };
            var findUncaughtFrozen = function(store, key) {
                return find(store.entries, (function(it) {
                    return it[0] === key;
                }));
            };
            UncaughtFrozenStore.prototype = {
                get: function(key) {
                    var entry = findUncaughtFrozen(this, key);
                    if (entry) return entry[1];
                },
                has: function(key) {
                    return !!findUncaughtFrozen(this, key);
                },
                set: function(key, value) {
                    var entry = findUncaughtFrozen(this, key);
                    if (entry) entry[1] = value; else this.entries.push([ key, value ]);
                },
                delete: function(key) {
                    var index = findIndex(this.entries, (function(it) {
                        return it[0] === key;
                    }));
                    if (~index) splice(this.entries, index, 1);
                    return !!~index;
                }
            };
            module.exports = {
                getConstructor: function(wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER) {
                    var Constructor = wrapper((function(that, iterable) {
                        anInstance(that, Prototype);
                        setInternalState(that, {
                            type: CONSTRUCTOR_NAME,
                            id: id++,
                            frozen: void 0
                        });
                        if (void 0 != iterable) iterate(iterable, that[ADDER], {
                            that,
                            AS_ENTRIES: IS_MAP
                        });
                    }));
                    var Prototype = Constructor.prototype;
                    var getInternalState = internalStateGetterFor(CONSTRUCTOR_NAME);
                    var define = function(that, key, value) {
                        var state = getInternalState(that);
                        var data = getWeakData(anObject(key), true);
                        if (true === data) uncaughtFrozenStore(state).set(key, value); else data[state.id] = value;
                        return that;
                    };
                    redefineAll(Prototype, {
                        delete: function(key) {
                            var state = getInternalState(this);
                            if (!isObject(key)) return false;
                            var data = getWeakData(key);
                            if (true === data) return uncaughtFrozenStore(state)["delete"](key);
                            return data && hasOwn(data, state.id) && delete data[state.id];
                        },
                        has: function has(key) {
                            var state = getInternalState(this);
                            if (!isObject(key)) return false;
                            var data = getWeakData(key);
                            if (true === data) return uncaughtFrozenStore(state).has(key);
                            return data && hasOwn(data, state.id);
                        }
                    });
                    redefineAll(Prototype, IS_MAP ? {
                        get: function get(key) {
                            var state = getInternalState(this);
                            if (isObject(key)) {
                                var data = getWeakData(key);
                                if (true === data) return uncaughtFrozenStore(state).get(key);
                                return data ? data[state.id] : void 0;
                            }
                        },
                        set: function set(key, value) {
                            return define(this, key, value);
                        }
                    } : {
                        add: function add(value) {
                            return define(this, value, true);
                        }
                    });
                    return Constructor;
                }
            };
        },
        6645: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $ = __webpack_require__(4761);
            var global = __webpack_require__(8454);
            var uncurryThis = __webpack_require__(1768);
            var isForced = __webpack_require__(1949);
            var redefine = __webpack_require__(3971);
            var InternalMetadataModule = __webpack_require__(6582);
            var iterate = __webpack_require__(1518);
            var anInstance = __webpack_require__(3046);
            var isCallable = __webpack_require__(6282);
            var isObject = __webpack_require__(5896);
            var fails = __webpack_require__(6183);
            var checkCorrectnessOfIteration = __webpack_require__(1751);
            var setToStringTag = __webpack_require__(820);
            var inheritIfRequired = __webpack_require__(7770);
            module.exports = function(CONSTRUCTOR_NAME, wrapper, common) {
                var IS_MAP = -1 !== CONSTRUCTOR_NAME.indexOf("Map");
                var IS_WEAK = -1 !== CONSTRUCTOR_NAME.indexOf("Weak");
                var ADDER = IS_MAP ? "set" : "add";
                var NativeConstructor = global[CONSTRUCTOR_NAME];
                var NativePrototype = NativeConstructor && NativeConstructor.prototype;
                var Constructor = NativeConstructor;
                var exported = {};
                var fixMethod = function(KEY) {
                    var uncurriedNativeMethod = uncurryThis(NativePrototype[KEY]);
                    redefine(NativePrototype, KEY, "add" == KEY ? function add(value) {
                        uncurriedNativeMethod(this, 0 === value ? 0 : value);
                        return this;
                    } : "delete" == KEY ? function(key) {
                        return IS_WEAK && !isObject(key) ? false : uncurriedNativeMethod(this, 0 === key ? 0 : key);
                    } : "get" == KEY ? function get(key) {
                        return IS_WEAK && !isObject(key) ? void 0 : uncurriedNativeMethod(this, 0 === key ? 0 : key);
                    } : "has" == KEY ? function has(key) {
                        return IS_WEAK && !isObject(key) ? false : uncurriedNativeMethod(this, 0 === key ? 0 : key);
                    } : function set(key, value) {
                        uncurriedNativeMethod(this, 0 === key ? 0 : key, value);
                        return this;
                    });
                };
                var REPLACE = isForced(CONSTRUCTOR_NAME, !isCallable(NativeConstructor) || !(IS_WEAK || NativePrototype.forEach && !fails((function() {
                    (new NativeConstructor).entries().next();
                }))));
                if (REPLACE) {
                    Constructor = common.getConstructor(wrapper, CONSTRUCTOR_NAME, IS_MAP, ADDER);
                    InternalMetadataModule.enable();
                } else if (isForced(CONSTRUCTOR_NAME, true)) {
                    var instance = new Constructor;
                    var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
                    var THROWS_ON_PRIMITIVES = fails((function() {
                        instance.has(1);
                    }));
                    var ACCEPT_ITERABLES = checkCorrectnessOfIteration((function(iterable) {
                        new NativeConstructor(iterable);
                    }));
                    var BUGGY_ZERO = !IS_WEAK && fails((function() {
                        var $instance = new NativeConstructor;
                        var index = 5;
                        while (index--) $instance[ADDER](index, index);
                        return !$instance.has(-0);
                    }));
                    if (!ACCEPT_ITERABLES) {
                        Constructor = wrapper((function(dummy, iterable) {
                            anInstance(dummy, NativePrototype);
                            var that = inheritIfRequired(new NativeConstructor, dummy, Constructor);
                            if (void 0 != iterable) iterate(iterable, that[ADDER], {
                                that,
                                AS_ENTRIES: IS_MAP
                            });
                            return that;
                        }));
                        Constructor.prototype = NativePrototype;
                        NativePrototype.constructor = Constructor;
                    }
                    if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
                        fixMethod("delete");
                        fixMethod("has");
                        IS_MAP && fixMethod("get");
                    }
                    if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);
                    if (IS_WEAK && NativePrototype.clear) delete NativePrototype.clear;
                }
                exported[CONSTRUCTOR_NAME] = Constructor;
                $({
                    global: true,
                    forced: Constructor != NativeConstructor
                }, exported);
                setToStringTag(Constructor, CONSTRUCTOR_NAME);
                if (!IS_WEAK) common.setStrong(Constructor, CONSTRUCTOR_NAME, IS_MAP);
                return Constructor;
            };
        },
        882: (module, __unused_webpack_exports, __webpack_require__) => {
            var hasOwn = __webpack_require__(8281);
            var ownKeys = __webpack_require__(1441);
            var getOwnPropertyDescriptorModule = __webpack_require__(5663);
            var definePropertyModule = __webpack_require__(9168);
            module.exports = function(target, source, exceptions) {
                var keys = ownKeys(source);
                var defineProperty = definePropertyModule.f;
                var getOwnPropertyDescriptor = getOwnPropertyDescriptorModule.f;
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    if (!hasOwn(target, key) && !(exceptions && hasOwn(exceptions, key))) defineProperty(target, key, getOwnPropertyDescriptor(source, key));
                }
            };
        },
        7401: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            module.exports = !fails((function() {
                function F() {}
                F.prototype.constructor = null;
                return Object.getPrototypeOf(new F) !== F.prototype;
            }));
        },
        2538: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var IteratorPrototype = __webpack_require__(6524).IteratorPrototype;
            var create = __webpack_require__(1525);
            var createPropertyDescriptor = __webpack_require__(9273);
            var setToStringTag = __webpack_require__(820);
            var Iterators = __webpack_require__(6126);
            var returnThis = function() {
                return this;
            };
            module.exports = function(IteratorConstructor, NAME, next, ENUMERABLE_NEXT) {
                var TO_STRING_TAG = NAME + " Iterator";
                IteratorConstructor.prototype = create(IteratorPrototype, {
                    next: createPropertyDescriptor(+!ENUMERABLE_NEXT, next)
                });
                setToStringTag(IteratorConstructor, TO_STRING_TAG, false, true);
                Iterators[TO_STRING_TAG] = returnThis;
                return IteratorConstructor;
            };
        },
        1501: (module, __unused_webpack_exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var definePropertyModule = __webpack_require__(9168);
            var createPropertyDescriptor = __webpack_require__(9273);
            module.exports = DESCRIPTORS ? function(object, key, value) {
                return definePropertyModule.f(object, key, createPropertyDescriptor(1, value));
            } : function(object, key, value) {
                object[key] = value;
                return object;
            };
        },
        9273: module => {
            module.exports = function(bitmap, value) {
                return {
                    enumerable: !(1 & bitmap),
                    configurable: !(2 & bitmap),
                    writable: !(4 & bitmap),
                    value
                };
            };
        },
        2759: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var toPropertyKey = __webpack_require__(2988);
            var definePropertyModule = __webpack_require__(9168);
            var createPropertyDescriptor = __webpack_require__(9273);
            module.exports = function(object, key, value) {
                var propertyKey = toPropertyKey(key);
                if (propertyKey in object) definePropertyModule.f(object, propertyKey, createPropertyDescriptor(0, value)); else object[propertyKey] = value;
            };
        },
        7583: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $ = __webpack_require__(4761);
            var call = __webpack_require__(4552);
            var IS_PURE = __webpack_require__(8977);
            var FunctionName = __webpack_require__(4530);
            var isCallable = __webpack_require__(6282);
            var createIteratorConstructor = __webpack_require__(2538);
            var getPrototypeOf = __webpack_require__(4204);
            var setPrototypeOf = __webpack_require__(5900);
            var setToStringTag = __webpack_require__(820);
            var createNonEnumerableProperty = __webpack_require__(1501);
            var redefine = __webpack_require__(3971);
            var wellKnownSymbol = __webpack_require__(8149);
            var Iterators = __webpack_require__(6126);
            var IteratorsCore = __webpack_require__(6524);
            var PROPER_FUNCTION_NAME = FunctionName.PROPER;
            var CONFIGURABLE_FUNCTION_NAME = FunctionName.CONFIGURABLE;
            var IteratorPrototype = IteratorsCore.IteratorPrototype;
            var BUGGY_SAFARI_ITERATORS = IteratorsCore.BUGGY_SAFARI_ITERATORS;
            var ITERATOR = wellKnownSymbol("iterator");
            var KEYS = "keys";
            var VALUES = "values";
            var ENTRIES = "entries";
            var returnThis = function() {
                return this;
            };
            module.exports = function(Iterable, NAME, IteratorConstructor, next, DEFAULT, IS_SET, FORCED) {
                createIteratorConstructor(IteratorConstructor, NAME, next);
                var getIterationMethod = function(KIND) {
                    if (KIND === DEFAULT && defaultIterator) return defaultIterator;
                    if (!BUGGY_SAFARI_ITERATORS && KIND in IterablePrototype) return IterablePrototype[KIND];
                    switch (KIND) {
                      case KEYS:
                        return function keys() {
                            return new IteratorConstructor(this, KIND);
                        };

                      case VALUES:
                        return function values() {
                            return new IteratorConstructor(this, KIND);
                        };

                      case ENTRIES:
                        return function entries() {
                            return new IteratorConstructor(this, KIND);
                        };
                    }
                    return function() {
                        return new IteratorConstructor(this);
                    };
                };
                var TO_STRING_TAG = NAME + " Iterator";
                var INCORRECT_VALUES_NAME = false;
                var IterablePrototype = Iterable.prototype;
                var nativeIterator = IterablePrototype[ITERATOR] || IterablePrototype["@@iterator"] || DEFAULT && IterablePrototype[DEFAULT];
                var defaultIterator = !BUGGY_SAFARI_ITERATORS && nativeIterator || getIterationMethod(DEFAULT);
                var anyNativeIterator = "Array" == NAME ? IterablePrototype.entries || nativeIterator : nativeIterator;
                var CurrentIteratorPrototype, methods, KEY;
                if (anyNativeIterator) {
                    CurrentIteratorPrototype = getPrototypeOf(anyNativeIterator.call(new Iterable));
                    if (CurrentIteratorPrototype !== Object.prototype && CurrentIteratorPrototype.next) {
                        if (!IS_PURE && getPrototypeOf(CurrentIteratorPrototype) !== IteratorPrototype) if (setPrototypeOf) setPrototypeOf(CurrentIteratorPrototype, IteratorPrototype); else if (!isCallable(CurrentIteratorPrototype[ITERATOR])) redefine(CurrentIteratorPrototype, ITERATOR, returnThis);
                        setToStringTag(CurrentIteratorPrototype, TO_STRING_TAG, true, true);
                        if (IS_PURE) Iterators[TO_STRING_TAG] = returnThis;
                    }
                }
                if (PROPER_FUNCTION_NAME && DEFAULT == VALUES && nativeIterator && nativeIterator.name !== VALUES) if (!IS_PURE && CONFIGURABLE_FUNCTION_NAME) createNonEnumerableProperty(IterablePrototype, "name", VALUES); else {
                    INCORRECT_VALUES_NAME = true;
                    defaultIterator = function values() {
                        return call(nativeIterator, this);
                    };
                }
                if (DEFAULT) {
                    methods = {
                        values: getIterationMethod(VALUES),
                        keys: IS_SET ? defaultIterator : getIterationMethod(KEYS),
                        entries: getIterationMethod(ENTRIES)
                    };
                    if (FORCED) {
                        for (KEY in methods) if (BUGGY_SAFARI_ITERATORS || INCORRECT_VALUES_NAME || !(KEY in IterablePrototype)) redefine(IterablePrototype, KEY, methods[KEY]);
                    } else $({
                        target: NAME,
                        proto: true,
                        forced: BUGGY_SAFARI_ITERATORS || INCORRECT_VALUES_NAME
                    }, methods);
                }
                if ((!IS_PURE || FORCED) && IterablePrototype[ITERATOR] !== defaultIterator) redefine(IterablePrototype, ITERATOR, defaultIterator, {
                    name: DEFAULT
                });
                Iterators[NAME] = defaultIterator;
                return methods;
            };
        },
        723: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            module.exports = !fails((function() {
                return 7 != Object.defineProperty({}, 1, {
                    get: function() {
                        return 7;
                    }
                })[1];
            }));
        },
        7282: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isObject = __webpack_require__(5896);
            var document = global.document;
            var EXISTS = isObject(document) && isObject(document.createElement);
            module.exports = function(it) {
                return EXISTS ? document.createElement(it) : {};
            };
        },
        6181: module => {
            module.exports = {
                CSSRuleList: 0,
                CSSStyleDeclaration: 0,
                CSSValueList: 0,
                ClientRectList: 0,
                DOMRectList: 0,
                DOMStringList: 0,
                DOMTokenList: 1,
                DataTransferItemList: 0,
                FileList: 0,
                HTMLAllCollection: 0,
                HTMLCollection: 0,
                HTMLFormElement: 0,
                HTMLSelectElement: 0,
                MediaList: 0,
                MimeTypeArray: 0,
                NamedNodeMap: 0,
                NodeList: 1,
                PaintRequestList: 0,
                Plugin: 0,
                PluginArray: 0,
                SVGLengthList: 0,
                SVGNumberList: 0,
                SVGPathSegList: 0,
                SVGPointList: 0,
                SVGStringList: 0,
                SVGTransformList: 0,
                SourceBufferList: 0,
                StyleSheetList: 0,
                TextTrackCueList: 0,
                TextTrackList: 0,
                TouchList: 0
            };
        },
        2387: (module, __unused_webpack_exports, __webpack_require__) => {
            var documentCreateElement = __webpack_require__(7282);
            var classList = documentCreateElement("span").classList;
            var DOMTokenListPrototype = classList && classList.constructor && classList.constructor.prototype;
            module.exports = DOMTokenListPrototype === Object.prototype ? void 0 : DOMTokenListPrototype;
        },
        7594: (module, __unused_webpack_exports, __webpack_require__) => {
            var classof = __webpack_require__(1510);
            var global = __webpack_require__(8454);
            module.exports = "process" == classof(global.process);
        },
        2543: (module, __unused_webpack_exports, __webpack_require__) => {
            var getBuiltIn = __webpack_require__(4991);
            module.exports = getBuiltIn("navigator", "userAgent") || "";
        },
        4324: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var userAgent = __webpack_require__(2543);
            var process = global.process;
            var Deno = global.Deno;
            var versions = process && process.versions || Deno && Deno.version;
            var v8 = versions && versions.v8;
            var match, version;
            if (v8) {
                match = v8.split(".");
                version = match[0] > 0 && match[0] < 4 ? 1 : +(match[0] + match[1]);
            }
            if (!version && userAgent) {
                match = userAgent.match(/Edge\/(\d+)/);
                if (!match || match[1] >= 74) {
                    match = userAgent.match(/Chrome\/(\d+)/);
                    if (match) version = +match[1];
                }
            }
            module.exports = version;
        },
        8409: module => {
            module.exports = [ "constructor", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "valueOf" ];
        },
        4761: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var getOwnPropertyDescriptor = __webpack_require__(5663).f;
            var createNonEnumerableProperty = __webpack_require__(1501);
            var redefine = __webpack_require__(3971);
            var setGlobal = __webpack_require__(7852);
            var copyConstructorProperties = __webpack_require__(882);
            var isForced = __webpack_require__(1949);
            module.exports = function(options, source) {
                var TARGET = options.target;
                var GLOBAL = options.global;
                var STATIC = options.stat;
                var FORCED, target, key, targetProperty, sourceProperty, descriptor;
                if (GLOBAL) target = global; else if (STATIC) target = global[TARGET] || setGlobal(TARGET, {}); else target = (global[TARGET] || {}).prototype;
                if (target) for (key in source) {
                    sourceProperty = source[key];
                    if (options.noTargetGet) {
                        descriptor = getOwnPropertyDescriptor(target, key);
                        targetProperty = descriptor && descriptor.value;
                    } else targetProperty = target[key];
                    FORCED = isForced(GLOBAL ? key : TARGET + (STATIC ? "." : "#") + key, options.forced);
                    if (!FORCED && void 0 !== targetProperty) {
                        if (typeof sourceProperty == typeof targetProperty) continue;
                        copyConstructorProperties(sourceProperty, targetProperty);
                    }
                    if (options.sham || targetProperty && targetProperty.sham) createNonEnumerableProperty(sourceProperty, "sham", true);
                    redefine(target, key, sourceProperty, options);
                }
            };
        },
        6183: module => {
            module.exports = function(exec) {
                try {
                    return !!exec();
                } catch (error) {
                    return true;
                }
            };
        },
        9696: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            __webpack_require__(9989);
            var uncurryThis = __webpack_require__(1768);
            var redefine = __webpack_require__(3971);
            var regexpExec = __webpack_require__(5510);
            var fails = __webpack_require__(6183);
            var wellKnownSymbol = __webpack_require__(8149);
            var createNonEnumerableProperty = __webpack_require__(1501);
            var SPECIES = wellKnownSymbol("species");
            var RegExpPrototype = RegExp.prototype;
            module.exports = function(KEY, exec, FORCED, SHAM) {
                var SYMBOL = wellKnownSymbol(KEY);
                var DELEGATES_TO_SYMBOL = !fails((function() {
                    var O = {};
                    O[SYMBOL] = function() {
                        return 7;
                    };
                    return 7 != ""[KEY](O);
                }));
                var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL && !fails((function() {
                    var execCalled = false;
                    var re = /a/;
                    if ("split" === KEY) {
                        re = {};
                        re.constructor = {};
                        re.constructor[SPECIES] = function() {
                            return re;
                        };
                        re.flags = "";
                        re[SYMBOL] = /./[SYMBOL];
                    }
                    re.exec = function() {
                        execCalled = true;
                        return null;
                    };
                    re[SYMBOL]("");
                    return !execCalled;
                }));
                if (!DELEGATES_TO_SYMBOL || !DELEGATES_TO_EXEC || FORCED) {
                    var uncurriedNativeRegExpMethod = uncurryThis(/./[SYMBOL]);
                    var methods = exec(SYMBOL, ""[KEY], (function(nativeMethod, regexp, str, arg2, forceStringMethod) {
                        var uncurriedNativeMethod = uncurryThis(nativeMethod);
                        var $exec = regexp.exec;
                        if ($exec === regexpExec || $exec === RegExpPrototype.exec) {
                            if (DELEGATES_TO_SYMBOL && !forceStringMethod) return {
                                done: true,
                                value: uncurriedNativeRegExpMethod(regexp, str, arg2)
                            };
                            return {
                                done: true,
                                value: uncurriedNativeMethod(str, regexp, arg2)
                            };
                        }
                        return {
                            done: false
                        };
                    }));
                    redefine(String.prototype, KEY, methods[0]);
                    redefine(RegExpPrototype, SYMBOL, methods[1]);
                }
                if (SHAM) createNonEnumerableProperty(RegExpPrototype[SYMBOL], "sham", true);
            };
        },
        3116: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            module.exports = !fails((function() {
                return Object.isExtensible(Object.preventExtensions({}));
            }));
        },
        6218: (module, __unused_webpack_exports, __webpack_require__) => {
            var NATIVE_BIND = __webpack_require__(160);
            var FunctionPrototype = Function.prototype;
            var apply = FunctionPrototype.apply;
            var call = FunctionPrototype.call;
            module.exports = "object" == typeof Reflect && Reflect.apply || (NATIVE_BIND ? call.bind(apply) : function() {
                return call.apply(apply, arguments);
            });
        },
        1098: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var aCallable = __webpack_require__(5055);
            var NATIVE_BIND = __webpack_require__(160);
            var bind = uncurryThis(uncurryThis.bind);
            module.exports = function(fn, that) {
                aCallable(fn);
                return void 0 === that ? fn : NATIVE_BIND ? bind(fn, that) : function() {
                    return fn.apply(that, arguments);
                };
            };
        },
        160: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            module.exports = !fails((function() {
                var test = function() {}.bind();
                return "function" != typeof test || test.hasOwnProperty("prototype");
            }));
        },
        4552: (module, __unused_webpack_exports, __webpack_require__) => {
            var NATIVE_BIND = __webpack_require__(160);
            var call = Function.prototype.call;
            module.exports = NATIVE_BIND ? call.bind(call) : function() {
                return call.apply(call, arguments);
            };
        },
        4530: (module, __unused_webpack_exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var hasOwn = __webpack_require__(8281);
            var FunctionPrototype = Function.prototype;
            var getDescriptor = DESCRIPTORS && Object.getOwnPropertyDescriptor;
            var EXISTS = hasOwn(FunctionPrototype, "name");
            var PROPER = EXISTS && "something" === function something() {}.name;
            var CONFIGURABLE = EXISTS && (!DESCRIPTORS || DESCRIPTORS && getDescriptor(FunctionPrototype, "name").configurable);
            module.exports = {
                EXISTS,
                PROPER,
                CONFIGURABLE
            };
        },
        1768: (module, __unused_webpack_exports, __webpack_require__) => {
            var NATIVE_BIND = __webpack_require__(160);
            var FunctionPrototype = Function.prototype;
            var bind = FunctionPrototype.bind;
            var call = FunctionPrototype.call;
            var uncurryThis = NATIVE_BIND && bind.bind(call, call);
            module.exports = NATIVE_BIND ? function(fn) {
                return fn && uncurryThis(fn);
            } : function(fn) {
                return fn && function() {
                    return call.apply(fn, arguments);
                };
            };
        },
        4991: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isCallable = __webpack_require__(6282);
            var aFunction = function(argument) {
                return isCallable(argument) ? argument : void 0;
            };
            module.exports = function(namespace, method) {
                return arguments.length < 2 ? aFunction(global[namespace]) : global[namespace] && global[namespace][method];
            };
        },
        650: (module, __unused_webpack_exports, __webpack_require__) => {
            var classof = __webpack_require__(9225);
            var getMethod = __webpack_require__(9827);
            var Iterators = __webpack_require__(6126);
            var wellKnownSymbol = __webpack_require__(8149);
            var ITERATOR = wellKnownSymbol("iterator");
            module.exports = function(it) {
                if (void 0 != it) return getMethod(it, ITERATOR) || getMethod(it, "@@iterator") || Iterators[classof(it)];
            };
        },
        7755: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var call = __webpack_require__(4552);
            var aCallable = __webpack_require__(5055);
            var anObject = __webpack_require__(1474);
            var tryToString = __webpack_require__(180);
            var getIteratorMethod = __webpack_require__(650);
            var TypeError = global.TypeError;
            module.exports = function(argument, usingIterator) {
                var iteratorMethod = arguments.length < 2 ? getIteratorMethod(argument) : usingIterator;
                if (aCallable(iteratorMethod)) return anObject(call(iteratorMethod, argument));
                throw TypeError(tryToString(argument) + " is not iterable");
            };
        },
        9827: (module, __unused_webpack_exports, __webpack_require__) => {
            var aCallable = __webpack_require__(5055);
            module.exports = function(V, P) {
                var func = V[P];
                return null == func ? void 0 : aCallable(func);
            };
        },
        4742: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var toObject = __webpack_require__(9473);
            var floor = Math.floor;
            var charAt = uncurryThis("".charAt);
            var replace = uncurryThis("".replace);
            var stringSlice = uncurryThis("".slice);
            var SUBSTITUTION_SYMBOLS = /\$([$&'`]|\d{1,2}|<[^>]*>)/g;
            var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&'`]|\d{1,2})/g;
            module.exports = function(matched, str, position, captures, namedCaptures, replacement) {
                var tailPos = position + matched.length;
                var m = captures.length;
                var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
                if (void 0 !== namedCaptures) {
                    namedCaptures = toObject(namedCaptures);
                    symbols = SUBSTITUTION_SYMBOLS;
                }
                return replace(replacement, symbols, (function(match, ch) {
                    var capture;
                    switch (charAt(ch, 0)) {
                      case "$":
                        return "$";

                      case "&":
                        return matched;

                      case "`":
                        return stringSlice(str, 0, position);

                      case "'":
                        return stringSlice(str, tailPos);

                      case "<":
                        capture = namedCaptures[stringSlice(ch, 1, -1)];
                        break;

                      default:
                        var n = +ch;
                        if (0 === n) return match;
                        if (n > m) {
                            var f = floor(n / 10);
                            if (0 === f) return match;
                            if (f <= m) return void 0 === captures[f - 1] ? charAt(ch, 1) : captures[f - 1] + charAt(ch, 1);
                            return match;
                        }
                        capture = captures[n - 1];
                    }
                    return void 0 === capture ? "" : capture;
                }));
            };
        },
        8454: (module, __unused_webpack_exports, __webpack_require__) => {
            var check = function(it) {
                return it && it.Math == Math && it;
            };
            module.exports = check("object" == typeof globalThis && globalThis) || check("object" == typeof window && window) || check("object" == typeof self && self) || check("object" == typeof __webpack_require__.g && __webpack_require__.g) || function() {
                return this;
            }() || Function("return this")();
        },
        8281: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var toObject = __webpack_require__(9473);
            var hasOwnProperty = uncurryThis({}.hasOwnProperty);
            module.exports = Object.hasOwn || function hasOwn(it, key) {
                return hasOwnProperty(toObject(it), key);
            };
        },
        4377: module => {
            module.exports = {};
        },
        7461: (module, __unused_webpack_exports, __webpack_require__) => {
            var getBuiltIn = __webpack_require__(4991);
            module.exports = getBuiltIn("document", "documentElement");
        },
        4985: (module, __unused_webpack_exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var fails = __webpack_require__(6183);
            var createElement = __webpack_require__(7282);
            module.exports = !DESCRIPTORS && !fails((function() {
                return 7 != Object.defineProperty(createElement("div"), "a", {
                    get: function() {
                        return 7;
                    }
                }).a;
            }));
        },
        7530: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var uncurryThis = __webpack_require__(1768);
            var fails = __webpack_require__(6183);
            var classof = __webpack_require__(1510);
            var Object = global.Object;
            var split = uncurryThis("".split);
            module.exports = fails((function() {
                return !Object("z").propertyIsEnumerable(0);
            })) ? function(it) {
                return "String" == classof(it) ? split(it, "") : Object(it);
            } : Object;
        },
        7770: (module, __unused_webpack_exports, __webpack_require__) => {
            var isCallable = __webpack_require__(6282);
            var isObject = __webpack_require__(5896);
            var setPrototypeOf = __webpack_require__(5900);
            module.exports = function($this, dummy, Wrapper) {
                var NewTarget, NewTargetPrototype;
                if (setPrototypeOf && isCallable(NewTarget = dummy.constructor) && NewTarget !== Wrapper && isObject(NewTargetPrototype = NewTarget.prototype) && NewTargetPrototype !== Wrapper.prototype) setPrototypeOf($this, NewTargetPrototype);
                return $this;
            };
        },
        6901: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var isCallable = __webpack_require__(6282);
            var store = __webpack_require__(2047);
            var functionToString = uncurryThis(Function.toString);
            if (!isCallable(store.inspectSource)) store.inspectSource = function(it) {
                return functionToString(it);
            };
            module.exports = store.inspectSource;
        },
        6582: (module, __unused_webpack_exports, __webpack_require__) => {
            var $ = __webpack_require__(4761);
            var uncurryThis = __webpack_require__(1768);
            var hiddenKeys = __webpack_require__(4377);
            var isObject = __webpack_require__(5896);
            var hasOwn = __webpack_require__(8281);
            var defineProperty = __webpack_require__(9168).f;
            var getOwnPropertyNamesModule = __webpack_require__(6785);
            var getOwnPropertyNamesExternalModule = __webpack_require__(6675);
            var isExtensible = __webpack_require__(6662);
            var uid = __webpack_require__(9059);
            var FREEZING = __webpack_require__(3116);
            var REQUIRED = false;
            var METADATA = uid("meta");
            var id = 0;
            var setMetadata = function(it) {
                defineProperty(it, METADATA, {
                    value: {
                        objectID: "O" + id++,
                        weakData: {}
                    }
                });
            };
            var fastKey = function(it, create) {
                if (!isObject(it)) return "symbol" == typeof it ? it : ("string" == typeof it ? "S" : "P") + it;
                if (!hasOwn(it, METADATA)) {
                    if (!isExtensible(it)) return "F";
                    if (!create) return "E";
                    setMetadata(it);
                }
                return it[METADATA].objectID;
            };
            var getWeakData = function(it, create) {
                if (!hasOwn(it, METADATA)) {
                    if (!isExtensible(it)) return true;
                    if (!create) return false;
                    setMetadata(it);
                }
                return it[METADATA].weakData;
            };
            var onFreeze = function(it) {
                if (FREEZING && REQUIRED && isExtensible(it) && !hasOwn(it, METADATA)) setMetadata(it);
                return it;
            };
            var enable = function() {
                meta.enable = function() {};
                REQUIRED = true;
                var getOwnPropertyNames = getOwnPropertyNamesModule.f;
                var splice = uncurryThis([].splice);
                var test = {};
                test[METADATA] = 1;
                if (getOwnPropertyNames(test).length) {
                    getOwnPropertyNamesModule.f = function(it) {
                        var result = getOwnPropertyNames(it);
                        for (var i = 0, length = result.length; i < length; i++) if (result[i] === METADATA) {
                            splice(result, i, 1);
                            break;
                        }
                        return result;
                    };
                    $({
                        target: "Object",
                        stat: true,
                        forced: true
                    }, {
                        getOwnPropertyNames: getOwnPropertyNamesExternalModule.f
                    });
                }
            };
            var meta = module.exports = {
                enable,
                fastKey,
                getWeakData,
                onFreeze
            };
            hiddenKeys[METADATA] = true;
        },
        1030: (module, __unused_webpack_exports, __webpack_require__) => {
            var NATIVE_WEAK_MAP = __webpack_require__(4404);
            var global = __webpack_require__(8454);
            var uncurryThis = __webpack_require__(1768);
            var isObject = __webpack_require__(5896);
            var createNonEnumerableProperty = __webpack_require__(1501);
            var hasOwn = __webpack_require__(8281);
            var shared = __webpack_require__(2047);
            var sharedKey = __webpack_require__(8873);
            var hiddenKeys = __webpack_require__(4377);
            var OBJECT_ALREADY_INITIALIZED = "Object already initialized";
            var TypeError = global.TypeError;
            var WeakMap = global.WeakMap;
            var set, get, has;
            var enforce = function(it) {
                return has(it) ? get(it) : set(it, {});
            };
            var getterFor = function(TYPE) {
                return function(it) {
                    var state;
                    if (!isObject(it) || (state = get(it)).type !== TYPE) throw TypeError("Incompatible receiver, " + TYPE + " required");
                    return state;
                };
            };
            if (NATIVE_WEAK_MAP || shared.state) {
                var store = shared.state || (shared.state = new WeakMap);
                var wmget = uncurryThis(store.get);
                var wmhas = uncurryThis(store.has);
                var wmset = uncurryThis(store.set);
                set = function(it, metadata) {
                    if (wmhas(store, it)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
                    metadata.facade = it;
                    wmset(store, it, metadata);
                    return metadata;
                };
                get = function(it) {
                    return wmget(store, it) || {};
                };
                has = function(it) {
                    return wmhas(store, it);
                };
            } else {
                var STATE = sharedKey("state");
                hiddenKeys[STATE] = true;
                set = function(it, metadata) {
                    if (hasOwn(it, STATE)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
                    metadata.facade = it;
                    createNonEnumerableProperty(it, STATE, metadata);
                    return metadata;
                };
                get = function(it) {
                    return hasOwn(it, STATE) ? it[STATE] : {};
                };
                has = function(it) {
                    return hasOwn(it, STATE);
                };
            }
            module.exports = {
                set,
                get,
                has,
                enforce,
                getterFor
            };
        },
        5859: (module, __unused_webpack_exports, __webpack_require__) => {
            var wellKnownSymbol = __webpack_require__(8149);
            var Iterators = __webpack_require__(6126);
            var ITERATOR = wellKnownSymbol("iterator");
            var ArrayPrototype = Array.prototype;
            module.exports = function(it) {
                return void 0 !== it && (Iterators.Array === it || ArrayPrototype[ITERATOR] === it);
            };
        },
        7931: (module, __unused_webpack_exports, __webpack_require__) => {
            var classof = __webpack_require__(1510);
            module.exports = Array.isArray || function isArray(argument) {
                return "Array" == classof(argument);
            };
        },
        6282: module => {
            module.exports = function(argument) {
                return "function" == typeof argument;
            };
        },
        2240: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var fails = __webpack_require__(6183);
            var isCallable = __webpack_require__(6282);
            var classof = __webpack_require__(9225);
            var getBuiltIn = __webpack_require__(4991);
            var inspectSource = __webpack_require__(6901);
            var noop = function() {};
            var empty = [];
            var construct = getBuiltIn("Reflect", "construct");
            var constructorRegExp = /^\s*(?:class|function)\b/;
            var exec = uncurryThis(constructorRegExp.exec);
            var INCORRECT_TO_STRING = !constructorRegExp.exec(noop);
            var isConstructorModern = function isConstructor(argument) {
                if (!isCallable(argument)) return false;
                try {
                    construct(noop, empty, argument);
                    return true;
                } catch (error) {
                    return false;
                }
            };
            var isConstructorLegacy = function isConstructor(argument) {
                if (!isCallable(argument)) return false;
                switch (classof(argument)) {
                  case "AsyncFunction":
                  case "GeneratorFunction":
                  case "AsyncGeneratorFunction":
                    return false;
                }
                try {
                    return INCORRECT_TO_STRING || !!exec(constructorRegExp, inspectSource(argument));
                } catch (error) {
                    return true;
                }
            };
            isConstructorLegacy.sham = true;
            module.exports = !construct || fails((function() {
                var called;
                return isConstructorModern(isConstructorModern.call) || !isConstructorModern(Object) || !isConstructorModern((function() {
                    called = true;
                })) || called;
            })) ? isConstructorLegacy : isConstructorModern;
        },
        1949: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            var isCallable = __webpack_require__(6282);
            var replacement = /#|\.prototype\./;
            var isForced = function(feature, detection) {
                var value = data[normalize(feature)];
                return value == POLYFILL ? true : value == NATIVE ? false : isCallable(detection) ? fails(detection) : !!detection;
            };
            var normalize = isForced.normalize = function(string) {
                return String(string).replace(replacement, ".").toLowerCase();
            };
            var data = isForced.data = {};
            var NATIVE = isForced.NATIVE = "N";
            var POLYFILL = isForced.POLYFILL = "P";
            module.exports = isForced;
        },
        5896: (module, __unused_webpack_exports, __webpack_require__) => {
            var isCallable = __webpack_require__(6282);
            module.exports = function(it) {
                return "object" == typeof it ? null !== it : isCallable(it);
            };
        },
        8977: module => {
            module.exports = false;
        },
        1527: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var getBuiltIn = __webpack_require__(4991);
            var isCallable = __webpack_require__(6282);
            var isPrototypeOf = __webpack_require__(1786);
            var USE_SYMBOL_AS_UID = __webpack_require__(4746);
            var Object = global.Object;
            module.exports = USE_SYMBOL_AS_UID ? function(it) {
                return "symbol" == typeof it;
            } : function(it) {
                var $Symbol = getBuiltIn("Symbol");
                return isCallable($Symbol) && isPrototypeOf($Symbol.prototype, Object(it));
            };
        },
        1518: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var bind = __webpack_require__(1098);
            var call = __webpack_require__(4552);
            var anObject = __webpack_require__(1474);
            var tryToString = __webpack_require__(180);
            var isArrayIteratorMethod = __webpack_require__(5859);
            var lengthOfArrayLike = __webpack_require__(1829);
            var isPrototypeOf = __webpack_require__(1786);
            var getIterator = __webpack_require__(7755);
            var getIteratorMethod = __webpack_require__(650);
            var iteratorClose = __webpack_require__(9193);
            var TypeError = global.TypeError;
            var Result = function(stopped, result) {
                this.stopped = stopped;
                this.result = result;
            };
            var ResultPrototype = Result.prototype;
            module.exports = function(iterable, unboundFunction, options) {
                var that = options && options.that;
                var AS_ENTRIES = !!(options && options.AS_ENTRIES);
                var IS_ITERATOR = !!(options && options.IS_ITERATOR);
                var INTERRUPTED = !!(options && options.INTERRUPTED);
                var fn = bind(unboundFunction, that);
                var iterator, iterFn, index, length, result, next, step;
                var stop = function(condition) {
                    if (iterator) iteratorClose(iterator, "normal", condition);
                    return new Result(true, condition);
                };
                var callFn = function(value) {
                    if (AS_ENTRIES) {
                        anObject(value);
                        return INTERRUPTED ? fn(value[0], value[1], stop) : fn(value[0], value[1]);
                    }
                    return INTERRUPTED ? fn(value, stop) : fn(value);
                };
                if (IS_ITERATOR) iterator = iterable; else {
                    iterFn = getIteratorMethod(iterable);
                    if (!iterFn) throw TypeError(tryToString(iterable) + " is not iterable");
                    if (isArrayIteratorMethod(iterFn)) {
                        for (index = 0, length = lengthOfArrayLike(iterable); length > index; index++) {
                            result = callFn(iterable[index]);
                            if (result && isPrototypeOf(ResultPrototype, result)) return result;
                        }
                        return new Result(false);
                    }
                    iterator = getIterator(iterable, iterFn);
                }
                next = iterator.next;
                while (!(step = call(next, iterator)).done) {
                    try {
                        result = callFn(step.value);
                    } catch (error) {
                        iteratorClose(iterator, "throw", error);
                    }
                    if ("object" == typeof result && result && isPrototypeOf(ResultPrototype, result)) return result;
                }
                return new Result(false);
            };
        },
        9193: (module, __unused_webpack_exports, __webpack_require__) => {
            var call = __webpack_require__(4552);
            var anObject = __webpack_require__(1474);
            var getMethod = __webpack_require__(9827);
            module.exports = function(iterator, kind, value) {
                var innerResult, innerError;
                anObject(iterator);
                try {
                    innerResult = getMethod(iterator, "return");
                    if (!innerResult) {
                        if ("throw" === kind) throw value;
                        return value;
                    }
                    innerResult = call(innerResult, iterator);
                } catch (error) {
                    innerError = true;
                    innerResult = error;
                }
                if ("throw" === kind) throw value;
                if (innerError) throw innerResult;
                anObject(innerResult);
                return value;
            };
        },
        6524: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var fails = __webpack_require__(6183);
            var isCallable = __webpack_require__(6282);
            var create = __webpack_require__(1525);
            var getPrototypeOf = __webpack_require__(4204);
            var redefine = __webpack_require__(3971);
            var wellKnownSymbol = __webpack_require__(8149);
            var IS_PURE = __webpack_require__(8977);
            var ITERATOR = wellKnownSymbol("iterator");
            var BUGGY_SAFARI_ITERATORS = false;
            var IteratorPrototype, PrototypeOfArrayIteratorPrototype, arrayIterator;
            if ([].keys) {
                arrayIterator = [].keys();
                if (!("next" in arrayIterator)) BUGGY_SAFARI_ITERATORS = true; else {
                    PrototypeOfArrayIteratorPrototype = getPrototypeOf(getPrototypeOf(arrayIterator));
                    if (PrototypeOfArrayIteratorPrototype !== Object.prototype) IteratorPrototype = PrototypeOfArrayIteratorPrototype;
                }
            }
            var NEW_ITERATOR_PROTOTYPE = void 0 == IteratorPrototype || fails((function() {
                var test = {};
                return IteratorPrototype[ITERATOR].call(test) !== test;
            }));
            if (NEW_ITERATOR_PROTOTYPE) IteratorPrototype = {}; else if (IS_PURE) IteratorPrototype = create(IteratorPrototype);
            if (!isCallable(IteratorPrototype[ITERATOR])) redefine(IteratorPrototype, ITERATOR, (function() {
                return this;
            }));
            module.exports = {
                IteratorPrototype,
                BUGGY_SAFARI_ITERATORS
            };
        },
        6126: module => {
            module.exports = {};
        },
        1829: (module, __unused_webpack_exports, __webpack_require__) => {
            var toLength = __webpack_require__(3917);
            module.exports = function(obj) {
                return toLength(obj.length);
            };
        },
        323: (module, __unused_webpack_exports, __webpack_require__) => {
            var V8_VERSION = __webpack_require__(4324);
            var fails = __webpack_require__(6183);
            module.exports = !!Object.getOwnPropertySymbols && !fails((function() {
                var symbol = Symbol();
                return !String(symbol) || !(Object(symbol) instanceof Symbol) || !Symbol.sham && V8_VERSION && V8_VERSION < 41;
            }));
        },
        4404: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isCallable = __webpack_require__(6282);
            var inspectSource = __webpack_require__(6901);
            var WeakMap = global.WeakMap;
            module.exports = isCallable(WeakMap) && /native code/.test(inspectSource(WeakMap));
        },
        8513: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var fails = __webpack_require__(6183);
            var uncurryThis = __webpack_require__(1768);
            var toString = __webpack_require__(7655);
            var trim = __webpack_require__(9749).trim;
            var whitespaces = __webpack_require__(8342);
            var $parseInt = global.parseInt;
            var Symbol = global.Symbol;
            var ITERATOR = Symbol && Symbol.iterator;
            var hex = /^[+-]?0x/i;
            var exec = uncurryThis(hex.exec);
            var FORCED = 8 !== $parseInt(whitespaces + "08") || 22 !== $parseInt(whitespaces + "0x16") || ITERATOR && !fails((function() {
                $parseInt(Object(ITERATOR));
            }));
            module.exports = FORCED ? function parseInt(string, radix) {
                var S = trim(toString(string));
                return $parseInt(S, radix >>> 0 || (exec(hex, S) ? 16 : 10));
            } : $parseInt;
        },
        4727: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var DESCRIPTORS = __webpack_require__(723);
            var uncurryThis = __webpack_require__(1768);
            var call = __webpack_require__(4552);
            var fails = __webpack_require__(6183);
            var objectKeys = __webpack_require__(1340);
            var getOwnPropertySymbolsModule = __webpack_require__(8074);
            var propertyIsEnumerableModule = __webpack_require__(4043);
            var toObject = __webpack_require__(9473);
            var IndexedObject = __webpack_require__(7530);
            var $assign = Object.assign;
            var defineProperty = Object.defineProperty;
            var concat = uncurryThis([].concat);
            module.exports = !$assign || fails((function() {
                if (DESCRIPTORS && 1 !== $assign({
                    b: 1
                }, $assign(defineProperty({}, "a", {
                    enumerable: true,
                    get: function() {
                        defineProperty(this, "b", {
                            value: 3,
                            enumerable: false
                        });
                    }
                }), {
                    b: 2
                })).b) return true;
                var A = {};
                var B = {};
                var symbol = Symbol();
                var alphabet = "abcdefghijklmnopqrst";
                A[symbol] = 7;
                alphabet.split("").forEach((function(chr) {
                    B[chr] = chr;
                }));
                return 7 != $assign({}, A)[symbol] || objectKeys($assign({}, B)).join("") != alphabet;
            })) ? function assign(target, source) {
                var T = toObject(target);
                var argumentsLength = arguments.length;
                var index = 1;
                var getOwnPropertySymbols = getOwnPropertySymbolsModule.f;
                var propertyIsEnumerable = propertyIsEnumerableModule.f;
                while (argumentsLength > index) {
                    var S = IndexedObject(arguments[index++]);
                    var keys = getOwnPropertySymbols ? concat(objectKeys(S), getOwnPropertySymbols(S)) : objectKeys(S);
                    var length = keys.length;
                    var j = 0;
                    var key;
                    while (length > j) {
                        key = keys[j++];
                        if (!DESCRIPTORS || call(propertyIsEnumerable, S, key)) T[key] = S[key];
                    }
                }
                return T;
            } : $assign;
        },
        1525: (module, __unused_webpack_exports, __webpack_require__) => {
            var anObject = __webpack_require__(1474);
            var definePropertiesModule = __webpack_require__(262);
            var enumBugKeys = __webpack_require__(8409);
            var hiddenKeys = __webpack_require__(4377);
            var html = __webpack_require__(7461);
            var documentCreateElement = __webpack_require__(7282);
            var sharedKey = __webpack_require__(8873);
            var GT = ">";
            var LT = "<";
            var PROTOTYPE = "prototype";
            var SCRIPT = "script";
            var IE_PROTO = sharedKey("IE_PROTO");
            var EmptyConstructor = function() {};
            var scriptTag = function(content) {
                return LT + SCRIPT + GT + content + LT + "/" + SCRIPT + GT;
            };
            var NullProtoObjectViaActiveX = function(activeXDocument) {
                activeXDocument.write(scriptTag(""));
                activeXDocument.close();
                var temp = activeXDocument.parentWindow.Object;
                activeXDocument = null;
                return temp;
            };
            var NullProtoObjectViaIFrame = function() {
                var iframe = documentCreateElement("iframe");
                var JS = "java" + SCRIPT + ":";
                var iframeDocument;
                iframe.style.display = "none";
                html.appendChild(iframe);
                iframe.src = String(JS);
                iframeDocument = iframe.contentWindow.document;
                iframeDocument.open();
                iframeDocument.write(scriptTag("document.F=Object"));
                iframeDocument.close();
                return iframeDocument.F;
            };
            var activeXDocument;
            var NullProtoObject = function() {
                try {
                    activeXDocument = new ActiveXObject("htmlfile");
                } catch (error) {}
                NullProtoObject = "undefined" != typeof document ? document.domain && activeXDocument ? NullProtoObjectViaActiveX(activeXDocument) : NullProtoObjectViaIFrame() : NullProtoObjectViaActiveX(activeXDocument);
                var length = enumBugKeys.length;
                while (length--) delete NullProtoObject[PROTOTYPE][enumBugKeys[length]];
                return NullProtoObject();
            };
            hiddenKeys[IE_PROTO] = true;
            module.exports = Object.create || function create(O, Properties) {
                var result;
                if (null !== O) {
                    EmptyConstructor[PROTOTYPE] = anObject(O);
                    result = new EmptyConstructor;
                    EmptyConstructor[PROTOTYPE] = null;
                    result[IE_PROTO] = O;
                } else result = NullProtoObject();
                return void 0 === Properties ? result : definePropertiesModule.f(result, Properties);
            };
        },
        262: (__unused_webpack_module, exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var V8_PROTOTYPE_DEFINE_BUG = __webpack_require__(8654);
            var definePropertyModule = __webpack_require__(9168);
            var anObject = __webpack_require__(1474);
            var toIndexedObject = __webpack_require__(3206);
            var objectKeys = __webpack_require__(1340);
            exports.f = DESCRIPTORS && !V8_PROTOTYPE_DEFINE_BUG ? Object.defineProperties : function defineProperties(O, Properties) {
                anObject(O);
                var props = toIndexedObject(Properties);
                var keys = objectKeys(Properties);
                var length = keys.length;
                var index = 0;
                var key;
                while (length > index) definePropertyModule.f(O, key = keys[index++], props[key]);
                return O;
            };
        },
        9168: (__unused_webpack_module, exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var DESCRIPTORS = __webpack_require__(723);
            var IE8_DOM_DEFINE = __webpack_require__(4985);
            var V8_PROTOTYPE_DEFINE_BUG = __webpack_require__(8654);
            var anObject = __webpack_require__(1474);
            var toPropertyKey = __webpack_require__(2988);
            var TypeError = global.TypeError;
            var $defineProperty = Object.defineProperty;
            var $getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
            var ENUMERABLE = "enumerable";
            var CONFIGURABLE = "configurable";
            var WRITABLE = "writable";
            exports.f = DESCRIPTORS ? V8_PROTOTYPE_DEFINE_BUG ? function defineProperty(O, P, Attributes) {
                anObject(O);
                P = toPropertyKey(P);
                anObject(Attributes);
                if ("function" === typeof O && "prototype" === P && "value" in Attributes && WRITABLE in Attributes && !Attributes[WRITABLE]) {
                    var current = $getOwnPropertyDescriptor(O, P);
                    if (current && current[WRITABLE]) {
                        O[P] = Attributes.value;
                        Attributes = {
                            configurable: CONFIGURABLE in Attributes ? Attributes[CONFIGURABLE] : current[CONFIGURABLE],
                            enumerable: ENUMERABLE in Attributes ? Attributes[ENUMERABLE] : current[ENUMERABLE],
                            writable: false
                        };
                    }
                }
                return $defineProperty(O, P, Attributes);
            } : $defineProperty : function defineProperty(O, P, Attributes) {
                anObject(O);
                P = toPropertyKey(P);
                anObject(Attributes);
                if (IE8_DOM_DEFINE) try {
                    return $defineProperty(O, P, Attributes);
                } catch (error) {}
                if ("get" in Attributes || "set" in Attributes) throw TypeError("Accessors not supported");
                if ("value" in Attributes) O[P] = Attributes.value;
                return O;
            };
        },
        5663: (__unused_webpack_module, exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var call = __webpack_require__(4552);
            var propertyIsEnumerableModule = __webpack_require__(4043);
            var createPropertyDescriptor = __webpack_require__(9273);
            var toIndexedObject = __webpack_require__(3206);
            var toPropertyKey = __webpack_require__(2988);
            var hasOwn = __webpack_require__(8281);
            var IE8_DOM_DEFINE = __webpack_require__(4985);
            var $getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
            exports.f = DESCRIPTORS ? $getOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
                O = toIndexedObject(O);
                P = toPropertyKey(P);
                if (IE8_DOM_DEFINE) try {
                    return $getOwnPropertyDescriptor(O, P);
                } catch (error) {}
                if (hasOwn(O, P)) return createPropertyDescriptor(!call(propertyIsEnumerableModule.f, O, P), O[P]);
            };
        },
        6675: (module, __unused_webpack_exports, __webpack_require__) => {
            var classof = __webpack_require__(1510);
            var toIndexedObject = __webpack_require__(3206);
            var $getOwnPropertyNames = __webpack_require__(6785).f;
            var arraySlice = __webpack_require__(4072);
            var windowNames = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [];
            var getWindowNames = function(it) {
                try {
                    return $getOwnPropertyNames(it);
                } catch (error) {
                    return arraySlice(windowNames);
                }
            };
            module.exports.f = function getOwnPropertyNames(it) {
                return windowNames && "Window" == classof(it) ? getWindowNames(it) : $getOwnPropertyNames(toIndexedObject(it));
            };
        },
        6785: (__unused_webpack_module, exports, __webpack_require__) => {
            var internalObjectKeys = __webpack_require__(5113);
            var enumBugKeys = __webpack_require__(8409);
            var hiddenKeys = enumBugKeys.concat("length", "prototype");
            exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
                return internalObjectKeys(O, hiddenKeys);
            };
        },
        8074: (__unused_webpack_module, exports) => {
            exports.f = Object.getOwnPropertySymbols;
        },
        4204: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var hasOwn = __webpack_require__(8281);
            var isCallable = __webpack_require__(6282);
            var toObject = __webpack_require__(9473);
            var sharedKey = __webpack_require__(8873);
            var CORRECT_PROTOTYPE_GETTER = __webpack_require__(7401);
            var IE_PROTO = sharedKey("IE_PROTO");
            var Object = global.Object;
            var ObjectPrototype = Object.prototype;
            module.exports = CORRECT_PROTOTYPE_GETTER ? Object.getPrototypeOf : function(O) {
                var object = toObject(O);
                if (hasOwn(object, IE_PROTO)) return object[IE_PROTO];
                var constructor = object.constructor;
                if (isCallable(constructor) && object instanceof constructor) return constructor.prototype;
                return object instanceof Object ? ObjectPrototype : null;
            };
        },
        6662: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            var isObject = __webpack_require__(5896);
            var classof = __webpack_require__(1510);
            var ARRAY_BUFFER_NON_EXTENSIBLE = __webpack_require__(8774);
            var $isExtensible = Object.isExtensible;
            var FAILS_ON_PRIMITIVES = fails((function() {
                $isExtensible(1);
            }));
            module.exports = FAILS_ON_PRIMITIVES || ARRAY_BUFFER_NON_EXTENSIBLE ? function isExtensible(it) {
                if (!isObject(it)) return false;
                if (ARRAY_BUFFER_NON_EXTENSIBLE && "ArrayBuffer" == classof(it)) return false;
                return $isExtensible ? $isExtensible(it) : true;
            } : $isExtensible;
        },
        1786: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            module.exports = uncurryThis({}.isPrototypeOf);
        },
        5113: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var hasOwn = __webpack_require__(8281);
            var toIndexedObject = __webpack_require__(3206);
            var indexOf = __webpack_require__(5675).indexOf;
            var hiddenKeys = __webpack_require__(4377);
            var push = uncurryThis([].push);
            module.exports = function(object, names) {
                var O = toIndexedObject(object);
                var i = 0;
                var result = [];
                var key;
                for (key in O) !hasOwn(hiddenKeys, key) && hasOwn(O, key) && push(result, key);
                while (names.length > i) if (hasOwn(O, key = names[i++])) ~indexOf(result, key) || push(result, key);
                return result;
            };
        },
        1340: (module, __unused_webpack_exports, __webpack_require__) => {
            var internalObjectKeys = __webpack_require__(5113);
            var enumBugKeys = __webpack_require__(8409);
            module.exports = Object.keys || function keys(O) {
                return internalObjectKeys(O, enumBugKeys);
            };
        },
        4043: (__unused_webpack_module, exports) => {
            "use strict";
            var $propertyIsEnumerable = {}.propertyIsEnumerable;
            var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
            var NASHORN_BUG = getOwnPropertyDescriptor && !$propertyIsEnumerable.call({
                1: 2
            }, 1);
            exports.f = NASHORN_BUG ? function propertyIsEnumerable(V) {
                var descriptor = getOwnPropertyDescriptor(this, V);
                return !!descriptor && descriptor.enumerable;
            } : $propertyIsEnumerable;
        },
        5900: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var anObject = __webpack_require__(1474);
            var aPossiblePrototype = __webpack_require__(2004);
            module.exports = Object.setPrototypeOf || ("__proto__" in {} ? function() {
                var CORRECT_SETTER = false;
                var test = {};
                var setter;
                try {
                    setter = uncurryThis(Object.getOwnPropertyDescriptor(Object.prototype, "__proto__").set);
                    setter(test, []);
                    CORRECT_SETTER = test instanceof Array;
                } catch (error) {}
                return function setPrototypeOf(O, proto) {
                    anObject(O);
                    aPossiblePrototype(proto);
                    if (CORRECT_SETTER) setter(O, proto); else O.__proto__ = proto;
                    return O;
                };
            }() : void 0);
        },
        4117: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var TO_STRING_TAG_SUPPORT = __webpack_require__(4823);
            var classof = __webpack_require__(9225);
            module.exports = TO_STRING_TAG_SUPPORT ? {}.toString : function toString() {
                return "[object " + classof(this) + "]";
            };
        },
        6891: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var call = __webpack_require__(4552);
            var isCallable = __webpack_require__(6282);
            var isObject = __webpack_require__(5896);
            var TypeError = global.TypeError;
            module.exports = function(input, pref) {
                var fn, val;
                if ("string" === pref && isCallable(fn = input.toString) && !isObject(val = call(fn, input))) return val;
                if (isCallable(fn = input.valueOf) && !isObject(val = call(fn, input))) return val;
                if ("string" !== pref && isCallable(fn = input.toString) && !isObject(val = call(fn, input))) return val;
                throw TypeError("Can't convert object to primitive value");
            };
        },
        1441: (module, __unused_webpack_exports, __webpack_require__) => {
            var getBuiltIn = __webpack_require__(4991);
            var uncurryThis = __webpack_require__(1768);
            var getOwnPropertyNamesModule = __webpack_require__(6785);
            var getOwnPropertySymbolsModule = __webpack_require__(8074);
            var anObject = __webpack_require__(1474);
            var concat = uncurryThis([].concat);
            module.exports = getBuiltIn("Reflect", "ownKeys") || function ownKeys(it) {
                var keys = getOwnPropertyNamesModule.f(anObject(it));
                var getOwnPropertySymbols = getOwnPropertySymbolsModule.f;
                return getOwnPropertySymbols ? concat(keys, getOwnPropertySymbols(it)) : keys;
            };
        },
        9573: (module, __unused_webpack_exports, __webpack_require__) => {
            var redefine = __webpack_require__(3971);
            module.exports = function(target, src, options) {
                for (var key in src) redefine(target, key, src[key], options);
                return target;
            };
        },
        3971: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var isCallable = __webpack_require__(6282);
            var hasOwn = __webpack_require__(8281);
            var createNonEnumerableProperty = __webpack_require__(1501);
            var setGlobal = __webpack_require__(7852);
            var inspectSource = __webpack_require__(6901);
            var InternalStateModule = __webpack_require__(1030);
            var CONFIGURABLE_FUNCTION_NAME = __webpack_require__(4530).CONFIGURABLE;
            var getInternalState = InternalStateModule.get;
            var enforceInternalState = InternalStateModule.enforce;
            var TEMPLATE = String(String).split("String");
            (module.exports = function(O, key, value, options) {
                var unsafe = options ? !!options.unsafe : false;
                var simple = options ? !!options.enumerable : false;
                var noTargetGet = options ? !!options.noTargetGet : false;
                var name = options && void 0 !== options.name ? options.name : key;
                var state;
                if (isCallable(value)) {
                    if ("Symbol(" === String(name).slice(0, 7)) name = "[" + String(name).replace(/^Symbol\(([^)]*)\)/, "$1") + "]";
                    if (!hasOwn(value, "name") || CONFIGURABLE_FUNCTION_NAME && value.name !== name) createNonEnumerableProperty(value, "name", name);
                    state = enforceInternalState(value);
                    if (!state.source) state.source = TEMPLATE.join("string" == typeof name ? name : "");
                }
                if (O === global) {
                    if (simple) O[key] = value; else setGlobal(key, value);
                    return;
                } else if (!unsafe) delete O[key]; else if (!noTargetGet && O[key]) simple = true;
                if (simple) O[key] = value; else createNonEnumerableProperty(O, key, value);
            })(Function.prototype, "toString", (function toString() {
                return isCallable(this) && getInternalState(this).source || inspectSource(this);
            }));
        },
        8734: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var call = __webpack_require__(4552);
            var anObject = __webpack_require__(1474);
            var isCallable = __webpack_require__(6282);
            var classof = __webpack_require__(1510);
            var regexpExec = __webpack_require__(5510);
            var TypeError = global.TypeError;
            module.exports = function(R, S) {
                var exec = R.exec;
                if (isCallable(exec)) {
                    var result = call(exec, R, S);
                    if (null !== result) anObject(result);
                    return result;
                }
                if ("RegExp" === classof(R)) return call(regexpExec, R, S);
                throw TypeError("RegExp#exec called on incompatible receiver");
            };
        },
        5510: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var call = __webpack_require__(4552);
            var uncurryThis = __webpack_require__(1768);
            var toString = __webpack_require__(7655);
            var regexpFlags = __webpack_require__(8383);
            var stickyHelpers = __webpack_require__(6558);
            var shared = __webpack_require__(1748);
            var create = __webpack_require__(1525);
            var getInternalState = __webpack_require__(1030).get;
            var UNSUPPORTED_DOT_ALL = __webpack_require__(7672);
            var UNSUPPORTED_NCG = __webpack_require__(9729);
            var nativeReplace = shared("native-string-replace", String.prototype.replace);
            var nativeExec = RegExp.prototype.exec;
            var patchedExec = nativeExec;
            var charAt = uncurryThis("".charAt);
            var indexOf = uncurryThis("".indexOf);
            var replace = uncurryThis("".replace);
            var stringSlice = uncurryThis("".slice);
            var UPDATES_LAST_INDEX_WRONG = function() {
                var re1 = /a/;
                var re2 = /b*/g;
                call(nativeExec, re1, "a");
                call(nativeExec, re2, "a");
                return 0 !== re1.lastIndex || 0 !== re2.lastIndex;
            }();
            var UNSUPPORTED_Y = stickyHelpers.BROKEN_CARET;
            var NPCG_INCLUDED = void 0 !== /()??/.exec("")[1];
            var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED || UNSUPPORTED_Y || UNSUPPORTED_DOT_ALL || UNSUPPORTED_NCG;
            if (PATCH) patchedExec = function exec(string) {
                var re = this;
                var state = getInternalState(re);
                var str = toString(string);
                var raw = state.raw;
                var result, reCopy, lastIndex, match, i, object, group;
                if (raw) {
                    raw.lastIndex = re.lastIndex;
                    result = call(patchedExec, raw, str);
                    re.lastIndex = raw.lastIndex;
                    return result;
                }
                var groups = state.groups;
                var sticky = UNSUPPORTED_Y && re.sticky;
                var flags = call(regexpFlags, re);
                var source = re.source;
                var charsAdded = 0;
                var strCopy = str;
                if (sticky) {
                    flags = replace(flags, "y", "");
                    if (-1 === indexOf(flags, "g")) flags += "g";
                    strCopy = stringSlice(str, re.lastIndex);
                    if (re.lastIndex > 0 && (!re.multiline || re.multiline && "\n" !== charAt(str, re.lastIndex - 1))) {
                        source = "(?: " + source + ")";
                        strCopy = " " + strCopy;
                        charsAdded++;
                    }
                    reCopy = new RegExp("^(?:" + source + ")", flags);
                }
                if (NPCG_INCLUDED) reCopy = new RegExp("^" + source + "$(?!\\s)", flags);
                if (UPDATES_LAST_INDEX_WRONG) lastIndex = re.lastIndex;
                match = call(nativeExec, sticky ? reCopy : re, strCopy);
                if (sticky) if (match) {
                    match.input = stringSlice(match.input, charsAdded);
                    match[0] = stringSlice(match[0], charsAdded);
                    match.index = re.lastIndex;
                    re.lastIndex += match[0].length;
                } else re.lastIndex = 0; else if (UPDATES_LAST_INDEX_WRONG && match) re.lastIndex = re.global ? match.index + match[0].length : lastIndex;
                if (NPCG_INCLUDED && match && match.length > 1) call(nativeReplace, match[0], reCopy, (function() {
                    for (i = 1; i < arguments.length - 2; i++) if (void 0 === arguments[i]) match[i] = void 0;
                }));
                if (match && groups) {
                    match.groups = object = create(null);
                    for (i = 0; i < groups.length; i++) {
                        group = groups[i];
                        object[group[0]] = match[group[1]];
                    }
                }
                return match;
            };
            module.exports = patchedExec;
        },
        8383: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var anObject = __webpack_require__(1474);
            module.exports = function() {
                var that = anObject(this);
                var result = "";
                if (that.global) result += "g";
                if (that.ignoreCase) result += "i";
                if (that.multiline) result += "m";
                if (that.dotAll) result += "s";
                if (that.unicode) result += "u";
                if (that.sticky) result += "y";
                return result;
            };
        },
        6558: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            var global = __webpack_require__(8454);
            var $RegExp = global.RegExp;
            var UNSUPPORTED_Y = fails((function() {
                var re = $RegExp("a", "y");
                re.lastIndex = 2;
                return null != re.exec("abcd");
            }));
            var MISSED_STICKY = UNSUPPORTED_Y || fails((function() {
                return !$RegExp("a", "y").sticky;
            }));
            var BROKEN_CARET = UNSUPPORTED_Y || fails((function() {
                var re = $RegExp("^r", "gy");
                re.lastIndex = 2;
                return null != re.exec("str");
            }));
            module.exports = {
                BROKEN_CARET,
                MISSED_STICKY,
                UNSUPPORTED_Y
            };
        },
        7672: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            var global = __webpack_require__(8454);
            var $RegExp = global.RegExp;
            module.exports = fails((function() {
                var re = $RegExp(".", "s");
                return !(re.dotAll && re.exec("\n") && "s" === re.flags);
            }));
        },
        9729: (module, __unused_webpack_exports, __webpack_require__) => {
            var fails = __webpack_require__(6183);
            var global = __webpack_require__(8454);
            var $RegExp = global.RegExp;
            module.exports = fails((function() {
                var re = $RegExp("(?<a>b)", "g");
                return "b" !== re.exec("b").groups.a || "bc" !== "b".replace(re, "$<a>c");
            }));
        },
        7431: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var TypeError = global.TypeError;
            module.exports = function(it) {
                if (void 0 == it) throw TypeError("Can't call method on " + it);
                return it;
            };
        },
        7852: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var defineProperty = Object.defineProperty;
            module.exports = function(key, value) {
                try {
                    defineProperty(global, key, {
                        value,
                        configurable: true,
                        writable: true
                    });
                } catch (error) {
                    global[key] = value;
                }
                return value;
            };
        },
        820: (module, __unused_webpack_exports, __webpack_require__) => {
            var defineProperty = __webpack_require__(9168).f;
            var hasOwn = __webpack_require__(8281);
            var wellKnownSymbol = __webpack_require__(8149);
            var TO_STRING_TAG = wellKnownSymbol("toStringTag");
            module.exports = function(target, TAG, STATIC) {
                if (target && !STATIC) target = target.prototype;
                if (target && !hasOwn(target, TO_STRING_TAG)) defineProperty(target, TO_STRING_TAG, {
                    configurable: true,
                    value: TAG
                });
            };
        },
        8873: (module, __unused_webpack_exports, __webpack_require__) => {
            var shared = __webpack_require__(1748);
            var uid = __webpack_require__(9059);
            var keys = shared("keys");
            module.exports = function(key) {
                return keys[key] || (keys[key] = uid(key));
            };
        },
        2047: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var setGlobal = __webpack_require__(7852);
            var SHARED = "__core-js_shared__";
            var store = global[SHARED] || setGlobal(SHARED, {});
            module.exports = store;
        },
        1748: (module, __unused_webpack_exports, __webpack_require__) => {
            var IS_PURE = __webpack_require__(8977);
            var store = __webpack_require__(2047);
            (module.exports = function(key, value) {
                return store[key] || (store[key] = void 0 !== value ? value : {});
            })("versions", []).push({
                version: "3.21.0",
                mode: IS_PURE ? "pure" : "global",
                copyright: "© 2014-2022 Denis Pushkarev (zloirock.ru)",
                license: "https://github.com/zloirock/core-js/blob/v3.21.0/LICENSE",
                source: "https://github.com/zloirock/core-js"
            });
        },
        7321: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var toIntegerOrInfinity = __webpack_require__(8037);
            var toString = __webpack_require__(7655);
            var requireObjectCoercible = __webpack_require__(7431);
            var charAt = uncurryThis("".charAt);
            var charCodeAt = uncurryThis("".charCodeAt);
            var stringSlice = uncurryThis("".slice);
            var createMethod = function(CONVERT_TO_STRING) {
                return function($this, pos) {
                    var S = toString(requireObjectCoercible($this));
                    var position = toIntegerOrInfinity(pos);
                    var size = S.length;
                    var first, second;
                    if (position < 0 || position >= size) return CONVERT_TO_STRING ? "" : void 0;
                    first = charCodeAt(S, position);
                    return first < 55296 || first > 56319 || position + 1 === size || (second = charCodeAt(S, position + 1)) < 56320 || second > 57343 ? CONVERT_TO_STRING ? charAt(S, position) : first : CONVERT_TO_STRING ? stringSlice(S, position, position + 2) : (first - 55296 << 10) + (second - 56320) + 65536;
                };
            };
            module.exports = {
                codeAt: createMethod(false),
                charAt: createMethod(true)
            };
        },
        9749: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var requireObjectCoercible = __webpack_require__(7431);
            var toString = __webpack_require__(7655);
            var whitespaces = __webpack_require__(8342);
            var replace = uncurryThis("".replace);
            var whitespace = "[" + whitespaces + "]";
            var ltrim = RegExp("^" + whitespace + whitespace + "*");
            var rtrim = RegExp(whitespace + whitespace + "*$");
            var createMethod = function(TYPE) {
                return function($this) {
                    var string = toString(requireObjectCoercible($this));
                    if (1 & TYPE) string = replace(string, ltrim, "");
                    if (2 & TYPE) string = replace(string, rtrim, "");
                    return string;
                };
            };
            module.exports = {
                start: createMethod(1),
                end: createMethod(2),
                trim: createMethod(3)
            };
        },
        9623: (module, __unused_webpack_exports, __webpack_require__) => {
            var toIntegerOrInfinity = __webpack_require__(8037);
            var max = Math.max;
            var min = Math.min;
            module.exports = function(index, length) {
                var integer = toIntegerOrInfinity(index);
                return integer < 0 ? max(integer + length, 0) : min(integer, length);
            };
        },
        3206: (module, __unused_webpack_exports, __webpack_require__) => {
            var IndexedObject = __webpack_require__(7530);
            var requireObjectCoercible = __webpack_require__(7431);
            module.exports = function(it) {
                return IndexedObject(requireObjectCoercible(it));
            };
        },
        8037: module => {
            var ceil = Math.ceil;
            var floor = Math.floor;
            module.exports = function(argument) {
                var number = +argument;
                return number !== number || 0 === number ? 0 : (number > 0 ? floor : ceil)(number);
            };
        },
        3917: (module, __unused_webpack_exports, __webpack_require__) => {
            var toIntegerOrInfinity = __webpack_require__(8037);
            var min = Math.min;
            module.exports = function(argument) {
                return argument > 0 ? min(toIntegerOrInfinity(argument), 9007199254740991) : 0;
            };
        },
        9473: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var requireObjectCoercible = __webpack_require__(7431);
            var Object = global.Object;
            module.exports = function(argument) {
                return Object(requireObjectCoercible(argument));
            };
        },
        3948: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var call = __webpack_require__(4552);
            var isObject = __webpack_require__(5896);
            var isSymbol = __webpack_require__(1527);
            var getMethod = __webpack_require__(9827);
            var ordinaryToPrimitive = __webpack_require__(6891);
            var wellKnownSymbol = __webpack_require__(8149);
            var TypeError = global.TypeError;
            var TO_PRIMITIVE = wellKnownSymbol("toPrimitive");
            module.exports = function(input, pref) {
                if (!isObject(input) || isSymbol(input)) return input;
                var exoticToPrim = getMethod(input, TO_PRIMITIVE);
                var result;
                if (exoticToPrim) {
                    if (void 0 === pref) pref = "default";
                    result = call(exoticToPrim, input, pref);
                    if (!isObject(result) || isSymbol(result)) return result;
                    throw TypeError("Can't convert object to primitive value");
                }
                if (void 0 === pref) pref = "number";
                return ordinaryToPrimitive(input, pref);
            };
        },
        2988: (module, __unused_webpack_exports, __webpack_require__) => {
            var toPrimitive = __webpack_require__(3948);
            var isSymbol = __webpack_require__(1527);
            module.exports = function(argument) {
                var key = toPrimitive(argument, "string");
                return isSymbol(key) ? key : key + "";
            };
        },
        4823: (module, __unused_webpack_exports, __webpack_require__) => {
            var wellKnownSymbol = __webpack_require__(8149);
            var TO_STRING_TAG = wellKnownSymbol("toStringTag");
            var test = {};
            test[TO_STRING_TAG] = "z";
            module.exports = "[object z]" === String(test);
        },
        7655: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var classof = __webpack_require__(9225);
            var String = global.String;
            module.exports = function(argument) {
                if ("Symbol" === classof(argument)) throw TypeError("Cannot convert a Symbol value to a string");
                return String(argument);
            };
        },
        180: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var String = global.String;
            module.exports = function(argument) {
                try {
                    return String(argument);
                } catch (error) {
                    return "Object";
                }
            };
        },
        9059: (module, __unused_webpack_exports, __webpack_require__) => {
            var uncurryThis = __webpack_require__(1768);
            var id = 0;
            var postfix = Math.random();
            var toString = uncurryThis(1..toString);
            module.exports = function(key) {
                return "Symbol(" + (void 0 === key ? "" : key) + ")_" + toString(++id + postfix, 36);
            };
        },
        4746: (module, __unused_webpack_exports, __webpack_require__) => {
            var NATIVE_SYMBOL = __webpack_require__(323);
            module.exports = NATIVE_SYMBOL && !Symbol.sham && "symbol" == typeof Symbol.iterator;
        },
        8654: (module, __unused_webpack_exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var fails = __webpack_require__(6183);
            module.exports = DESCRIPTORS && fails((function() {
                return 42 != Object.defineProperty((function() {}), "prototype", {
                    value: 42,
                    writable: false
                }).prototype;
            }));
        },
        8149: (module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var shared = __webpack_require__(1748);
            var hasOwn = __webpack_require__(8281);
            var uid = __webpack_require__(9059);
            var NATIVE_SYMBOL = __webpack_require__(323);
            var USE_SYMBOL_AS_UID = __webpack_require__(4746);
            var WellKnownSymbolsStore = shared("wks");
            var Symbol = global.Symbol;
            var symbolFor = Symbol && Symbol["for"];
            var createWellKnownSymbol = USE_SYMBOL_AS_UID ? Symbol : Symbol && Symbol.withoutSetter || uid;
            module.exports = function(name) {
                if (!hasOwn(WellKnownSymbolsStore, name) || !(NATIVE_SYMBOL || "string" == typeof WellKnownSymbolsStore[name])) {
                    var description = "Symbol." + name;
                    if (NATIVE_SYMBOL && hasOwn(Symbol, name)) WellKnownSymbolsStore[name] = Symbol[name]; else if (USE_SYMBOL_AS_UID && symbolFor) WellKnownSymbolsStore[name] = symbolFor(description); else WellKnownSymbolsStore[name] = createWellKnownSymbol(description);
                }
                return WellKnownSymbolsStore[name];
            };
        },
        8342: module => {
            module.exports = "\t\n\v\f\r      " + "          　\u2028\u2029\ufeff";
        },
        8165: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $ = __webpack_require__(4761);
            var $filter = __webpack_require__(528).filter;
            var arrayMethodHasSpeciesSupport = __webpack_require__(4820);
            var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport("filter");
            $({
                target: "Array",
                proto: true,
                forced: !HAS_SPECIES_SUPPORT
            }, {
                filter: function filter(callbackfn) {
                    return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : void 0);
                }
            });
        },
        9399: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $ = __webpack_require__(4761);
            var forEach = __webpack_require__(1269);
            $({
                target: "Array",
                proto: true,
                forced: [].forEach != forEach
            }, {
                forEach
            });
        },
        7543: (module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var toIndexedObject = __webpack_require__(3206);
            var addToUnscopables = __webpack_require__(9256);
            var Iterators = __webpack_require__(6126);
            var InternalStateModule = __webpack_require__(1030);
            var defineProperty = __webpack_require__(9168).f;
            var defineIterator = __webpack_require__(7583);
            var IS_PURE = __webpack_require__(8977);
            var DESCRIPTORS = __webpack_require__(723);
            var ARRAY_ITERATOR = "Array Iterator";
            var setInternalState = InternalStateModule.set;
            var getInternalState = InternalStateModule.getterFor(ARRAY_ITERATOR);
            module.exports = defineIterator(Array, "Array", (function(iterated, kind) {
                setInternalState(this, {
                    type: ARRAY_ITERATOR,
                    target: toIndexedObject(iterated),
                    index: 0,
                    kind
                });
            }), (function() {
                var state = getInternalState(this);
                var target = state.target;
                var kind = state.kind;
                var index = state.index++;
                if (!target || index >= target.length) {
                    state.target = void 0;
                    return {
                        value: void 0,
                        done: true
                    };
                }
                if ("keys" == kind) return {
                    value: index,
                    done: false
                };
                if ("values" == kind) return {
                    value: target[index],
                    done: false
                };
                return {
                    value: [ index, target[index] ],
                    done: false
                };
            }), "values");
            var values = Iterators.Arguments = Iterators.Array;
            addToUnscopables("keys");
            addToUnscopables("values");
            addToUnscopables("entries");
            if (!IS_PURE && DESCRIPTORS && "values" !== values.name) try {
                defineProperty(values, "name", {
                    value: "values"
                });
            } catch (error) {}
        },
        7985: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $ = __webpack_require__(4761);
            var $reduce = __webpack_require__(6589).left;
            var arrayMethodIsStrict = __webpack_require__(1923);
            var CHROME_VERSION = __webpack_require__(4324);
            var IS_NODE = __webpack_require__(7594);
            var STRICT_METHOD = arrayMethodIsStrict("reduce");
            var CHROME_BUG = !IS_NODE && CHROME_VERSION > 79 && CHROME_VERSION < 83;
            $({
                target: "Array",
                proto: true,
                forced: !STRICT_METHOD || CHROME_BUG
            }, {
                reduce: function reduce(callbackfn) {
                    var length = arguments.length;
                    return $reduce(this, callbackfn, length, length > 1 ? arguments[1] : void 0);
                }
            });
        },
        6618: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            var DESCRIPTORS = __webpack_require__(723);
            var FUNCTION_NAME_EXISTS = __webpack_require__(4530).EXISTS;
            var uncurryThis = __webpack_require__(1768);
            var defineProperty = __webpack_require__(9168).f;
            var FunctionPrototype = Function.prototype;
            var functionToString = uncurryThis(FunctionPrototype.toString);
            var nameRE = /function\b(?:\s|\/\*[\S\s]*?\*\/|\/\/[^\n\r]*[\n\r]+)*([^\s(/]*)/;
            var regExpExec = uncurryThis(nameRE.exec);
            var NAME = "name";
            if (DESCRIPTORS && !FUNCTION_NAME_EXISTS) defineProperty(FunctionPrototype, NAME, {
                configurable: true,
                get: function() {
                    try {
                        return regExpExec(nameRE, functionToString(this))[1];
                    } catch (error) {
                        return "";
                    }
                }
            });
        },
        7692: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            var $ = __webpack_require__(4761);
            var assign = __webpack_require__(4727);
            $({
                target: "Object",
                stat: true,
                forced: Object.assign !== assign
            }, {
                assign
            });
        },
        2352: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            var TO_STRING_TAG_SUPPORT = __webpack_require__(4823);
            var redefine = __webpack_require__(3971);
            var toString = __webpack_require__(4117);
            if (!TO_STRING_TAG_SUPPORT) redefine(Object.prototype, "toString", toString, {
                unsafe: true
            });
        },
        4249: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            var $ = __webpack_require__(4761);
            var $parseInt = __webpack_require__(8513);
            $({
                global: true,
                forced: parseInt != $parseInt
            }, {
                parseInt: $parseInt
            });
        },
        9989: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var $ = __webpack_require__(4761);
            var exec = __webpack_require__(5510);
            $({
                target: "RegExp",
                proto: true,
                forced: /./.exec !== exec
            }, {
                exec
            });
        },
        3344: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var charAt = __webpack_require__(7321).charAt;
            var toString = __webpack_require__(7655);
            var InternalStateModule = __webpack_require__(1030);
            var defineIterator = __webpack_require__(7583);
            var STRING_ITERATOR = "String Iterator";
            var setInternalState = InternalStateModule.set;
            var getInternalState = InternalStateModule.getterFor(STRING_ITERATOR);
            defineIterator(String, "String", (function(iterated) {
                setInternalState(this, {
                    type: STRING_ITERATOR,
                    string: toString(iterated),
                    index: 0
                });
            }), (function next() {
                var state = getInternalState(this);
                var string = state.string;
                var index = state.index;
                var point;
                if (index >= string.length) return {
                    value: void 0,
                    done: true
                };
                point = charAt(string, index);
                state.index += point.length;
                return {
                    value: point,
                    done: false
                };
            }));
        },
        8307: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var call = __webpack_require__(4552);
            var fixRegExpWellKnownSymbolLogic = __webpack_require__(9696);
            var anObject = __webpack_require__(1474);
            var toLength = __webpack_require__(3917);
            var toString = __webpack_require__(7655);
            var requireObjectCoercible = __webpack_require__(7431);
            var getMethod = __webpack_require__(9827);
            var advanceStringIndex = __webpack_require__(3615);
            var regExpExec = __webpack_require__(8734);
            fixRegExpWellKnownSymbolLogic("match", (function(MATCH, nativeMatch, maybeCallNative) {
                return [ function match(regexp) {
                    var O = requireObjectCoercible(this);
                    var matcher = void 0 == regexp ? void 0 : getMethod(regexp, MATCH);
                    return matcher ? call(matcher, regexp, O) : new RegExp(regexp)[MATCH](toString(O));
                }, function(string) {
                    var rx = anObject(this);
                    var S = toString(string);
                    var res = maybeCallNative(nativeMatch, rx, S);
                    if (res.done) return res.value;
                    if (!rx.global) return regExpExec(rx, S);
                    var fullUnicode = rx.unicode;
                    rx.lastIndex = 0;
                    var A = [];
                    var n = 0;
                    var result;
                    while (null !== (result = regExpExec(rx, S))) {
                        var matchStr = toString(result[0]);
                        A[n] = matchStr;
                        if ("" === matchStr) rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
                        n++;
                    }
                    return 0 === n ? null : A;
                } ];
            }));
        },
        4390: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var apply = __webpack_require__(6218);
            var call = __webpack_require__(4552);
            var uncurryThis = __webpack_require__(1768);
            var fixRegExpWellKnownSymbolLogic = __webpack_require__(9696);
            var fails = __webpack_require__(6183);
            var anObject = __webpack_require__(1474);
            var isCallable = __webpack_require__(6282);
            var toIntegerOrInfinity = __webpack_require__(8037);
            var toLength = __webpack_require__(3917);
            var toString = __webpack_require__(7655);
            var requireObjectCoercible = __webpack_require__(7431);
            var advanceStringIndex = __webpack_require__(3615);
            var getMethod = __webpack_require__(9827);
            var getSubstitution = __webpack_require__(4742);
            var regExpExec = __webpack_require__(8734);
            var wellKnownSymbol = __webpack_require__(8149);
            var REPLACE = wellKnownSymbol("replace");
            var max = Math.max;
            var min = Math.min;
            var concat = uncurryThis([].concat);
            var push = uncurryThis([].push);
            var stringIndexOf = uncurryThis("".indexOf);
            var stringSlice = uncurryThis("".slice);
            var maybeToString = function(it) {
                return void 0 === it ? it : String(it);
            };
            var REPLACE_KEEPS_$0 = function() {
                return "$0" === "a".replace(/./, "$0");
            }();
            var REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE = function() {
                if (/./[REPLACE]) return "" === /./[REPLACE]("a", "$0");
                return false;
            }();
            var REPLACE_SUPPORTS_NAMED_GROUPS = !fails((function() {
                var re = /./;
                re.exec = function() {
                    var result = [];
                    result.groups = {
                        a: "7"
                    };
                    return result;
                };
                return "7" !== "".replace(re, "$<a>");
            }));
            fixRegExpWellKnownSymbolLogic("replace", (function(_, nativeReplace, maybeCallNative) {
                var UNSAFE_SUBSTITUTE = REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE ? "$" : "$0";
                return [ function replace(searchValue, replaceValue) {
                    var O = requireObjectCoercible(this);
                    var replacer = void 0 == searchValue ? void 0 : getMethod(searchValue, REPLACE);
                    return replacer ? call(replacer, searchValue, O, replaceValue) : call(nativeReplace, toString(O), searchValue, replaceValue);
                }, function(string, replaceValue) {
                    var rx = anObject(this);
                    var S = toString(string);
                    if ("string" == typeof replaceValue && -1 === stringIndexOf(replaceValue, UNSAFE_SUBSTITUTE) && -1 === stringIndexOf(replaceValue, "$<")) {
                        var res = maybeCallNative(nativeReplace, rx, S, replaceValue);
                        if (res.done) return res.value;
                    }
                    var functionalReplace = isCallable(replaceValue);
                    if (!functionalReplace) replaceValue = toString(replaceValue);
                    var global = rx.global;
                    if (global) {
                        var fullUnicode = rx.unicode;
                        rx.lastIndex = 0;
                    }
                    var results = [];
                    while (true) {
                        var result = regExpExec(rx, S);
                        if (null === result) break;
                        push(results, result);
                        if (!global) break;
                        var matchStr = toString(result[0]);
                        if ("" === matchStr) rx.lastIndex = advanceStringIndex(S, toLength(rx.lastIndex), fullUnicode);
                    }
                    var accumulatedResult = "";
                    var nextSourcePosition = 0;
                    for (var i = 0; i < results.length; i++) {
                        result = results[i];
                        var matched = toString(result[0]);
                        var position = max(min(toIntegerOrInfinity(result.index), S.length), 0);
                        var captures = [];
                        for (var j = 1; j < result.length; j++) push(captures, maybeToString(result[j]));
                        var namedCaptures = result.groups;
                        if (functionalReplace) {
                            var replacerArgs = concat([ matched ], captures, position, S);
                            if (void 0 !== namedCaptures) push(replacerArgs, namedCaptures);
                            var replacement = toString(apply(replaceValue, void 0, replacerArgs));
                        } else replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
                        if (position >= nextSourcePosition) {
                            accumulatedResult += stringSlice(S, nextSourcePosition, position) + replacement;
                            nextSourcePosition = position + matched.length;
                        }
                    }
                    return accumulatedResult + stringSlice(S, nextSourcePosition);
                } ];
            }), !REPLACE_SUPPORTS_NAMED_GROUPS || !REPLACE_KEEPS_$0 || REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE);
        },
        7323: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            "use strict";
            var global = __webpack_require__(8454);
            var uncurryThis = __webpack_require__(1768);
            var redefineAll = __webpack_require__(9573);
            var InternalMetadataModule = __webpack_require__(6582);
            var collection = __webpack_require__(6645);
            var collectionWeak = __webpack_require__(7790);
            var isObject = __webpack_require__(5896);
            var isExtensible = __webpack_require__(6662);
            var enforceInternalState = __webpack_require__(1030).enforce;
            var NATIVE_WEAK_MAP = __webpack_require__(4404);
            var IS_IE11 = !global.ActiveXObject && "ActiveXObject" in global;
            var InternalWeakMap;
            var wrapper = function(init) {
                return function WeakMap() {
                    return init(this, arguments.length ? arguments[0] : void 0);
                };
            };
            var $WeakMap = collection("WeakMap", wrapper, collectionWeak);
            if (NATIVE_WEAK_MAP && IS_IE11) {
                InternalWeakMap = collectionWeak.getConstructor(wrapper, "WeakMap", true);
                InternalMetadataModule.enable();
                var WeakMapPrototype = $WeakMap.prototype;
                var nativeDelete = uncurryThis(WeakMapPrototype["delete"]);
                var nativeHas = uncurryThis(WeakMapPrototype.has);
                var nativeGet = uncurryThis(WeakMapPrototype.get);
                var nativeSet = uncurryThis(WeakMapPrototype.set);
                redefineAll(WeakMapPrototype, {
                    delete: function(key) {
                        if (isObject(key) && !isExtensible(key)) {
                            var state = enforceInternalState(this);
                            if (!state.frozen) state.frozen = new InternalWeakMap;
                            return nativeDelete(this, key) || state.frozen["delete"](key);
                        }
                        return nativeDelete(this, key);
                    },
                    has: function has(key) {
                        if (isObject(key) && !isExtensible(key)) {
                            var state = enforceInternalState(this);
                            if (!state.frozen) state.frozen = new InternalWeakMap;
                            return nativeHas(this, key) || state.frozen.has(key);
                        }
                        return nativeHas(this, key);
                    },
                    get: function get(key) {
                        if (isObject(key) && !isExtensible(key)) {
                            var state = enforceInternalState(this);
                            if (!state.frozen) state.frozen = new InternalWeakMap;
                            return nativeHas(this, key) ? nativeGet(this, key) : state.frozen.get(key);
                        }
                        return nativeGet(this, key);
                    },
                    set: function set(key, value) {
                        if (isObject(key) && !isExtensible(key)) {
                            var state = enforceInternalState(this);
                            if (!state.frozen) state.frozen = new InternalWeakMap;
                            nativeHas(this, key) ? nativeSet(this, key, value) : state.frozen.set(key, value);
                        } else nativeSet(this, key, value);
                        return this;
                    }
                });
            }
        },
        3542: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var DOMIterables = __webpack_require__(6181);
            var DOMTokenListPrototype = __webpack_require__(2387);
            var forEach = __webpack_require__(1269);
            var createNonEnumerableProperty = __webpack_require__(1501);
            var handlePrototype = function(CollectionPrototype) {
                if (CollectionPrototype && CollectionPrototype.forEach !== forEach) try {
                    createNonEnumerableProperty(CollectionPrototype, "forEach", forEach);
                } catch (error) {
                    CollectionPrototype.forEach = forEach;
                }
            };
            for (var COLLECTION_NAME in DOMIterables) if (DOMIterables[COLLECTION_NAME]) handlePrototype(global[COLLECTION_NAME] && global[COLLECTION_NAME].prototype);
            handlePrototype(DOMTokenListPrototype);
        },
        4079: (__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {
            var global = __webpack_require__(8454);
            var DOMIterables = __webpack_require__(6181);
            var DOMTokenListPrototype = __webpack_require__(2387);
            var ArrayIteratorMethods = __webpack_require__(7543);
            var createNonEnumerableProperty = __webpack_require__(1501);
            var wellKnownSymbol = __webpack_require__(8149);
            var ITERATOR = wellKnownSymbol("iterator");
            var TO_STRING_TAG = wellKnownSymbol("toStringTag");
            var ArrayValues = ArrayIteratorMethods.values;
            var handlePrototype = function(CollectionPrototype, COLLECTION_NAME) {
                if (CollectionPrototype) {
                    if (CollectionPrototype[ITERATOR] !== ArrayValues) try {
                        createNonEnumerableProperty(CollectionPrototype, ITERATOR, ArrayValues);
                    } catch (error) {
                        CollectionPrototype[ITERATOR] = ArrayValues;
                    }
                    if (!CollectionPrototype[TO_STRING_TAG]) createNonEnumerableProperty(CollectionPrototype, TO_STRING_TAG, COLLECTION_NAME);
                    if (DOMIterables[COLLECTION_NAME]) for (var METHOD_NAME in ArrayIteratorMethods) if (CollectionPrototype[METHOD_NAME] !== ArrayIteratorMethods[METHOD_NAME]) try {
                        createNonEnumerableProperty(CollectionPrototype, METHOD_NAME, ArrayIteratorMethods[METHOD_NAME]);
                    } catch (error) {
                        CollectionPrototype[METHOD_NAME] = ArrayIteratorMethods[METHOD_NAME];
                    }
                }
            };
            for (var COLLECTION_NAME in DOMIterables) handlePrototype(global[COLLECTION_NAME] && global[COLLECTION_NAME].prototype, COLLECTION_NAME);
            handlePrototype(DOMTokenListPrototype, "DOMTokenList");
        }
    };
    var __webpack_module_cache__ = {};
    function __webpack_require__(moduleId) {
        var cachedModule = __webpack_module_cache__[moduleId];
        if (void 0 !== cachedModule) return cachedModule.exports;
        var module = __webpack_module_cache__[moduleId] = {
            exports: {}
        };
        __webpack_modules__[moduleId](module, module.exports, __webpack_require__);
        return module.exports;
    }
    (() => {
        __webpack_require__.n = module => {
            var getter = module && module.__esModule ? () => module["default"] : () => module;
            __webpack_require__.d(getter, {
                a: getter
            });
            return getter;
        };
    })();
    (() => {
        __webpack_require__.d = (exports, definition) => {
            for (var key in definition) if (__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) Object.defineProperty(exports, key, {
                enumerable: true,
                get: definition[key]
            });
        };
    })();
    (() => {
        __webpack_require__.g = function() {
            if ("object" === typeof globalThis) return globalThis;
            try {
                return this || new Function("return this")();
            } catch (e) {
                if ("object" === typeof window) return window;
            }
        }();
    })();
    (() => {
        __webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);
    })();
    (() => {
        "use strict";
        const flsModules = {};
        function isWebp() {
            function testWebP(callback) {
                let webP = new Image;
                webP.onload = webP.onerror = function() {
                    callback(2 == webP.height);
                };
                webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
            }
            testWebP((function(support) {
                let className = true === support ? "webp" : "no-webp";
                document.documentElement.classList.add(className);
            }));
        }
        let isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
            }
        };
        function functions_getHash() {
            if (location.hash) return location.hash.replace("#", "");
        }
        function setHash(hash) {
            hash = hash ? `#${hash}` : window.location.href.split("#")[0];
            history.pushState("", "", hash);
        }
        let _slideUp = (target, duration = 500, showmore = 0) => {
            if (!target.classList.contains("_slide")) {
                target.classList.add("_slide");
                target.style.transitionProperty = "height, margin, padding";
                target.style.transitionDuration = duration + "ms";
                target.style.height = `${target.offsetHeight}px`;
                target.offsetHeight;
                target.style.overflow = "hidden";
                target.style.height = showmore ? `${showmore}px` : `0px`;
                target.style.paddingTop = 0;
                target.style.paddingBottom = 0;
                target.style.marginTop = 0;
                target.style.marginBottom = 0;
                window.setTimeout((() => {
                    target.hidden = !showmore ? true : false;
                    !showmore ? target.style.removeProperty("height") : null;
                    target.style.removeProperty("padding-top");
                    target.style.removeProperty("padding-bottom");
                    target.style.removeProperty("margin-top");
                    target.style.removeProperty("margin-bottom");
                    !showmore ? target.style.removeProperty("overflow") : null;
                    target.style.removeProperty("transition-duration");
                    target.style.removeProperty("transition-property");
                    target.classList.remove("_slide");
                    document.dispatchEvent(new CustomEvent("slideUpDone", {
                        detail: {
                            target
                        }
                    }));
                }), duration);
            }
        };
        let _slideDown = (target, duration = 500, showmore = 0) => {
            if (!target.classList.contains("_slide")) {
                target.classList.add("_slide");
                target.hidden = target.hidden ? false : null;
                showmore ? target.style.removeProperty("height") : null;
                let height = target.offsetHeight;
                target.style.overflow = "hidden";
                target.style.height = showmore ? `${showmore}px` : `0px`;
                target.style.paddingTop = 0;
                target.style.paddingBottom = 0;
                target.style.marginTop = 0;
                target.style.marginBottom = 0;
                target.offsetHeight;
                target.style.transitionProperty = "height, margin, padding";
                target.style.transitionDuration = duration + "ms";
                target.style.height = height + "px";
                target.style.removeProperty("padding-top");
                target.style.removeProperty("padding-bottom");
                target.style.removeProperty("margin-top");
                target.style.removeProperty("margin-bottom");
                window.setTimeout((() => {
                    target.style.removeProperty("height");
                    target.style.removeProperty("overflow");
                    target.style.removeProperty("transition-duration");
                    target.style.removeProperty("transition-property");
                    target.classList.remove("_slide");
                    document.dispatchEvent(new CustomEvent("slideDownDone", {
                        detail: {
                            target
                        }
                    }));
                }), duration);
            }
        };
        let _slideToggle = (target, duration = 500) => {
            if (target.hidden) return _slideDown(target, duration); else return _slideUp(target, duration);
        };
        let bodyLockStatus = true;
        let bodyLockToggle = (delay = 500) => {
            if (document.documentElement.classList.contains("lock")) bodyUnlock(delay); else bodyLock(delay);
        };
        let bodyUnlock = (delay = 500) => {
            let body = document.querySelector("body");
            if (bodyLockStatus) {
                let lock_padding = document.querySelectorAll("[data-lp]");
                setTimeout((() => {
                    for (let index = 0; index < lock_padding.length; index++) {
                        const el = lock_padding[index];
                        el.style.paddingRight = "0px";
                    }
                    body.style.paddingRight = "0px";
                    document.documentElement.classList.remove("lock");
                }), delay);
                bodyLockStatus = false;
                setTimeout((function() {
                    bodyLockStatus = true;
                }), delay);
            }
        };
        let bodyLock = (delay = 500) => {
            let body = document.querySelector("body");
            if (bodyLockStatus) {
                let lock_padding = document.querySelectorAll("[data-lp]");
                for (let index = 0; index < lock_padding.length; index++) {
                    const el = lock_padding[index];
                    el.style.paddingRight = window.innerWidth - document.querySelector(".wrapper").offsetWidth + "px";
                }
                body.style.paddingRight = window.innerWidth - document.querySelector(".wrapper").offsetWidth + "px";
                document.documentElement.classList.add("lock");
                bodyLockStatus = false;
                setTimeout((function() {
                    bodyLockStatus = true;
                }), delay);
            }
        };
        function spollers() {
            const spollersArray = document.querySelectorAll("[data-spollers]");
            if (spollersArray.length > 0) {
                const spollersRegular = Array.from(spollersArray).filter((function(item, index, self) {
                    return !item.dataset.spollers.split(",")[0];
                }));
                if (spollersRegular.length) initSpollers(spollersRegular);
                let mdQueriesArray = dataMediaQueries(spollersArray, "spollers");
                if (mdQueriesArray && mdQueriesArray.length) mdQueriesArray.forEach((mdQueriesItem => {
                    mdQueriesItem.matchMedia.addEventListener("change", (function() {
                        initSpollers(mdQueriesItem.itemsArray, mdQueriesItem.matchMedia);
                    }));
                    initSpollers(mdQueriesItem.itemsArray, mdQueriesItem.matchMedia);
                }));
                function initSpollers(spollersArray, matchMedia = false) {
                    spollersArray.forEach((spollersBlock => {
                        spollersBlock = matchMedia ? spollersBlock.item : spollersBlock;
                        if (matchMedia.matches || !matchMedia) {
                            spollersBlock.classList.add("_spoller-init");
                            initSpollerBody(spollersBlock);
                            spollersBlock.addEventListener("click", setSpollerAction);
                        } else {
                            spollersBlock.classList.remove("_spoller-init");
                            initSpollerBody(spollersBlock, false);
                            spollersBlock.removeEventListener("click", setSpollerAction);
                        }
                    }));
                }
                function initSpollerBody(spollersBlock, hideSpollerBody = true) {
                    let spollerTitles = spollersBlock.querySelectorAll("[data-spoller]");
                    if (spollerTitles.length) {
                        spollerTitles = Array.from(spollerTitles).filter((item => item.closest("[data-spollers]") === spollersBlock));
                        spollerTitles.forEach((spollerTitle => {
                            if (hideSpollerBody) {
                                spollerTitle.removeAttribute("tabindex");
                                if (!spollerTitle.classList.contains("_spoller-active")) spollerTitle.nextElementSibling.hidden = true;
                            } else {
                                spollerTitle.setAttribute("tabindex", "-1");
                                spollerTitle.nextElementSibling.hidden = false;
                            }
                        }));
                    }
                }
                function setSpollerAction(e) {
                    const el = e.target;
                    if (el.closest("[data-spoller]")) {
                        const spollerTitle = el.closest("[data-spoller]");
                        const spollersBlock = spollerTitle.closest("[data-spollers]");
                        const oneSpoller = spollersBlock.hasAttribute("data-one-spoller");
                        const spollerSpeed = spollersBlock.dataset.spollersSpeed ? parseInt(spollersBlock.dataset.spollersSpeed) : 500;
                        if (!spollersBlock.querySelectorAll("._slide").length) {
                            if (oneSpoller && !spollerTitle.classList.contains("_spoller-active")) hideSpollersBody(spollersBlock);
                            spollerTitle.classList.toggle("_spoller-active");
                            _slideToggle(spollerTitle.nextElementSibling, spollerSpeed);
                        }
                        e.preventDefault();
                    }
                }
                function hideSpollersBody(spollersBlock) {
                    const spollerActiveTitle = spollersBlock.querySelector("[data-spoller]._spoller-active");
                    const spollerSpeed = spollersBlock.dataset.spollersSpeed ? parseInt(spollersBlock.dataset.spollersSpeed) : 500;
                    if (spollerActiveTitle && !spollersBlock.querySelectorAll("._slide").length) {
                        spollerActiveTitle.classList.remove("_spoller-active");
                        _slideUp(spollerActiveTitle.nextElementSibling, spollerSpeed);
                    }
                }
            }
        }
        function tabs() {
            const tabs = document.querySelectorAll("[data-tabs]");
            let tabsActiveHash = [];
            if (tabs.length > 0) {
                const hash = functions_getHash();
                if (hash && hash.startsWith("tab-")) tabsActiveHash = hash.replace("tab-", "").split("-");
                tabs.forEach(((tabsBlock, index) => {
                    tabsBlock.classList.add("_tab-init");
                    tabsBlock.setAttribute("data-tabs-index", index);
                    tabsBlock.addEventListener("click", setTabsAction);
                    initTabs(tabsBlock);
                }));
                let mdQueriesArray = dataMediaQueries(tabs, "tabs");
                if (mdQueriesArray && mdQueriesArray.length) mdQueriesArray.forEach((mdQueriesItem => {
                    mdQueriesItem.matchMedia.addEventListener("change", (function() {
                        setTitlePosition(mdQueriesItem.itemsArray, mdQueriesItem.matchMedia);
                    }));
                    setTitlePosition(mdQueriesItem.itemsArray, mdQueriesItem.matchMedia);
                }));
            }
            function setTitlePosition(tabsMediaArray, matchMedia) {
                tabsMediaArray.forEach((tabsMediaItem => {
                    tabsMediaItem = tabsMediaItem.item;
                    let tabsTitles = tabsMediaItem.querySelector("[data-tabs-titles]");
                    let tabsTitleItems = tabsMediaItem.querySelectorAll("[data-tabs-title]");
                    let tabsContent = tabsMediaItem.querySelector("[data-tabs-body]");
                    let tabsContentItems = tabsMediaItem.querySelectorAll("[data-tabs-item]");
                    tabsTitleItems = Array.from(tabsTitleItems).filter((item => item.closest("[data-tabs]") === tabsMediaItem));
                    tabsContentItems = Array.from(tabsContentItems).filter((item => item.closest("[data-tabs]") === tabsMediaItem));
                    tabsContentItems.forEach(((tabsContentItem, index) => {
                        if (matchMedia.matches) {
                            tabsContent.append(tabsTitleItems[index]);
                            tabsContent.append(tabsContentItem);
                            tabsMediaItem.classList.add("_tab-spoller");
                        } else {
                            tabsTitles.append(tabsTitleItems[index]);
                            tabsMediaItem.classList.remove("_tab-spoller");
                        }
                    }));
                }));
            }
            function initTabs(tabsBlock) {
                let tabsTitles = tabsBlock.querySelectorAll("[data-tabs-titles]>*");
                let tabsContent = tabsBlock.querySelectorAll("[data-tabs-body]>*");
                const tabsBlockIndex = tabsBlock.dataset.tabsIndex;
                const tabsActiveHashBlock = tabsActiveHash[0] == tabsBlockIndex;
                if (tabsActiveHashBlock) {
                    const tabsActiveTitle = tabsBlock.querySelector("[data-tabs-titles]>._tab-active");
                    tabsActiveTitle ? tabsActiveTitle.classList.remove("_tab-active") : null;
                }
                if (tabsContent.length) {
                    tabsContent = Array.from(tabsContent).filter((item => item.closest("[data-tabs]") === tabsBlock));
                    tabsTitles = Array.from(tabsTitles).filter((item => item.closest("[data-tabs]") === tabsBlock));
                    tabsContent.forEach(((tabsContentItem, index) => {
                        tabsTitles[index].setAttribute("data-tabs-title", "");
                        tabsContentItem.setAttribute("data-tabs-item", "");
                        if (tabsActiveHashBlock && index == tabsActiveHash[1]) tabsTitles[index].classList.add("_tab-active");
                        tabsContentItem.hidden = !tabsTitles[index].classList.contains("_tab-active");
                    }));
                }
            }
            function setTabsStatus(tabsBlock) {
                let tabsTitles = tabsBlock.querySelectorAll("[data-tabs-title]");
                let tabsContent = tabsBlock.querySelectorAll("[data-tabs-item]");
                const tabsBlockIndex = tabsBlock.dataset.tabsIndex;
                function isTabsAnamate(tabsBlock) {
                    if (tabsBlock.hasAttribute("data-tabs-animate")) return tabsBlock.dataset.tabsAnimate > 0 ? Number(tabsBlock.dataset.tabsAnimate) : 500;
                }
                const tabsBlockAnimate = isTabsAnamate(tabsBlock);
                if (tabsContent.length > 0) {
                    const isHash = tabsBlock.hasAttribute("data-tabs-hash");
                    tabsContent = Array.from(tabsContent).filter((item => item.closest("[data-tabs]") === tabsBlock));
                    tabsTitles = Array.from(tabsTitles).filter((item => item.closest("[data-tabs]") === tabsBlock));
                    tabsContent.forEach(((tabsContentItem, index) => {
                        if (tabsTitles[index].classList.contains("_tab-active")) {
                            if (tabsBlockAnimate) _slideDown(tabsContentItem, tabsBlockAnimate); else tabsContentItem.hidden = false;
                            if (isHash && !tabsContentItem.closest(".popup")) setHash(`tab-${tabsBlockIndex}-${index}`);
                        } else if (tabsBlockAnimate) _slideUp(tabsContentItem, tabsBlockAnimate); else tabsContentItem.hidden = true;
                    }));
                }
            }
            function setTabsAction(e) {
                const el = e.target;
                if (el.closest("[data-tabs-title]")) {
                    const tabTitle = el.closest("[data-tabs-title]");
                    const tabsBlock = tabTitle.closest("[data-tabs]");
                    if (!tabTitle.classList.contains("_tab-active") && !tabsBlock.querySelector("._slide")) {
                        let tabActiveTitle = tabsBlock.querySelectorAll("[data-tabs-title]._tab-active");
                        tabActiveTitle.length ? tabActiveTitle = Array.from(tabActiveTitle).filter((item => item.closest("[data-tabs]") === tabsBlock)) : null;
                        tabActiveTitle.length ? tabActiveTitle[0].classList.remove("_tab-active") : null;
                        tabTitle.classList.add("_tab-active");
                        setTabsStatus(tabsBlock);
                    }
                    e.preventDefault();
                }
            }
        }
        function menuInit() {
            if (document.querySelector(".icon-menu")) document.addEventListener("click", (function(e) {
                if (bodyLockStatus && e.target.closest(".icon-menu")) {
                    document.documentElement.classList.remove("list-open");
                    bodyLockToggle();
                    document.documentElement.classList.toggle("menu-open");
                }
            }));
        }
        function menuClose() {
            bodyUnlock();
            document.documentElement.classList.remove("menu-open");
        }
        function showMore() {
            window.addEventListener("load", (function(e) {
                const showMoreBlocks = document.querySelectorAll("[data-showmore]");
                let showMoreBlocksRegular;
                let mdQueriesArray;
                if (showMoreBlocks.length) {
                    showMoreBlocksRegular = Array.from(showMoreBlocks).filter((function(item, index, self) {
                        return !item.dataset.showmoreMedia;
                    }));
                    showMoreBlocksRegular.length ? initItems(showMoreBlocksRegular) : null;
                    document.addEventListener("click", showMoreActions);
                    window.addEventListener("resize", showMoreActions);
                    mdQueriesArray = dataMediaQueries(showMoreBlocks, "showmoreMedia");
                    if (mdQueriesArray && mdQueriesArray.length) {
                        mdQueriesArray.forEach((mdQueriesItem => {
                            mdQueriesItem.matchMedia.addEventListener("change", (function() {
                                initItems(mdQueriesItem.itemsArray, mdQueriesItem.matchMedia);
                            }));
                        }));
                        initItemsMedia(mdQueriesArray);
                    }
                }
                function initItemsMedia(mdQueriesArray) {
                    mdQueriesArray.forEach((mdQueriesItem => {
                        initItems(mdQueriesItem.itemsArray, mdQueriesItem.matchMedia);
                    }));
                }
                function initItems(showMoreBlocks, matchMedia) {
                    showMoreBlocks.forEach((showMoreBlock => {
                        initItem(showMoreBlock, matchMedia);
                    }));
                }
                function initItem(showMoreBlock, matchMedia = false) {
                    showMoreBlock = matchMedia ? showMoreBlock.item : showMoreBlock;
                    let showMoreContent = showMoreBlock.querySelectorAll("[data-showmore-content]");
                    let showMoreButton = showMoreBlock.querySelectorAll("[data-showmore-button]");
                    showMoreContent = Array.from(showMoreContent).filter((item => item.closest("[data-showmore]") === showMoreBlock))[0];
                    showMoreButton = Array.from(showMoreButton).filter((item => item.closest("[data-showmore]") === showMoreBlock))[0];
                    const hiddenHeight = getHeight(showMoreBlock, showMoreContent);
                    if (matchMedia.matches || !matchMedia) if (hiddenHeight < getOriginalHeight(showMoreContent)) {
                        _slideUp(showMoreContent, 0, hiddenHeight);
                        showMoreButton.hidden = false;
                    } else {
                        _slideDown(showMoreContent, 0, hiddenHeight);
                        showMoreButton.hidden = true;
                    } else {
                        _slideDown(showMoreContent, 0, hiddenHeight);
                        showMoreButton.hidden = true;
                    }
                }
                function getHeight(showMoreBlock, showMoreContent) {
                    let hiddenHeight = 0;
                    const showMoreType = showMoreBlock.dataset.showmore ? showMoreBlock.dataset.showmore : "size";
                    if ("items" === showMoreType) {
                        const showMoreTypeValue = showMoreContent.dataset.showmoreContent ? showMoreContent.dataset.showmoreContent : 3;
                        const showMoreItems = showMoreContent.children;
                        for (let index = 1; index < showMoreItems.length; index++) {
                            const showMoreItem = showMoreItems[index - 1];
                            hiddenHeight += showMoreItem.offsetHeight;
                            if (index == showMoreTypeValue) break;
                        }
                    } else {
                        const showMoreTypeValue = showMoreContent.dataset.showmoreContent ? showMoreContent.dataset.showmoreContent : 150;
                        hiddenHeight = showMoreTypeValue;
                    }
                    return hiddenHeight;
                }
                function getOriginalHeight(showMoreContent) {
                    let parentHidden;
                    let hiddenHeight = showMoreContent.offsetHeight;
                    showMoreContent.style.removeProperty("height");
                    if (showMoreContent.closest(`[hidden]`)) {
                        parentHidden = showMoreContent.closest(`[hidden]`);
                        parentHidden.hidden = false;
                    }
                    let originalHeight = showMoreContent.offsetHeight;
                    parentHidden ? parentHidden.hidden = true : null;
                    showMoreContent.style.height = `${hiddenHeight}px`;
                    return originalHeight;
                }
                function showMoreActions(e) {
                    const targetEvent = e.target;
                    const targetType = e.type;
                    if ("click" === targetType) {
                        if (targetEvent.closest("[data-showmore-button]")) {
                            const showMoreButton = targetEvent.closest("[data-showmore-button]");
                            const showMoreBlock = showMoreButton.closest("[data-showmore]");
                            const showMoreContent = showMoreBlock.querySelector("[data-showmore-content]");
                            const showMoreSpeed = showMoreBlock.dataset.showmoreButton ? showMoreBlock.dataset.showmoreButton : "500";
                            const hiddenHeight = getHeight(showMoreBlock, showMoreContent);
                            if (!showMoreContent.classList.contains("_slide")) {
                                showMoreBlock.classList.contains("_showmore-active") ? _slideUp(showMoreContent, showMoreSpeed, hiddenHeight) : _slideDown(showMoreContent, showMoreSpeed, hiddenHeight);
                                showMoreBlock.classList.toggle("_showmore-active");
                            }
                        }
                    } else if ("resize" === targetType) {
                        showMoreBlocksRegular && showMoreBlocksRegular.length ? initItems(showMoreBlocksRegular) : null;
                        mdQueriesArray && mdQueriesArray.length ? initItemsMedia(mdQueriesArray) : null;
                    }
                }
            }));
        }
        function FLS(message) {
            setTimeout((() => {
                if (window.FLS) console.log(message);
            }), 0);
        }
        function uniqArray(array) {
            return array.filter((function(item, index, self) {
                return self.indexOf(item) === index;
            }));
        }
        function dataMediaQueries(array, dataSetValue) {
            const media = Array.from(array).filter((function(item, index, self) {
                if (item.dataset[dataSetValue]) return item.dataset[dataSetValue].split(",")[0];
            }));
            if (media.length) {
                const breakpointsArray = [];
                media.forEach((item => {
                    const params = item.dataset[dataSetValue];
                    const breakpoint = {};
                    const paramsArray = params.split(",");
                    breakpoint.value = paramsArray[0];
                    breakpoint.type = paramsArray[1] ? paramsArray[1].trim() : "max";
                    breakpoint.item = item;
                    breakpointsArray.push(breakpoint);
                }));
                let mdQueries = breakpointsArray.map((function(item) {
                    return "(" + item.type + "-width: " + item.value + "px)," + item.value + "," + item.type;
                }));
                mdQueries = uniqArray(mdQueries);
                const mdQueriesArray = [];
                if (mdQueries.length) {
                    mdQueries.forEach((breakpoint => {
                        const paramsArray = breakpoint.split(",");
                        const mediaBreakpoint = paramsArray[1];
                        const mediaType = paramsArray[2];
                        const matchMedia = window.matchMedia(paramsArray[0]);
                        const itemsArray = breakpointsArray.filter((function(item) {
                            if (item.value === mediaBreakpoint && item.type === mediaType) return true;
                        }));
                        mdQueriesArray.push({
                            itemsArray,
                            matchMedia
                        });
                    }));
                    return mdQueriesArray;
                }
            }
        }
        const requestAnimFrame = function() {
            if ("undefined" === typeof window) return function(callback) {
                return callback();
            };
            return window.requestAnimationFrame;
        }();
        function throttled(fn, thisArg, updateFn) {
            const updateArgs = updateFn || (args => Array.prototype.slice.call(args));
            let ticking = false;
            let args = [];
            return function(...rest) {
                args = updateArgs(rest);
                if (!ticking) {
                    ticking = true;
                    requestAnimFrame.call(window, (() => {
                        ticking = false;
                        fn.apply(thisArg, args);
                    }));
                }
            };
        }
        function debounce(fn, delay) {
            let timeout;
            return function(...args) {
                if (delay) {
                    clearTimeout(timeout);
                    timeout = setTimeout(fn, delay, args);
                } else fn.apply(this, args);
                return delay;
            };
        }
        const _toLeftRightCenter = align => "start" === align ? "left" : "end" === align ? "right" : "center";
        const _alignStartEnd = (align, start, end) => "start" === align ? start : "end" === align ? end : (start + end) / 2;
        const _textX = (align, left, right, rtl) => {
            const check = rtl ? "left" : "right";
            return align === check ? right : "center" === align ? (left + right) / 2 : left;
        };
        function noop() {}
        const uid = function() {
            let id = 0;
            return function() {
                return id++;
            };
        }();
        function isNullOrUndef(value) {
            return null === value || "undefined" === typeof value;
        }
        function isArray(value) {
            if (Array.isArray && Array.isArray(value)) return true;
            const type = Object.prototype.toString.call(value);
            if ("[object" === type.substr(0, 7) && "Array]" === type.substr(-6)) return true;
            return false;
        }
        function isObject(value) {
            return null !== value && "[object Object]" === Object.prototype.toString.call(value);
        }
        const isNumberFinite = value => ("number" === typeof value || value instanceof Number) && isFinite(+value);
        function finiteOrDefault(value, defaultValue) {
            return isNumberFinite(value) ? value : defaultValue;
        }
        function valueOrDefault(value, defaultValue) {
            return "undefined" === typeof value ? defaultValue : value;
        }
        const toPercentage = (value, dimension) => "string" === typeof value && value.endsWith("%") ? parseFloat(value) / 100 : value / dimension;
        const toDimension = (value, dimension) => "string" === typeof value && value.endsWith("%") ? parseFloat(value) / 100 * dimension : +value;
        function callback(fn, args, thisArg) {
            if (fn && "function" === typeof fn.call) return fn.apply(thisArg, args);
        }
        function each(loopable, fn, thisArg, reverse) {
            let i, len, keys;
            if (isArray(loopable)) {
                len = loopable.length;
                if (reverse) for (i = len - 1; i >= 0; i--) fn.call(thisArg, loopable[i], i); else for (i = 0; i < len; i++) fn.call(thisArg, loopable[i], i);
            } else if (isObject(loopable)) {
                keys = Object.keys(loopable);
                len = keys.length;
                for (i = 0; i < len; i++) fn.call(thisArg, loopable[keys[i]], keys[i]);
            }
        }
        function _elementsEqual(a0, a1) {
            let i, ilen, v0, v1;
            if (!a0 || !a1 || a0.length !== a1.length) return false;
            for (i = 0, ilen = a0.length; i < ilen; ++i) {
                v0 = a0[i];
                v1 = a1[i];
                if (v0.datasetIndex !== v1.datasetIndex || v0.index !== v1.index) return false;
            }
            return true;
        }
        function clone$1(source) {
            if (isArray(source)) return source.map(clone$1);
            if (isObject(source)) {
                const target = Object.create(null);
                const keys = Object.keys(source);
                const klen = keys.length;
                let k = 0;
                for (;k < klen; ++k) target[keys[k]] = clone$1(source[keys[k]]);
                return target;
            }
            return source;
        }
        function isValidKey(key) {
            return -1 === [ "__proto__", "prototype", "constructor" ].indexOf(key);
        }
        function _merger(key, target, source, options) {
            if (!isValidKey(key)) return;
            const tval = target[key];
            const sval = source[key];
            if (isObject(tval) && isObject(sval)) merge(tval, sval, options); else target[key] = clone$1(sval);
        }
        function merge(target, source, options) {
            const sources = isArray(source) ? source : [ source ];
            const ilen = sources.length;
            if (!isObject(target)) return target;
            options = options || {};
            const merger = options.merger || _merger;
            for (let i = 0; i < ilen; ++i) {
                source = sources[i];
                if (!isObject(source)) continue;
                const keys = Object.keys(source);
                for (let k = 0, klen = keys.length; k < klen; ++k) merger(keys[k], target, source, options);
            }
            return target;
        }
        function mergeIf(target, source) {
            return merge(target, source, {
                merger: _mergerIf
            });
        }
        function _mergerIf(key, target, source) {
            if (!isValidKey(key)) return;
            const tval = target[key];
            const sval = source[key];
            if (isObject(tval) && isObject(sval)) mergeIf(tval, sval); else if (!Object.prototype.hasOwnProperty.call(target, key)) target[key] = clone$1(sval);
        }
        const emptyString = "";
        const dot = ".";
        function indexOfDotOrLength(key, start) {
            const idx = key.indexOf(dot, start);
            return -1 === idx ? key.length : idx;
        }
        function resolveObjectKey(obj, key) {
            if (key === emptyString) return obj;
            let pos = 0;
            let idx = indexOfDotOrLength(key, pos);
            while (obj && idx > pos) {
                obj = obj[key.substr(pos, idx - pos)];
                pos = idx + 1;
                idx = indexOfDotOrLength(key, pos);
            }
            return obj;
        }
        function _capitalize(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }
        const defined = value => "undefined" !== typeof value;
        const isFunction = value => "function" === typeof value;
        const setsEqual = (a, b) => {
            if (a.size !== b.size) return false;
            for (const item of a) if (!b.has(item)) return false;
            return true;
        };
        function _isClickEvent(e) {
            return "mouseup" === e.type || "click" === e.type || "contextmenu" === e.type;
        }
        const PI = Math.PI;
        const TAU = 2 * PI;
        const PITAU = TAU + PI;
        const INFINITY = Number.POSITIVE_INFINITY;
        const RAD_PER_DEG = PI / 180;
        const HALF_PI = PI / 2;
        const QUARTER_PI = PI / 4;
        const TWO_THIRDS_PI = 2 * PI / 3;
        const log10 = Math.log10;
        const sign = Math.sign;
        function niceNum(range) {
            const roundedRange = Math.round(range);
            range = almostEquals(range, roundedRange, range / 1e3) ? roundedRange : range;
            const niceRange = Math.pow(10, Math.floor(log10(range)));
            const fraction = range / niceRange;
            const niceFraction = fraction <= 1 ? 1 : fraction <= 2 ? 2 : fraction <= 5 ? 5 : 10;
            return niceFraction * niceRange;
        }
        function _factorize(value) {
            const result = [];
            const sqrt = Math.sqrt(value);
            let i;
            for (i = 1; i < sqrt; i++) if (value % i === 0) {
                result.push(i);
                result.push(value / i);
            }
            if (sqrt === (0 | sqrt)) result.push(sqrt);
            result.sort(((a, b) => a - b)).pop();
            return result;
        }
        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
        function almostEquals(x, y, epsilon) {
            return Math.abs(x - y) < epsilon;
        }
        function almostWhole(x, epsilon) {
            const rounded = Math.round(x);
            return rounded - epsilon <= x && rounded + epsilon >= x;
        }
        function _setMinAndMaxByKey(array, target, property) {
            let i, ilen, value;
            for (i = 0, ilen = array.length; i < ilen; i++) {
                value = array[i][property];
                if (!isNaN(value)) {
                    target.min = Math.min(target.min, value);
                    target.max = Math.max(target.max, value);
                }
            }
        }
        function toRadians(degrees) {
            return degrees * (PI / 180);
        }
        function toDegrees(radians) {
            return radians * (180 / PI);
        }
        function _decimalPlaces(x) {
            if (!isNumberFinite(x)) return;
            let e = 1;
            let p = 0;
            while (Math.round(x * e) / e !== x) {
                e *= 10;
                p++;
            }
            return p;
        }
        function getAngleFromPoint(centrePoint, anglePoint) {
            const distanceFromXCenter = anglePoint.x - centrePoint.x;
            const distanceFromYCenter = anglePoint.y - centrePoint.y;
            const radialDistanceFromCenter = Math.sqrt(distanceFromXCenter * distanceFromXCenter + distanceFromYCenter * distanceFromYCenter);
            let angle = Math.atan2(distanceFromYCenter, distanceFromXCenter);
            if (angle < -.5 * PI) angle += TAU;
            return {
                angle,
                distance: radialDistanceFromCenter
            };
        }
        function distanceBetweenPoints(pt1, pt2) {
            return Math.sqrt(Math.pow(pt2.x - pt1.x, 2) + Math.pow(pt2.y - pt1.y, 2));
        }
        function _angleDiff(a, b) {
            return (a - b + PITAU) % TAU - PI;
        }
        function _normalizeAngle(a) {
            return (a % TAU + TAU) % TAU;
        }
        function _angleBetween(angle, start, end, sameAngleIsFullCircle) {
            const a = _normalizeAngle(angle);
            const s = _normalizeAngle(start);
            const e = _normalizeAngle(end);
            const angleToStart = _normalizeAngle(s - a);
            const angleToEnd = _normalizeAngle(e - a);
            const startToAngle = _normalizeAngle(a - s);
            const endToAngle = _normalizeAngle(a - e);
            return a === s || a === e || sameAngleIsFullCircle && s === e || angleToStart > angleToEnd && startToAngle < endToAngle;
        }
        function _limitValue(value, min, max) {
            return Math.max(min, Math.min(max, value));
        }
        function _int16Range(value) {
            return _limitValue(value, -32768, 32767);
        }
        function _isBetween(value, start, end, epsilon = 1e-6) {
            return value >= Math.min(start, end) - epsilon && value <= Math.max(start, end) + epsilon;
        }
        const atEdge = t => 0 === t || 1 === t;
        const elasticIn = (t, s, p) => -Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * TAU / p);
        const elasticOut = (t, s, p) => Math.pow(2, -10 * t) * Math.sin((t - s) * TAU / p) + 1;
        const effects = {
            linear: t => t,
            easeInQuad: t => t * t,
            easeOutQuad: t => -t * (t - 2),
            easeInOutQuad: t => (t /= .5) < 1 ? .5 * t * t : -.5 * (--t * (t - 2) - 1),
            easeInCubic: t => t * t * t,
            easeOutCubic: t => (t -= 1) * t * t + 1,
            easeInOutCubic: t => (t /= .5) < 1 ? .5 * t * t * t : .5 * ((t -= 2) * t * t + 2),
            easeInQuart: t => t * t * t * t,
            easeOutQuart: t => -((t -= 1) * t * t * t - 1),
            easeInOutQuart: t => (t /= .5) < 1 ? .5 * t * t * t * t : -.5 * ((t -= 2) * t * t * t - 2),
            easeInQuint: t => t * t * t * t * t,
            easeOutQuint: t => (t -= 1) * t * t * t * t + 1,
            easeInOutQuint: t => (t /= .5) < 1 ? .5 * t * t * t * t * t : .5 * ((t -= 2) * t * t * t * t + 2),
            easeInSine: t => -Math.cos(t * HALF_PI) + 1,
            easeOutSine: t => Math.sin(t * HALF_PI),
            easeInOutSine: t => -.5 * (Math.cos(PI * t) - 1),
            easeInExpo: t => 0 === t ? 0 : Math.pow(2, 10 * (t - 1)),
            easeOutExpo: t => 1 === t ? 1 : -Math.pow(2, -10 * t) + 1,
            easeInOutExpo: t => atEdge(t) ? t : t < .5 ? .5 * Math.pow(2, 10 * (2 * t - 1)) : .5 * (-Math.pow(2, -10 * (2 * t - 1)) + 2),
            easeInCirc: t => t >= 1 ? t : -(Math.sqrt(1 - t * t) - 1),
            easeOutCirc: t => Math.sqrt(1 - (t -= 1) * t),
            easeInOutCirc: t => (t /= .5) < 1 ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1),
            easeInElastic: t => atEdge(t) ? t : elasticIn(t, .075, .3),
            easeOutElastic: t => atEdge(t) ? t : elasticOut(t, .075, .3),
            easeInOutElastic(t) {
                const s = .1125;
                const p = .45;
                return atEdge(t) ? t : t < .5 ? .5 * elasticIn(2 * t, s, p) : .5 + .5 * elasticOut(2 * t - 1, s, p);
            },
            easeInBack(t) {
                const s = 1.70158;
                return t * t * ((s + 1) * t - s);
            },
            easeOutBack(t) {
                const s = 1.70158;
                return (t -= 1) * t * ((s + 1) * t + s) + 1;
            },
            easeInOutBack(t) {
                let s = 1.70158;
                if ((t /= .5) < 1) return .5 * (t * t * (((s *= 1.525) + 1) * t - s));
                return .5 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2);
            },
            easeInBounce: t => 1 - effects.easeOutBounce(1 - t),
            easeOutBounce(t) {
                const m = 7.5625;
                const d = 2.75;
                if (t < 1 / d) return m * t * t;
                if (t < 2 / d) return m * (t -= 1.5 / d) * t + .75;
                if (t < 2.5 / d) return m * (t -= 2.25 / d) * t + .9375;
                return m * (t -= 2.625 / d) * t + .984375;
            },
            easeInOutBounce: t => t < .5 ? .5 * effects.easeInBounce(2 * t) : .5 * effects.easeOutBounce(2 * t - 1) + .5
        };
        /*!
 * @kurkle/color v0.1.9
 * https://github.com/kurkle/color#readme
 * (c) 2020 Jukka Kurkela
 * Released under the MIT License
 */        const map = {
            0: 0,
            1: 1,
            2: 2,
            3: 3,
            4: 4,
            5: 5,
            6: 6,
            7: 7,
            8: 8,
            9: 9,
            A: 10,
            B: 11,
            C: 12,
            D: 13,
            E: 14,
            F: 15,
            a: 10,
            b: 11,
            c: 12,
            d: 13,
            e: 14,
            f: 15
        };
        const hex = "0123456789ABCDEF";
        const h1 = b => hex[15 & b];
        const h2 = b => hex[(240 & b) >> 4] + hex[15 & b];
        const eq = b => (240 & b) >> 4 === (15 & b);
        function isShort(v) {
            return eq(v.r) && eq(v.g) && eq(v.b) && eq(v.a);
        }
        function hexParse(str) {
            var len = str.length;
            var ret;
            if ("#" === str[0]) if (4 === len || 5 === len) ret = {
                r: 255 & 17 * map[str[1]],
                g: 255 & 17 * map[str[2]],
                b: 255 & 17 * map[str[3]],
                a: 5 === len ? 17 * map[str[4]] : 255
            }; else if (7 === len || 9 === len) ret = {
                r: map[str[1]] << 4 | map[str[2]],
                g: map[str[3]] << 4 | map[str[4]],
                b: map[str[5]] << 4 | map[str[6]],
                a: 9 === len ? map[str[7]] << 4 | map[str[8]] : 255
            };
            return ret;
        }
        function hexString(v) {
            var f = isShort(v) ? h1 : h2;
            return v ? "#" + f(v.r) + f(v.g) + f(v.b) + (v.a < 255 ? f(v.a) : "") : v;
        }
        function round(v) {
            return v + .5 | 0;
        }
        const lim = (v, l, h) => Math.max(Math.min(v, h), l);
        function p2b(v) {
            return lim(round(2.55 * v), 0, 255);
        }
        function n2b(v) {
            return lim(round(255 * v), 0, 255);
        }
        function b2n(v) {
            return lim(round(v / 2.55) / 100, 0, 1);
        }
        function n2p(v) {
            return lim(round(100 * v), 0, 100);
        }
        const RGB_RE = /^rgba?\(\s*([-+.\d]+)(%)?[\s,]+([-+.e\d]+)(%)?[\s,]+([-+.e\d]+)(%)?(?:[\s,/]+([-+.e\d]+)(%)?)?\s*\)$/;
        function rgbParse(str) {
            const m = RGB_RE.exec(str);
            let a = 255;
            let r, g, b;
            if (!m) return;
            if (m[7] !== r) {
                const v = +m[7];
                a = 255 & (m[8] ? p2b(v) : 255 * v);
            }
            r = +m[1];
            g = +m[3];
            b = +m[5];
            r = 255 & (m[2] ? p2b(r) : r);
            g = 255 & (m[4] ? p2b(g) : g);
            b = 255 & (m[6] ? p2b(b) : b);
            return {
                r,
                g,
                b,
                a
            };
        }
        function rgbString(v) {
            return v && (v.a < 255 ? `rgba(${v.r}, ${v.g}, ${v.b}, ${b2n(v.a)})` : `rgb(${v.r}, ${v.g}, ${v.b})`);
        }
        const HUE_RE = /^(hsla?|hwb|hsv)\(\s*([-+.e\d]+)(?:deg)?[\s,]+([-+.e\d]+)%[\s,]+([-+.e\d]+)%(?:[\s,]+([-+.e\d]+)(%)?)?\s*\)$/;
        function hsl2rgbn(h, s, l) {
            const a = s * Math.min(l, 1 - l);
            const f = (n, k = (n + h / 30) % 12) => l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
            return [ f(0), f(8), f(4) ];
        }
        function hsv2rgbn(h, s, v) {
            const f = (n, k = (n + h / 60) % 6) => v - v * s * Math.max(Math.min(k, 4 - k, 1), 0);
            return [ f(5), f(3), f(1) ];
        }
        function hwb2rgbn(h, w, b) {
            const rgb = hsl2rgbn(h, 1, .5);
            let i;
            if (w + b > 1) {
                i = 1 / (w + b);
                w *= i;
                b *= i;
            }
            for (i = 0; i < 3; i++) {
                rgb[i] *= 1 - w - b;
                rgb[i] += w;
            }
            return rgb;
        }
        function rgb2hsl(v) {
            const range = 255;
            const r = v.r / range;
            const g = v.g / range;
            const b = v.b / range;
            const max = Math.max(r, g, b);
            const min = Math.min(r, g, b);
            const l = (max + min) / 2;
            let h, s, d;
            if (max !== min) {
                d = max - min;
                s = l > .5 ? d / (2 - max - min) : d / (max + min);
                h = max === r ? (g - b) / d + (g < b ? 6 : 0) : max === g ? (b - r) / d + 2 : (r - g) / d + 4;
                h = 60 * h + .5;
            }
            return [ 0 | h, s || 0, l ];
        }
        function calln(f, a, b, c) {
            return (Array.isArray(a) ? f(a[0], a[1], a[2]) : f(a, b, c)).map(n2b);
        }
        function hsl2rgb(h, s, l) {
            return calln(hsl2rgbn, h, s, l);
        }
        function hwb2rgb(h, w, b) {
            return calln(hwb2rgbn, h, w, b);
        }
        function hsv2rgb(h, s, v) {
            return calln(hsv2rgbn, h, s, v);
        }
        function hue(h) {
            return (h % 360 + 360) % 360;
        }
        function hueParse(str) {
            const m = HUE_RE.exec(str);
            let a = 255;
            let v;
            if (!m) return;
            if (m[5] !== v) a = m[6] ? p2b(+m[5]) : n2b(+m[5]);
            const h = hue(+m[2]);
            const p1 = +m[3] / 100;
            const p2 = +m[4] / 100;
            if ("hwb" === m[1]) v = hwb2rgb(h, p1, p2); else if ("hsv" === m[1]) v = hsv2rgb(h, p1, p2); else v = hsl2rgb(h, p1, p2);
            return {
                r: v[0],
                g: v[1],
                b: v[2],
                a
            };
        }
        function rotate(v, deg) {
            var h = rgb2hsl(v);
            h[0] = hue(h[0] + deg);
            h = hsl2rgb(h);
            v.r = h[0];
            v.g = h[1];
            v.b = h[2];
        }
        function hslString(v) {
            if (!v) return;
            const a = rgb2hsl(v);
            const h = a[0];
            const s = n2p(a[1]);
            const l = n2p(a[2]);
            return v.a < 255 ? `hsla(${h}, ${s}%, ${l}%, ${b2n(v.a)})` : `hsl(${h}, ${s}%, ${l}%)`;
        }
        const map$1 = {
            x: "dark",
            Z: "light",
            Y: "re",
            X: "blu",
            W: "gr",
            V: "medium",
            U: "slate",
            A: "ee",
            T: "ol",
            S: "or",
            B: "ra",
            C: "lateg",
            D: "ights",
            R: "in",
            Q: "turquois",
            E: "hi",
            P: "ro",
            O: "al",
            N: "le",
            M: "de",
            L: "yello",
            F: "en",
            K: "ch",
            G: "arks",
            H: "ea",
            I: "ightg",
            J: "wh"
        };
        const names = {
            OiceXe: "f0f8ff",
            antiquewEte: "faebd7",
            aqua: "ffff",
            aquamarRe: "7fffd4",
            azuY: "f0ffff",
            beige: "f5f5dc",
            bisque: "ffe4c4",
            black: "0",
            blanKedOmond: "ffebcd",
            Xe: "ff",
            XeviTet: "8a2be2",
            bPwn: "a52a2a",
            burlywood: "deb887",
            caMtXe: "5f9ea0",
            KartYuse: "7fff00",
            KocTate: "d2691e",
            cSO: "ff7f50",
            cSnflowerXe: "6495ed",
            cSnsilk: "fff8dc",
            crimson: "dc143c",
            cyan: "ffff",
            xXe: "8b",
            xcyan: "8b8b",
            xgTMnPd: "b8860b",
            xWay: "a9a9a9",
            xgYF: "6400",
            xgYy: "a9a9a9",
            xkhaki: "bdb76b",
            xmagFta: "8b008b",
            xTivegYF: "556b2f",
            xSange: "ff8c00",
            xScEd: "9932cc",
            xYd: "8b0000",
            xsOmon: "e9967a",
            xsHgYF: "8fbc8f",
            xUXe: "483d8b",
            xUWay: "2f4f4f",
            xUgYy: "2f4f4f",
            xQe: "ced1",
            xviTet: "9400d3",
            dAppRk: "ff1493",
            dApskyXe: "bfff",
            dimWay: "696969",
            dimgYy: "696969",
            dodgerXe: "1e90ff",
            fiYbrick: "b22222",
            flSOwEte: "fffaf0",
            foYstWAn: "228b22",
            fuKsia: "ff00ff",
            gaRsbSo: "dcdcdc",
            ghostwEte: "f8f8ff",
            gTd: "ffd700",
            gTMnPd: "daa520",
            Way: "808080",
            gYF: "8000",
            gYFLw: "adff2f",
            gYy: "808080",
            honeyMw: "f0fff0",
            hotpRk: "ff69b4",
            RdianYd: "cd5c5c",
            Rdigo: "4b0082",
            ivSy: "fffff0",
            khaki: "f0e68c",
            lavFMr: "e6e6fa",
            lavFMrXsh: "fff0f5",
            lawngYF: "7cfc00",
            NmoncEffon: "fffacd",
            ZXe: "add8e6",
            ZcSO: "f08080",
            Zcyan: "e0ffff",
            ZgTMnPdLw: "fafad2",
            ZWay: "d3d3d3",
            ZgYF: "90ee90",
            ZgYy: "d3d3d3",
            ZpRk: "ffb6c1",
            ZsOmon: "ffa07a",
            ZsHgYF: "20b2aa",
            ZskyXe: "87cefa",
            ZUWay: "778899",
            ZUgYy: "778899",
            ZstAlXe: "b0c4de",
            ZLw: "ffffe0",
            lime: "ff00",
            limegYF: "32cd32",
            lRF: "faf0e6",
            magFta: "ff00ff",
            maPon: "800000",
            VaquamarRe: "66cdaa",
            VXe: "cd",
            VScEd: "ba55d3",
            VpurpN: "9370db",
            VsHgYF: "3cb371",
            VUXe: "7b68ee",
            VsprRggYF: "fa9a",
            VQe: "48d1cc",
            VviTetYd: "c71585",
            midnightXe: "191970",
            mRtcYam: "f5fffa",
            mistyPse: "ffe4e1",
            moccasR: "ffe4b5",
            navajowEte: "ffdead",
            navy: "80",
            Tdlace: "fdf5e6",
            Tive: "808000",
            TivedBb: "6b8e23",
            Sange: "ffa500",
            SangeYd: "ff4500",
            ScEd: "da70d6",
            pOegTMnPd: "eee8aa",
            pOegYF: "98fb98",
            pOeQe: "afeeee",
            pOeviTetYd: "db7093",
            papayawEp: "ffefd5",
            pHKpuff: "ffdab9",
            peru: "cd853f",
            pRk: "ffc0cb",
            plum: "dda0dd",
            powMrXe: "b0e0e6",
            purpN: "800080",
            YbeccapurpN: "663399",
            Yd: "ff0000",
            Psybrown: "bc8f8f",
            PyOXe: "4169e1",
            saddNbPwn: "8b4513",
            sOmon: "fa8072",
            sandybPwn: "f4a460",
            sHgYF: "2e8b57",
            sHshell: "fff5ee",
            siFna: "a0522d",
            silver: "c0c0c0",
            skyXe: "87ceeb",
            UXe: "6a5acd",
            UWay: "708090",
            UgYy: "708090",
            snow: "fffafa",
            sprRggYF: "ff7f",
            stAlXe: "4682b4",
            tan: "d2b48c",
            teO: "8080",
            tEstN: "d8bfd8",
            tomato: "ff6347",
            Qe: "40e0d0",
            viTet: "ee82ee",
            JHt: "f5deb3",
            wEte: "ffffff",
            wEtesmoke: "f5f5f5",
            Lw: "ffff00",
            LwgYF: "9acd32"
        };
        function unpack() {
            const unpacked = {};
            const keys = Object.keys(names);
            const tkeys = Object.keys(map$1);
            let i, j, k, ok, nk;
            for (i = 0; i < keys.length; i++) {
                ok = nk = keys[i];
                for (j = 0; j < tkeys.length; j++) {
                    k = tkeys[j];
                    nk = nk.replace(k, map$1[k]);
                }
                k = parseInt(names[ok], 16);
                unpacked[nk] = [ k >> 16 & 255, k >> 8 & 255, 255 & k ];
            }
            return unpacked;
        }
        let names$1;
        function nameParse(str) {
            if (!names$1) {
                names$1 = unpack();
                names$1.transparent = [ 0, 0, 0, 0 ];
            }
            const a = names$1[str.toLowerCase()];
            return a && {
                r: a[0],
                g: a[1],
                b: a[2],
                a: 4 === a.length ? a[3] : 255
            };
        }
        function modHSL(v, i, ratio) {
            if (v) {
                let tmp = rgb2hsl(v);
                tmp[i] = Math.max(0, Math.min(tmp[i] + tmp[i] * ratio, 0 === i ? 360 : 1));
                tmp = hsl2rgb(tmp);
                v.r = tmp[0];
                v.g = tmp[1];
                v.b = tmp[2];
            }
        }
        function clone(v, proto) {
            return v ? Object.assign(proto || {}, v) : v;
        }
        function fromObject(input) {
            var v = {
                r: 0,
                g: 0,
                b: 0,
                a: 255
            };
            if (Array.isArray(input)) {
                if (input.length >= 3) {
                    v = {
                        r: input[0],
                        g: input[1],
                        b: input[2],
                        a: 255
                    };
                    if (input.length > 3) v.a = n2b(input[3]);
                }
            } else {
                v = clone(input, {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 1
                });
                v.a = n2b(v.a);
            }
            return v;
        }
        function functionParse(str) {
            if ("r" === str.charAt(0)) return rgbParse(str);
            return hueParse(str);
        }
        class Color {
            constructor(input) {
                if (input instanceof Color) return input;
                const type = typeof input;
                let v;
                if ("object" === type) v = fromObject(input); else if ("string" === type) v = hexParse(input) || nameParse(input) || functionParse(input);
                this._rgb = v;
                this._valid = !!v;
            }
            get valid() {
                return this._valid;
            }
            get rgb() {
                var v = clone(this._rgb);
                if (v) v.a = b2n(v.a);
                return v;
            }
            set rgb(obj) {
                this._rgb = fromObject(obj);
            }
            rgbString() {
                return this._valid ? rgbString(this._rgb) : this._rgb;
            }
            hexString() {
                return this._valid ? hexString(this._rgb) : this._rgb;
            }
            hslString() {
                return this._valid ? hslString(this._rgb) : this._rgb;
            }
            mix(color, weight) {
                const me = this;
                if (color) {
                    const c1 = me.rgb;
                    const c2 = color.rgb;
                    let w2;
                    const p = weight === w2 ? .5 : weight;
                    const w = 2 * p - 1;
                    const a = c1.a - c2.a;
                    const w1 = ((w * a === -1 ? w : (w + a) / (1 + w * a)) + 1) / 2;
                    w2 = 1 - w1;
                    c1.r = 255 & w1 * c1.r + w2 * c2.r + .5;
                    c1.g = 255 & w1 * c1.g + w2 * c2.g + .5;
                    c1.b = 255 & w1 * c1.b + w2 * c2.b + .5;
                    c1.a = p * c1.a + (1 - p) * c2.a;
                    me.rgb = c1;
                }
                return me;
            }
            clone() {
                return new Color(this.rgb);
            }
            alpha(a) {
                this._rgb.a = n2b(a);
                return this;
            }
            clearer(ratio) {
                const rgb = this._rgb;
                rgb.a *= 1 - ratio;
                return this;
            }
            greyscale() {
                const rgb = this._rgb;
                const val = round(.3 * rgb.r + .59 * rgb.g + .11 * rgb.b);
                rgb.r = rgb.g = rgb.b = val;
                return this;
            }
            opaquer(ratio) {
                const rgb = this._rgb;
                rgb.a *= 1 + ratio;
                return this;
            }
            negate() {
                const v = this._rgb;
                v.r = 255 - v.r;
                v.g = 255 - v.g;
                v.b = 255 - v.b;
                return this;
            }
            lighten(ratio) {
                modHSL(this._rgb, 2, ratio);
                return this;
            }
            darken(ratio) {
                modHSL(this._rgb, 2, -ratio);
                return this;
            }
            saturate(ratio) {
                modHSL(this._rgb, 1, ratio);
                return this;
            }
            desaturate(ratio) {
                modHSL(this._rgb, 1, -ratio);
                return this;
            }
            rotate(deg) {
                rotate(this._rgb, deg);
                return this;
            }
        }
        function index_esm(input) {
            return new Color(input);
        }
        const isPatternOrGradient = value => value instanceof CanvasGradient || value instanceof CanvasPattern;
        function color(value) {
            return isPatternOrGradient(value) ? value : index_esm(value);
        }
        function getHoverColor(value) {
            return isPatternOrGradient(value) ? value : index_esm(value).saturate(.5).darken(.1).hexString();
        }
        const overrides = Object.create(null);
        const descriptors = Object.create(null);
        function getScope$1(node, key) {
            if (!key) return node;
            const keys = key.split(".");
            for (let i = 0, n = keys.length; i < n; ++i) {
                const k = keys[i];
                node = node[k] || (node[k] = Object.create(null));
            }
            return node;
        }
        function set(root, scope, values) {
            if ("string" === typeof scope) return merge(getScope$1(root, scope), values);
            return merge(getScope$1(root, ""), scope);
        }
        class Defaults {
            constructor(_descriptors) {
                this.animation = void 0;
                this.backgroundColor = "rgba(0,0,0,0.1)";
                this.borderColor = "rgba(0,0,0,0.1)";
                this.color = "#666";
                this.datasets = {};
                this.devicePixelRatio = context => context.chart.platform.getDevicePixelRatio();
                this.elements = {};
                this.events = [ "mousemove", "mouseout", "click", "touchstart", "touchmove" ];
                this.font = {
                    family: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                    size: 12,
                    style: "normal",
                    lineHeight: 1.2,
                    weight: null
                };
                this.hover = {};
                this.hoverBackgroundColor = (ctx, options) => getHoverColor(options.backgroundColor);
                this.hoverBorderColor = (ctx, options) => getHoverColor(options.borderColor);
                this.hoverColor = (ctx, options) => getHoverColor(options.color);
                this.indexAxis = "x";
                this.interaction = {
                    mode: "nearest",
                    intersect: true
                };
                this.maintainAspectRatio = true;
                this.onHover = null;
                this.onClick = null;
                this.parsing = true;
                this.plugins = {};
                this.responsive = true;
                this.scale = void 0;
                this.scales = {};
                this.showLine = true;
                this.drawActiveElementsOnTop = true;
                this.describe(_descriptors);
            }
            set(scope, values) {
                return set(this, scope, values);
            }
            get(scope) {
                return getScope$1(this, scope);
            }
            describe(scope, values) {
                return set(descriptors, scope, values);
            }
            override(scope, values) {
                return set(overrides, scope, values);
            }
            route(scope, name, targetScope, targetName) {
                const scopeObject = getScope$1(this, scope);
                const targetScopeObject = getScope$1(this, targetScope);
                const privateName = "_" + name;
                Object.defineProperties(scopeObject, {
                    [privateName]: {
                        value: scopeObject[name],
                        writable: true
                    },
                    [name]: {
                        enumerable: true,
                        get() {
                            const local = this[privateName];
                            const target = targetScopeObject[targetName];
                            if (isObject(local)) return Object.assign({}, target, local);
                            return valueOrDefault(local, target);
                        },
                        set(value) {
                            this[privateName] = value;
                        }
                    }
                });
            }
        }
        var defaults = new Defaults({
            _scriptable: name => !name.startsWith("on"),
            _indexable: name => "events" !== name,
            hover: {
                _fallback: "interaction"
            },
            interaction: {
                _scriptable: false,
                _indexable: false
            }
        });
        function toFontString(font) {
            if (!font || isNullOrUndef(font.size) || isNullOrUndef(font.family)) return null;
            return (font.style ? font.style + " " : "") + (font.weight ? font.weight + " " : "") + font.size + "px " + font.family;
        }
        function _measureText(ctx, data, gc, longest, string) {
            let textWidth = data[string];
            if (!textWidth) {
                textWidth = data[string] = ctx.measureText(string).width;
                gc.push(string);
            }
            if (textWidth > longest) longest = textWidth;
            return longest;
        }
        function _longestText(ctx, font, arrayOfThings, cache) {
            cache = cache || {};
            let data = cache.data = cache.data || {};
            let gc = cache.garbageCollect = cache.garbageCollect || [];
            if (cache.font !== font) {
                data = cache.data = {};
                gc = cache.garbageCollect = [];
                cache.font = font;
            }
            ctx.save();
            ctx.font = font;
            let longest = 0;
            const ilen = arrayOfThings.length;
            let i, j, jlen, thing, nestedThing;
            for (i = 0; i < ilen; i++) {
                thing = arrayOfThings[i];
                if (void 0 !== thing && null !== thing && true !== isArray(thing)) longest = _measureText(ctx, data, gc, longest, thing); else if (isArray(thing)) for (j = 0, 
                jlen = thing.length; j < jlen; j++) {
                    nestedThing = thing[j];
                    if (void 0 !== nestedThing && null !== nestedThing && !isArray(nestedThing)) longest = _measureText(ctx, data, gc, longest, nestedThing);
                }
            }
            ctx.restore();
            const gcLen = gc.length / 2;
            if (gcLen > arrayOfThings.length) {
                for (i = 0; i < gcLen; i++) delete data[gc[i]];
                gc.splice(0, gcLen);
            }
            return longest;
        }
        function _alignPixel(chart, pixel, width) {
            const devicePixelRatio = chart.currentDevicePixelRatio;
            const halfWidth = 0 !== width ? Math.max(width / 2, .5) : 0;
            return Math.round((pixel - halfWidth) * devicePixelRatio) / devicePixelRatio + halfWidth;
        }
        function clearCanvas(canvas, ctx) {
            ctx = ctx || canvas.getContext("2d");
            ctx.save();
            ctx.resetTransform();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.restore();
        }
        function drawPoint(ctx, options, x, y) {
            let type, xOffset, yOffset, size, cornerRadius;
            const style = options.pointStyle;
            const rotation = options.rotation;
            const radius = options.radius;
            let rad = (rotation || 0) * RAD_PER_DEG;
            if (style && "object" === typeof style) {
                type = style.toString();
                if ("[object HTMLImageElement]" === type || "[object HTMLCanvasElement]" === type) {
                    ctx.save();
                    ctx.translate(x, y);
                    ctx.rotate(rad);
                    ctx.drawImage(style, -style.width / 2, -style.height / 2, style.width, style.height);
                    ctx.restore();
                    return;
                }
            }
            if (isNaN(radius) || radius <= 0) return;
            ctx.beginPath();
            switch (style) {
              default:
                ctx.arc(x, y, radius, 0, TAU);
                ctx.closePath();
                break;

              case "triangle":
                ctx.moveTo(x + Math.sin(rad) * radius, y - Math.cos(rad) * radius);
                rad += TWO_THIRDS_PI;
                ctx.lineTo(x + Math.sin(rad) * radius, y - Math.cos(rad) * radius);
                rad += TWO_THIRDS_PI;
                ctx.lineTo(x + Math.sin(rad) * radius, y - Math.cos(rad) * radius);
                ctx.closePath();
                break;

              case "rectRounded":
                cornerRadius = .516 * radius;
                size = radius - cornerRadius;
                xOffset = Math.cos(rad + QUARTER_PI) * size;
                yOffset = Math.sin(rad + QUARTER_PI) * size;
                ctx.arc(x - xOffset, y - yOffset, cornerRadius, rad - PI, rad - HALF_PI);
                ctx.arc(x + yOffset, y - xOffset, cornerRadius, rad - HALF_PI, rad);
                ctx.arc(x + xOffset, y + yOffset, cornerRadius, rad, rad + HALF_PI);
                ctx.arc(x - yOffset, y + xOffset, cornerRadius, rad + HALF_PI, rad + PI);
                ctx.closePath();
                break;

              case "rect":
                if (!rotation) {
                    size = Math.SQRT1_2 * radius;
                    ctx.rect(x - size, y - size, 2 * size, 2 * size);
                    break;
                }
                rad += QUARTER_PI;

              case "rectRot":
                xOffset = Math.cos(rad) * radius;
                yOffset = Math.sin(rad) * radius;
                ctx.moveTo(x - xOffset, y - yOffset);
                ctx.lineTo(x + yOffset, y - xOffset);
                ctx.lineTo(x + xOffset, y + yOffset);
                ctx.lineTo(x - yOffset, y + xOffset);
                ctx.closePath();
                break;

              case "crossRot":
                rad += QUARTER_PI;

              case "cross":
                xOffset = Math.cos(rad) * radius;
                yOffset = Math.sin(rad) * radius;
                ctx.moveTo(x - xOffset, y - yOffset);
                ctx.lineTo(x + xOffset, y + yOffset);
                ctx.moveTo(x + yOffset, y - xOffset);
                ctx.lineTo(x - yOffset, y + xOffset);
                break;

              case "star":
                xOffset = Math.cos(rad) * radius;
                yOffset = Math.sin(rad) * radius;
                ctx.moveTo(x - xOffset, y - yOffset);
                ctx.lineTo(x + xOffset, y + yOffset);
                ctx.moveTo(x + yOffset, y - xOffset);
                ctx.lineTo(x - yOffset, y + xOffset);
                rad += QUARTER_PI;
                xOffset = Math.cos(rad) * radius;
                yOffset = Math.sin(rad) * radius;
                ctx.moveTo(x - xOffset, y - yOffset);
                ctx.lineTo(x + xOffset, y + yOffset);
                ctx.moveTo(x + yOffset, y - xOffset);
                ctx.lineTo(x - yOffset, y + xOffset);
                break;

              case "line":
                xOffset = Math.cos(rad) * radius;
                yOffset = Math.sin(rad) * radius;
                ctx.moveTo(x - xOffset, y - yOffset);
                ctx.lineTo(x + xOffset, y + yOffset);
                break;

              case "dash":
                ctx.moveTo(x, y);
                ctx.lineTo(x + Math.cos(rad) * radius, y + Math.sin(rad) * radius);
                break;
            }
            ctx.fill();
            if (options.borderWidth > 0) ctx.stroke();
        }
        function _isPointInArea(point, area, margin) {
            margin = margin || .5;
            return !area || point && point.x > area.left - margin && point.x < area.right + margin && point.y > area.top - margin && point.y < area.bottom + margin;
        }
        function clipArea(ctx, area) {
            ctx.save();
            ctx.beginPath();
            ctx.rect(area.left, area.top, area.right - area.left, area.bottom - area.top);
            ctx.clip();
        }
        function unclipArea(ctx) {
            ctx.restore();
        }
        function _steppedLineTo(ctx, previous, target, flip, mode) {
            if (!previous) return ctx.lineTo(target.x, target.y);
            if ("middle" === mode) {
                const midpoint = (previous.x + target.x) / 2;
                ctx.lineTo(midpoint, previous.y);
                ctx.lineTo(midpoint, target.y);
            } else if ("after" === mode !== !!flip) ctx.lineTo(previous.x, target.y); else ctx.lineTo(target.x, previous.y);
            ctx.lineTo(target.x, target.y);
        }
        function _bezierCurveTo(ctx, previous, target, flip) {
            if (!previous) return ctx.lineTo(target.x, target.y);
            ctx.bezierCurveTo(flip ? previous.cp1x : previous.cp2x, flip ? previous.cp1y : previous.cp2y, flip ? target.cp2x : target.cp1x, flip ? target.cp2y : target.cp1y, target.x, target.y);
        }
        function renderText(ctx, text, x, y, font, opts = {}) {
            const lines = isArray(text) ? text : [ text ];
            const stroke = opts.strokeWidth > 0 && "" !== opts.strokeColor;
            let i, line;
            ctx.save();
            ctx.font = font.string;
            setRenderOpts(ctx, opts);
            for (i = 0; i < lines.length; ++i) {
                line = lines[i];
                if (stroke) {
                    if (opts.strokeColor) ctx.strokeStyle = opts.strokeColor;
                    if (!isNullOrUndef(opts.strokeWidth)) ctx.lineWidth = opts.strokeWidth;
                    ctx.strokeText(line, x, y, opts.maxWidth);
                }
                ctx.fillText(line, x, y, opts.maxWidth);
                decorateText(ctx, x, y, line, opts);
                y += font.lineHeight;
            }
            ctx.restore();
        }
        function setRenderOpts(ctx, opts) {
            if (opts.translation) ctx.translate(opts.translation[0], opts.translation[1]);
            if (!isNullOrUndef(opts.rotation)) ctx.rotate(opts.rotation);
            if (opts.color) ctx.fillStyle = opts.color;
            if (opts.textAlign) ctx.textAlign = opts.textAlign;
            if (opts.textBaseline) ctx.textBaseline = opts.textBaseline;
        }
        function decorateText(ctx, x, y, line, opts) {
            if (opts.strikethrough || opts.underline) {
                const metrics = ctx.measureText(line);
                const left = x - metrics.actualBoundingBoxLeft;
                const right = x + metrics.actualBoundingBoxRight;
                const top = y - metrics.actualBoundingBoxAscent;
                const bottom = y + metrics.actualBoundingBoxDescent;
                const yDecoration = opts.strikethrough ? (top + bottom) / 2 : bottom;
                ctx.strokeStyle = ctx.fillStyle;
                ctx.beginPath();
                ctx.lineWidth = opts.decorationWidth || 2;
                ctx.moveTo(left, yDecoration);
                ctx.lineTo(right, yDecoration);
                ctx.stroke();
            }
        }
        function addRoundedRectPath(ctx, rect) {
            const {x, y, w, h, radius} = rect;
            ctx.arc(x + radius.topLeft, y + radius.topLeft, radius.topLeft, -HALF_PI, PI, true);
            ctx.lineTo(x, y + h - radius.bottomLeft);
            ctx.arc(x + radius.bottomLeft, y + h - radius.bottomLeft, radius.bottomLeft, PI, HALF_PI, true);
            ctx.lineTo(x + w - radius.bottomRight, y + h);
            ctx.arc(x + w - radius.bottomRight, y + h - radius.bottomRight, radius.bottomRight, HALF_PI, 0, true);
            ctx.lineTo(x + w, y + radius.topRight);
            ctx.arc(x + w - radius.topRight, y + radius.topRight, radius.topRight, 0, -HALF_PI, true);
            ctx.lineTo(x + radius.topLeft, y);
        }
        const LINE_HEIGHT = new RegExp(/^(normal|(\d+(?:\.\d+)?)(px|em|%)?)$/);
        const FONT_STYLE = new RegExp(/^(normal|italic|initial|inherit|unset|(oblique( -?[0-9]?[0-9]deg)?))$/);
        function toLineHeight(value, size) {
            const matches = ("" + value).match(LINE_HEIGHT);
            if (!matches || "normal" === matches[1]) return 1.2 * size;
            value = +matches[2];
            switch (matches[3]) {
              case "px":
                return value;

              case "%":
                value /= 100;
                break;
            }
            return size * value;
        }
        const numberOrZero = v => +v || 0;
        function _readValueToProps(value, props) {
            const ret = {};
            const objProps = isObject(props);
            const keys = objProps ? Object.keys(props) : props;
            const read = isObject(value) ? objProps ? prop => valueOrDefault(value[prop], value[props[prop]]) : prop => value[prop] : () => value;
            for (const prop of keys) ret[prop] = numberOrZero(read(prop));
            return ret;
        }
        function toTRBL(value) {
            return _readValueToProps(value, {
                top: "y",
                right: "x",
                bottom: "y",
                left: "x"
            });
        }
        function toTRBLCorners(value) {
            return _readValueToProps(value, [ "topLeft", "topRight", "bottomLeft", "bottomRight" ]);
        }
        function toPadding(value) {
            const obj = toTRBL(value);
            obj.width = obj.left + obj.right;
            obj.height = obj.top + obj.bottom;
            return obj;
        }
        function toFont(options, fallback) {
            options = options || {};
            fallback = fallback || defaults.font;
            let size = valueOrDefault(options.size, fallback.size);
            if ("string" === typeof size) size = parseInt(size, 10);
            let style = valueOrDefault(options.style, fallback.style);
            if (style && !("" + style).match(FONT_STYLE)) {
                console.warn('Invalid font style specified: "' + style + '"');
                style = "";
            }
            const font = {
                family: valueOrDefault(options.family, fallback.family),
                lineHeight: toLineHeight(valueOrDefault(options.lineHeight, fallback.lineHeight), size),
                size,
                style,
                weight: valueOrDefault(options.weight, fallback.weight),
                string: ""
            };
            font.string = toFontString(font);
            return font;
        }
        function resolve(inputs, context, index, info) {
            let cacheable = true;
            let i, ilen, value;
            for (i = 0, ilen = inputs.length; i < ilen; ++i) {
                value = inputs[i];
                if (void 0 === value) continue;
                if (void 0 !== context && "function" === typeof value) {
                    value = value(context);
                    cacheable = false;
                }
                if (void 0 !== index && isArray(value)) {
                    value = value[index % value.length];
                    cacheable = false;
                }
                if (void 0 !== value) {
                    if (info && !cacheable) info.cacheable = false;
                    return value;
                }
            }
        }
        function _addGrace(minmax, grace, beginAtZero) {
            const {min, max} = minmax;
            const change = toDimension(grace, (max - min) / 2);
            const keepZero = (value, add) => beginAtZero && 0 === value ? 0 : value + add;
            return {
                min: keepZero(min, -Math.abs(change)),
                max: keepZero(max, change)
            };
        }
        function createContext(parentContext, context) {
            return Object.assign(Object.create(parentContext), context);
        }
        function _lookup(table, value, cmp) {
            cmp = cmp || (index => table[index] < value);
            let hi = table.length - 1;
            let lo = 0;
            let mid;
            while (hi - lo > 1) {
                mid = lo + hi >> 1;
                if (cmp(mid)) lo = mid; else hi = mid;
            }
            return {
                lo,
                hi
            };
        }
        const _lookupByKey = (table, key, value) => _lookup(table, value, (index => table[index][key] < value));
        const _rlookupByKey = (table, key, value) => _lookup(table, value, (index => table[index][key] >= value));
        function _filterBetween(values, min, max) {
            let start = 0;
            let end = values.length;
            while (start < end && values[start] < min) start++;
            while (end > start && values[end - 1] > max) end--;
            return start > 0 || end < values.length ? values.slice(start, end) : values;
        }
        const arrayEvents = [ "push", "pop", "shift", "splice", "unshift" ];
        function listenArrayEvents(array, listener) {
            if (array._chartjs) {
                array._chartjs.listeners.push(listener);
                return;
            }
            Object.defineProperty(array, "_chartjs", {
                configurable: true,
                enumerable: false,
                value: {
                    listeners: [ listener ]
                }
            });
            arrayEvents.forEach((key => {
                const method = "_onData" + _capitalize(key);
                const base = array[key];
                Object.defineProperty(array, key, {
                    configurable: true,
                    enumerable: false,
                    value(...args) {
                        const res = base.apply(this, args);
                        array._chartjs.listeners.forEach((object => {
                            if ("function" === typeof object[method]) object[method](...args);
                        }));
                        return res;
                    }
                });
            }));
        }
        function unlistenArrayEvents(array, listener) {
            const stub = array._chartjs;
            if (!stub) return;
            const listeners = stub.listeners;
            const index = listeners.indexOf(listener);
            if (-1 !== index) listeners.splice(index, 1);
            if (listeners.length > 0) return;
            arrayEvents.forEach((key => {
                delete array[key];
            }));
            delete array._chartjs;
        }
        function _arrayUnique(items) {
            const set = new Set;
            let i, ilen;
            for (i = 0, ilen = items.length; i < ilen; ++i) set.add(items[i]);
            if (set.size === ilen) return items;
            return Array.from(set);
        }
        function _createResolver(scopes, prefixes = [ "" ], rootScopes = scopes, fallback, getTarget = (() => scopes[0])) {
            if (!defined(fallback)) fallback = _resolve("_fallback", scopes);
            const cache = {
                [Symbol.toStringTag]: "Object",
                _cacheable: true,
                _scopes: scopes,
                _rootScopes: rootScopes,
                _fallback: fallback,
                _getTarget: getTarget,
                override: scope => _createResolver([ scope, ...scopes ], prefixes, rootScopes, fallback)
            };
            return new Proxy(cache, {
                deleteProperty(target, prop) {
                    delete target[prop];
                    delete target._keys;
                    delete scopes[0][prop];
                    return true;
                },
                get(target, prop) {
                    return _cached(target, prop, (() => _resolveWithPrefixes(prop, prefixes, scopes, target)));
                },
                getOwnPropertyDescriptor(target, prop) {
                    return Reflect.getOwnPropertyDescriptor(target._scopes[0], prop);
                },
                getPrototypeOf() {
                    return Reflect.getPrototypeOf(scopes[0]);
                },
                has(target, prop) {
                    return getKeysFromAllScopes(target).includes(prop);
                },
                ownKeys(target) {
                    return getKeysFromAllScopes(target);
                },
                set(target, prop, value) {
                    const storage = target._storage || (target._storage = getTarget());
                    target[prop] = storage[prop] = value;
                    delete target._keys;
                    return true;
                }
            });
        }
        function _attachContext(proxy, context, subProxy, descriptorDefaults) {
            const cache = {
                _cacheable: false,
                _proxy: proxy,
                _context: context,
                _subProxy: subProxy,
                _stack: new Set,
                _descriptors: _descriptors(proxy, descriptorDefaults),
                setContext: ctx => _attachContext(proxy, ctx, subProxy, descriptorDefaults),
                override: scope => _attachContext(proxy.override(scope), context, subProxy, descriptorDefaults)
            };
            return new Proxy(cache, {
                deleteProperty(target, prop) {
                    delete target[prop];
                    delete proxy[prop];
                    return true;
                },
                get(target, prop, receiver) {
                    return _cached(target, prop, (() => _resolveWithContext(target, prop, receiver)));
                },
                getOwnPropertyDescriptor(target, prop) {
                    return target._descriptors.allKeys ? Reflect.has(proxy, prop) ? {
                        enumerable: true,
                        configurable: true
                    } : void 0 : Reflect.getOwnPropertyDescriptor(proxy, prop);
                },
                getPrototypeOf() {
                    return Reflect.getPrototypeOf(proxy);
                },
                has(target, prop) {
                    return Reflect.has(proxy, prop);
                },
                ownKeys() {
                    return Reflect.ownKeys(proxy);
                },
                set(target, prop, value) {
                    proxy[prop] = value;
                    delete target[prop];
                    return true;
                }
            });
        }
        function _descriptors(proxy, defaults = {
            scriptable: true,
            indexable: true
        }) {
            const {_scriptable = defaults.scriptable, _indexable = defaults.indexable, _allKeys = defaults.allKeys} = proxy;
            return {
                allKeys: _allKeys,
                scriptable: _scriptable,
                indexable: _indexable,
                isScriptable: isFunction(_scriptable) ? _scriptable : () => _scriptable,
                isIndexable: isFunction(_indexable) ? _indexable : () => _indexable
            };
        }
        const readKey = (prefix, name) => prefix ? prefix + _capitalize(name) : name;
        const needsSubResolver = (prop, value) => isObject(value) && "adapters" !== prop && (null === Object.getPrototypeOf(value) || value.constructor === Object);
        function _cached(target, prop, resolve) {
            if (Object.prototype.hasOwnProperty.call(target, prop)) return target[prop];
            const value = resolve();
            target[prop] = value;
            return value;
        }
        function _resolveWithContext(target, prop, receiver) {
            const {_proxy, _context, _subProxy, _descriptors: descriptors} = target;
            let value = _proxy[prop];
            if (isFunction(value) && descriptors.isScriptable(prop)) value = _resolveScriptable(prop, value, target, receiver);
            if (isArray(value) && value.length) value = _resolveArray(prop, value, target, descriptors.isIndexable);
            if (needsSubResolver(prop, value)) value = _attachContext(value, _context, _subProxy && _subProxy[prop], descriptors);
            return value;
        }
        function _resolveScriptable(prop, value, target, receiver) {
            const {_proxy, _context, _subProxy, _stack} = target;
            if (_stack.has(prop)) throw new Error("Recursion detected: " + Array.from(_stack).join("->") + "->" + prop);
            _stack.add(prop);
            value = value(_context, _subProxy || receiver);
            _stack.delete(prop);
            if (needsSubResolver(prop, value)) value = createSubResolver(_proxy._scopes, _proxy, prop, value);
            return value;
        }
        function _resolveArray(prop, value, target, isIndexable) {
            const {_proxy, _context, _subProxy, _descriptors: descriptors} = target;
            if (defined(_context.index) && isIndexable(prop)) value = value[_context.index % value.length]; else if (isObject(value[0])) {
                const arr = value;
                const scopes = _proxy._scopes.filter((s => s !== arr));
                value = [];
                for (const item of arr) {
                    const resolver = createSubResolver(scopes, _proxy, prop, item);
                    value.push(_attachContext(resolver, _context, _subProxy && _subProxy[prop], descriptors));
                }
            }
            return value;
        }
        function resolveFallback(fallback, prop, value) {
            return isFunction(fallback) ? fallback(prop, value) : fallback;
        }
        const getScope = (key, parent) => true === key ? parent : "string" === typeof key ? resolveObjectKey(parent, key) : void 0;
        function addScopes(set, parentScopes, key, parentFallback, value) {
            for (const parent of parentScopes) {
                const scope = getScope(key, parent);
                if (scope) {
                    set.add(scope);
                    const fallback = resolveFallback(scope._fallback, key, value);
                    if (defined(fallback) && fallback !== key && fallback !== parentFallback) return fallback;
                } else if (false === scope && defined(parentFallback) && key !== parentFallback) return null;
            }
            return false;
        }
        function createSubResolver(parentScopes, resolver, prop, value) {
            const rootScopes = resolver._rootScopes;
            const fallback = resolveFallback(resolver._fallback, prop, value);
            const allScopes = [ ...parentScopes, ...rootScopes ];
            const set = new Set;
            set.add(value);
            let key = addScopesFromKey(set, allScopes, prop, fallback || prop, value);
            if (null === key) return false;
            if (defined(fallback) && fallback !== prop) {
                key = addScopesFromKey(set, allScopes, fallback, key, value);
                if (null === key) return false;
            }
            return _createResolver(Array.from(set), [ "" ], rootScopes, fallback, (() => subGetTarget(resolver, prop, value)));
        }
        function addScopesFromKey(set, allScopes, key, fallback, item) {
            while (key) key = addScopes(set, allScopes, key, fallback, item);
            return key;
        }
        function subGetTarget(resolver, prop, value) {
            const parent = resolver._getTarget();
            if (!(prop in parent)) parent[prop] = {};
            const target = parent[prop];
            if (isArray(target) && isObject(value)) return value;
            return target;
        }
        function _resolveWithPrefixes(prop, prefixes, scopes, proxy) {
            let value;
            for (const prefix of prefixes) {
                value = _resolve(readKey(prefix, prop), scopes);
                if (defined(value)) return needsSubResolver(prop, value) ? createSubResolver(scopes, proxy, prop, value) : value;
            }
        }
        function _resolve(key, scopes) {
            for (const scope of scopes) {
                if (!scope) continue;
                const value = scope[key];
                if (defined(value)) return value;
            }
        }
        function getKeysFromAllScopes(target) {
            let keys = target._keys;
            if (!keys) keys = target._keys = resolveKeysFromAllScopes(target._scopes);
            return keys;
        }
        function resolveKeysFromAllScopes(scopes) {
            const set = new Set;
            for (const scope of scopes) for (const key of Object.keys(scope).filter((k => !k.startsWith("_")))) set.add(key);
            return Array.from(set);
        }
        const EPSILON = Number.EPSILON || 1e-14;
        const getPoint = (points, i) => i < points.length && !points[i].skip && points[i];
        const getValueAxis = indexAxis => "x" === indexAxis ? "y" : "x";
        function splineCurve(firstPoint, middlePoint, afterPoint, t) {
            const previous = firstPoint.skip ? middlePoint : firstPoint;
            const current = middlePoint;
            const next = afterPoint.skip ? middlePoint : afterPoint;
            const d01 = distanceBetweenPoints(current, previous);
            const d12 = distanceBetweenPoints(next, current);
            let s01 = d01 / (d01 + d12);
            let s12 = d12 / (d01 + d12);
            s01 = isNaN(s01) ? 0 : s01;
            s12 = isNaN(s12) ? 0 : s12;
            const fa = t * s01;
            const fb = t * s12;
            return {
                previous: {
                    x: current.x - fa * (next.x - previous.x),
                    y: current.y - fa * (next.y - previous.y)
                },
                next: {
                    x: current.x + fb * (next.x - previous.x),
                    y: current.y + fb * (next.y - previous.y)
                }
            };
        }
        function monotoneAdjust(points, deltaK, mK) {
            const pointsLen = points.length;
            let alphaK, betaK, tauK, squaredMagnitude, pointCurrent;
            let pointAfter = getPoint(points, 0);
            for (let i = 0; i < pointsLen - 1; ++i) {
                pointCurrent = pointAfter;
                pointAfter = getPoint(points, i + 1);
                if (!pointCurrent || !pointAfter) continue;
                if (almostEquals(deltaK[i], 0, EPSILON)) {
                    mK[i] = mK[i + 1] = 0;
                    continue;
                }
                alphaK = mK[i] / deltaK[i];
                betaK = mK[i + 1] / deltaK[i];
                squaredMagnitude = Math.pow(alphaK, 2) + Math.pow(betaK, 2);
                if (squaredMagnitude <= 9) continue;
                tauK = 3 / Math.sqrt(squaredMagnitude);
                mK[i] = alphaK * tauK * deltaK[i];
                mK[i + 1] = betaK * tauK * deltaK[i];
            }
        }
        function monotoneCompute(points, mK, indexAxis = "x") {
            const valueAxis = getValueAxis(indexAxis);
            const pointsLen = points.length;
            let delta, pointBefore, pointCurrent;
            let pointAfter = getPoint(points, 0);
            for (let i = 0; i < pointsLen; ++i) {
                pointBefore = pointCurrent;
                pointCurrent = pointAfter;
                pointAfter = getPoint(points, i + 1);
                if (!pointCurrent) continue;
                const iPixel = pointCurrent[indexAxis];
                const vPixel = pointCurrent[valueAxis];
                if (pointBefore) {
                    delta = (iPixel - pointBefore[indexAxis]) / 3;
                    pointCurrent[`cp1${indexAxis}`] = iPixel - delta;
                    pointCurrent[`cp1${valueAxis}`] = vPixel - delta * mK[i];
                }
                if (pointAfter) {
                    delta = (pointAfter[indexAxis] - iPixel) / 3;
                    pointCurrent[`cp2${indexAxis}`] = iPixel + delta;
                    pointCurrent[`cp2${valueAxis}`] = vPixel + delta * mK[i];
                }
            }
        }
        function splineCurveMonotone(points, indexAxis = "x") {
            const valueAxis = getValueAxis(indexAxis);
            const pointsLen = points.length;
            const deltaK = Array(pointsLen).fill(0);
            const mK = Array(pointsLen);
            let i, pointBefore, pointCurrent;
            let pointAfter = getPoint(points, 0);
            for (i = 0; i < pointsLen; ++i) {
                pointBefore = pointCurrent;
                pointCurrent = pointAfter;
                pointAfter = getPoint(points, i + 1);
                if (!pointCurrent) continue;
                if (pointAfter) {
                    const slopeDelta = pointAfter[indexAxis] - pointCurrent[indexAxis];
                    deltaK[i] = 0 !== slopeDelta ? (pointAfter[valueAxis] - pointCurrent[valueAxis]) / slopeDelta : 0;
                }
                mK[i] = !pointBefore ? deltaK[i] : !pointAfter ? deltaK[i - 1] : sign(deltaK[i - 1]) !== sign(deltaK[i]) ? 0 : (deltaK[i - 1] + deltaK[i]) / 2;
            }
            monotoneAdjust(points, deltaK, mK);
            monotoneCompute(points, mK, indexAxis);
        }
        function capControlPoint(pt, min, max) {
            return Math.max(Math.min(pt, max), min);
        }
        function capBezierPoints(points, area) {
            let i, ilen, point, inArea, inAreaPrev;
            let inAreaNext = _isPointInArea(points[0], area);
            for (i = 0, ilen = points.length; i < ilen; ++i) {
                inAreaPrev = inArea;
                inArea = inAreaNext;
                inAreaNext = i < ilen - 1 && _isPointInArea(points[i + 1], area);
                if (!inArea) continue;
                point = points[i];
                if (inAreaPrev) {
                    point.cp1x = capControlPoint(point.cp1x, area.left, area.right);
                    point.cp1y = capControlPoint(point.cp1y, area.top, area.bottom);
                }
                if (inAreaNext) {
                    point.cp2x = capControlPoint(point.cp2x, area.left, area.right);
                    point.cp2y = capControlPoint(point.cp2y, area.top, area.bottom);
                }
            }
        }
        function _updateBezierControlPoints(points, options, area, loop, indexAxis) {
            let i, ilen, point, controlPoints;
            if (options.spanGaps) points = points.filter((pt => !pt.skip));
            if ("monotone" === options.cubicInterpolationMode) splineCurveMonotone(points, indexAxis); else {
                let prev = loop ? points[points.length - 1] : points[0];
                for (i = 0, ilen = points.length; i < ilen; ++i) {
                    point = points[i];
                    controlPoints = splineCurve(prev, point, points[Math.min(i + 1, ilen - (loop ? 0 : 1)) % ilen], options.tension);
                    point.cp1x = controlPoints.previous.x;
                    point.cp1y = controlPoints.previous.y;
                    point.cp2x = controlPoints.next.x;
                    point.cp2y = controlPoints.next.y;
                    prev = point;
                }
            }
            if (options.capBezierPoints) capBezierPoints(points, area);
        }
        function _isDomSupported() {
            return "undefined" !== typeof window && "undefined" !== typeof document;
        }
        function _getParentNode(domNode) {
            let parent = domNode.parentNode;
            if (parent && "[object ShadowRoot]" === parent.toString()) parent = parent.host;
            return parent;
        }
        function parseMaxStyle(styleValue, node, parentProperty) {
            let valueInPixels;
            if ("string" === typeof styleValue) {
                valueInPixels = parseInt(styleValue, 10);
                if (-1 !== styleValue.indexOf("%")) valueInPixels = valueInPixels / 100 * node.parentNode[parentProperty];
            } else valueInPixels = styleValue;
            return valueInPixels;
        }
        const helpers_segment_getComputedStyle = element => window.getComputedStyle(element, null);
        function getStyle(el, property) {
            return helpers_segment_getComputedStyle(el).getPropertyValue(property);
        }
        const positions = [ "top", "right", "bottom", "left" ];
        function getPositionedStyle(styles, style, suffix) {
            const result = {};
            suffix = suffix ? "-" + suffix : "";
            for (let i = 0; i < 4; i++) {
                const pos = positions[i];
                result[pos] = parseFloat(styles[style + "-" + pos + suffix]) || 0;
            }
            result.width = result.left + result.right;
            result.height = result.top + result.bottom;
            return result;
        }
        const useOffsetPos = (x, y, target) => (x > 0 || y > 0) && (!target || !target.shadowRoot);
        function getCanvasPosition(evt, canvas) {
            const e = evt.native || evt;
            const touches = e.touches;
            const source = touches && touches.length ? touches[0] : e;
            const {offsetX, offsetY} = source;
            let box = false;
            let x, y;
            if (useOffsetPos(offsetX, offsetY, e.target)) {
                x = offsetX;
                y = offsetY;
            } else {
                const rect = canvas.getBoundingClientRect();
                x = source.clientX - rect.left;
                y = source.clientY - rect.top;
                box = true;
            }
            return {
                x,
                y,
                box
            };
        }
        function getRelativePosition(evt, chart) {
            const {canvas, currentDevicePixelRatio} = chart;
            const style = helpers_segment_getComputedStyle(canvas);
            const borderBox = "border-box" === style.boxSizing;
            const paddings = getPositionedStyle(style, "padding");
            const borders = getPositionedStyle(style, "border", "width");
            const {x, y, box} = getCanvasPosition(evt, canvas);
            const xOffset = paddings.left + (box && borders.left);
            const yOffset = paddings.top + (box && borders.top);
            let {width, height} = chart;
            if (borderBox) {
                width -= paddings.width + borders.width;
                height -= paddings.height + borders.height;
            }
            return {
                x: Math.round((x - xOffset) / width * canvas.width / currentDevicePixelRatio),
                y: Math.round((y - yOffset) / height * canvas.height / currentDevicePixelRatio)
            };
        }
        function getContainerSize(canvas, width, height) {
            let maxWidth, maxHeight;
            if (void 0 === width || void 0 === height) {
                const container = _getParentNode(canvas);
                if (!container) {
                    width = canvas.clientWidth;
                    height = canvas.clientHeight;
                } else {
                    const rect = container.getBoundingClientRect();
                    const containerStyle = helpers_segment_getComputedStyle(container);
                    const containerBorder = getPositionedStyle(containerStyle, "border", "width");
                    const containerPadding = getPositionedStyle(containerStyle, "padding");
                    width = rect.width - containerPadding.width - containerBorder.width;
                    height = rect.height - containerPadding.height - containerBorder.height;
                    maxWidth = parseMaxStyle(containerStyle.maxWidth, container, "clientWidth");
                    maxHeight = parseMaxStyle(containerStyle.maxHeight, container, "clientHeight");
                }
            }
            return {
                width,
                height,
                maxWidth: maxWidth || INFINITY,
                maxHeight: maxHeight || INFINITY
            };
        }
        const round1 = v => Math.round(10 * v) / 10;
        function getMaximumSize(canvas, bbWidth, bbHeight, aspectRatio) {
            const style = helpers_segment_getComputedStyle(canvas);
            const margins = getPositionedStyle(style, "margin");
            const maxWidth = parseMaxStyle(style.maxWidth, canvas, "clientWidth") || INFINITY;
            const maxHeight = parseMaxStyle(style.maxHeight, canvas, "clientHeight") || INFINITY;
            const containerSize = getContainerSize(canvas, bbWidth, bbHeight);
            let {width, height} = containerSize;
            if ("content-box" === style.boxSizing) {
                const borders = getPositionedStyle(style, "border", "width");
                const paddings = getPositionedStyle(style, "padding");
                width -= paddings.width + borders.width;
                height -= paddings.height + borders.height;
            }
            width = Math.max(0, width - margins.width);
            height = Math.max(0, aspectRatio ? Math.floor(width / aspectRatio) : height - margins.height);
            width = round1(Math.min(width, maxWidth, containerSize.maxWidth));
            height = round1(Math.min(height, maxHeight, containerSize.maxHeight));
            if (width && !height) height = round1(width / 2);
            return {
                width,
                height
            };
        }
        function retinaScale(chart, forceRatio, forceStyle) {
            const pixelRatio = forceRatio || 1;
            const deviceHeight = Math.floor(chart.height * pixelRatio);
            const deviceWidth = Math.floor(chart.width * pixelRatio);
            chart.height = deviceHeight / pixelRatio;
            chart.width = deviceWidth / pixelRatio;
            const canvas = chart.canvas;
            if (canvas.style && (forceStyle || !canvas.style.height && !canvas.style.width)) {
                canvas.style.height = `${chart.height}px`;
                canvas.style.width = `${chart.width}px`;
            }
            if (chart.currentDevicePixelRatio !== pixelRatio || canvas.height !== deviceHeight || canvas.width !== deviceWidth) {
                chart.currentDevicePixelRatio = pixelRatio;
                canvas.height = deviceHeight;
                canvas.width = deviceWidth;
                chart.ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
                return true;
            }
            return false;
        }
        const supportsEventListenerOptions = function() {
            let passiveSupported = false;
            try {
                const options = {
                    get passive() {
                        passiveSupported = true;
                        return false;
                    }
                };
                window.addEventListener("test", null, options);
                window.removeEventListener("test", null, options);
            } catch (e) {}
            return passiveSupported;
        }();
        function readUsedSize(element, property) {
            const value = getStyle(element, property);
            const matches = value && value.match(/^(\d+)(\.\d+)?px$/);
            return matches ? +matches[1] : void 0;
        }
        function _pointInLine(p1, p2, t, mode) {
            return {
                x: p1.x + t * (p2.x - p1.x),
                y: p1.y + t * (p2.y - p1.y)
            };
        }
        function _steppedInterpolation(p1, p2, t, mode) {
            return {
                x: p1.x + t * (p2.x - p1.x),
                y: "middle" === mode ? t < .5 ? p1.y : p2.y : "after" === mode ? t < 1 ? p1.y : p2.y : t > 0 ? p2.y : p1.y
            };
        }
        function _bezierInterpolation(p1, p2, t, mode) {
            const cp1 = {
                x: p1.cp2x,
                y: p1.cp2y
            };
            const cp2 = {
                x: p2.cp1x,
                y: p2.cp1y
            };
            const a = _pointInLine(p1, cp1, t);
            const b = _pointInLine(cp1, cp2, t);
            const c = _pointInLine(cp2, p2, t);
            const d = _pointInLine(a, b, t);
            const e = _pointInLine(b, c, t);
            return _pointInLine(d, e, t);
        }
        const intlCache = new Map;
        function getNumberFormat(locale, options) {
            options = options || {};
            const cacheKey = locale + JSON.stringify(options);
            let formatter = intlCache.get(cacheKey);
            if (!formatter) {
                formatter = new Intl.NumberFormat(locale, options);
                intlCache.set(cacheKey, formatter);
            }
            return formatter;
        }
        function formatNumber(num, locale, options) {
            return getNumberFormat(locale, options).format(num);
        }
        const getRightToLeftAdapter = function(rectX, width) {
            return {
                x(x) {
                    return rectX + rectX + width - x;
                },
                setWidth(w) {
                    width = w;
                },
                textAlign(align) {
                    if ("center" === align) return align;
                    return "right" === align ? "left" : "right";
                },
                xPlus(x, value) {
                    return x - value;
                },
                leftForLtr(x, itemWidth) {
                    return x - itemWidth;
                }
            };
        };
        const getLeftToRightAdapter = function() {
            return {
                x(x) {
                    return x;
                },
                setWidth(w) {},
                textAlign(align) {
                    return align;
                },
                xPlus(x, value) {
                    return x + value;
                },
                leftForLtr(x, _itemWidth) {
                    return x;
                }
            };
        };
        function getRtlAdapter(rtl, rectX, width) {
            return rtl ? getRightToLeftAdapter(rectX, width) : getLeftToRightAdapter();
        }
        function overrideTextDirection(ctx, direction) {
            let style, original;
            if ("ltr" === direction || "rtl" === direction) {
                style = ctx.canvas.style;
                original = [ style.getPropertyValue("direction"), style.getPropertyPriority("direction") ];
                style.setProperty("direction", direction, "important");
                ctx.prevTextDirection = original;
            }
        }
        function restoreTextDirection(ctx, original) {
            if (void 0 !== original) {
                delete ctx.prevTextDirection;
                ctx.canvas.style.setProperty("direction", original[0], original[1]);
            }
        }
        function propertyFn(property) {
            if ("angle" === property) return {
                between: _angleBetween,
                compare: _angleDiff,
                normalize: _normalizeAngle
            };
            return {
                between: _isBetween,
                compare: (a, b) => a - b,
                normalize: x => x
            };
        }
        function normalizeSegment({start, end, count, loop, style}) {
            return {
                start: start % count,
                end: end % count,
                loop: loop && (end - start + 1) % count === 0,
                style
            };
        }
        function getSegment(segment, points, bounds) {
            const {property, start: startBound, end: endBound} = bounds;
            const {between, normalize} = propertyFn(property);
            const count = points.length;
            let {start, end, loop} = segment;
            let i, ilen;
            if (loop) {
                start += count;
                end += count;
                for (i = 0, ilen = count; i < ilen; ++i) {
                    if (!between(normalize(points[start % count][property]), startBound, endBound)) break;
                    start--;
                    end--;
                }
                start %= count;
                end %= count;
            }
            if (end < start) end += count;
            return {
                start,
                end,
                loop,
                style: segment.style
            };
        }
        function _boundSegment(segment, points, bounds) {
            if (!bounds) return [ segment ];
            const {property, start: startBound, end: endBound} = bounds;
            const count = points.length;
            const {compare, between, normalize} = propertyFn(property);
            const {start, end, loop, style} = getSegment(segment, points, bounds);
            const result = [];
            let inside = false;
            let subStart = null;
            let value, point, prevValue;
            const startIsBefore = () => between(startBound, prevValue, value) && 0 !== compare(startBound, prevValue);
            const endIsBefore = () => 0 === compare(endBound, value) || between(endBound, prevValue, value);
            const shouldStart = () => inside || startIsBefore();
            const shouldStop = () => !inside || endIsBefore();
            for (let i = start, prev = start; i <= end; ++i) {
                point = points[i % count];
                if (point.skip) continue;
                value = normalize(point[property]);
                if (value === prevValue) continue;
                inside = between(value, startBound, endBound);
                if (null === subStart && shouldStart()) subStart = 0 === compare(value, startBound) ? i : prev;
                if (null !== subStart && shouldStop()) {
                    result.push(normalizeSegment({
                        start: subStart,
                        end: i,
                        loop,
                        count,
                        style
                    }));
                    subStart = null;
                }
                prev = i;
                prevValue = value;
            }
            if (null !== subStart) result.push(normalizeSegment({
                start: subStart,
                end,
                loop,
                count,
                style
            }));
            return result;
        }
        function _boundSegments(line, bounds) {
            const result = [];
            const segments = line.segments;
            for (let i = 0; i < segments.length; i++) {
                const sub = _boundSegment(segments[i], line.points, bounds);
                if (sub.length) result.push(...sub);
            }
            return result;
        }
        function findStartAndEnd(points, count, loop, spanGaps) {
            let start = 0;
            let end = count - 1;
            if (loop && !spanGaps) while (start < count && !points[start].skip) start++;
            while (start < count && points[start].skip) start++;
            start %= count;
            if (loop) end += start;
            while (end > start && points[end % count].skip) end--;
            end %= count;
            return {
                start,
                end
            };
        }
        function solidSegments(points, start, max, loop) {
            const count = points.length;
            const result = [];
            let last = start;
            let prev = points[start];
            let end;
            for (end = start + 1; end <= max; ++end) {
                const cur = points[end % count];
                if (cur.skip || cur.stop) {
                    if (!prev.skip) {
                        loop = false;
                        result.push({
                            start: start % count,
                            end: (end - 1) % count,
                            loop
                        });
                        start = last = cur.stop ? end : null;
                    }
                } else {
                    last = end;
                    if (prev.skip) start = end;
                }
                prev = cur;
            }
            if (null !== last) result.push({
                start: start % count,
                end: last % count,
                loop
            });
            return result;
        }
        function _computeSegments(line, segmentOptions) {
            const points = line.points;
            const spanGaps = line.options.spanGaps;
            const count = points.length;
            if (!count) return [];
            const loop = !!line._loop;
            const {start, end} = findStartAndEnd(points, count, loop, spanGaps);
            if (true === spanGaps) return splitByStyles(line, [ {
                start,
                end,
                loop
            } ], points, segmentOptions);
            const max = end < start ? end + count : end;
            const completeLoop = !!line._fullLoop && 0 === start && end === count - 1;
            return splitByStyles(line, solidSegments(points, start, max, completeLoop), points, segmentOptions);
        }
        function splitByStyles(line, segments, points, segmentOptions) {
            if (!segmentOptions || !segmentOptions.setContext || !points) return segments;
            return doSplitByStyles(line, segments, points, segmentOptions);
        }
        function doSplitByStyles(line, segments, points, segmentOptions) {
            const chartContext = line._chart.getContext();
            const baseStyle = readStyle(line.options);
            const {_datasetIndex: datasetIndex, options: {spanGaps}} = line;
            const count = points.length;
            const result = [];
            let prevStyle = baseStyle;
            let start = segments[0].start;
            let i = start;
            function addStyle(s, e, l, st) {
                const dir = spanGaps ? -1 : 1;
                if (s === e) return;
                s += count;
                while (points[s % count].skip) s -= dir;
                while (points[e % count].skip) e += dir;
                if (s % count !== e % count) {
                    result.push({
                        start: s % count,
                        end: e % count,
                        loop: l,
                        style: st
                    });
                    prevStyle = st;
                    start = e % count;
                }
            }
            for (const segment of segments) {
                start = spanGaps ? start : segment.start;
                let prev = points[start % count];
                let style;
                for (i = start + 1; i <= segment.end; i++) {
                    const pt = points[i % count];
                    style = readStyle(segmentOptions.setContext(createContext(chartContext, {
                        type: "segment",
                        p0: prev,
                        p1: pt,
                        p0DataIndex: (i - 1) % count,
                        p1DataIndex: i % count,
                        datasetIndex
                    })));
                    if (styleChanged(style, prevStyle)) addStyle(start, i - 1, segment.loop, prevStyle);
                    prev = pt;
                    prevStyle = style;
                }
                if (start < i - 1) addStyle(start, i - 1, segment.loop, prevStyle);
            }
            return result;
        }
        function readStyle(options) {
            return {
                backgroundColor: options.backgroundColor,
                borderCapStyle: options.borderCapStyle,
                borderDash: options.borderDash,
                borderDashOffset: options.borderDashOffset,
                borderJoinStyle: options.borderJoinStyle,
                borderWidth: options.borderWidth,
                borderColor: options.borderColor
            };
        }
        function styleChanged(style, prevStyle) {
            return prevStyle && JSON.stringify(style) !== JSON.stringify(prevStyle);
        }
        /*!
 * Chart.js v3.7.0
 * https://www.chartjs.org
 * (c) 2021 Chart.js Contributors
 * Released under the MIT License
 */
        class Animator {
            constructor() {
                this._request = null;
                this._charts = new Map;
                this._running = false;
                this._lastDate = void 0;
            }
            _notify(chart, anims, date, type) {
                const callbacks = anims.listeners[type];
                const numSteps = anims.duration;
                callbacks.forEach((fn => fn({
                    chart,
                    initial: anims.initial,
                    numSteps,
                    currentStep: Math.min(date - anims.start, numSteps)
                })));
            }
            _refresh() {
                if (this._request) return;
                this._running = true;
                this._request = requestAnimFrame.call(window, (() => {
                    this._update();
                    this._request = null;
                    if (this._running) this._refresh();
                }));
            }
            _update(date = Date.now()) {
                let remaining = 0;
                this._charts.forEach(((anims, chart) => {
                    if (!anims.running || !anims.items.length) return;
                    const items = anims.items;
                    let i = items.length - 1;
                    let draw = false;
                    let item;
                    for (;i >= 0; --i) {
                        item = items[i];
                        if (item._active) {
                            if (item._total > anims.duration) anims.duration = item._total;
                            item.tick(date);
                            draw = true;
                        } else {
                            items[i] = items[items.length - 1];
                            items.pop();
                        }
                    }
                    if (draw) {
                        chart.draw();
                        this._notify(chart, anims, date, "progress");
                    }
                    if (!items.length) {
                        anims.running = false;
                        this._notify(chart, anims, date, "complete");
                        anims.initial = false;
                    }
                    remaining += items.length;
                }));
                this._lastDate = date;
                if (0 === remaining) this._running = false;
            }
            _getAnims(chart) {
                const charts = this._charts;
                let anims = charts.get(chart);
                if (!anims) {
                    anims = {
                        running: false,
                        initial: true,
                        items: [],
                        listeners: {
                            complete: [],
                            progress: []
                        }
                    };
                    charts.set(chart, anims);
                }
                return anims;
            }
            listen(chart, event, cb) {
                this._getAnims(chart).listeners[event].push(cb);
            }
            add(chart, items) {
                if (!items || !items.length) return;
                this._getAnims(chart).items.push(...items);
            }
            has(chart) {
                return this._getAnims(chart).items.length > 0;
            }
            start(chart) {
                const anims = this._charts.get(chart);
                if (!anims) return;
                anims.running = true;
                anims.start = Date.now();
                anims.duration = anims.items.reduce(((acc, cur) => Math.max(acc, cur._duration)), 0);
                this._refresh();
            }
            running(chart) {
                if (!this._running) return false;
                const anims = this._charts.get(chart);
                if (!anims || !anims.running || !anims.items.length) return false;
                return true;
            }
            stop(chart) {
                const anims = this._charts.get(chart);
                if (!anims || !anims.items.length) return;
                const items = anims.items;
                let i = items.length - 1;
                for (;i >= 0; --i) items[i].cancel();
                anims.items = [];
                this._notify(chart, anims, Date.now(), "complete");
            }
            remove(chart) {
                return this._charts.delete(chart);
            }
        }
        var animator = new Animator;
        const transparent = "transparent";
        const interpolators = {
            boolean(from, to, factor) {
                return factor > .5 ? to : from;
            },
            color(from, to, factor) {
                const c0 = color(from || transparent);
                const c1 = c0.valid && color(to || transparent);
                return c1 && c1.valid ? c1.mix(c0, factor).hexString() : to;
            },
            number(from, to, factor) {
                return from + (to - from) * factor;
            }
        };
        class Animation {
            constructor(cfg, target, prop, to) {
                const currentValue = target[prop];
                to = resolve([ cfg.to, to, currentValue, cfg.from ]);
                const from = resolve([ cfg.from, currentValue, to ]);
                this._active = true;
                this._fn = cfg.fn || interpolators[cfg.type || typeof from];
                this._easing = effects[cfg.easing] || effects.linear;
                this._start = Math.floor(Date.now() + (cfg.delay || 0));
                this._duration = this._total = Math.floor(cfg.duration);
                this._loop = !!cfg.loop;
                this._target = target;
                this._prop = prop;
                this._from = from;
                this._to = to;
                this._promises = void 0;
            }
            active() {
                return this._active;
            }
            update(cfg, to, date) {
                if (this._active) {
                    this._notify(false);
                    const currentValue = this._target[this._prop];
                    const elapsed = date - this._start;
                    const remain = this._duration - elapsed;
                    this._start = date;
                    this._duration = Math.floor(Math.max(remain, cfg.duration));
                    this._total += elapsed;
                    this._loop = !!cfg.loop;
                    this._to = resolve([ cfg.to, to, currentValue, cfg.from ]);
                    this._from = resolve([ cfg.from, currentValue, to ]);
                }
            }
            cancel() {
                if (this._active) {
                    this.tick(Date.now());
                    this._active = false;
                    this._notify(false);
                }
            }
            tick(date) {
                const elapsed = date - this._start;
                const duration = this._duration;
                const prop = this._prop;
                const from = this._from;
                const loop = this._loop;
                const to = this._to;
                let factor;
                this._active = from !== to && (loop || elapsed < duration);
                if (!this._active) {
                    this._target[prop] = to;
                    this._notify(true);
                    return;
                }
                if (elapsed < 0) {
                    this._target[prop] = from;
                    return;
                }
                factor = elapsed / duration % 2;
                factor = loop && factor > 1 ? 2 - factor : factor;
                factor = this._easing(Math.min(1, Math.max(0, factor)));
                this._target[prop] = this._fn(from, to, factor);
            }
            wait() {
                const promises = this._promises || (this._promises = []);
                return new Promise(((res, rej) => {
                    promises.push({
                        res,
                        rej
                    });
                }));
            }
            _notify(resolved) {
                const method = resolved ? "res" : "rej";
                const promises = this._promises || [];
                for (let i = 0; i < promises.length; i++) promises[i][method]();
            }
        }
        const numbers = [ "x", "y", "borderWidth", "radius", "tension" ];
        const colors = [ "color", "borderColor", "backgroundColor" ];
        defaults.set("animation", {
            delay: void 0,
            duration: 1e3,
            easing: "easeOutQuart",
            fn: void 0,
            from: void 0,
            loop: void 0,
            to: void 0,
            type: void 0
        });
        const animationOptions = Object.keys(defaults.animation);
        defaults.describe("animation", {
            _fallback: false,
            _indexable: false,
            _scriptable: name => "onProgress" !== name && "onComplete" !== name && "fn" !== name
        });
        defaults.set("animations", {
            colors: {
                type: "color",
                properties: colors
            },
            numbers: {
                type: "number",
                properties: numbers
            }
        });
        defaults.describe("animations", {
            _fallback: "animation"
        });
        defaults.set("transitions", {
            active: {
                animation: {
                    duration: 400
                }
            },
            resize: {
                animation: {
                    duration: 0
                }
            },
            show: {
                animations: {
                    colors: {
                        from: "transparent"
                    },
                    visible: {
                        type: "boolean",
                        duration: 0
                    }
                }
            },
            hide: {
                animations: {
                    colors: {
                        to: "transparent"
                    },
                    visible: {
                        type: "boolean",
                        easing: "linear",
                        fn: v => 0 | v
                    }
                }
            }
        });
        class Animations {
            constructor(chart, config) {
                this._chart = chart;
                this._properties = new Map;
                this.configure(config);
            }
            configure(config) {
                if (!isObject(config)) return;
                const animatedProps = this._properties;
                Object.getOwnPropertyNames(config).forEach((key => {
                    const cfg = config[key];
                    if (!isObject(cfg)) return;
                    const resolved = {};
                    for (const option of animationOptions) resolved[option] = cfg[option];
                    (isArray(cfg.properties) && cfg.properties || [ key ]).forEach((prop => {
                        if (prop === key || !animatedProps.has(prop)) animatedProps.set(prop, resolved);
                    }));
                }));
            }
            _animateOptions(target, values) {
                const newOptions = values.options;
                const options = resolveTargetOptions(target, newOptions);
                if (!options) return [];
                const animations = this._createAnimations(options, newOptions);
                if (newOptions.$shared) awaitAll(target.options.$animations, newOptions).then((() => {
                    target.options = newOptions;
                }), (() => {}));
                return animations;
            }
            _createAnimations(target, values) {
                const animatedProps = this._properties;
                const animations = [];
                const running = target.$animations || (target.$animations = {});
                const props = Object.keys(values);
                const date = Date.now();
                let i;
                for (i = props.length - 1; i >= 0; --i) {
                    const prop = props[i];
                    if ("$" === prop.charAt(0)) continue;
                    if ("options" === prop) {
                        animations.push(...this._animateOptions(target, values));
                        continue;
                    }
                    const value = values[prop];
                    let animation = running[prop];
                    const cfg = animatedProps.get(prop);
                    if (animation) if (cfg && animation.active()) {
                        animation.update(cfg, value, date);
                        continue;
                    } else animation.cancel();
                    if (!cfg || !cfg.duration) {
                        target[prop] = value;
                        continue;
                    }
                    running[prop] = animation = new Animation(cfg, target, prop, value);
                    animations.push(animation);
                }
                return animations;
            }
            update(target, values) {
                if (0 === this._properties.size) {
                    Object.assign(target, values);
                    return;
                }
                const animations = this._createAnimations(target, values);
                if (animations.length) {
                    animator.add(this._chart, animations);
                    return true;
                }
            }
        }
        function awaitAll(animations, properties) {
            const running = [];
            const keys = Object.keys(properties);
            for (let i = 0; i < keys.length; i++) {
                const anim = animations[keys[i]];
                if (anim && anim.active()) running.push(anim.wait());
            }
            return Promise.all(running);
        }
        function resolveTargetOptions(target, newOptions) {
            if (!newOptions) return;
            let options = target.options;
            if (!options) {
                target.options = newOptions;
                return;
            }
            if (options.$shared) target.options = options = Object.assign({}, options, {
                $shared: false,
                $animations: {}
            });
            return options;
        }
        function scaleClip(scale, allowedOverflow) {
            const opts = scale && scale.options || {};
            const reverse = opts.reverse;
            const min = void 0 === opts.min ? allowedOverflow : 0;
            const max = void 0 === opts.max ? allowedOverflow : 0;
            return {
                start: reverse ? max : min,
                end: reverse ? min : max
            };
        }
        function defaultClip(xScale, yScale, allowedOverflow) {
            if (false === allowedOverflow) return false;
            const x = scaleClip(xScale, allowedOverflow);
            const y = scaleClip(yScale, allowedOverflow);
            return {
                top: y.end,
                right: x.end,
                bottom: y.start,
                left: x.start
            };
        }
        function toClip(value) {
            let t, r, b, l;
            if (isObject(value)) {
                t = value.top;
                r = value.right;
                b = value.bottom;
                l = value.left;
            } else t = r = b = l = value;
            return {
                top: t,
                right: r,
                bottom: b,
                left: l,
                disabled: false === value
            };
        }
        function getSortedDatasetIndices(chart, filterVisible) {
            const keys = [];
            const metasets = chart._getSortedDatasetMetas(filterVisible);
            let i, ilen;
            for (i = 0, ilen = metasets.length; i < ilen; ++i) keys.push(metasets[i].index);
            return keys;
        }
        function applyStack(stack, value, dsIndex, options = {}) {
            const keys = stack.keys;
            const singleMode = "single" === options.mode;
            let i, ilen, datasetIndex, otherValue;
            if (null === value) return;
            for (i = 0, ilen = keys.length; i < ilen; ++i) {
                datasetIndex = +keys[i];
                if (datasetIndex === dsIndex) {
                    if (options.all) continue;
                    break;
                }
                otherValue = stack.values[datasetIndex];
                if (isNumberFinite(otherValue) && (singleMode || 0 === value || sign(value) === sign(otherValue))) value += otherValue;
            }
            return value;
        }
        function convertObjectDataToArray(data) {
            const keys = Object.keys(data);
            const adata = new Array(keys.length);
            let i, ilen, key;
            for (i = 0, ilen = keys.length; i < ilen; ++i) {
                key = keys[i];
                adata[i] = {
                    x: key,
                    y: data[key]
                };
            }
            return adata;
        }
        function isStacked(scale, meta) {
            const stacked = scale && scale.options.stacked;
            return stacked || void 0 === stacked && void 0 !== meta.stack;
        }
        function getStackKey(indexScale, valueScale, meta) {
            return `${indexScale.id}.${valueScale.id}.${meta.stack || meta.type}`;
        }
        function getUserBounds(scale) {
            const {min, max, minDefined, maxDefined} = scale.getUserBounds();
            return {
                min: minDefined ? min : Number.NEGATIVE_INFINITY,
                max: maxDefined ? max : Number.POSITIVE_INFINITY
            };
        }
        function getOrCreateStack(stacks, stackKey, indexValue) {
            const subStack = stacks[stackKey] || (stacks[stackKey] = {});
            return subStack[indexValue] || (subStack[indexValue] = {});
        }
        function getLastIndexInStack(stack, vScale, positive, type) {
            for (const meta of vScale.getMatchingVisibleMetas(type).reverse()) {
                const value = stack[meta.index];
                if (positive && value > 0 || !positive && value < 0) return meta.index;
            }
            return null;
        }
        function updateStacks(controller, parsed) {
            const {chart, _cachedMeta: meta} = controller;
            const stacks = chart._stacks || (chart._stacks = {});
            const {iScale, vScale, index: datasetIndex} = meta;
            const iAxis = iScale.axis;
            const vAxis = vScale.axis;
            const key = getStackKey(iScale, vScale, meta);
            const ilen = parsed.length;
            let stack;
            for (let i = 0; i < ilen; ++i) {
                const item = parsed[i];
                const {[iAxis]: index, [vAxis]: value} = item;
                const itemStacks = item._stacks || (item._stacks = {});
                stack = itemStacks[vAxis] = getOrCreateStack(stacks, key, index);
                stack[datasetIndex] = value;
                stack._top = getLastIndexInStack(stack, vScale, true, meta.type);
                stack._bottom = getLastIndexInStack(stack, vScale, false, meta.type);
            }
        }
        function getFirstScaleId(chart, axis) {
            const scales = chart.scales;
            return Object.keys(scales).filter((key => scales[key].axis === axis)).shift();
        }
        function createDatasetContext(parent, index) {
            return createContext(parent, {
                active: false,
                dataset: void 0,
                datasetIndex: index,
                index,
                mode: "default",
                type: "dataset"
            });
        }
        function createDataContext(parent, index, element) {
            return createContext(parent, {
                active: false,
                dataIndex: index,
                parsed: void 0,
                raw: void 0,
                element,
                index,
                mode: "default",
                type: "data"
            });
        }
        function clearStacks(meta, items) {
            const datasetIndex = meta.controller.index;
            const axis = meta.vScale && meta.vScale.axis;
            if (!axis) return;
            items = items || meta._parsed;
            for (const parsed of items) {
                const stacks = parsed._stacks;
                if (!stacks || void 0 === stacks[axis] || void 0 === stacks[axis][datasetIndex]) return;
                delete stacks[axis][datasetIndex];
            }
        }
        const isDirectUpdateMode = mode => "reset" === mode || "none" === mode;
        const cloneIfNotShared = (cached, shared) => shared ? cached : Object.assign({}, cached);
        const createStack = (canStack, meta, chart) => canStack && !meta.hidden && meta._stacked && {
            keys: getSortedDatasetIndices(chart, true),
            values: null
        };
        class DatasetController {
            constructor(chart, datasetIndex) {
                this.chart = chart;
                this._ctx = chart.ctx;
                this.index = datasetIndex;
                this._cachedDataOpts = {};
                this._cachedMeta = this.getMeta();
                this._type = this._cachedMeta.type;
                this.options = void 0;
                this._parsing = false;
                this._data = void 0;
                this._objectData = void 0;
                this._sharedOptions = void 0;
                this._drawStart = void 0;
                this._drawCount = void 0;
                this.enableOptionSharing = false;
                this.$context = void 0;
                this._syncList = [];
                this.initialize();
            }
            initialize() {
                const meta = this._cachedMeta;
                this.configure();
                this.linkScales();
                meta._stacked = isStacked(meta.vScale, meta);
                this.addElements();
            }
            updateIndex(datasetIndex) {
                if (this.index !== datasetIndex) clearStacks(this._cachedMeta);
                this.index = datasetIndex;
            }
            linkScales() {
                const chart = this.chart;
                const meta = this._cachedMeta;
                const dataset = this.getDataset();
                const chooseId = (axis, x, y, r) => "x" === axis ? x : "r" === axis ? r : y;
                const xid = meta.xAxisID = valueOrDefault(dataset.xAxisID, getFirstScaleId(chart, "x"));
                const yid = meta.yAxisID = valueOrDefault(dataset.yAxisID, getFirstScaleId(chart, "y"));
                const rid = meta.rAxisID = valueOrDefault(dataset.rAxisID, getFirstScaleId(chart, "r"));
                const indexAxis = meta.indexAxis;
                const iid = meta.iAxisID = chooseId(indexAxis, xid, yid, rid);
                const vid = meta.vAxisID = chooseId(indexAxis, yid, xid, rid);
                meta.xScale = this.getScaleForId(xid);
                meta.yScale = this.getScaleForId(yid);
                meta.rScale = this.getScaleForId(rid);
                meta.iScale = this.getScaleForId(iid);
                meta.vScale = this.getScaleForId(vid);
            }
            getDataset() {
                return this.chart.data.datasets[this.index];
            }
            getMeta() {
                return this.chart.getDatasetMeta(this.index);
            }
            getScaleForId(scaleID) {
                return this.chart.scales[scaleID];
            }
            _getOtherScale(scale) {
                const meta = this._cachedMeta;
                return scale === meta.iScale ? meta.vScale : meta.iScale;
            }
            reset() {
                this._update("reset");
            }
            _destroy() {
                const meta = this._cachedMeta;
                if (this._data) unlistenArrayEvents(this._data, this);
                if (meta._stacked) clearStacks(meta);
            }
            _dataCheck() {
                const dataset = this.getDataset();
                const data = dataset.data || (dataset.data = []);
                const _data = this._data;
                if (isObject(data)) this._data = convertObjectDataToArray(data); else if (_data !== data) {
                    if (_data) {
                        unlistenArrayEvents(_data, this);
                        const meta = this._cachedMeta;
                        clearStacks(meta);
                        meta._parsed = [];
                    }
                    if (data && Object.isExtensible(data)) listenArrayEvents(data, this);
                    this._syncList = [];
                    this._data = data;
                }
            }
            addElements() {
                const meta = this._cachedMeta;
                this._dataCheck();
                if (this.datasetElementType) meta.dataset = new this.datasetElementType;
            }
            buildOrUpdateElements(resetNewElements) {
                const meta = this._cachedMeta;
                const dataset = this.getDataset();
                let stackChanged = false;
                this._dataCheck();
                const oldStacked = meta._stacked;
                meta._stacked = isStacked(meta.vScale, meta);
                if (meta.stack !== dataset.stack) {
                    stackChanged = true;
                    clearStacks(meta);
                    meta.stack = dataset.stack;
                }
                this._resyncElements(resetNewElements);
                if (stackChanged || oldStacked !== meta._stacked) updateStacks(this, meta._parsed);
            }
            configure() {
                const config = this.chart.config;
                const scopeKeys = config.datasetScopeKeys(this._type);
                const scopes = config.getOptionScopes(this.getDataset(), scopeKeys, true);
                this.options = config.createResolver(scopes, this.getContext());
                this._parsing = this.options.parsing;
                this._cachedDataOpts = {};
            }
            parse(start, count) {
                const {_cachedMeta: meta, _data: data} = this;
                const {iScale, _stacked} = meta;
                const iAxis = iScale.axis;
                let sorted = 0 === start && count === data.length ? true : meta._sorted;
                let prev = start > 0 && meta._parsed[start - 1];
                let i, cur, parsed;
                if (false === this._parsing) {
                    meta._parsed = data;
                    meta._sorted = true;
                    parsed = data;
                } else {
                    if (isArray(data[start])) parsed = this.parseArrayData(meta, data, start, count); else if (isObject(data[start])) parsed = this.parseObjectData(meta, data, start, count); else parsed = this.parsePrimitiveData(meta, data, start, count);
                    const isNotInOrderComparedToPrev = () => null === cur[iAxis] || prev && cur[iAxis] < prev[iAxis];
                    for (i = 0; i < count; ++i) {
                        meta._parsed[i + start] = cur = parsed[i];
                        if (sorted) {
                            if (isNotInOrderComparedToPrev()) sorted = false;
                            prev = cur;
                        }
                    }
                    meta._sorted = sorted;
                }
                if (_stacked) updateStacks(this, parsed);
            }
            parsePrimitiveData(meta, data, start, count) {
                const {iScale, vScale} = meta;
                const iAxis = iScale.axis;
                const vAxis = vScale.axis;
                const labels = iScale.getLabels();
                const singleScale = iScale === vScale;
                const parsed = new Array(count);
                let i, ilen, index;
                for (i = 0, ilen = count; i < ilen; ++i) {
                    index = i + start;
                    parsed[i] = {
                        [iAxis]: singleScale || iScale.parse(labels[index], index),
                        [vAxis]: vScale.parse(data[index], index)
                    };
                }
                return parsed;
            }
            parseArrayData(meta, data, start, count) {
                const {xScale, yScale} = meta;
                const parsed = new Array(count);
                let i, ilen, index, item;
                for (i = 0, ilen = count; i < ilen; ++i) {
                    index = i + start;
                    item = data[index];
                    parsed[i] = {
                        x: xScale.parse(item[0], index),
                        y: yScale.parse(item[1], index)
                    };
                }
                return parsed;
            }
            parseObjectData(meta, data, start, count) {
                const {xScale, yScale} = meta;
                const {xAxisKey = "x", yAxisKey = "y"} = this._parsing;
                const parsed = new Array(count);
                let i, ilen, index, item;
                for (i = 0, ilen = count; i < ilen; ++i) {
                    index = i + start;
                    item = data[index];
                    parsed[i] = {
                        x: xScale.parse(resolveObjectKey(item, xAxisKey), index),
                        y: yScale.parse(resolveObjectKey(item, yAxisKey), index)
                    };
                }
                return parsed;
            }
            getParsed(index) {
                return this._cachedMeta._parsed[index];
            }
            getDataElement(index) {
                return this._cachedMeta.data[index];
            }
            applyStack(scale, parsed, mode) {
                const chart = this.chart;
                const meta = this._cachedMeta;
                const value = parsed[scale.axis];
                const stack = {
                    keys: getSortedDatasetIndices(chart, true),
                    values: parsed._stacks[scale.axis]
                };
                return applyStack(stack, value, meta.index, {
                    mode
                });
            }
            updateRangeFromParsed(range, scale, parsed, stack) {
                const parsedValue = parsed[scale.axis];
                let value = null === parsedValue ? NaN : parsedValue;
                const values = stack && parsed._stacks[scale.axis];
                if (stack && values) {
                    stack.values = values;
                    value = applyStack(stack, parsedValue, this._cachedMeta.index);
                }
                range.min = Math.min(range.min, value);
                range.max = Math.max(range.max, value);
            }
            getMinMax(scale, canStack) {
                const meta = this._cachedMeta;
                const _parsed = meta._parsed;
                const sorted = meta._sorted && scale === meta.iScale;
                const ilen = _parsed.length;
                const otherScale = this._getOtherScale(scale);
                const stack = createStack(canStack, meta, this.chart);
                const range = {
                    min: Number.POSITIVE_INFINITY,
                    max: Number.NEGATIVE_INFINITY
                };
                const {min: otherMin, max: otherMax} = getUserBounds(otherScale);
                let i, parsed;
                function _skip() {
                    parsed = _parsed[i];
                    const otherValue = parsed[otherScale.axis];
                    return !isNumberFinite(parsed[scale.axis]) || otherMin > otherValue || otherMax < otherValue;
                }
                for (i = 0; i < ilen; ++i) {
                    if (_skip()) continue;
                    this.updateRangeFromParsed(range, scale, parsed, stack);
                    if (sorted) break;
                }
                if (sorted) for (i = ilen - 1; i >= 0; --i) {
                    if (_skip()) continue;
                    this.updateRangeFromParsed(range, scale, parsed, stack);
                    break;
                }
                return range;
            }
            getAllParsedValues(scale) {
                const parsed = this._cachedMeta._parsed;
                const values = [];
                let i, ilen, value;
                for (i = 0, ilen = parsed.length; i < ilen; ++i) {
                    value = parsed[i][scale.axis];
                    if (isNumberFinite(value)) values.push(value);
                }
                return values;
            }
            getMaxOverflow() {
                return false;
            }
            getLabelAndValue(index) {
                const meta = this._cachedMeta;
                const iScale = meta.iScale;
                const vScale = meta.vScale;
                const parsed = this.getParsed(index);
                return {
                    label: iScale ? "" + iScale.getLabelForValue(parsed[iScale.axis]) : "",
                    value: vScale ? "" + vScale.getLabelForValue(parsed[vScale.axis]) : ""
                };
            }
            _update(mode) {
                const meta = this._cachedMeta;
                this.update(mode || "default");
                meta._clip = toClip(valueOrDefault(this.options.clip, defaultClip(meta.xScale, meta.yScale, this.getMaxOverflow())));
            }
            update(mode) {}
            draw() {
                const ctx = this._ctx;
                const chart = this.chart;
                const meta = this._cachedMeta;
                const elements = meta.data || [];
                const area = chart.chartArea;
                const active = [];
                const start = this._drawStart || 0;
                const count = this._drawCount || elements.length - start;
                const drawActiveElementsOnTop = this.options.drawActiveElementsOnTop;
                let i;
                if (meta.dataset) meta.dataset.draw(ctx, area, start, count);
                for (i = start; i < start + count; ++i) {
                    const element = elements[i];
                    if (element.hidden) continue;
                    if (element.active && drawActiveElementsOnTop) active.push(element); else element.draw(ctx, area);
                }
                for (i = 0; i < active.length; ++i) active[i].draw(ctx, area);
            }
            getStyle(index, active) {
                const mode = active ? "active" : "default";
                return void 0 === index && this._cachedMeta.dataset ? this.resolveDatasetElementOptions(mode) : this.resolveDataElementOptions(index || 0, mode);
            }
            getContext(index, active, mode) {
                const dataset = this.getDataset();
                let context;
                if (index >= 0 && index < this._cachedMeta.data.length) {
                    const element = this._cachedMeta.data[index];
                    context = element.$context || (element.$context = createDataContext(this.getContext(), index, element));
                    context.parsed = this.getParsed(index);
                    context.raw = dataset.data[index];
                    context.index = context.dataIndex = index;
                } else {
                    context = this.$context || (this.$context = createDatasetContext(this.chart.getContext(), this.index));
                    context.dataset = dataset;
                    context.index = context.datasetIndex = this.index;
                }
                context.active = !!active;
                context.mode = mode;
                return context;
            }
            resolveDatasetElementOptions(mode) {
                return this._resolveElementOptions(this.datasetElementType.id, mode);
            }
            resolveDataElementOptions(index, mode) {
                return this._resolveElementOptions(this.dataElementType.id, mode, index);
            }
            _resolveElementOptions(elementType, mode = "default", index) {
                const active = "active" === mode;
                const cache = this._cachedDataOpts;
                const cacheKey = elementType + "-" + mode;
                const cached = cache[cacheKey];
                const sharing = this.enableOptionSharing && defined(index);
                if (cached) return cloneIfNotShared(cached, sharing);
                const config = this.chart.config;
                const scopeKeys = config.datasetElementScopeKeys(this._type, elementType);
                const prefixes = active ? [ `${elementType}Hover`, "hover", elementType, "" ] : [ elementType, "" ];
                const scopes = config.getOptionScopes(this.getDataset(), scopeKeys);
                const names = Object.keys(defaults.elements[elementType]);
                const context = () => this.getContext(index, active);
                const values = config.resolveNamedOptions(scopes, names, context, prefixes);
                if (values.$shared) {
                    values.$shared = sharing;
                    cache[cacheKey] = Object.freeze(cloneIfNotShared(values, sharing));
                }
                return values;
            }
            _resolveAnimations(index, transition, active) {
                const chart = this.chart;
                const cache = this._cachedDataOpts;
                const cacheKey = `animation-${transition}`;
                const cached = cache[cacheKey];
                if (cached) return cached;
                let options;
                if (false !== chart.options.animation) {
                    const config = this.chart.config;
                    const scopeKeys = config.datasetAnimationScopeKeys(this._type, transition);
                    const scopes = config.getOptionScopes(this.getDataset(), scopeKeys);
                    options = config.createResolver(scopes, this.getContext(index, active, transition));
                }
                const animations = new Animations(chart, options && options.animations);
                if (options && options._cacheable) cache[cacheKey] = Object.freeze(animations);
                return animations;
            }
            getSharedOptions(options) {
                if (!options.$shared) return;
                return this._sharedOptions || (this._sharedOptions = Object.assign({}, options));
            }
            includeOptions(mode, sharedOptions) {
                return !sharedOptions || isDirectUpdateMode(mode) || this.chart._animationsDisabled;
            }
            updateElement(element, index, properties, mode) {
                if (isDirectUpdateMode(mode)) Object.assign(element, properties); else this._resolveAnimations(index, mode).update(element, properties);
            }
            updateSharedOptions(sharedOptions, mode, newOptions) {
                if (sharedOptions && !isDirectUpdateMode(mode)) this._resolveAnimations(void 0, mode).update(sharedOptions, newOptions);
            }
            _setStyle(element, index, mode, active) {
                element.active = active;
                const options = this.getStyle(index, active);
                this._resolveAnimations(index, mode, active).update(element, {
                    options: !active && this.getSharedOptions(options) || options
                });
            }
            removeHoverStyle(element, datasetIndex, index) {
                this._setStyle(element, index, "active", false);
            }
            setHoverStyle(element, datasetIndex, index) {
                this._setStyle(element, index, "active", true);
            }
            _removeDatasetHoverStyle() {
                const element = this._cachedMeta.dataset;
                if (element) this._setStyle(element, void 0, "active", false);
            }
            _setDatasetHoverStyle() {
                const element = this._cachedMeta.dataset;
                if (element) this._setStyle(element, void 0, "active", true);
            }
            _resyncElements(resetNewElements) {
                const data = this._data;
                const elements = this._cachedMeta.data;
                for (const [method, arg1, arg2] of this._syncList) this[method](arg1, arg2);
                this._syncList = [];
                const numMeta = elements.length;
                const numData = data.length;
                const count = Math.min(numData, numMeta);
                if (count) this.parse(0, count);
                if (numData > numMeta) this._insertElements(numMeta, numData - numMeta, resetNewElements); else if (numData < numMeta) this._removeElements(numData, numMeta - numData);
            }
            _insertElements(start, count, resetNewElements = true) {
                const meta = this._cachedMeta;
                const data = meta.data;
                const end = start + count;
                let i;
                const move = arr => {
                    arr.length += count;
                    for (i = arr.length - 1; i >= end; i--) arr[i] = arr[i - count];
                };
                move(data);
                for (i = start; i < end; ++i) data[i] = new this.dataElementType;
                if (this._parsing) move(meta._parsed);
                this.parse(start, count);
                if (resetNewElements) this.updateElements(data, start, count, "reset");
            }
            updateElements(element, start, count, mode) {}
            _removeElements(start, count) {
                const meta = this._cachedMeta;
                if (this._parsing) {
                    const removed = meta._parsed.splice(start, count);
                    if (meta._stacked) clearStacks(meta, removed);
                }
                meta.data.splice(start, count);
            }
            _sync(args) {
                if (this._parsing) this._syncList.push(args); else {
                    const [method, arg1, arg2] = args;
                    this[method](arg1, arg2);
                }
                this.chart._dataChanges.push([ this.index, ...args ]);
            }
            _onDataPush() {
                const count = arguments.length;
                this._sync([ "_insertElements", this.getDataset().data.length - count, count ]);
            }
            _onDataPop() {
                this._sync([ "_removeElements", this._cachedMeta.data.length - 1, 1 ]);
            }
            _onDataShift() {
                this._sync([ "_removeElements", 0, 1 ]);
            }
            _onDataSplice(start, count) {
                if (count) this._sync([ "_removeElements", start, count ]);
                const newCount = arguments.length - 2;
                if (newCount) this._sync([ "_insertElements", start, newCount ]);
            }
            _onDataUnshift() {
                this._sync([ "_insertElements", 0, arguments.length ]);
            }
        }
        DatasetController.defaults = {};
        DatasetController.prototype.datasetElementType = null;
        DatasetController.prototype.dataElementType = null;
        function getAllScaleValues(scale, type) {
            if (!scale._cache.$bar) {
                const visibleMetas = scale.getMatchingVisibleMetas(type);
                let values = [];
                for (let i = 0, ilen = visibleMetas.length; i < ilen; i++) values = values.concat(visibleMetas[i].controller.getAllParsedValues(scale));
                scale._cache.$bar = _arrayUnique(values.sort(((a, b) => a - b)));
            }
            return scale._cache.$bar;
        }
        function computeMinSampleSize(meta) {
            const scale = meta.iScale;
            const values = getAllScaleValues(scale, meta.type);
            let min = scale._length;
            let i, ilen, curr, prev;
            const updateMinAndPrev = () => {
                if (32767 === curr || -32768 === curr) return;
                if (defined(prev)) min = Math.min(min, Math.abs(curr - prev) || min);
                prev = curr;
            };
            for (i = 0, ilen = values.length; i < ilen; ++i) {
                curr = scale.getPixelForValue(values[i]);
                updateMinAndPrev();
            }
            prev = void 0;
            for (i = 0, ilen = scale.ticks.length; i < ilen; ++i) {
                curr = scale.getPixelForTick(i);
                updateMinAndPrev();
            }
            return min;
        }
        function computeFitCategoryTraits(index, ruler, options, stackCount) {
            const thickness = options.barThickness;
            let size, ratio;
            if (isNullOrUndef(thickness)) {
                size = ruler.min * options.categoryPercentage;
                ratio = options.barPercentage;
            } else {
                size = thickness * stackCount;
                ratio = 1;
            }
            return {
                chunk: size / stackCount,
                ratio,
                start: ruler.pixels[index] - size / 2
            };
        }
        function computeFlexCategoryTraits(index, ruler, options, stackCount) {
            const pixels = ruler.pixels;
            const curr = pixels[index];
            let prev = index > 0 ? pixels[index - 1] : null;
            let next = index < pixels.length - 1 ? pixels[index + 1] : null;
            const percent = options.categoryPercentage;
            if (null === prev) prev = curr - (null === next ? ruler.end - ruler.start : next - curr);
            if (null === next) next = curr + curr - prev;
            const start = curr - (curr - Math.min(prev, next)) / 2 * percent;
            const size = Math.abs(next - prev) / 2 * percent;
            return {
                chunk: size / stackCount,
                ratio: options.barPercentage,
                start
            };
        }
        function parseFloatBar(entry, item, vScale, i) {
            const startValue = vScale.parse(entry[0], i);
            const endValue = vScale.parse(entry[1], i);
            const min = Math.min(startValue, endValue);
            const max = Math.max(startValue, endValue);
            let barStart = min;
            let barEnd = max;
            if (Math.abs(min) > Math.abs(max)) {
                barStart = max;
                barEnd = min;
            }
            item[vScale.axis] = barEnd;
            item._custom = {
                barStart,
                barEnd,
                start: startValue,
                end: endValue,
                min,
                max
            };
        }
        function parseValue(entry, item, vScale, i) {
            if (isArray(entry)) parseFloatBar(entry, item, vScale, i); else item[vScale.axis] = vScale.parse(entry, i);
            return item;
        }
        function parseArrayOrPrimitive(meta, data, start, count) {
            const iScale = meta.iScale;
            const vScale = meta.vScale;
            const labels = iScale.getLabels();
            const singleScale = iScale === vScale;
            const parsed = [];
            let i, ilen, item, entry;
            for (i = start, ilen = start + count; i < ilen; ++i) {
                entry = data[i];
                item = {};
                item[iScale.axis] = singleScale || iScale.parse(labels[i], i);
                parsed.push(parseValue(entry, item, vScale, i));
            }
            return parsed;
        }
        function isFloatBar(custom) {
            return custom && void 0 !== custom.barStart && void 0 !== custom.barEnd;
        }
        function barSign(size, vScale, actualBase) {
            if (0 !== size) return sign(size);
            return (vScale.isHorizontal() ? 1 : -1) * (vScale.min >= actualBase ? 1 : -1);
        }
        function borderProps(properties) {
            let reverse, start, end, top, bottom;
            if (properties.horizontal) {
                reverse = properties.base > properties.x;
                start = "left";
                end = "right";
            } else {
                reverse = properties.base < properties.y;
                start = "bottom";
                end = "top";
            }
            if (reverse) {
                top = "end";
                bottom = "start";
            } else {
                top = "start";
                bottom = "end";
            }
            return {
                start,
                end,
                reverse,
                top,
                bottom
            };
        }
        function setBorderSkipped(properties, options, stack, index) {
            let edge = options.borderSkipped;
            const res = {};
            if (!edge) {
                properties.borderSkipped = res;
                return;
            }
            const {start, end, reverse, top, bottom} = borderProps(properties);
            if ("middle" === edge && stack) {
                properties.enableBorderRadius = true;
                if ((stack._top || 0) === index) edge = top; else if ((stack._bottom || 0) === index) edge = bottom; else {
                    res[parseEdge(bottom, start, end, reverse)] = true;
                    edge = top;
                }
            }
            res[parseEdge(edge, start, end, reverse)] = true;
            properties.borderSkipped = res;
        }
        function parseEdge(edge, a, b, reverse) {
            if (reverse) {
                edge = swap(edge, a, b);
                edge = startEnd(edge, b, a);
            } else edge = startEnd(edge, a, b);
            return edge;
        }
        function swap(orig, v1, v2) {
            return orig === v1 ? v2 : orig === v2 ? v1 : orig;
        }
        function startEnd(v, start, end) {
            return "start" === v ? start : "end" === v ? end : v;
        }
        function setInflateAmount(properties, {inflateAmount}, ratio) {
            properties.inflateAmount = "auto" === inflateAmount ? 1 === ratio ? .33 : 0 : inflateAmount;
        }
        class BarController extends DatasetController {
            parsePrimitiveData(meta, data, start, count) {
                return parseArrayOrPrimitive(meta, data, start, count);
            }
            parseArrayData(meta, data, start, count) {
                return parseArrayOrPrimitive(meta, data, start, count);
            }
            parseObjectData(meta, data, start, count) {
                const {iScale, vScale} = meta;
                const {xAxisKey = "x", yAxisKey = "y"} = this._parsing;
                const iAxisKey = "x" === iScale.axis ? xAxisKey : yAxisKey;
                const vAxisKey = "x" === vScale.axis ? xAxisKey : yAxisKey;
                const parsed = [];
                let i, ilen, item, obj;
                for (i = start, ilen = start + count; i < ilen; ++i) {
                    obj = data[i];
                    item = {};
                    item[iScale.axis] = iScale.parse(resolveObjectKey(obj, iAxisKey), i);
                    parsed.push(parseValue(resolveObjectKey(obj, vAxisKey), item, vScale, i));
                }
                return parsed;
            }
            updateRangeFromParsed(range, scale, parsed, stack) {
                super.updateRangeFromParsed(range, scale, parsed, stack);
                const custom = parsed._custom;
                if (custom && scale === this._cachedMeta.vScale) {
                    range.min = Math.min(range.min, custom.min);
                    range.max = Math.max(range.max, custom.max);
                }
            }
            getMaxOverflow() {
                return 0;
            }
            getLabelAndValue(index) {
                const meta = this._cachedMeta;
                const {iScale, vScale} = meta;
                const parsed = this.getParsed(index);
                const custom = parsed._custom;
                const value = isFloatBar(custom) ? "[" + custom.start + ", " + custom.end + "]" : "" + vScale.getLabelForValue(parsed[vScale.axis]);
                return {
                    label: "" + iScale.getLabelForValue(parsed[iScale.axis]),
                    value
                };
            }
            initialize() {
                this.enableOptionSharing = true;
                super.initialize();
                const meta = this._cachedMeta;
                meta.stack = this.getDataset().stack;
            }
            update(mode) {
                const meta = this._cachedMeta;
                this.updateElements(meta.data, 0, meta.data.length, mode);
            }
            updateElements(bars, start, count, mode) {
                const reset = "reset" === mode;
                const {index, _cachedMeta: {vScale}} = this;
                const base = vScale.getBasePixel();
                const horizontal = vScale.isHorizontal();
                const ruler = this._getRuler();
                const firstOpts = this.resolveDataElementOptions(start, mode);
                const sharedOptions = this.getSharedOptions(firstOpts);
                const includeOptions = this.includeOptions(mode, sharedOptions);
                this.updateSharedOptions(sharedOptions, mode, firstOpts);
                for (let i = start; i < start + count; i++) {
                    const parsed = this.getParsed(i);
                    const vpixels = reset || isNullOrUndef(parsed[vScale.axis]) ? {
                        base,
                        head: base
                    } : this._calculateBarValuePixels(i);
                    const ipixels = this._calculateBarIndexPixels(i, ruler);
                    const stack = (parsed._stacks || {})[vScale.axis];
                    const properties = {
                        horizontal,
                        base: vpixels.base,
                        enableBorderRadius: !stack || isFloatBar(parsed._custom) || index === stack._top || index === stack._bottom,
                        x: horizontal ? vpixels.head : ipixels.center,
                        y: horizontal ? ipixels.center : vpixels.head,
                        height: horizontal ? ipixels.size : Math.abs(vpixels.size),
                        width: horizontal ? Math.abs(vpixels.size) : ipixels.size
                    };
                    if (includeOptions) properties.options = sharedOptions || this.resolveDataElementOptions(i, bars[i].active ? "active" : mode);
                    const options = properties.options || bars[i].options;
                    setBorderSkipped(properties, options, stack, index);
                    setInflateAmount(properties, options, ruler.ratio);
                    this.updateElement(bars[i], i, properties, mode);
                }
            }
            _getStacks(last, dataIndex) {
                const meta = this._cachedMeta;
                const iScale = meta.iScale;
                const metasets = iScale.getMatchingVisibleMetas(this._type);
                const stacked = iScale.options.stacked;
                const ilen = metasets.length;
                const stacks = [];
                let i, item;
                for (i = 0; i < ilen; ++i) {
                    item = metasets[i];
                    if (!item.controller.options.grouped) continue;
                    if ("undefined" !== typeof dataIndex) {
                        const val = item.controller.getParsed(dataIndex)[item.controller._cachedMeta.vScale.axis];
                        if (isNullOrUndef(val) || isNaN(val)) continue;
                    }
                    if (false === stacked || -1 === stacks.indexOf(item.stack) || void 0 === stacked && void 0 === item.stack) stacks.push(item.stack);
                    if (item.index === last) break;
                }
                if (!stacks.length) stacks.push(void 0);
                return stacks;
            }
            _getStackCount(index) {
                return this._getStacks(void 0, index).length;
            }
            _getStackIndex(datasetIndex, name, dataIndex) {
                const stacks = this._getStacks(datasetIndex, dataIndex);
                const index = void 0 !== name ? stacks.indexOf(name) : -1;
                return -1 === index ? stacks.length - 1 : index;
            }
            _getRuler() {
                const opts = this.options;
                const meta = this._cachedMeta;
                const iScale = meta.iScale;
                const pixels = [];
                let i, ilen;
                for (i = 0, ilen = meta.data.length; i < ilen; ++i) pixels.push(iScale.getPixelForValue(this.getParsed(i)[iScale.axis], i));
                const barThickness = opts.barThickness;
                const min = barThickness || computeMinSampleSize(meta);
                return {
                    min,
                    pixels,
                    start: iScale._startPixel,
                    end: iScale._endPixel,
                    stackCount: this._getStackCount(),
                    scale: iScale,
                    grouped: opts.grouped,
                    ratio: barThickness ? 1 : opts.categoryPercentage * opts.barPercentage
                };
            }
            _calculateBarValuePixels(index) {
                const {_cachedMeta: {vScale, _stacked}, options: {base: baseValue, minBarLength}} = this;
                const actualBase = baseValue || 0;
                const parsed = this.getParsed(index);
                const custom = parsed._custom;
                const floating = isFloatBar(custom);
                let value = parsed[vScale.axis];
                let start = 0;
                let length = _stacked ? this.applyStack(vScale, parsed, _stacked) : value;
                let head, size;
                if (length !== value) {
                    start = length - value;
                    length = value;
                }
                if (floating) {
                    value = custom.barStart;
                    length = custom.barEnd - custom.barStart;
                    if (0 !== value && sign(value) !== sign(custom.barEnd)) start = 0;
                    start += value;
                }
                const startValue = !isNullOrUndef(baseValue) && !floating ? baseValue : start;
                let base = vScale.getPixelForValue(startValue);
                if (this.chart.getDataVisibility(index)) head = vScale.getPixelForValue(start + length); else head = base;
                size = head - base;
                if (Math.abs(size) < minBarLength) {
                    size = barSign(size, vScale, actualBase) * minBarLength;
                    if (value === actualBase) base -= size / 2;
                    head = base + size;
                }
                if (base === vScale.getPixelForValue(actualBase)) {
                    const halfGrid = sign(size) * vScale.getLineWidthForValue(actualBase) / 2;
                    base += halfGrid;
                    size -= halfGrid;
                }
                return {
                    size,
                    base,
                    head,
                    center: head + size / 2
                };
            }
            _calculateBarIndexPixels(index, ruler) {
                const scale = ruler.scale;
                const options = this.options;
                const skipNull = options.skipNull;
                const maxBarThickness = valueOrDefault(options.maxBarThickness, 1 / 0);
                let center, size;
                if (ruler.grouped) {
                    const stackCount = skipNull ? this._getStackCount(index) : ruler.stackCount;
                    const range = "flex" === options.barThickness ? computeFlexCategoryTraits(index, ruler, options, stackCount) : computeFitCategoryTraits(index, ruler, options, stackCount);
                    const stackIndex = this._getStackIndex(this.index, this._cachedMeta.stack, skipNull ? index : void 0);
                    center = range.start + range.chunk * stackIndex + range.chunk / 2;
                    size = Math.min(maxBarThickness, range.chunk * range.ratio);
                } else {
                    center = scale.getPixelForValue(this.getParsed(index)[scale.axis], index);
                    size = Math.min(maxBarThickness, ruler.min * ruler.ratio);
                }
                return {
                    base: center - size / 2,
                    head: center + size / 2,
                    center,
                    size
                };
            }
            draw() {
                const meta = this._cachedMeta;
                const vScale = meta.vScale;
                const rects = meta.data;
                const ilen = rects.length;
                let i = 0;
                for (;i < ilen; ++i) if (null !== this.getParsed(i)[vScale.axis]) rects[i].draw(this._ctx);
            }
        }
        BarController.id = "bar";
        BarController.defaults = {
            datasetElementType: false,
            dataElementType: "bar",
            categoryPercentage: .8,
            barPercentage: .9,
            grouped: true,
            animations: {
                numbers: {
                    type: "number",
                    properties: [ "x", "y", "base", "width", "height" ]
                }
            }
        };
        BarController.overrides = {
            scales: {
                _index_: {
                    type: "category",
                    offset: true,
                    grid: {
                        offset: true
                    }
                },
                _value_: {
                    type: "linear",
                    beginAtZero: true
                }
            }
        };
        class BubbleController extends DatasetController {
            initialize() {
                this.enableOptionSharing = true;
                super.initialize();
            }
            parsePrimitiveData(meta, data, start, count) {
                const parsed = super.parsePrimitiveData(meta, data, start, count);
                for (let i = 0; i < parsed.length; i++) parsed[i]._custom = this.resolveDataElementOptions(i + start).radius;
                return parsed;
            }
            parseArrayData(meta, data, start, count) {
                const parsed = super.parseArrayData(meta, data, start, count);
                for (let i = 0; i < parsed.length; i++) {
                    const item = data[start + i];
                    parsed[i]._custom = valueOrDefault(item[2], this.resolveDataElementOptions(i + start).radius);
                }
                return parsed;
            }
            parseObjectData(meta, data, start, count) {
                const parsed = super.parseObjectData(meta, data, start, count);
                for (let i = 0; i < parsed.length; i++) {
                    const item = data[start + i];
                    parsed[i]._custom = valueOrDefault(item && item.r && +item.r, this.resolveDataElementOptions(i + start).radius);
                }
                return parsed;
            }
            getMaxOverflow() {
                const data = this._cachedMeta.data;
                let max = 0;
                for (let i = data.length - 1; i >= 0; --i) max = Math.max(max, data[i].size(this.resolveDataElementOptions(i)) / 2);
                return max > 0 && max;
            }
            getLabelAndValue(index) {
                const meta = this._cachedMeta;
                const {xScale, yScale} = meta;
                const parsed = this.getParsed(index);
                const x = xScale.getLabelForValue(parsed.x);
                const y = yScale.getLabelForValue(parsed.y);
                const r = parsed._custom;
                return {
                    label: meta.label,
                    value: "(" + x + ", " + y + (r ? ", " + r : "") + ")"
                };
            }
            update(mode) {
                const points = this._cachedMeta.data;
                this.updateElements(points, 0, points.length, mode);
            }
            updateElements(points, start, count, mode) {
                const reset = "reset" === mode;
                const {iScale, vScale} = this._cachedMeta;
                const firstOpts = this.resolveDataElementOptions(start, mode);
                const sharedOptions = this.getSharedOptions(firstOpts);
                const includeOptions = this.includeOptions(mode, sharedOptions);
                const iAxis = iScale.axis;
                const vAxis = vScale.axis;
                for (let i = start; i < start + count; i++) {
                    const point = points[i];
                    const parsed = !reset && this.getParsed(i);
                    const properties = {};
                    const iPixel = properties[iAxis] = reset ? iScale.getPixelForDecimal(.5) : iScale.getPixelForValue(parsed[iAxis]);
                    const vPixel = properties[vAxis] = reset ? vScale.getBasePixel() : vScale.getPixelForValue(parsed[vAxis]);
                    properties.skip = isNaN(iPixel) || isNaN(vPixel);
                    if (includeOptions) {
                        properties.options = this.resolveDataElementOptions(i, point.active ? "active" : mode);
                        if (reset) properties.options.radius = 0;
                    }
                    this.updateElement(point, i, properties, mode);
                }
                this.updateSharedOptions(sharedOptions, mode, firstOpts);
            }
            resolveDataElementOptions(index, mode) {
                const parsed = this.getParsed(index);
                let values = super.resolveDataElementOptions(index, mode);
                if (values.$shared) values = Object.assign({}, values, {
                    $shared: false
                });
                const radius = values.radius;
                if ("active" !== mode) values.radius = 0;
                values.radius += valueOrDefault(parsed && parsed._custom, radius);
                return values;
            }
        }
        BubbleController.id = "bubble";
        BubbleController.defaults = {
            datasetElementType: false,
            dataElementType: "point",
            animations: {
                numbers: {
                    type: "number",
                    properties: [ "x", "y", "borderWidth", "radius" ]
                }
            }
        };
        BubbleController.overrides = {
            scales: {
                x: {
                    type: "linear"
                },
                y: {
                    type: "linear"
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        title() {
                            return "";
                        }
                    }
                }
            }
        };
        function getRatioAndOffset(rotation, circumference, cutout) {
            let ratioX = 1;
            let ratioY = 1;
            let offsetX = 0;
            let offsetY = 0;
            if (circumference < TAU) {
                const startAngle = rotation;
                const endAngle = startAngle + circumference;
                const startX = Math.cos(startAngle);
                const startY = Math.sin(startAngle);
                const endX = Math.cos(endAngle);
                const endY = Math.sin(endAngle);
                const calcMax = (angle, a, b) => _angleBetween(angle, startAngle, endAngle, true) ? 1 : Math.max(a, a * cutout, b, b * cutout);
                const calcMin = (angle, a, b) => _angleBetween(angle, startAngle, endAngle, true) ? -1 : Math.min(a, a * cutout, b, b * cutout);
                const maxX = calcMax(0, startX, endX);
                const maxY = calcMax(HALF_PI, startY, endY);
                const minX = calcMin(PI, startX, endX);
                const minY = calcMin(PI + HALF_PI, startY, endY);
                ratioX = (maxX - minX) / 2;
                ratioY = (maxY - minY) / 2;
                offsetX = -(maxX + minX) / 2;
                offsetY = -(maxY + minY) / 2;
            }
            return {
                ratioX,
                ratioY,
                offsetX,
                offsetY
            };
        }
        class DoughnutController extends DatasetController {
            constructor(chart, datasetIndex) {
                super(chart, datasetIndex);
                this.enableOptionSharing = true;
                this.innerRadius = void 0;
                this.outerRadius = void 0;
                this.offsetX = void 0;
                this.offsetY = void 0;
            }
            linkScales() {}
            parse(start, count) {
                const data = this.getDataset().data;
                const meta = this._cachedMeta;
                if (false === this._parsing) meta._parsed = data; else {
                    let getter = i => +data[i];
                    if (isObject(data[start])) {
                        const {key = "value"} = this._parsing;
                        getter = i => +resolveObjectKey(data[i], key);
                    }
                    let i, ilen;
                    for (i = start, ilen = start + count; i < ilen; ++i) meta._parsed[i] = getter(i);
                }
            }
            _getRotation() {
                return toRadians(this.options.rotation - 90);
            }
            _getCircumference() {
                return toRadians(this.options.circumference);
            }
            _getRotationExtents() {
                let min = TAU;
                let max = -TAU;
                for (let i = 0; i < this.chart.data.datasets.length; ++i) if (this.chart.isDatasetVisible(i)) {
                    const controller = this.chart.getDatasetMeta(i).controller;
                    const rotation = controller._getRotation();
                    const circumference = controller._getCircumference();
                    min = Math.min(min, rotation);
                    max = Math.max(max, rotation + circumference);
                }
                return {
                    rotation: min,
                    circumference: max - min
                };
            }
            update(mode) {
                const chart = this.chart;
                const {chartArea} = chart;
                const meta = this._cachedMeta;
                const arcs = meta.data;
                const spacing = this.getMaxBorderWidth() + this.getMaxOffset(arcs) + this.options.spacing;
                const maxSize = Math.max((Math.min(chartArea.width, chartArea.height) - spacing) / 2, 0);
                const cutout = Math.min(toPercentage(this.options.cutout, maxSize), 1);
                const chartWeight = this._getRingWeight(this.index);
                const {circumference, rotation} = this._getRotationExtents();
                const {ratioX, ratioY, offsetX, offsetY} = getRatioAndOffset(rotation, circumference, cutout);
                const maxWidth = (chartArea.width - spacing) / ratioX;
                const maxHeight = (chartArea.height - spacing) / ratioY;
                const maxRadius = Math.max(Math.min(maxWidth, maxHeight) / 2, 0);
                const outerRadius = toDimension(this.options.radius, maxRadius);
                const innerRadius = Math.max(outerRadius * cutout, 0);
                const radiusLength = (outerRadius - innerRadius) / this._getVisibleDatasetWeightTotal();
                this.offsetX = offsetX * outerRadius;
                this.offsetY = offsetY * outerRadius;
                meta.total = this.calculateTotal();
                this.outerRadius = outerRadius - radiusLength * this._getRingWeightOffset(this.index);
                this.innerRadius = Math.max(this.outerRadius - radiusLength * chartWeight, 0);
                this.updateElements(arcs, 0, arcs.length, mode);
            }
            _circumference(i, reset) {
                const opts = this.options;
                const meta = this._cachedMeta;
                const circumference = this._getCircumference();
                if (reset && opts.animation.animateRotate || !this.chart.getDataVisibility(i) || null === meta._parsed[i] || meta.data[i].hidden) return 0;
                return this.calculateCircumference(meta._parsed[i] * circumference / TAU);
            }
            updateElements(arcs, start, count, mode) {
                const reset = "reset" === mode;
                const chart = this.chart;
                const chartArea = chart.chartArea;
                const opts = chart.options;
                const animationOpts = opts.animation;
                const centerX = (chartArea.left + chartArea.right) / 2;
                const centerY = (chartArea.top + chartArea.bottom) / 2;
                const animateScale = reset && animationOpts.animateScale;
                const innerRadius = animateScale ? 0 : this.innerRadius;
                const outerRadius = animateScale ? 0 : this.outerRadius;
                const firstOpts = this.resolveDataElementOptions(start, mode);
                const sharedOptions = this.getSharedOptions(firstOpts);
                const includeOptions = this.includeOptions(mode, sharedOptions);
                let startAngle = this._getRotation();
                let i;
                for (i = 0; i < start; ++i) startAngle += this._circumference(i, reset);
                for (i = start; i < start + count; ++i) {
                    const circumference = this._circumference(i, reset);
                    const arc = arcs[i];
                    const properties = {
                        x: centerX + this.offsetX,
                        y: centerY + this.offsetY,
                        startAngle,
                        endAngle: startAngle + circumference,
                        circumference,
                        outerRadius,
                        innerRadius
                    };
                    if (includeOptions) properties.options = sharedOptions || this.resolveDataElementOptions(i, arc.active ? "active" : mode);
                    startAngle += circumference;
                    this.updateElement(arc, i, properties, mode);
                }
                this.updateSharedOptions(sharedOptions, mode, firstOpts);
            }
            calculateTotal() {
                const meta = this._cachedMeta;
                const metaData = meta.data;
                let total = 0;
                let i;
                for (i = 0; i < metaData.length; i++) {
                    const value = meta._parsed[i];
                    if (null !== value && !isNaN(value) && this.chart.getDataVisibility(i) && !metaData[i].hidden) total += Math.abs(value);
                }
                return total;
            }
            calculateCircumference(value) {
                const total = this._cachedMeta.total;
                if (total > 0 && !isNaN(value)) return TAU * (Math.abs(value) / total);
                return 0;
            }
            getLabelAndValue(index) {
                const meta = this._cachedMeta;
                const chart = this.chart;
                const labels = chart.data.labels || [];
                const value = formatNumber(meta._parsed[index], chart.options.locale);
                return {
                    label: labels[index] || "",
                    value
                };
            }
            getMaxBorderWidth(arcs) {
                let max = 0;
                const chart = this.chart;
                let i, ilen, meta, controller, options;
                if (!arcs) for (i = 0, ilen = chart.data.datasets.length; i < ilen; ++i) if (chart.isDatasetVisible(i)) {
                    meta = chart.getDatasetMeta(i);
                    arcs = meta.data;
                    controller = meta.controller;
                    break;
                }
                if (!arcs) return 0;
                for (i = 0, ilen = arcs.length; i < ilen; ++i) {
                    options = controller.resolveDataElementOptions(i);
                    if ("inner" !== options.borderAlign) max = Math.max(max, options.borderWidth || 0, options.hoverBorderWidth || 0);
                }
                return max;
            }
            getMaxOffset(arcs) {
                let max = 0;
                for (let i = 0, ilen = arcs.length; i < ilen; ++i) {
                    const options = this.resolveDataElementOptions(i);
                    max = Math.max(max, options.offset || 0, options.hoverOffset || 0);
                }
                return max;
            }
            _getRingWeightOffset(datasetIndex) {
                let ringWeightOffset = 0;
                for (let i = 0; i < datasetIndex; ++i) if (this.chart.isDatasetVisible(i)) ringWeightOffset += this._getRingWeight(i);
                return ringWeightOffset;
            }
            _getRingWeight(datasetIndex) {
                return Math.max(valueOrDefault(this.chart.data.datasets[datasetIndex].weight, 1), 0);
            }
            _getVisibleDatasetWeightTotal() {
                return this._getRingWeightOffset(this.chart.data.datasets.length) || 1;
            }
        }
        DoughnutController.id = "doughnut";
        DoughnutController.defaults = {
            datasetElementType: false,
            dataElementType: "arc",
            animation: {
                animateRotate: true,
                animateScale: false
            },
            animations: {
                numbers: {
                    type: "number",
                    properties: [ "circumference", "endAngle", "innerRadius", "outerRadius", "startAngle", "x", "y", "offset", "borderWidth", "spacing" ]
                }
            },
            cutout: "50%",
            rotation: 0,
            circumference: 360,
            radius: "100%",
            spacing: 0,
            indexAxis: "r"
        };
        DoughnutController.descriptors = {
            _scriptable: name => "spacing" !== name,
            _indexable: name => "spacing" !== name
        };
        DoughnutController.overrides = {
            aspectRatio: 1,
            plugins: {
                legend: {
                    labels: {
                        generateLabels(chart) {
                            const data = chart.data;
                            if (data.labels.length && data.datasets.length) {
                                const {labels: {pointStyle}} = chart.legend.options;
                                return data.labels.map(((label, i) => {
                                    const meta = chart.getDatasetMeta(0);
                                    const style = meta.controller.getStyle(i);
                                    return {
                                        text: label,
                                        fillStyle: style.backgroundColor,
                                        strokeStyle: style.borderColor,
                                        lineWidth: style.borderWidth,
                                        pointStyle,
                                        hidden: !chart.getDataVisibility(i),
                                        index: i
                                    };
                                }));
                            }
                            return [];
                        }
                    },
                    onClick(e, legendItem, legend) {
                        legend.chart.toggleDataVisibility(legendItem.index);
                        legend.chart.update();
                    }
                },
                tooltip: {
                    callbacks: {
                        title() {
                            return "";
                        },
                        label(tooltipItem) {
                            let dataLabel = tooltipItem.label;
                            const value = ": " + tooltipItem.formattedValue;
                            if (isArray(dataLabel)) {
                                dataLabel = dataLabel.slice();
                                dataLabel[0] += value;
                            } else dataLabel += value;
                            return dataLabel;
                        }
                    }
                }
            }
        };
        class LineController extends DatasetController {
            initialize() {
                this.enableOptionSharing = true;
                super.initialize();
            }
            update(mode) {
                const meta = this._cachedMeta;
                const {dataset: line, data: points = [], _dataset} = meta;
                const animationsDisabled = this.chart._animationsDisabled;
                let {start, count} = getStartAndCountOfVisiblePoints(meta, points, animationsDisabled);
                this._drawStart = start;
                this._drawCount = count;
                if (scaleRangesChanged(meta)) {
                    start = 0;
                    count = points.length;
                }
                line._chart = this.chart;
                line._datasetIndex = this.index;
                line._decimated = !!_dataset._decimated;
                line.points = points;
                const options = this.resolveDatasetElementOptions(mode);
                if (!this.options.showLine) options.borderWidth = 0;
                options.segment = this.options.segment;
                this.updateElement(line, void 0, {
                    animated: !animationsDisabled,
                    options
                }, mode);
                this.updateElements(points, start, count, mode);
            }
            updateElements(points, start, count, mode) {
                const reset = "reset" === mode;
                const {iScale, vScale, _stacked, _dataset} = this._cachedMeta;
                const firstOpts = this.resolveDataElementOptions(start, mode);
                const sharedOptions = this.getSharedOptions(firstOpts);
                const includeOptions = this.includeOptions(mode, sharedOptions);
                const iAxis = iScale.axis;
                const vAxis = vScale.axis;
                const {spanGaps, segment} = this.options;
                const maxGapLength = isNumber(spanGaps) ? spanGaps : Number.POSITIVE_INFINITY;
                const directUpdate = this.chart._animationsDisabled || reset || "none" === mode;
                let prevParsed = start > 0 && this.getParsed(start - 1);
                for (let i = start; i < start + count; ++i) {
                    const point = points[i];
                    const parsed = this.getParsed(i);
                    const properties = directUpdate ? point : {};
                    const nullData = isNullOrUndef(parsed[vAxis]);
                    const iPixel = properties[iAxis] = iScale.getPixelForValue(parsed[iAxis], i);
                    const vPixel = properties[vAxis] = reset || nullData ? vScale.getBasePixel() : vScale.getPixelForValue(_stacked ? this.applyStack(vScale, parsed, _stacked) : parsed[vAxis], i);
                    properties.skip = isNaN(iPixel) || isNaN(vPixel) || nullData;
                    properties.stop = i > 0 && parsed[iAxis] - prevParsed[iAxis] > maxGapLength;
                    if (segment) {
                        properties.parsed = parsed;
                        properties.raw = _dataset.data[i];
                    }
                    if (includeOptions) properties.options = sharedOptions || this.resolveDataElementOptions(i, point.active ? "active" : mode);
                    if (!directUpdate) this.updateElement(point, i, properties, mode);
                    prevParsed = parsed;
                }
                this.updateSharedOptions(sharedOptions, mode, firstOpts);
            }
            getMaxOverflow() {
                const meta = this._cachedMeta;
                const dataset = meta.dataset;
                const border = dataset.options && dataset.options.borderWidth || 0;
                const data = meta.data || [];
                if (!data.length) return border;
                const firstPoint = data[0].size(this.resolveDataElementOptions(0));
                const lastPoint = data[data.length - 1].size(this.resolveDataElementOptions(data.length - 1));
                return Math.max(border, firstPoint, lastPoint) / 2;
            }
            draw() {
                const meta = this._cachedMeta;
                meta.dataset.updateControlPoints(this.chart.chartArea, meta.iScale.axis);
                super.draw();
            }
        }
        LineController.id = "line";
        LineController.defaults = {
            datasetElementType: "line",
            dataElementType: "point",
            showLine: true,
            spanGaps: false
        };
        LineController.overrides = {
            scales: {
                _index_: {
                    type: "category"
                },
                _value_: {
                    type: "linear"
                }
            }
        };
        function getStartAndCountOfVisiblePoints(meta, points, animationsDisabled) {
            const pointCount = points.length;
            let start = 0;
            let count = pointCount;
            if (meta._sorted) {
                const {iScale, _parsed} = meta;
                const axis = iScale.axis;
                const {min, max, minDefined, maxDefined} = iScale.getUserBounds();
                if (minDefined) start = _limitValue(Math.min(_lookupByKey(_parsed, iScale.axis, min).lo, animationsDisabled ? pointCount : _lookupByKey(points, axis, iScale.getPixelForValue(min)).lo), 0, pointCount - 1);
                if (maxDefined) count = _limitValue(Math.max(_lookupByKey(_parsed, iScale.axis, max).hi + 1, animationsDisabled ? 0 : _lookupByKey(points, axis, iScale.getPixelForValue(max)).hi + 1), start, pointCount) - start; else count = pointCount - start;
            }
            return {
                start,
                count
            };
        }
        function scaleRangesChanged(meta) {
            const {xScale, yScale, _scaleRanges} = meta;
            const newRanges = {
                xmin: xScale.min,
                xmax: xScale.max,
                ymin: yScale.min,
                ymax: yScale.max
            };
            if (!_scaleRanges) {
                meta._scaleRanges = newRanges;
                return true;
            }
            const changed = _scaleRanges.xmin !== xScale.min || _scaleRanges.xmax !== xScale.max || _scaleRanges.ymin !== yScale.min || _scaleRanges.ymax !== yScale.max;
            Object.assign(_scaleRanges, newRanges);
            return changed;
        }
        class PolarAreaController extends DatasetController {
            constructor(chart, datasetIndex) {
                super(chart, datasetIndex);
                this.innerRadius = void 0;
                this.outerRadius = void 0;
            }
            getLabelAndValue(index) {
                const meta = this._cachedMeta;
                const chart = this.chart;
                const labels = chart.data.labels || [];
                const value = formatNumber(meta._parsed[index].r, chart.options.locale);
                return {
                    label: labels[index] || "",
                    value
                };
            }
            update(mode) {
                const arcs = this._cachedMeta.data;
                this._updateRadius();
                this.updateElements(arcs, 0, arcs.length, mode);
            }
            _updateRadius() {
                const chart = this.chart;
                const chartArea = chart.chartArea;
                const opts = chart.options;
                const minSize = Math.min(chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                const outerRadius = Math.max(minSize / 2, 0);
                const innerRadius = Math.max(opts.cutoutPercentage ? outerRadius / 100 * opts.cutoutPercentage : 1, 0);
                const radiusLength = (outerRadius - innerRadius) / chart.getVisibleDatasetCount();
                this.outerRadius = outerRadius - radiusLength * this.index;
                this.innerRadius = this.outerRadius - radiusLength;
            }
            updateElements(arcs, start, count, mode) {
                const reset = "reset" === mode;
                const chart = this.chart;
                const dataset = this.getDataset();
                const opts = chart.options;
                const animationOpts = opts.animation;
                const scale = this._cachedMeta.rScale;
                const centerX = scale.xCenter;
                const centerY = scale.yCenter;
                const datasetStartAngle = scale.getIndexAngle(0) - .5 * PI;
                let angle = datasetStartAngle;
                let i;
                const defaultAngle = 360 / this.countVisibleElements();
                for (i = 0; i < start; ++i) angle += this._computeAngle(i, mode, defaultAngle);
                for (i = start; i < start + count; i++) {
                    const arc = arcs[i];
                    let startAngle = angle;
                    let endAngle = angle + this._computeAngle(i, mode, defaultAngle);
                    let outerRadius = chart.getDataVisibility(i) ? scale.getDistanceFromCenterForValue(dataset.data[i]) : 0;
                    angle = endAngle;
                    if (reset) {
                        if (animationOpts.animateScale) outerRadius = 0;
                        if (animationOpts.animateRotate) startAngle = endAngle = datasetStartAngle;
                    }
                    const properties = {
                        x: centerX,
                        y: centerY,
                        innerRadius: 0,
                        outerRadius,
                        startAngle,
                        endAngle,
                        options: this.resolveDataElementOptions(i, arc.active ? "active" : mode)
                    };
                    this.updateElement(arc, i, properties, mode);
                }
            }
            countVisibleElements() {
                const dataset = this.getDataset();
                const meta = this._cachedMeta;
                let count = 0;
                meta.data.forEach(((element, index) => {
                    if (!isNaN(dataset.data[index]) && this.chart.getDataVisibility(index)) count++;
                }));
                return count;
            }
            _computeAngle(index, mode, defaultAngle) {
                return this.chart.getDataVisibility(index) ? toRadians(this.resolveDataElementOptions(index, mode).angle || defaultAngle) : 0;
            }
        }
        PolarAreaController.id = "polarArea";
        PolarAreaController.defaults = {
            dataElementType: "arc",
            animation: {
                animateRotate: true,
                animateScale: true
            },
            animations: {
                numbers: {
                    type: "number",
                    properties: [ "x", "y", "startAngle", "endAngle", "innerRadius", "outerRadius" ]
                }
            },
            indexAxis: "r",
            startAngle: 0
        };
        PolarAreaController.overrides = {
            aspectRatio: 1,
            plugins: {
                legend: {
                    labels: {
                        generateLabels(chart) {
                            const data = chart.data;
                            if (data.labels.length && data.datasets.length) {
                                const {labels: {pointStyle}} = chart.legend.options;
                                return data.labels.map(((label, i) => {
                                    const meta = chart.getDatasetMeta(0);
                                    const style = meta.controller.getStyle(i);
                                    return {
                                        text: label,
                                        fillStyle: style.backgroundColor,
                                        strokeStyle: style.borderColor,
                                        lineWidth: style.borderWidth,
                                        pointStyle,
                                        hidden: !chart.getDataVisibility(i),
                                        index: i
                                    };
                                }));
                            }
                            return [];
                        }
                    },
                    onClick(e, legendItem, legend) {
                        legend.chart.toggleDataVisibility(legendItem.index);
                        legend.chart.update();
                    }
                },
                tooltip: {
                    callbacks: {
                        title() {
                            return "";
                        },
                        label(context) {
                            return context.chart.data.labels[context.dataIndex] + ": " + context.formattedValue;
                        }
                    }
                }
            },
            scales: {
                r: {
                    type: "radialLinear",
                    angleLines: {
                        display: false
                    },
                    beginAtZero: true,
                    grid: {
                        circular: true
                    },
                    pointLabels: {
                        display: false
                    },
                    startAngle: 0
                }
            }
        };
        class PieController extends DoughnutController {}
        PieController.id = "pie";
        PieController.defaults = {
            cutout: 0,
            rotation: 0,
            circumference: 360,
            radius: "100%"
        };
        class RadarController extends DatasetController {
            getLabelAndValue(index) {
                const vScale = this._cachedMeta.vScale;
                const parsed = this.getParsed(index);
                return {
                    label: vScale.getLabels()[index],
                    value: "" + vScale.getLabelForValue(parsed[vScale.axis])
                };
            }
            update(mode) {
                const meta = this._cachedMeta;
                const line = meta.dataset;
                const points = meta.data || [];
                const labels = meta.iScale.getLabels();
                line.points = points;
                if ("resize" !== mode) {
                    const options = this.resolveDatasetElementOptions(mode);
                    if (!this.options.showLine) options.borderWidth = 0;
                    const properties = {
                        _loop: true,
                        _fullLoop: labels.length === points.length,
                        options
                    };
                    this.updateElement(line, void 0, properties, mode);
                }
                this.updateElements(points, 0, points.length, mode);
            }
            updateElements(points, start, count, mode) {
                const dataset = this.getDataset();
                const scale = this._cachedMeta.rScale;
                const reset = "reset" === mode;
                for (let i = start; i < start + count; i++) {
                    const point = points[i];
                    const options = this.resolveDataElementOptions(i, point.active ? "active" : mode);
                    const pointPosition = scale.getPointPositionForValue(i, dataset.data[i]);
                    const x = reset ? scale.xCenter : pointPosition.x;
                    const y = reset ? scale.yCenter : pointPosition.y;
                    const properties = {
                        x,
                        y,
                        angle: pointPosition.angle,
                        skip: isNaN(x) || isNaN(y),
                        options
                    };
                    this.updateElement(point, i, properties, mode);
                }
            }
        }
        RadarController.id = "radar";
        RadarController.defaults = {
            datasetElementType: "line",
            dataElementType: "point",
            indexAxis: "r",
            showLine: true,
            elements: {
                line: {
                    fill: "start"
                }
            }
        };
        RadarController.overrides = {
            aspectRatio: 1,
            scales: {
                r: {
                    type: "radialLinear"
                }
            }
        };
        class ScatterController extends LineController {}
        ScatterController.id = "scatter";
        ScatterController.defaults = {
            showLine: false,
            fill: false
        };
        ScatterController.overrides = {
            interaction: {
                mode: "point"
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        title() {
                            return "";
                        },
                        label(item) {
                            return "(" + item.label + ", " + item.formattedValue + ")";
                        }
                    }
                }
            },
            scales: {
                x: {
                    type: "linear"
                },
                y: {
                    type: "linear"
                }
            }
        };
        function chart_esm_abstract() {
            throw new Error("This method is not implemented: Check that a complete date adapter is provided.");
        }
        class DateAdapter {
            constructor(options) {
                this.options = options || {};
            }
            formats() {
                return chart_esm_abstract();
            }
            parse(value, format) {
                return chart_esm_abstract();
            }
            format(timestamp, format) {
                return chart_esm_abstract();
            }
            add(timestamp, amount, unit) {
                return chart_esm_abstract();
            }
            diff(a, b, unit) {
                return chart_esm_abstract();
            }
            startOf(timestamp, unit, weekday) {
                return chart_esm_abstract();
            }
            endOf(timestamp, unit) {
                return chart_esm_abstract();
            }
        }
        DateAdapter.override = function(members) {
            Object.assign(DateAdapter.prototype, members);
        };
        var adapters = {
            _date: DateAdapter
        };
        function chart_esm_getRelativePosition(e, chart) {
            if ("native" in e) return {
                x: e.x,
                y: e.y
            };
            return getRelativePosition(e, chart);
        }
        function evaluateAllVisibleItems(chart, handler) {
            const metasets = chart.getSortedVisibleDatasetMetas();
            let index, data, element;
            for (let i = 0, ilen = metasets.length; i < ilen; ++i) {
                ({index, data} = metasets[i]);
                for (let j = 0, jlen = data.length; j < jlen; ++j) {
                    element = data[j];
                    if (!element.skip) handler(element, index, j);
                }
            }
        }
        function binarySearch(metaset, axis, value, intersect) {
            const {controller, data, _sorted} = metaset;
            const iScale = controller._cachedMeta.iScale;
            if (iScale && axis === iScale.axis && "r" !== axis && _sorted && data.length) {
                const lookupMethod = iScale._reversePixels ? _rlookupByKey : _lookupByKey;
                if (!intersect) return lookupMethod(data, axis, value); else if (controller._sharedOptions) {
                    const el = data[0];
                    const range = "function" === typeof el.getRange && el.getRange(axis);
                    if (range) {
                        const start = lookupMethod(data, axis, value - range);
                        const end = lookupMethod(data, axis, value + range);
                        return {
                            lo: start.lo,
                            hi: end.hi
                        };
                    }
                }
            }
            return {
                lo: 0,
                hi: data.length - 1
            };
        }
        function optimizedEvaluateItems(chart, axis, position, handler, intersect) {
            const metasets = chart.getSortedVisibleDatasetMetas();
            const value = position[axis];
            for (let i = 0, ilen = metasets.length; i < ilen; ++i) {
                const {index, data} = metasets[i];
                const {lo, hi} = binarySearch(metasets[i], axis, value, intersect);
                for (let j = lo; j <= hi; ++j) {
                    const element = data[j];
                    if (!element.skip) handler(element, index, j);
                }
            }
        }
        function getDistanceMetricForAxis(axis) {
            const useX = -1 !== axis.indexOf("x");
            const useY = -1 !== axis.indexOf("y");
            return function(pt1, pt2) {
                const deltaX = useX ? Math.abs(pt1.x - pt2.x) : 0;
                const deltaY = useY ? Math.abs(pt1.y - pt2.y) : 0;
                return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
            };
        }
        function getIntersectItems(chart, position, axis, useFinalPosition) {
            const items = [];
            if (!_isPointInArea(position, chart.chartArea, chart._minPadding)) return items;
            const evaluationFunc = function(element, datasetIndex, index) {
                if (element.inRange(position.x, position.y, useFinalPosition)) items.push({
                    element,
                    datasetIndex,
                    index
                });
            };
            optimizedEvaluateItems(chart, axis, position, evaluationFunc, true);
            return items;
        }
        function getNearestRadialItems(chart, position, axis, useFinalPosition) {
            let items = [];
            function evaluationFunc(element, datasetIndex, index) {
                const {startAngle, endAngle} = element.getProps([ "startAngle", "endAngle" ], useFinalPosition);
                const {angle} = getAngleFromPoint(element, {
                    x: position.x,
                    y: position.y
                });
                if (_angleBetween(angle, startAngle, endAngle)) items.push({
                    element,
                    datasetIndex,
                    index
                });
            }
            optimizedEvaluateItems(chart, axis, position, evaluationFunc);
            return items;
        }
        function getNearestCartesianItems(chart, position, axis, intersect, useFinalPosition) {
            let items = [];
            const distanceMetric = getDistanceMetricForAxis(axis);
            let minDistance = Number.POSITIVE_INFINITY;
            function evaluationFunc(element, datasetIndex, index) {
                const inRange = element.inRange(position.x, position.y, useFinalPosition);
                if (intersect && !inRange) return;
                const center = element.getCenterPoint(useFinalPosition);
                const pointInArea = _isPointInArea(center, chart.chartArea, chart._minPadding);
                if (!pointInArea && !inRange) return;
                const distance = distanceMetric(position, center);
                if (distance < minDistance) {
                    items = [ {
                        element,
                        datasetIndex,
                        index
                    } ];
                    minDistance = distance;
                } else if (distance === minDistance) items.push({
                    element,
                    datasetIndex,
                    index
                });
            }
            optimizedEvaluateItems(chart, axis, position, evaluationFunc);
            return items;
        }
        function getNearestItems(chart, position, axis, intersect, useFinalPosition) {
            if (!_isPointInArea(position, chart.chartArea, chart._minPadding)) return [];
            return "r" === axis && !intersect ? getNearestRadialItems(chart, position, axis, useFinalPosition) : getNearestCartesianItems(chart, position, axis, intersect, useFinalPosition);
        }
        function getAxisItems(chart, e, options, useFinalPosition) {
            const position = chart_esm_getRelativePosition(e, chart);
            const items = [];
            const axis = options.axis;
            const rangeMethod = "x" === axis ? "inXRange" : "inYRange";
            let intersectsItem = false;
            evaluateAllVisibleItems(chart, ((element, datasetIndex, index) => {
                if (element[rangeMethod](position[axis], useFinalPosition)) items.push({
                    element,
                    datasetIndex,
                    index
                });
                if (element.inRange(position.x, position.y, useFinalPosition)) intersectsItem = true;
            }));
            if (options.intersect && !intersectsItem) return [];
            return items;
        }
        var Interaction = {
            modes: {
                index(chart, e, options, useFinalPosition) {
                    const position = chart_esm_getRelativePosition(e, chart);
                    const axis = options.axis || "x";
                    const items = options.intersect ? getIntersectItems(chart, position, axis, useFinalPosition) : getNearestItems(chart, position, axis, false, useFinalPosition);
                    const elements = [];
                    if (!items.length) return [];
                    chart.getSortedVisibleDatasetMetas().forEach((meta => {
                        const index = items[0].index;
                        const element = meta.data[index];
                        if (element && !element.skip) elements.push({
                            element,
                            datasetIndex: meta.index,
                            index
                        });
                    }));
                    return elements;
                },
                dataset(chart, e, options, useFinalPosition) {
                    const position = chart_esm_getRelativePosition(e, chart);
                    const axis = options.axis || "xy";
                    let items = options.intersect ? getIntersectItems(chart, position, axis, useFinalPosition) : getNearestItems(chart, position, axis, false, useFinalPosition);
                    if (items.length > 0) {
                        const datasetIndex = items[0].datasetIndex;
                        const data = chart.getDatasetMeta(datasetIndex).data;
                        items = [];
                        for (let i = 0; i < data.length; ++i) items.push({
                            element: data[i],
                            datasetIndex,
                            index: i
                        });
                    }
                    return items;
                },
                point(chart, e, options, useFinalPosition) {
                    const position = chart_esm_getRelativePosition(e, chart);
                    const axis = options.axis || "xy";
                    return getIntersectItems(chart, position, axis, useFinalPosition);
                },
                nearest(chart, e, options, useFinalPosition) {
                    const position = chart_esm_getRelativePosition(e, chart);
                    const axis = options.axis || "xy";
                    return getNearestItems(chart, position, axis, options.intersect, useFinalPosition);
                },
                x(chart, e, options, useFinalPosition) {
                    return getAxisItems(chart, e, {
                        axis: "x",
                        intersect: options.intersect
                    }, useFinalPosition);
                },
                y(chart, e, options, useFinalPosition) {
                    return getAxisItems(chart, e, {
                        axis: "y",
                        intersect: options.intersect
                    }, useFinalPosition);
                }
            }
        };
        const STATIC_POSITIONS = [ "left", "top", "right", "bottom" ];
        function filterByPosition(array, position) {
            return array.filter((v => v.pos === position));
        }
        function filterDynamicPositionByAxis(array, axis) {
            return array.filter((v => -1 === STATIC_POSITIONS.indexOf(v.pos) && v.box.axis === axis));
        }
        function sortByWeight(array, reverse) {
            return array.sort(((a, b) => {
                const v0 = reverse ? b : a;
                const v1 = reverse ? a : b;
                return v0.weight === v1.weight ? v0.index - v1.index : v0.weight - v1.weight;
            }));
        }
        function wrapBoxes(boxes) {
            const layoutBoxes = [];
            let i, ilen, box, pos, stack, stackWeight;
            for (i = 0, ilen = (boxes || []).length; i < ilen; ++i) {
                box = boxes[i];
                ({position: pos, options: {stack, stackWeight = 1}} = box);
                layoutBoxes.push({
                    index: i,
                    box,
                    pos,
                    horizontal: box.isHorizontal(),
                    weight: box.weight,
                    stack: stack && pos + stack,
                    stackWeight
                });
            }
            return layoutBoxes;
        }
        function buildStacks(layouts) {
            const stacks = {};
            for (const wrap of layouts) {
                const {stack, pos, stackWeight} = wrap;
                if (!stack || !STATIC_POSITIONS.includes(pos)) continue;
                const _stack = stacks[stack] || (stacks[stack] = {
                    count: 0,
                    placed: 0,
                    weight: 0,
                    size: 0
                });
                _stack.count++;
                _stack.weight += stackWeight;
            }
            return stacks;
        }
        function setLayoutDims(layouts, params) {
            const stacks = buildStacks(layouts);
            const {vBoxMaxWidth, hBoxMaxHeight} = params;
            let i, ilen, layout;
            for (i = 0, ilen = layouts.length; i < ilen; ++i) {
                layout = layouts[i];
                const {fullSize} = layout.box;
                const stack = stacks[layout.stack];
                const factor = stack && layout.stackWeight / stack.weight;
                if (layout.horizontal) {
                    layout.width = factor ? factor * vBoxMaxWidth : fullSize && params.availableWidth;
                    layout.height = hBoxMaxHeight;
                } else {
                    layout.width = vBoxMaxWidth;
                    layout.height = factor ? factor * hBoxMaxHeight : fullSize && params.availableHeight;
                }
            }
            return stacks;
        }
        function buildLayoutBoxes(boxes) {
            const layoutBoxes = wrapBoxes(boxes);
            const fullSize = sortByWeight(layoutBoxes.filter((wrap => wrap.box.fullSize)), true);
            const left = sortByWeight(filterByPosition(layoutBoxes, "left"), true);
            const right = sortByWeight(filterByPosition(layoutBoxes, "right"));
            const top = sortByWeight(filterByPosition(layoutBoxes, "top"), true);
            const bottom = sortByWeight(filterByPosition(layoutBoxes, "bottom"));
            const centerHorizontal = filterDynamicPositionByAxis(layoutBoxes, "x");
            const centerVertical = filterDynamicPositionByAxis(layoutBoxes, "y");
            return {
                fullSize,
                leftAndTop: left.concat(top),
                rightAndBottom: right.concat(centerVertical).concat(bottom).concat(centerHorizontal),
                chartArea: filterByPosition(layoutBoxes, "chartArea"),
                vertical: left.concat(right).concat(centerVertical),
                horizontal: top.concat(bottom).concat(centerHorizontal)
            };
        }
        function getCombinedMax(maxPadding, chartArea, a, b) {
            return Math.max(maxPadding[a], chartArea[a]) + Math.max(maxPadding[b], chartArea[b]);
        }
        function updateMaxPadding(maxPadding, boxPadding) {
            maxPadding.top = Math.max(maxPadding.top, boxPadding.top);
            maxPadding.left = Math.max(maxPadding.left, boxPadding.left);
            maxPadding.bottom = Math.max(maxPadding.bottom, boxPadding.bottom);
            maxPadding.right = Math.max(maxPadding.right, boxPadding.right);
        }
        function updateDims(chartArea, params, layout, stacks) {
            const {pos, box} = layout;
            const maxPadding = chartArea.maxPadding;
            if (!isObject(pos)) {
                if (layout.size) chartArea[pos] -= layout.size;
                const stack = stacks[layout.stack] || {
                    size: 0,
                    count: 1
                };
                stack.size = Math.max(stack.size, layout.horizontal ? box.height : box.width);
                layout.size = stack.size / stack.count;
                chartArea[pos] += layout.size;
            }
            if (box.getPadding) updateMaxPadding(maxPadding, box.getPadding());
            const newWidth = Math.max(0, params.outerWidth - getCombinedMax(maxPadding, chartArea, "left", "right"));
            const newHeight = Math.max(0, params.outerHeight - getCombinedMax(maxPadding, chartArea, "top", "bottom"));
            const widthChanged = newWidth !== chartArea.w;
            const heightChanged = newHeight !== chartArea.h;
            chartArea.w = newWidth;
            chartArea.h = newHeight;
            return layout.horizontal ? {
                same: widthChanged,
                other: heightChanged
            } : {
                same: heightChanged,
                other: widthChanged
            };
        }
        function handleMaxPadding(chartArea) {
            const maxPadding = chartArea.maxPadding;
            function updatePos(pos) {
                const change = Math.max(maxPadding[pos] - chartArea[pos], 0);
                chartArea[pos] += change;
                return change;
            }
            chartArea.y += updatePos("top");
            chartArea.x += updatePos("left");
            updatePos("right");
            updatePos("bottom");
        }
        function getMargins(horizontal, chartArea) {
            const maxPadding = chartArea.maxPadding;
            function marginForPositions(positions) {
                const margin = {
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0
                };
                positions.forEach((pos => {
                    margin[pos] = Math.max(chartArea[pos], maxPadding[pos]);
                }));
                return margin;
            }
            return horizontal ? marginForPositions([ "left", "right" ]) : marginForPositions([ "top", "bottom" ]);
        }
        function fitBoxes(boxes, chartArea, params, stacks) {
            const refitBoxes = [];
            let i, ilen, layout, box, refit, changed;
            for (i = 0, ilen = boxes.length, refit = 0; i < ilen; ++i) {
                layout = boxes[i];
                box = layout.box;
                box.update(layout.width || chartArea.w, layout.height || chartArea.h, getMargins(layout.horizontal, chartArea));
                const {same, other} = updateDims(chartArea, params, layout, stacks);
                refit |= same && refitBoxes.length;
                changed = changed || other;
                if (!box.fullSize) refitBoxes.push(layout);
            }
            return refit && fitBoxes(refitBoxes, chartArea, params, stacks) || changed;
        }
        function setBoxDims(box, left, top, width, height) {
            box.top = top;
            box.left = left;
            box.right = left + width;
            box.bottom = top + height;
            box.width = width;
            box.height = height;
        }
        function placeBoxes(boxes, chartArea, params, stacks) {
            const userPadding = params.padding;
            let {x, y} = chartArea;
            for (const layout of boxes) {
                const box = layout.box;
                const stack = stacks[layout.stack] || {
                    count: 1,
                    placed: 0,
                    weight: 1
                };
                const weight = layout.stackWeight / stack.weight || 1;
                if (layout.horizontal) {
                    const width = chartArea.w * weight;
                    const height = stack.size || box.height;
                    if (defined(stack.start)) y = stack.start;
                    if (box.fullSize) setBoxDims(box, userPadding.left, y, params.outerWidth - userPadding.right - userPadding.left, height); else setBoxDims(box, chartArea.left + stack.placed, y, width, height);
                    stack.start = y;
                    stack.placed += width;
                    y = box.bottom;
                } else {
                    const height = chartArea.h * weight;
                    const width = stack.size || box.width;
                    if (defined(stack.start)) x = stack.start;
                    if (box.fullSize) setBoxDims(box, x, userPadding.top, width, params.outerHeight - userPadding.bottom - userPadding.top); else setBoxDims(box, x, chartArea.top + stack.placed, width, height);
                    stack.start = x;
                    stack.placed += height;
                    x = box.right;
                }
            }
            chartArea.x = x;
            chartArea.y = y;
        }
        defaults.set("layout", {
            autoPadding: true,
            padding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            }
        });
        var layouts = {
            addBox(chart, item) {
                if (!chart.boxes) chart.boxes = [];
                item.fullSize = item.fullSize || false;
                item.position = item.position || "top";
                item.weight = item.weight || 0;
                item._layers = item._layers || function() {
                    return [ {
                        z: 0,
                        draw(chartArea) {
                            item.draw(chartArea);
                        }
                    } ];
                };
                chart.boxes.push(item);
            },
            removeBox(chart, layoutItem) {
                const index = chart.boxes ? chart.boxes.indexOf(layoutItem) : -1;
                if (-1 !== index) chart.boxes.splice(index, 1);
            },
            configure(chart, item, options) {
                item.fullSize = options.fullSize;
                item.position = options.position;
                item.weight = options.weight;
            },
            update(chart, width, height, minPadding) {
                if (!chart) return;
                const padding = toPadding(chart.options.layout.padding);
                const availableWidth = Math.max(width - padding.width, 0);
                const availableHeight = Math.max(height - padding.height, 0);
                const boxes = buildLayoutBoxes(chart.boxes);
                const verticalBoxes = boxes.vertical;
                const horizontalBoxes = boxes.horizontal;
                each(chart.boxes, (box => {
                    if ("function" === typeof box.beforeLayout) box.beforeLayout();
                }));
                const visibleVerticalBoxCount = verticalBoxes.reduce(((total, wrap) => wrap.box.options && false === wrap.box.options.display ? total : total + 1), 0) || 1;
                const params = Object.freeze({
                    outerWidth: width,
                    outerHeight: height,
                    padding,
                    availableWidth,
                    availableHeight,
                    vBoxMaxWidth: availableWidth / 2 / visibleVerticalBoxCount,
                    hBoxMaxHeight: availableHeight / 2
                });
                const maxPadding = Object.assign({}, padding);
                updateMaxPadding(maxPadding, toPadding(minPadding));
                const chartArea = Object.assign({
                    maxPadding,
                    w: availableWidth,
                    h: availableHeight,
                    x: padding.left,
                    y: padding.top
                }, padding);
                const stacks = setLayoutDims(verticalBoxes.concat(horizontalBoxes), params);
                fitBoxes(boxes.fullSize, chartArea, params, stacks);
                fitBoxes(verticalBoxes, chartArea, params, stacks);
                if (fitBoxes(horizontalBoxes, chartArea, params, stacks)) fitBoxes(verticalBoxes, chartArea, params, stacks);
                handleMaxPadding(chartArea);
                placeBoxes(boxes.leftAndTop, chartArea, params, stacks);
                chartArea.x += chartArea.w;
                chartArea.y += chartArea.h;
                placeBoxes(boxes.rightAndBottom, chartArea, params, stacks);
                chart.chartArea = {
                    left: chartArea.left,
                    top: chartArea.top,
                    right: chartArea.left + chartArea.w,
                    bottom: chartArea.top + chartArea.h,
                    height: chartArea.h,
                    width: chartArea.w
                };
                each(boxes.chartArea, (layout => {
                    const box = layout.box;
                    Object.assign(box, chart.chartArea);
                    box.update(chartArea.w, chartArea.h, {
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0
                    });
                }));
            }
        };
        class BasePlatform {
            acquireContext(canvas, aspectRatio) {}
            releaseContext(context) {
                return false;
            }
            addEventListener(chart, type, listener) {}
            removeEventListener(chart, type, listener) {}
            getDevicePixelRatio() {
                return 1;
            }
            getMaximumSize(element, width, height, aspectRatio) {
                width = Math.max(0, width || element.width);
                height = height || element.height;
                return {
                    width,
                    height: Math.max(0, aspectRatio ? Math.floor(width / aspectRatio) : height)
                };
            }
            isAttached(canvas) {
                return true;
            }
            updateConfig(config) {}
        }
        class BasicPlatform extends BasePlatform {
            acquireContext(item) {
                return item && item.getContext && item.getContext("2d") || null;
            }
            updateConfig(config) {
                config.options.animation = false;
            }
        }
        const EXPANDO_KEY = "$chartjs";
        const EVENT_TYPES = {
            touchstart: "mousedown",
            touchmove: "mousemove",
            touchend: "mouseup",
            pointerenter: "mouseenter",
            pointerdown: "mousedown",
            pointermove: "mousemove",
            pointerup: "mouseup",
            pointerleave: "mouseout",
            pointerout: "mouseout"
        };
        const isNullOrEmpty = value => null === value || "" === value;
        function initCanvas(canvas, aspectRatio) {
            const style = canvas.style;
            const renderHeight = canvas.getAttribute("height");
            const renderWidth = canvas.getAttribute("width");
            canvas[EXPANDO_KEY] = {
                initial: {
                    height: renderHeight,
                    width: renderWidth,
                    style: {
                        display: style.display,
                        height: style.height,
                        width: style.width
                    }
                }
            };
            style.display = style.display || "block";
            style.boxSizing = style.boxSizing || "border-box";
            if (isNullOrEmpty(renderWidth)) {
                const displayWidth = readUsedSize(canvas, "width");
                if (void 0 !== displayWidth) canvas.width = displayWidth;
            }
            if (isNullOrEmpty(renderHeight)) if ("" === canvas.style.height) canvas.height = canvas.width / (aspectRatio || 2); else {
                const displayHeight = readUsedSize(canvas, "height");
                if (void 0 !== displayHeight) canvas.height = displayHeight;
            }
            return canvas;
        }
        const eventListenerOptions = supportsEventListenerOptions ? {
            passive: true
        } : false;
        function addListener(node, type, listener) {
            node.addEventListener(type, listener, eventListenerOptions);
        }
        function removeListener(chart, type, listener) {
            chart.canvas.removeEventListener(type, listener, eventListenerOptions);
        }
        function fromNativeEvent(event, chart) {
            const type = EVENT_TYPES[event.type] || event.type;
            const {x, y} = getRelativePosition(event, chart);
            return {
                type,
                chart,
                native: event,
                x: void 0 !== x ? x : null,
                y: void 0 !== y ? y : null
            };
        }
        function nodeListContains(nodeList, canvas) {
            for (const node of nodeList) if (node === canvas || node.contains(canvas)) return true;
        }
        function createAttachObserver(chart, type, listener) {
            const canvas = chart.canvas;
            const observer = new MutationObserver((entries => {
                let trigger = false;
                for (const entry of entries) {
                    trigger = trigger || nodeListContains(entry.addedNodes, canvas);
                    trigger = trigger && !nodeListContains(entry.removedNodes, canvas);
                }
                if (trigger) listener();
            }));
            observer.observe(document, {
                childList: true,
                subtree: true
            });
            return observer;
        }
        function createDetachObserver(chart, type, listener) {
            const canvas = chart.canvas;
            const observer = new MutationObserver((entries => {
                let trigger = false;
                for (const entry of entries) {
                    trigger = trigger || nodeListContains(entry.removedNodes, canvas);
                    trigger = trigger && !nodeListContains(entry.addedNodes, canvas);
                }
                if (trigger) listener();
            }));
            observer.observe(document, {
                childList: true,
                subtree: true
            });
            return observer;
        }
        const drpListeningCharts = new Map;
        let oldDevicePixelRatio = 0;
        function onWindowResize() {
            const dpr = window.devicePixelRatio;
            if (dpr === oldDevicePixelRatio) return;
            oldDevicePixelRatio = dpr;
            drpListeningCharts.forEach(((resize, chart) => {
                if (chart.currentDevicePixelRatio !== dpr) resize();
            }));
        }
        function listenDevicePixelRatioChanges(chart, resize) {
            if (!drpListeningCharts.size) window.addEventListener("resize", onWindowResize);
            drpListeningCharts.set(chart, resize);
        }
        function unlistenDevicePixelRatioChanges(chart) {
            drpListeningCharts.delete(chart);
            if (!drpListeningCharts.size) window.removeEventListener("resize", onWindowResize);
        }
        function createResizeObserver(chart, type, listener) {
            const canvas = chart.canvas;
            const container = canvas && _getParentNode(canvas);
            if (!container) return;
            const resize = throttled(((width, height) => {
                const w = container.clientWidth;
                listener(width, height);
                if (w < container.clientWidth) listener();
            }), window);
            const observer = new ResizeObserver((entries => {
                const entry = entries[0];
                const width = entry.contentRect.width;
                const height = entry.contentRect.height;
                if (0 === width && 0 === height) return;
                resize(width, height);
            }));
            observer.observe(container);
            listenDevicePixelRatioChanges(chart, resize);
            return observer;
        }
        function releaseObserver(chart, type, observer) {
            if (observer) observer.disconnect();
            if ("resize" === type) unlistenDevicePixelRatioChanges(chart);
        }
        function createProxyAndListen(chart, type, listener) {
            const canvas = chart.canvas;
            const proxy = throttled((event => {
                if (null !== chart.ctx) listener(fromNativeEvent(event, chart));
            }), chart, (args => {
                const event = args[0];
                return [ event, event.offsetX, event.offsetY ];
            }));
            addListener(canvas, type, proxy);
            return proxy;
        }
        class DomPlatform extends BasePlatform {
            acquireContext(canvas, aspectRatio) {
                const context = canvas && canvas.getContext && canvas.getContext("2d");
                if (context && context.canvas === canvas) {
                    initCanvas(canvas, aspectRatio);
                    return context;
                }
                return null;
            }
            releaseContext(context) {
                const canvas = context.canvas;
                if (!canvas[EXPANDO_KEY]) return false;
                const initial = canvas[EXPANDO_KEY].initial;
                [ "height", "width" ].forEach((prop => {
                    const value = initial[prop];
                    if (isNullOrUndef(value)) canvas.removeAttribute(prop); else canvas.setAttribute(prop, value);
                }));
                const style = initial.style || {};
                Object.keys(style).forEach((key => {
                    canvas.style[key] = style[key];
                }));
                canvas.width = canvas.width;
                delete canvas[EXPANDO_KEY];
                return true;
            }
            addEventListener(chart, type, listener) {
                this.removeEventListener(chart, type);
                const proxies = chart.$proxies || (chart.$proxies = {});
                const handlers = {
                    attach: createAttachObserver,
                    detach: createDetachObserver,
                    resize: createResizeObserver
                };
                const handler = handlers[type] || createProxyAndListen;
                proxies[type] = handler(chart, type, listener);
            }
            removeEventListener(chart, type) {
                const proxies = chart.$proxies || (chart.$proxies = {});
                const proxy = proxies[type];
                if (!proxy) return;
                const handlers = {
                    attach: releaseObserver,
                    detach: releaseObserver,
                    resize: releaseObserver
                };
                const handler = handlers[type] || removeListener;
                handler(chart, type, proxy);
                proxies[type] = void 0;
            }
            getDevicePixelRatio() {
                return window.devicePixelRatio;
            }
            getMaximumSize(canvas, width, height, aspectRatio) {
                return getMaximumSize(canvas, width, height, aspectRatio);
            }
            isAttached(canvas) {
                const container = _getParentNode(canvas);
                return !!(container && container.isConnected);
            }
        }
        function _detectPlatform(canvas) {
            if (!_isDomSupported() || "undefined" !== typeof OffscreenCanvas && canvas instanceof OffscreenCanvas) return BasicPlatform;
            return DomPlatform;
        }
        class chart_esm_Element {
            constructor() {
                this.x = void 0;
                this.y = void 0;
                this.active = false;
                this.options = void 0;
                this.$animations = void 0;
            }
            tooltipPosition(useFinalPosition) {
                const {x, y} = this.getProps([ "x", "y" ], useFinalPosition);
                return {
                    x,
                    y
                };
            }
            hasValue() {
                return isNumber(this.x) && isNumber(this.y);
            }
            getProps(props, final) {
                const anims = this.$animations;
                if (!final || !anims) return this;
                const ret = {};
                props.forEach((prop => {
                    ret[prop] = anims[prop] && anims[prop].active() ? anims[prop]._to : this[prop];
                }));
                return ret;
            }
        }
        chart_esm_Element.defaults = {};
        chart_esm_Element.defaultRoutes = void 0;
        const formatters = {
            values(value) {
                return isArray(value) ? value : "" + value;
            },
            numeric(tickValue, index, ticks) {
                if (0 === tickValue) return "0";
                const locale = this.chart.options.locale;
                let notation;
                let delta = tickValue;
                if (ticks.length > 1) {
                    const maxTick = Math.max(Math.abs(ticks[0].value), Math.abs(ticks[ticks.length - 1].value));
                    if (maxTick < 1e-4 || maxTick > 1e15) notation = "scientific";
                    delta = calculateDelta(tickValue, ticks);
                }
                const logDelta = log10(Math.abs(delta));
                const numDecimal = Math.max(Math.min(-1 * Math.floor(logDelta), 20), 0);
                const options = {
                    notation,
                    minimumFractionDigits: numDecimal,
                    maximumFractionDigits: numDecimal
                };
                Object.assign(options, this.options.ticks.format);
                return formatNumber(tickValue, locale, options);
            },
            logarithmic(tickValue, index, ticks) {
                if (0 === tickValue) return "0";
                const remain = tickValue / Math.pow(10, Math.floor(log10(tickValue)));
                if (1 === remain || 2 === remain || 5 === remain) return formatters.numeric.call(this, tickValue, index, ticks);
                return "";
            }
        };
        function calculateDelta(tickValue, ticks) {
            let delta = ticks.length > 3 ? ticks[2].value - ticks[1].value : ticks[1].value - ticks[0].value;
            if (Math.abs(delta) >= 1 && tickValue !== Math.floor(tickValue)) delta = tickValue - Math.floor(tickValue);
            return delta;
        }
        var Ticks = {
            formatters
        };
        defaults.set("scale", {
            display: true,
            offset: false,
            reverse: false,
            beginAtZero: false,
            bounds: "ticks",
            grace: 0,
            grid: {
                display: true,
                lineWidth: 1,
                drawBorder: true,
                drawOnChartArea: true,
                drawTicks: true,
                tickLength: 8,
                tickWidth: (_ctx, options) => options.lineWidth,
                tickColor: (_ctx, options) => options.color,
                offset: false,
                borderDash: [],
                borderDashOffset: 0,
                borderWidth: 1
            },
            title: {
                display: false,
                text: "",
                padding: {
                    top: 4,
                    bottom: 4
                }
            },
            ticks: {
                minRotation: 0,
                maxRotation: 50,
                mirror: false,
                textStrokeWidth: 0,
                textStrokeColor: "",
                padding: 3,
                display: true,
                autoSkip: true,
                autoSkipPadding: 3,
                labelOffset: 0,
                callback: Ticks.formatters.values,
                minor: {},
                major: {},
                align: "center",
                crossAlign: "near",
                showLabelBackdrop: false,
                backdropColor: "rgba(255, 255, 255, 0.75)",
                backdropPadding: 2
            }
        });
        defaults.route("scale.ticks", "color", "", "color");
        defaults.route("scale.grid", "color", "", "borderColor");
        defaults.route("scale.grid", "borderColor", "", "borderColor");
        defaults.route("scale.title", "color", "", "color");
        defaults.describe("scale", {
            _fallback: false,
            _scriptable: name => !name.startsWith("before") && !name.startsWith("after") && "callback" !== name && "parser" !== name,
            _indexable: name => "borderDash" !== name && "tickBorderDash" !== name
        });
        defaults.describe("scales", {
            _fallback: "scale"
        });
        defaults.describe("scale.ticks", {
            _scriptable: name => "backdropPadding" !== name && "callback" !== name,
            _indexable: name => "backdropPadding" !== name
        });
        function autoSkip(scale, ticks) {
            const tickOpts = scale.options.ticks;
            const ticksLimit = tickOpts.maxTicksLimit || determineMaxTicks(scale);
            const majorIndices = tickOpts.major.enabled ? getMajorIndices(ticks) : [];
            const numMajorIndices = majorIndices.length;
            const first = majorIndices[0];
            const last = majorIndices[numMajorIndices - 1];
            const newTicks = [];
            if (numMajorIndices > ticksLimit) {
                skipMajors(ticks, newTicks, majorIndices, numMajorIndices / ticksLimit);
                return newTicks;
            }
            const spacing = calculateSpacing(majorIndices, ticks, ticksLimit);
            if (numMajorIndices > 0) {
                let i, ilen;
                const avgMajorSpacing = numMajorIndices > 1 ? Math.round((last - first) / (numMajorIndices - 1)) : null;
                skip(ticks, newTicks, spacing, isNullOrUndef(avgMajorSpacing) ? 0 : first - avgMajorSpacing, first);
                for (i = 0, ilen = numMajorIndices - 1; i < ilen; i++) skip(ticks, newTicks, spacing, majorIndices[i], majorIndices[i + 1]);
                skip(ticks, newTicks, spacing, last, isNullOrUndef(avgMajorSpacing) ? ticks.length : last + avgMajorSpacing);
                return newTicks;
            }
            skip(ticks, newTicks, spacing);
            return newTicks;
        }
        function determineMaxTicks(scale) {
            const offset = scale.options.offset;
            const tickLength = scale._tickSize();
            const maxScale = scale._length / tickLength + (offset ? 0 : 1);
            const maxChart = scale._maxLength / tickLength;
            return Math.floor(Math.min(maxScale, maxChart));
        }
        function calculateSpacing(majorIndices, ticks, ticksLimit) {
            const evenMajorSpacing = getEvenSpacing(majorIndices);
            const spacing = ticks.length / ticksLimit;
            if (!evenMajorSpacing) return Math.max(spacing, 1);
            const factors = _factorize(evenMajorSpacing);
            for (let i = 0, ilen = factors.length - 1; i < ilen; i++) {
                const factor = factors[i];
                if (factor > spacing) return factor;
            }
            return Math.max(spacing, 1);
        }
        function getMajorIndices(ticks) {
            const result = [];
            let i, ilen;
            for (i = 0, ilen = ticks.length; i < ilen; i++) if (ticks[i].major) result.push(i);
            return result;
        }
        function skipMajors(ticks, newTicks, majorIndices, spacing) {
            let count = 0;
            let next = majorIndices[0];
            let i;
            spacing = Math.ceil(spacing);
            for (i = 0; i < ticks.length; i++) if (i === next) {
                newTicks.push(ticks[i]);
                count++;
                next = majorIndices[count * spacing];
            }
        }
        function skip(ticks, newTicks, spacing, majorStart, majorEnd) {
            const start = valueOrDefault(majorStart, 0);
            const end = Math.min(valueOrDefault(majorEnd, ticks.length), ticks.length);
            let count = 0;
            let length, i, next;
            spacing = Math.ceil(spacing);
            if (majorEnd) {
                length = majorEnd - majorStart;
                spacing = length / Math.floor(length / spacing);
            }
            next = start;
            while (next < 0) {
                count++;
                next = Math.round(start + count * spacing);
            }
            for (i = Math.max(start, 0); i < end; i++) if (i === next) {
                newTicks.push(ticks[i]);
                count++;
                next = Math.round(start + count * spacing);
            }
        }
        function getEvenSpacing(arr) {
            const len = arr.length;
            let i, diff;
            if (len < 2) return false;
            for (diff = arr[0], i = 1; i < len; ++i) if (arr[i] - arr[i - 1] !== diff) return false;
            return diff;
        }
        const reverseAlign = align => "left" === align ? "right" : "right" === align ? "left" : align;
        const offsetFromEdge = (scale, edge, offset) => "top" === edge || "left" === edge ? scale[edge] + offset : scale[edge] - offset;
        function sample(arr, numItems) {
            const result = [];
            const increment = arr.length / numItems;
            const len = arr.length;
            let i = 0;
            for (;i < len; i += increment) result.push(arr[Math.floor(i)]);
            return result;
        }
        function getPixelForGridLine(scale, index, offsetGridLines) {
            const length = scale.ticks.length;
            const validIndex = Math.min(index, length - 1);
            const start = scale._startPixel;
            const end = scale._endPixel;
            const epsilon = 1e-6;
            let lineValue = scale.getPixelForTick(validIndex);
            let offset;
            if (offsetGridLines) {
                if (1 === length) offset = Math.max(lineValue - start, end - lineValue); else if (0 === index) offset = (scale.getPixelForTick(1) - lineValue) / 2; else offset = (lineValue - scale.getPixelForTick(validIndex - 1)) / 2;
                lineValue += validIndex < index ? offset : -offset;
                if (lineValue < start - epsilon || lineValue > end + epsilon) return;
            }
            return lineValue;
        }
        function garbageCollect(caches, length) {
            each(caches, (cache => {
                const gc = cache.gc;
                const gcLen = gc.length / 2;
                let i;
                if (gcLen > length) {
                    for (i = 0; i < gcLen; ++i) delete cache.data[gc[i]];
                    gc.splice(0, gcLen);
                }
            }));
        }
        function getTickMarkLength(options) {
            return options.drawTicks ? options.tickLength : 0;
        }
        function getTitleHeight(options, fallback) {
            if (!options.display) return 0;
            const font = toFont(options.font, fallback);
            const padding = toPadding(options.padding);
            const lines = isArray(options.text) ? options.text.length : 1;
            return lines * font.lineHeight + padding.height;
        }
        function createScaleContext(parent, scale) {
            return createContext(parent, {
                scale,
                type: "scale"
            });
        }
        function createTickContext(parent, index, tick) {
            return createContext(parent, {
                tick,
                index,
                type: "tick"
            });
        }
        function titleAlign(align, position, reverse) {
            let ret = _toLeftRightCenter(align);
            if (reverse && "right" !== position || !reverse && "right" === position) ret = reverseAlign(ret);
            return ret;
        }
        function titleArgs(scale, offset, position, align) {
            const {top, left, bottom, right, chart} = scale;
            const {chartArea, scales} = chart;
            let rotation = 0;
            let maxWidth, titleX, titleY;
            const height = bottom - top;
            const width = right - left;
            if (scale.isHorizontal()) {
                titleX = _alignStartEnd(align, left, right);
                if (isObject(position)) {
                    const positionAxisID = Object.keys(position)[0];
                    const value = position[positionAxisID];
                    titleY = scales[positionAxisID].getPixelForValue(value) + height - offset;
                } else if ("center" === position) titleY = (chartArea.bottom + chartArea.top) / 2 + height - offset; else titleY = offsetFromEdge(scale, position, offset);
                maxWidth = right - left;
            } else {
                if (isObject(position)) {
                    const positionAxisID = Object.keys(position)[0];
                    const value = position[positionAxisID];
                    titleX = scales[positionAxisID].getPixelForValue(value) - width + offset;
                } else if ("center" === position) titleX = (chartArea.left + chartArea.right) / 2 - width + offset; else titleX = offsetFromEdge(scale, position, offset);
                titleY = _alignStartEnd(align, bottom, top);
                rotation = "left" === position ? -HALF_PI : HALF_PI;
            }
            return {
                titleX,
                titleY,
                maxWidth,
                rotation
            };
        }
        class Scale extends chart_esm_Element {
            constructor(cfg) {
                super();
                this.id = cfg.id;
                this.type = cfg.type;
                this.options = void 0;
                this.ctx = cfg.ctx;
                this.chart = cfg.chart;
                this.top = void 0;
                this.bottom = void 0;
                this.left = void 0;
                this.right = void 0;
                this.width = void 0;
                this.height = void 0;
                this._margins = {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                };
                this.maxWidth = void 0;
                this.maxHeight = void 0;
                this.paddingTop = void 0;
                this.paddingBottom = void 0;
                this.paddingLeft = void 0;
                this.paddingRight = void 0;
                this.axis = void 0;
                this.labelRotation = void 0;
                this.min = void 0;
                this.max = void 0;
                this._range = void 0;
                this.ticks = [];
                this._gridLineItems = null;
                this._labelItems = null;
                this._labelSizes = null;
                this._length = 0;
                this._maxLength = 0;
                this._longestTextCache = {};
                this._startPixel = void 0;
                this._endPixel = void 0;
                this._reversePixels = false;
                this._userMax = void 0;
                this._userMin = void 0;
                this._suggestedMax = void 0;
                this._suggestedMin = void 0;
                this._ticksLength = 0;
                this._borderValue = 0;
                this._cache = {};
                this._dataLimitsCached = false;
                this.$context = void 0;
            }
            init(options) {
                this.options = options.setContext(this.getContext());
                this.axis = options.axis;
                this._userMin = this.parse(options.min);
                this._userMax = this.parse(options.max);
                this._suggestedMin = this.parse(options.suggestedMin);
                this._suggestedMax = this.parse(options.suggestedMax);
            }
            parse(raw, index) {
                return raw;
            }
            getUserBounds() {
                let {_userMin, _userMax, _suggestedMin, _suggestedMax} = this;
                _userMin = finiteOrDefault(_userMin, Number.POSITIVE_INFINITY);
                _userMax = finiteOrDefault(_userMax, Number.NEGATIVE_INFINITY);
                _suggestedMin = finiteOrDefault(_suggestedMin, Number.POSITIVE_INFINITY);
                _suggestedMax = finiteOrDefault(_suggestedMax, Number.NEGATIVE_INFINITY);
                return {
                    min: finiteOrDefault(_userMin, _suggestedMin),
                    max: finiteOrDefault(_userMax, _suggestedMax),
                    minDefined: isNumberFinite(_userMin),
                    maxDefined: isNumberFinite(_userMax)
                };
            }
            getMinMax(canStack) {
                let {min, max, minDefined, maxDefined} = this.getUserBounds();
                let range;
                if (minDefined && maxDefined) return {
                    min,
                    max
                };
                const metas = this.getMatchingVisibleMetas();
                for (let i = 0, ilen = metas.length; i < ilen; ++i) {
                    range = metas[i].controller.getMinMax(this, canStack);
                    if (!minDefined) min = Math.min(min, range.min);
                    if (!maxDefined) max = Math.max(max, range.max);
                }
                min = maxDefined && min > max ? max : min;
                max = minDefined && min > max ? min : max;
                return {
                    min: finiteOrDefault(min, finiteOrDefault(max, min)),
                    max: finiteOrDefault(max, finiteOrDefault(min, max))
                };
            }
            getPadding() {
                return {
                    left: this.paddingLeft || 0,
                    top: this.paddingTop || 0,
                    right: this.paddingRight || 0,
                    bottom: this.paddingBottom || 0
                };
            }
            getTicks() {
                return this.ticks;
            }
            getLabels() {
                const data = this.chart.data;
                return this.options.labels || (this.isHorizontal() ? data.xLabels : data.yLabels) || data.labels || [];
            }
            beforeLayout() {
                this._cache = {};
                this._dataLimitsCached = false;
            }
            beforeUpdate() {
                callback(this.options.beforeUpdate, [ this ]);
            }
            update(maxWidth, maxHeight, margins) {
                const {beginAtZero, grace, ticks: tickOpts} = this.options;
                const sampleSize = tickOpts.sampleSize;
                this.beforeUpdate();
                this.maxWidth = maxWidth;
                this.maxHeight = maxHeight;
                this._margins = margins = Object.assign({
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }, margins);
                this.ticks = null;
                this._labelSizes = null;
                this._gridLineItems = null;
                this._labelItems = null;
                this.beforeSetDimensions();
                this.setDimensions();
                this.afterSetDimensions();
                this._maxLength = this.isHorizontal() ? this.width + margins.left + margins.right : this.height + margins.top + margins.bottom;
                if (!this._dataLimitsCached) {
                    this.beforeDataLimits();
                    this.determineDataLimits();
                    this.afterDataLimits();
                    this._range = _addGrace(this, grace, beginAtZero);
                    this._dataLimitsCached = true;
                }
                this.beforeBuildTicks();
                this.ticks = this.buildTicks() || [];
                this.afterBuildTicks();
                const samplingEnabled = sampleSize < this.ticks.length;
                this._convertTicksToLabels(samplingEnabled ? sample(this.ticks, sampleSize) : this.ticks);
                this.configure();
                this.beforeCalculateLabelRotation();
                this.calculateLabelRotation();
                this.afterCalculateLabelRotation();
                if (tickOpts.display && (tickOpts.autoSkip || "auto" === tickOpts.source)) {
                    this.ticks = autoSkip(this, this.ticks);
                    this._labelSizes = null;
                }
                if (samplingEnabled) this._convertTicksToLabels(this.ticks);
                this.beforeFit();
                this.fit();
                this.afterFit();
                this.afterUpdate();
            }
            configure() {
                let reversePixels = this.options.reverse;
                let startPixel, endPixel;
                if (this.isHorizontal()) {
                    startPixel = this.left;
                    endPixel = this.right;
                } else {
                    startPixel = this.top;
                    endPixel = this.bottom;
                    reversePixels = !reversePixels;
                }
                this._startPixel = startPixel;
                this._endPixel = endPixel;
                this._reversePixels = reversePixels;
                this._length = endPixel - startPixel;
                this._alignToPixels = this.options.alignToPixels;
            }
            afterUpdate() {
                callback(this.options.afterUpdate, [ this ]);
            }
            beforeSetDimensions() {
                callback(this.options.beforeSetDimensions, [ this ]);
            }
            setDimensions() {
                if (this.isHorizontal()) {
                    this.width = this.maxWidth;
                    this.left = 0;
                    this.right = this.width;
                } else {
                    this.height = this.maxHeight;
                    this.top = 0;
                    this.bottom = this.height;
                }
                this.paddingLeft = 0;
                this.paddingTop = 0;
                this.paddingRight = 0;
                this.paddingBottom = 0;
            }
            afterSetDimensions() {
                callback(this.options.afterSetDimensions, [ this ]);
            }
            _callHooks(name) {
                this.chart.notifyPlugins(name, this.getContext());
                callback(this.options[name], [ this ]);
            }
            beforeDataLimits() {
                this._callHooks("beforeDataLimits");
            }
            determineDataLimits() {}
            afterDataLimits() {
                this._callHooks("afterDataLimits");
            }
            beforeBuildTicks() {
                this._callHooks("beforeBuildTicks");
            }
            buildTicks() {
                return [];
            }
            afterBuildTicks() {
                this._callHooks("afterBuildTicks");
            }
            beforeTickToLabelConversion() {
                callback(this.options.beforeTickToLabelConversion, [ this ]);
            }
            generateTickLabels(ticks) {
                const tickOpts = this.options.ticks;
                let i, ilen, tick;
                for (i = 0, ilen = ticks.length; i < ilen; i++) {
                    tick = ticks[i];
                    tick.label = callback(tickOpts.callback, [ tick.value, i, ticks ], this);
                }
            }
            afterTickToLabelConversion() {
                callback(this.options.afterTickToLabelConversion, [ this ]);
            }
            beforeCalculateLabelRotation() {
                callback(this.options.beforeCalculateLabelRotation, [ this ]);
            }
            calculateLabelRotation() {
                const options = this.options;
                const tickOpts = options.ticks;
                const numTicks = this.ticks.length;
                const minRotation = tickOpts.minRotation || 0;
                const maxRotation = tickOpts.maxRotation;
                let labelRotation = minRotation;
                let tickWidth, maxHeight, maxLabelDiagonal;
                if (!this._isVisible() || !tickOpts.display || minRotation >= maxRotation || numTicks <= 1 || !this.isHorizontal()) {
                    this.labelRotation = minRotation;
                    return;
                }
                const labelSizes = this._getLabelSizes();
                const maxLabelWidth = labelSizes.widest.width;
                const maxLabelHeight = labelSizes.highest.height;
                const maxWidth = _limitValue(this.chart.width - maxLabelWidth, 0, this.maxWidth);
                tickWidth = options.offset ? this.maxWidth / numTicks : maxWidth / (numTicks - 1);
                if (maxLabelWidth + 6 > tickWidth) {
                    tickWidth = maxWidth / (numTicks - (options.offset ? .5 : 1));
                    maxHeight = this.maxHeight - getTickMarkLength(options.grid) - tickOpts.padding - getTitleHeight(options.title, this.chart.options.font);
                    maxLabelDiagonal = Math.sqrt(maxLabelWidth * maxLabelWidth + maxLabelHeight * maxLabelHeight);
                    labelRotation = toDegrees(Math.min(Math.asin(_limitValue((labelSizes.highest.height + 6) / tickWidth, -1, 1)), Math.asin(_limitValue(maxHeight / maxLabelDiagonal, -1, 1)) - Math.asin(_limitValue(maxLabelHeight / maxLabelDiagonal, -1, 1))));
                    labelRotation = Math.max(minRotation, Math.min(maxRotation, labelRotation));
                }
                this.labelRotation = labelRotation;
            }
            afterCalculateLabelRotation() {
                callback(this.options.afterCalculateLabelRotation, [ this ]);
            }
            beforeFit() {
                callback(this.options.beforeFit, [ this ]);
            }
            fit() {
                const minSize = {
                    width: 0,
                    height: 0
                };
                const {chart, options: {ticks: tickOpts, title: titleOpts, grid: gridOpts}} = this;
                const display = this._isVisible();
                const isHorizontal = this.isHorizontal();
                if (display) {
                    const titleHeight = getTitleHeight(titleOpts, chart.options.font);
                    if (isHorizontal) {
                        minSize.width = this.maxWidth;
                        minSize.height = getTickMarkLength(gridOpts) + titleHeight;
                    } else {
                        minSize.height = this.maxHeight;
                        minSize.width = getTickMarkLength(gridOpts) + titleHeight;
                    }
                    if (tickOpts.display && this.ticks.length) {
                        const {first, last, widest, highest} = this._getLabelSizes();
                        const tickPadding = 2 * tickOpts.padding;
                        const angleRadians = toRadians(this.labelRotation);
                        const cos = Math.cos(angleRadians);
                        const sin = Math.sin(angleRadians);
                        if (isHorizontal) {
                            const labelHeight = tickOpts.mirror ? 0 : sin * widest.width + cos * highest.height;
                            minSize.height = Math.min(this.maxHeight, minSize.height + labelHeight + tickPadding);
                        } else {
                            const labelWidth = tickOpts.mirror ? 0 : cos * widest.width + sin * highest.height;
                            minSize.width = Math.min(this.maxWidth, minSize.width + labelWidth + tickPadding);
                        }
                        this._calculatePadding(first, last, sin, cos);
                    }
                }
                this._handleMargins();
                if (isHorizontal) {
                    this.width = this._length = chart.width - this._margins.left - this._margins.right;
                    this.height = minSize.height;
                } else {
                    this.width = minSize.width;
                    this.height = this._length = chart.height - this._margins.top - this._margins.bottom;
                }
            }
            _calculatePadding(first, last, sin, cos) {
                const {ticks: {align, padding}, position} = this.options;
                const isRotated = 0 !== this.labelRotation;
                const labelsBelowTicks = "top" !== position && "x" === this.axis;
                if (this.isHorizontal()) {
                    const offsetLeft = this.getPixelForTick(0) - this.left;
                    const offsetRight = this.right - this.getPixelForTick(this.ticks.length - 1);
                    let paddingLeft = 0;
                    let paddingRight = 0;
                    if (isRotated) if (labelsBelowTicks) {
                        paddingLeft = cos * first.width;
                        paddingRight = sin * last.height;
                    } else {
                        paddingLeft = sin * first.height;
                        paddingRight = cos * last.width;
                    } else if ("start" === align) paddingRight = last.width; else if ("end" === align) paddingLeft = first.width; else {
                        paddingLeft = first.width / 2;
                        paddingRight = last.width / 2;
                    }
                    this.paddingLeft = Math.max((paddingLeft - offsetLeft + padding) * this.width / (this.width - offsetLeft), 0);
                    this.paddingRight = Math.max((paddingRight - offsetRight + padding) * this.width / (this.width - offsetRight), 0);
                } else {
                    let paddingTop = last.height / 2;
                    let paddingBottom = first.height / 2;
                    if ("start" === align) {
                        paddingTop = 0;
                        paddingBottom = first.height;
                    } else if ("end" === align) {
                        paddingTop = last.height;
                        paddingBottom = 0;
                    }
                    this.paddingTop = paddingTop + padding;
                    this.paddingBottom = paddingBottom + padding;
                }
            }
            _handleMargins() {
                if (this._margins) {
                    this._margins.left = Math.max(this.paddingLeft, this._margins.left);
                    this._margins.top = Math.max(this.paddingTop, this._margins.top);
                    this._margins.right = Math.max(this.paddingRight, this._margins.right);
                    this._margins.bottom = Math.max(this.paddingBottom, this._margins.bottom);
                }
            }
            afterFit() {
                callback(this.options.afterFit, [ this ]);
            }
            isHorizontal() {
                const {axis, position} = this.options;
                return "top" === position || "bottom" === position || "x" === axis;
            }
            isFullSize() {
                return this.options.fullSize;
            }
            _convertTicksToLabels(ticks) {
                this.beforeTickToLabelConversion();
                this.generateTickLabels(ticks);
                let i, ilen;
                for (i = 0, ilen = ticks.length; i < ilen; i++) if (isNullOrUndef(ticks[i].label)) {
                    ticks.splice(i, 1);
                    ilen--;
                    i--;
                }
                this.afterTickToLabelConversion();
            }
            _getLabelSizes() {
                let labelSizes = this._labelSizes;
                if (!labelSizes) {
                    const sampleSize = this.options.ticks.sampleSize;
                    let ticks = this.ticks;
                    if (sampleSize < ticks.length) ticks = sample(ticks, sampleSize);
                    this._labelSizes = labelSizes = this._computeLabelSizes(ticks, ticks.length);
                }
                return labelSizes;
            }
            _computeLabelSizes(ticks, length) {
                const {ctx, _longestTextCache: caches} = this;
                const widths = [];
                const heights = [];
                let widestLabelSize = 0;
                let highestLabelSize = 0;
                let i, j, jlen, label, tickFont, fontString, cache, lineHeight, width, height, nestedLabel;
                for (i = 0; i < length; ++i) {
                    label = ticks[i].label;
                    tickFont = this._resolveTickFontOptions(i);
                    ctx.font = fontString = tickFont.string;
                    cache = caches[fontString] = caches[fontString] || {
                        data: {},
                        gc: []
                    };
                    lineHeight = tickFont.lineHeight;
                    width = height = 0;
                    if (!isNullOrUndef(label) && !isArray(label)) {
                        width = _measureText(ctx, cache.data, cache.gc, width, label);
                        height = lineHeight;
                    } else if (isArray(label)) for (j = 0, jlen = label.length; j < jlen; ++j) {
                        nestedLabel = label[j];
                        if (!isNullOrUndef(nestedLabel) && !isArray(nestedLabel)) {
                            width = _measureText(ctx, cache.data, cache.gc, width, nestedLabel);
                            height += lineHeight;
                        }
                    }
                    widths.push(width);
                    heights.push(height);
                    widestLabelSize = Math.max(width, widestLabelSize);
                    highestLabelSize = Math.max(height, highestLabelSize);
                }
                garbageCollect(caches, length);
                const widest = widths.indexOf(widestLabelSize);
                const highest = heights.indexOf(highestLabelSize);
                const valueAt = idx => ({
                    width: widths[idx] || 0,
                    height: heights[idx] || 0
                });
                return {
                    first: valueAt(0),
                    last: valueAt(length - 1),
                    widest: valueAt(widest),
                    highest: valueAt(highest),
                    widths,
                    heights
                };
            }
            getLabelForValue(value) {
                return value;
            }
            getPixelForValue(value, index) {
                return NaN;
            }
            getValueForPixel(pixel) {}
            getPixelForTick(index) {
                const ticks = this.ticks;
                if (index < 0 || index > ticks.length - 1) return null;
                return this.getPixelForValue(ticks[index].value);
            }
            getPixelForDecimal(decimal) {
                if (this._reversePixels) decimal = 1 - decimal;
                const pixel = this._startPixel + decimal * this._length;
                return _int16Range(this._alignToPixels ? _alignPixel(this.chart, pixel, 0) : pixel);
            }
            getDecimalForPixel(pixel) {
                const decimal = (pixel - this._startPixel) / this._length;
                return this._reversePixels ? 1 - decimal : decimal;
            }
            getBasePixel() {
                return this.getPixelForValue(this.getBaseValue());
            }
            getBaseValue() {
                const {min, max} = this;
                return min < 0 && max < 0 ? max : min > 0 && max > 0 ? min : 0;
            }
            getContext(index) {
                const ticks = this.ticks || [];
                if (index >= 0 && index < ticks.length) {
                    const tick = ticks[index];
                    return tick.$context || (tick.$context = createTickContext(this.getContext(), index, tick));
                }
                return this.$context || (this.$context = createScaleContext(this.chart.getContext(), this));
            }
            _tickSize() {
                const optionTicks = this.options.ticks;
                const rot = toRadians(this.labelRotation);
                const cos = Math.abs(Math.cos(rot));
                const sin = Math.abs(Math.sin(rot));
                const labelSizes = this._getLabelSizes();
                const padding = optionTicks.autoSkipPadding || 0;
                const w = labelSizes ? labelSizes.widest.width + padding : 0;
                const h = labelSizes ? labelSizes.highest.height + padding : 0;
                return this.isHorizontal() ? h * cos > w * sin ? w / cos : h / sin : h * sin < w * cos ? h / cos : w / sin;
            }
            _isVisible() {
                const display = this.options.display;
                if ("auto" !== display) return !!display;
                return this.getMatchingVisibleMetas().length > 0;
            }
            _computeGridLineItems(chartArea) {
                const axis = this.axis;
                const chart = this.chart;
                const options = this.options;
                const {grid, position} = options;
                const offset = grid.offset;
                const isHorizontal = this.isHorizontal();
                const ticks = this.ticks;
                const ticksLength = ticks.length + (offset ? 1 : 0);
                const tl = getTickMarkLength(grid);
                const items = [];
                const borderOpts = grid.setContext(this.getContext());
                const axisWidth = borderOpts.drawBorder ? borderOpts.borderWidth : 0;
                const axisHalfWidth = axisWidth / 2;
                const alignBorderValue = function(pixel) {
                    return _alignPixel(chart, pixel, axisWidth);
                };
                let borderValue, i, lineValue, alignedLineValue;
                let tx1, ty1, tx2, ty2, x1, y1, x2, y2;
                if ("top" === position) {
                    borderValue = alignBorderValue(this.bottom);
                    ty1 = this.bottom - tl;
                    ty2 = borderValue - axisHalfWidth;
                    y1 = alignBorderValue(chartArea.top) + axisHalfWidth;
                    y2 = chartArea.bottom;
                } else if ("bottom" === position) {
                    borderValue = alignBorderValue(this.top);
                    y1 = chartArea.top;
                    y2 = alignBorderValue(chartArea.bottom) - axisHalfWidth;
                    ty1 = borderValue + axisHalfWidth;
                    ty2 = this.top + tl;
                } else if ("left" === position) {
                    borderValue = alignBorderValue(this.right);
                    tx1 = this.right - tl;
                    tx2 = borderValue - axisHalfWidth;
                    x1 = alignBorderValue(chartArea.left) + axisHalfWidth;
                    x2 = chartArea.right;
                } else if ("right" === position) {
                    borderValue = alignBorderValue(this.left);
                    x1 = chartArea.left;
                    x2 = alignBorderValue(chartArea.right) - axisHalfWidth;
                    tx1 = borderValue + axisHalfWidth;
                    tx2 = this.left + tl;
                } else if ("x" === axis) {
                    if ("center" === position) borderValue = alignBorderValue((chartArea.top + chartArea.bottom) / 2 + .5); else if (isObject(position)) {
                        const positionAxisID = Object.keys(position)[0];
                        const value = position[positionAxisID];
                        borderValue = alignBorderValue(this.chart.scales[positionAxisID].getPixelForValue(value));
                    }
                    y1 = chartArea.top;
                    y2 = chartArea.bottom;
                    ty1 = borderValue + axisHalfWidth;
                    ty2 = ty1 + tl;
                } else if ("y" === axis) {
                    if ("center" === position) borderValue = alignBorderValue((chartArea.left + chartArea.right) / 2); else if (isObject(position)) {
                        const positionAxisID = Object.keys(position)[0];
                        const value = position[positionAxisID];
                        borderValue = alignBorderValue(this.chart.scales[positionAxisID].getPixelForValue(value));
                    }
                    tx1 = borderValue - axisHalfWidth;
                    tx2 = tx1 - tl;
                    x1 = chartArea.left;
                    x2 = chartArea.right;
                }
                const limit = valueOrDefault(options.ticks.maxTicksLimit, ticksLength);
                const step = Math.max(1, Math.ceil(ticksLength / limit));
                for (i = 0; i < ticksLength; i += step) {
                    const optsAtIndex = grid.setContext(this.getContext(i));
                    const lineWidth = optsAtIndex.lineWidth;
                    const lineColor = optsAtIndex.color;
                    const borderDash = grid.borderDash || [];
                    const borderDashOffset = optsAtIndex.borderDashOffset;
                    const tickWidth = optsAtIndex.tickWidth;
                    const tickColor = optsAtIndex.tickColor;
                    const tickBorderDash = optsAtIndex.tickBorderDash || [];
                    const tickBorderDashOffset = optsAtIndex.tickBorderDashOffset;
                    lineValue = getPixelForGridLine(this, i, offset);
                    if (void 0 === lineValue) continue;
                    alignedLineValue = _alignPixel(chart, lineValue, lineWidth);
                    if (isHorizontal) tx1 = tx2 = x1 = x2 = alignedLineValue; else ty1 = ty2 = y1 = y2 = alignedLineValue;
                    items.push({
                        tx1,
                        ty1,
                        tx2,
                        ty2,
                        x1,
                        y1,
                        x2,
                        y2,
                        width: lineWidth,
                        color: lineColor,
                        borderDash,
                        borderDashOffset,
                        tickWidth,
                        tickColor,
                        tickBorderDash,
                        tickBorderDashOffset
                    });
                }
                this._ticksLength = ticksLength;
                this._borderValue = borderValue;
                return items;
            }
            _computeLabelItems(chartArea) {
                const axis = this.axis;
                const options = this.options;
                const {position, ticks: optionTicks} = options;
                const isHorizontal = this.isHorizontal();
                const ticks = this.ticks;
                const {align, crossAlign, padding, mirror} = optionTicks;
                const tl = getTickMarkLength(options.grid);
                const tickAndPadding = tl + padding;
                const hTickAndPadding = mirror ? -padding : tickAndPadding;
                const rotation = -toRadians(this.labelRotation);
                const items = [];
                let i, ilen, tick, label, x, y, textAlign, pixel, font, lineHeight, lineCount, textOffset;
                let textBaseline = "middle";
                if ("top" === position) {
                    y = this.bottom - hTickAndPadding;
                    textAlign = this._getXAxisLabelAlignment();
                } else if ("bottom" === position) {
                    y = this.top + hTickAndPadding;
                    textAlign = this._getXAxisLabelAlignment();
                } else if ("left" === position) {
                    const ret = this._getYAxisLabelAlignment(tl);
                    textAlign = ret.textAlign;
                    x = ret.x;
                } else if ("right" === position) {
                    const ret = this._getYAxisLabelAlignment(tl);
                    textAlign = ret.textAlign;
                    x = ret.x;
                } else if ("x" === axis) {
                    if ("center" === position) y = (chartArea.top + chartArea.bottom) / 2 + tickAndPadding; else if (isObject(position)) {
                        const positionAxisID = Object.keys(position)[0];
                        const value = position[positionAxisID];
                        y = this.chart.scales[positionAxisID].getPixelForValue(value) + tickAndPadding;
                    }
                    textAlign = this._getXAxisLabelAlignment();
                } else if ("y" === axis) {
                    if ("center" === position) x = (chartArea.left + chartArea.right) / 2 - tickAndPadding; else if (isObject(position)) {
                        const positionAxisID = Object.keys(position)[0];
                        const value = position[positionAxisID];
                        x = this.chart.scales[positionAxisID].getPixelForValue(value);
                    }
                    textAlign = this._getYAxisLabelAlignment(tl).textAlign;
                }
                if ("y" === axis) if ("start" === align) textBaseline = "top"; else if ("end" === align) textBaseline = "bottom";
                const labelSizes = this._getLabelSizes();
                for (i = 0, ilen = ticks.length; i < ilen; ++i) {
                    tick = ticks[i];
                    label = tick.label;
                    const optsAtIndex = optionTicks.setContext(this.getContext(i));
                    pixel = this.getPixelForTick(i) + optionTicks.labelOffset;
                    font = this._resolveTickFontOptions(i);
                    lineHeight = font.lineHeight;
                    lineCount = isArray(label) ? label.length : 1;
                    const halfCount = lineCount / 2;
                    const color = optsAtIndex.color;
                    const strokeColor = optsAtIndex.textStrokeColor;
                    const strokeWidth = optsAtIndex.textStrokeWidth;
                    if (isHorizontal) {
                        x = pixel;
                        if ("top" === position) if ("near" === crossAlign || 0 !== rotation) textOffset = -lineCount * lineHeight + lineHeight / 2; else if ("center" === crossAlign) textOffset = -labelSizes.highest.height / 2 - halfCount * lineHeight + lineHeight; else textOffset = -labelSizes.highest.height + lineHeight / 2; else if ("near" === crossAlign || 0 !== rotation) textOffset = lineHeight / 2; else if ("center" === crossAlign) textOffset = labelSizes.highest.height / 2 - halfCount * lineHeight; else textOffset = labelSizes.highest.height - lineCount * lineHeight;
                        if (mirror) textOffset *= -1;
                    } else {
                        y = pixel;
                        textOffset = (1 - lineCount) * lineHeight / 2;
                    }
                    let backdrop;
                    if (optsAtIndex.showLabelBackdrop) {
                        const labelPadding = toPadding(optsAtIndex.backdropPadding);
                        const height = labelSizes.heights[i];
                        const width = labelSizes.widths[i];
                        let top = y + textOffset - labelPadding.top;
                        let left = x - labelPadding.left;
                        switch (textBaseline) {
                          case "middle":
                            top -= height / 2;
                            break;

                          case "bottom":
                            top -= height;
                            break;
                        }
                        switch (textAlign) {
                          case "center":
                            left -= width / 2;
                            break;

                          case "right":
                            left -= width;
                            break;
                        }
                        backdrop = {
                            left,
                            top,
                            width: width + labelPadding.width,
                            height: height + labelPadding.height,
                            color: optsAtIndex.backdropColor
                        };
                    }
                    items.push({
                        rotation,
                        label,
                        font,
                        color,
                        strokeColor,
                        strokeWidth,
                        textOffset,
                        textAlign,
                        textBaseline,
                        translation: [ x, y ],
                        backdrop
                    });
                }
                return items;
            }
            _getXAxisLabelAlignment() {
                const {position, ticks} = this.options;
                const rotation = -toRadians(this.labelRotation);
                if (rotation) return "top" === position ? "left" : "right";
                let align = "center";
                if ("start" === ticks.align) align = "left"; else if ("end" === ticks.align) align = "right";
                return align;
            }
            _getYAxisLabelAlignment(tl) {
                const {position, ticks: {crossAlign, mirror, padding}} = this.options;
                const labelSizes = this._getLabelSizes();
                const tickAndPadding = tl + padding;
                const widest = labelSizes.widest.width;
                let textAlign;
                let x;
                if ("left" === position) if (mirror) {
                    x = this.right + padding;
                    if ("near" === crossAlign) textAlign = "left"; else if ("center" === crossAlign) {
                        textAlign = "center";
                        x += widest / 2;
                    } else {
                        textAlign = "right";
                        x += widest;
                    }
                } else {
                    x = this.right - tickAndPadding;
                    if ("near" === crossAlign) textAlign = "right"; else if ("center" === crossAlign) {
                        textAlign = "center";
                        x -= widest / 2;
                    } else {
                        textAlign = "left";
                        x = this.left;
                    }
                } else if ("right" === position) if (mirror) {
                    x = this.left + padding;
                    if ("near" === crossAlign) textAlign = "right"; else if ("center" === crossAlign) {
                        textAlign = "center";
                        x -= widest / 2;
                    } else {
                        textAlign = "left";
                        x -= widest;
                    }
                } else {
                    x = this.left + tickAndPadding;
                    if ("near" === crossAlign) textAlign = "left"; else if ("center" === crossAlign) {
                        textAlign = "center";
                        x += widest / 2;
                    } else {
                        textAlign = "right";
                        x = this.right;
                    }
                } else textAlign = "right";
                return {
                    textAlign,
                    x
                };
            }
            _computeLabelArea() {
                if (this.options.ticks.mirror) return;
                const chart = this.chart;
                const position = this.options.position;
                if ("left" === position || "right" === position) return {
                    top: 0,
                    left: this.left,
                    bottom: chart.height,
                    right: this.right
                };
                if ("top" === position || "bottom" === position) return {
                    top: this.top,
                    left: 0,
                    bottom: this.bottom,
                    right: chart.width
                };
            }
            drawBackground() {
                const {ctx, options: {backgroundColor}, left, top, width, height} = this;
                if (backgroundColor) {
                    ctx.save();
                    ctx.fillStyle = backgroundColor;
                    ctx.fillRect(left, top, width, height);
                    ctx.restore();
                }
            }
            getLineWidthForValue(value) {
                const grid = this.options.grid;
                if (!this._isVisible() || !grid.display) return 0;
                const ticks = this.ticks;
                const index = ticks.findIndex((t => t.value === value));
                if (index >= 0) {
                    const opts = grid.setContext(this.getContext(index));
                    return opts.lineWidth;
                }
                return 0;
            }
            drawGrid(chartArea) {
                const grid = this.options.grid;
                const ctx = this.ctx;
                const items = this._gridLineItems || (this._gridLineItems = this._computeGridLineItems(chartArea));
                let i, ilen;
                const drawLine = (p1, p2, style) => {
                    if (!style.width || !style.color) return;
                    ctx.save();
                    ctx.lineWidth = style.width;
                    ctx.strokeStyle = style.color;
                    ctx.setLineDash(style.borderDash || []);
                    ctx.lineDashOffset = style.borderDashOffset;
                    ctx.beginPath();
                    ctx.moveTo(p1.x, p1.y);
                    ctx.lineTo(p2.x, p2.y);
                    ctx.stroke();
                    ctx.restore();
                };
                if (grid.display) for (i = 0, ilen = items.length; i < ilen; ++i) {
                    const item = items[i];
                    if (grid.drawOnChartArea) drawLine({
                        x: item.x1,
                        y: item.y1
                    }, {
                        x: item.x2,
                        y: item.y2
                    }, item);
                    if (grid.drawTicks) drawLine({
                        x: item.tx1,
                        y: item.ty1
                    }, {
                        x: item.tx2,
                        y: item.ty2
                    }, {
                        color: item.tickColor,
                        width: item.tickWidth,
                        borderDash: item.tickBorderDash,
                        borderDashOffset: item.tickBorderDashOffset
                    });
                }
            }
            drawBorder() {
                const {chart, ctx, options: {grid}} = this;
                const borderOpts = grid.setContext(this.getContext());
                const axisWidth = grid.drawBorder ? borderOpts.borderWidth : 0;
                if (!axisWidth) return;
                const lastLineWidth = grid.setContext(this.getContext(0)).lineWidth;
                const borderValue = this._borderValue;
                let x1, x2, y1, y2;
                if (this.isHorizontal()) {
                    x1 = _alignPixel(chart, this.left, axisWidth) - axisWidth / 2;
                    x2 = _alignPixel(chart, this.right, lastLineWidth) + lastLineWidth / 2;
                    y1 = y2 = borderValue;
                } else {
                    y1 = _alignPixel(chart, this.top, axisWidth) - axisWidth / 2;
                    y2 = _alignPixel(chart, this.bottom, lastLineWidth) + lastLineWidth / 2;
                    x1 = x2 = borderValue;
                }
                ctx.save();
                ctx.lineWidth = borderOpts.borderWidth;
                ctx.strokeStyle = borderOpts.borderColor;
                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x2, y2);
                ctx.stroke();
                ctx.restore();
            }
            drawLabels(chartArea) {
                const optionTicks = this.options.ticks;
                if (!optionTicks.display) return;
                const ctx = this.ctx;
                const area = this._computeLabelArea();
                if (area) clipArea(ctx, area);
                const items = this._labelItems || (this._labelItems = this._computeLabelItems(chartArea));
                let i, ilen;
                for (i = 0, ilen = items.length; i < ilen; ++i) {
                    const item = items[i];
                    const tickFont = item.font;
                    const label = item.label;
                    if (item.backdrop) {
                        ctx.fillStyle = item.backdrop.color;
                        ctx.fillRect(item.backdrop.left, item.backdrop.top, item.backdrop.width, item.backdrop.height);
                    }
                    let y = item.textOffset;
                    renderText(ctx, label, 0, y, tickFont, item);
                }
                if (area) unclipArea(ctx);
            }
            drawTitle() {
                const {ctx, options: {position, title, reverse}} = this;
                if (!title.display) return;
                const font = toFont(title.font);
                const padding = toPadding(title.padding);
                const align = title.align;
                let offset = font.lineHeight / 2;
                if ("bottom" === position || "center" === position || isObject(position)) {
                    offset += padding.bottom;
                    if (isArray(title.text)) offset += font.lineHeight * (title.text.length - 1);
                } else offset += padding.top;
                const {titleX, titleY, maxWidth, rotation} = titleArgs(this, offset, position, align);
                renderText(ctx, title.text, 0, 0, font, {
                    color: title.color,
                    maxWidth,
                    rotation,
                    textAlign: titleAlign(align, position, reverse),
                    textBaseline: "middle",
                    translation: [ titleX, titleY ]
                });
            }
            draw(chartArea) {
                if (!this._isVisible()) return;
                this.drawBackground();
                this.drawGrid(chartArea);
                this.drawBorder();
                this.drawTitle();
                this.drawLabels(chartArea);
            }
            _layers() {
                const opts = this.options;
                const tz = opts.ticks && opts.ticks.z || 0;
                const gz = valueOrDefault(opts.grid && opts.grid.z, -1);
                if (!this._isVisible() || this.draw !== Scale.prototype.draw) return [ {
                    z: tz,
                    draw: chartArea => {
                        this.draw(chartArea);
                    }
                } ];
                return [ {
                    z: gz,
                    draw: chartArea => {
                        this.drawBackground();
                        this.drawGrid(chartArea);
                        this.drawTitle();
                    }
                }, {
                    z: gz + 1,
                    draw: () => {
                        this.drawBorder();
                    }
                }, {
                    z: tz,
                    draw: chartArea => {
                        this.drawLabels(chartArea);
                    }
                } ];
            }
            getMatchingVisibleMetas(type) {
                const metas = this.chart.getSortedVisibleDatasetMetas();
                const axisID = this.axis + "AxisID";
                const result = [];
                let i, ilen;
                for (i = 0, ilen = metas.length; i < ilen; ++i) {
                    const meta = metas[i];
                    if (meta[axisID] === this.id && (!type || meta.type === type)) result.push(meta);
                }
                return result;
            }
            _resolveTickFontOptions(index) {
                const opts = this.options.ticks.setContext(this.getContext(index));
                return toFont(opts.font);
            }
            _maxDigits() {
                const fontSize = this._resolveTickFontOptions(0).lineHeight;
                return (this.isHorizontal() ? this.width : this.height) / fontSize;
            }
        }
        class TypedRegistry {
            constructor(type, scope, override) {
                this.type = type;
                this.scope = scope;
                this.override = override;
                this.items = Object.create(null);
            }
            isForType(type) {
                return Object.prototype.isPrototypeOf.call(this.type.prototype, type.prototype);
            }
            register(item) {
                const proto = Object.getPrototypeOf(item);
                let parentScope;
                if (isIChartComponent(proto)) parentScope = this.register(proto);
                const items = this.items;
                const id = item.id;
                const scope = this.scope + "." + id;
                if (!id) throw new Error("class does not have id: " + item);
                if (id in items) return scope;
                items[id] = item;
                registerDefaults(item, scope, parentScope);
                if (this.override) defaults.override(item.id, item.overrides);
                return scope;
            }
            get(id) {
                return this.items[id];
            }
            unregister(item) {
                const items = this.items;
                const id = item.id;
                const scope = this.scope;
                if (id in items) delete items[id];
                if (scope && id in defaults[scope]) {
                    delete defaults[scope][id];
                    if (this.override) delete overrides[id];
                }
            }
        }
        function registerDefaults(item, scope, parentScope) {
            const itemDefaults = merge(Object.create(null), [ parentScope ? defaults.get(parentScope) : {}, defaults.get(scope), item.defaults ]);
            defaults.set(scope, itemDefaults);
            if (item.defaultRoutes) routeDefaults(scope, item.defaultRoutes);
            if (item.descriptors) defaults.describe(scope, item.descriptors);
        }
        function routeDefaults(scope, routes) {
            Object.keys(routes).forEach((property => {
                const propertyParts = property.split(".");
                const sourceName = propertyParts.pop();
                const sourceScope = [ scope ].concat(propertyParts).join(".");
                const parts = routes[property].split(".");
                const targetName = parts.pop();
                const targetScope = parts.join(".");
                defaults.route(sourceScope, sourceName, targetScope, targetName);
            }));
        }
        function isIChartComponent(proto) {
            return "id" in proto && "defaults" in proto;
        }
        class Registry {
            constructor() {
                this.controllers = new TypedRegistry(DatasetController, "datasets", true);
                this.elements = new TypedRegistry(chart_esm_Element, "elements");
                this.plugins = new TypedRegistry(Object, "plugins");
                this.scales = new TypedRegistry(Scale, "scales");
                this._typedRegistries = [ this.controllers, this.scales, this.elements ];
            }
            add(...args) {
                this._each("register", args);
            }
            remove(...args) {
                this._each("unregister", args);
            }
            addControllers(...args) {
                this._each("register", args, this.controllers);
            }
            addElements(...args) {
                this._each("register", args, this.elements);
            }
            addPlugins(...args) {
                this._each("register", args, this.plugins);
            }
            addScales(...args) {
                this._each("register", args, this.scales);
            }
            getController(id) {
                return this._get(id, this.controllers, "controller");
            }
            getElement(id) {
                return this._get(id, this.elements, "element");
            }
            getPlugin(id) {
                return this._get(id, this.plugins, "plugin");
            }
            getScale(id) {
                return this._get(id, this.scales, "scale");
            }
            removeControllers(...args) {
                this._each("unregister", args, this.controllers);
            }
            removeElements(...args) {
                this._each("unregister", args, this.elements);
            }
            removePlugins(...args) {
                this._each("unregister", args, this.plugins);
            }
            removeScales(...args) {
                this._each("unregister", args, this.scales);
            }
            _each(method, args, typedRegistry) {
                [ ...args ].forEach((arg => {
                    const reg = typedRegistry || this._getRegistryForType(arg);
                    if (typedRegistry || reg.isForType(arg) || reg === this.plugins && arg.id) this._exec(method, reg, arg); else each(arg, (item => {
                        const itemReg = typedRegistry || this._getRegistryForType(item);
                        this._exec(method, itemReg, item);
                    }));
                }));
            }
            _exec(method, registry, component) {
                const camelMethod = _capitalize(method);
                callback(component["before" + camelMethod], [], component);
                registry[method](component);
                callback(component["after" + camelMethod], [], component);
            }
            _getRegistryForType(type) {
                for (let i = 0; i < this._typedRegistries.length; i++) {
                    const reg = this._typedRegistries[i];
                    if (reg.isForType(type)) return reg;
                }
                return this.plugins;
            }
            _get(id, typedRegistry, type) {
                const item = typedRegistry.get(id);
                if (void 0 === item) throw new Error('"' + id + '" is not a registered ' + type + ".");
                return item;
            }
        }
        var registry = new Registry;
        class PluginService {
            constructor() {
                this._init = [];
            }
            notify(chart, hook, args, filter) {
                if ("beforeInit" === hook) {
                    this._init = this._createDescriptors(chart, true);
                    this._notify(this._init, chart, "install");
                }
                const descriptors = filter ? this._descriptors(chart).filter(filter) : this._descriptors(chart);
                const result = this._notify(descriptors, chart, hook, args);
                if ("afterDestroy" === hook) {
                    this._notify(descriptors, chart, "stop");
                    this._notify(this._init, chart, "uninstall");
                }
                return result;
            }
            _notify(descriptors, chart, hook, args) {
                args = args || {};
                for (const descriptor of descriptors) {
                    const plugin = descriptor.plugin;
                    const method = plugin[hook];
                    const params = [ chart, args, descriptor.options ];
                    if (false === callback(method, params, plugin) && args.cancelable) return false;
                }
                return true;
            }
            invalidate() {
                if (!isNullOrUndef(this._cache)) {
                    this._oldCache = this._cache;
                    this._cache = void 0;
                }
            }
            _descriptors(chart) {
                if (this._cache) return this._cache;
                const descriptors = this._cache = this._createDescriptors(chart);
                this._notifyStateChanges(chart);
                return descriptors;
            }
            _createDescriptors(chart, all) {
                const config = chart && chart.config;
                const options = valueOrDefault(config.options && config.options.plugins, {});
                const plugins = allPlugins(config);
                return false === options && !all ? [] : createDescriptors(chart, plugins, options, all);
            }
            _notifyStateChanges(chart) {
                const previousDescriptors = this._oldCache || [];
                const descriptors = this._cache;
                const diff = (a, b) => a.filter((x => !b.some((y => x.plugin.id === y.plugin.id))));
                this._notify(diff(previousDescriptors, descriptors), chart, "stop");
                this._notify(diff(descriptors, previousDescriptors), chart, "start");
            }
        }
        function allPlugins(config) {
            const plugins = [];
            const keys = Object.keys(registry.plugins.items);
            for (let i = 0; i < keys.length; i++) plugins.push(registry.getPlugin(keys[i]));
            const local = config.plugins || [];
            for (let i = 0; i < local.length; i++) {
                const plugin = local[i];
                if (-1 === plugins.indexOf(plugin)) plugins.push(plugin);
            }
            return plugins;
        }
        function getOpts(options, all) {
            if (!all && false === options) return null;
            if (true === options) return {};
            return options;
        }
        function createDescriptors(chart, plugins, options, all) {
            const result = [];
            const context = chart.getContext();
            for (let i = 0; i < plugins.length; i++) {
                const plugin = plugins[i];
                const id = plugin.id;
                const opts = getOpts(options[id], all);
                if (null === opts) continue;
                result.push({
                    plugin,
                    options: pluginOpts(chart.config, plugin, opts, context)
                });
            }
            return result;
        }
        function pluginOpts(config, plugin, opts, context) {
            const keys = config.pluginScopeKeys(plugin);
            const scopes = config.getOptionScopes(opts, keys);
            return config.createResolver(scopes, context, [ "" ], {
                scriptable: false,
                indexable: false,
                allKeys: true
            });
        }
        function getIndexAxis(type, options) {
            const datasetDefaults = defaults.datasets[type] || {};
            const datasetOptions = (options.datasets || {})[type] || {};
            return datasetOptions.indexAxis || options.indexAxis || datasetDefaults.indexAxis || "x";
        }
        function getAxisFromDefaultScaleID(id, indexAxis) {
            let axis = id;
            if ("_index_" === id) axis = indexAxis; else if ("_value_" === id) axis = "x" === indexAxis ? "y" : "x";
            return axis;
        }
        function getDefaultScaleIDFromAxis(axis, indexAxis) {
            return axis === indexAxis ? "_index_" : "_value_";
        }
        function axisFromPosition(position) {
            if ("top" === position || "bottom" === position) return "x";
            if ("left" === position || "right" === position) return "y";
        }
        function determineAxis(id, scaleOptions) {
            if ("x" === id || "y" === id) return id;
            return scaleOptions.axis || axisFromPosition(scaleOptions.position) || id.charAt(0).toLowerCase();
        }
        function mergeScaleConfig(config, options) {
            const chartDefaults = overrides[config.type] || {
                scales: {}
            };
            const configScales = options.scales || {};
            const chartIndexAxis = getIndexAxis(config.type, options);
            const firstIDs = Object.create(null);
            const scales = Object.create(null);
            Object.keys(configScales).forEach((id => {
                const scaleConf = configScales[id];
                if (!isObject(scaleConf)) return console.error(`Invalid scale configuration for scale: ${id}`);
                if (scaleConf._proxy) return console.warn(`Ignoring resolver passed as options for scale: ${id}`);
                const axis = determineAxis(id, scaleConf);
                const defaultId = getDefaultScaleIDFromAxis(axis, chartIndexAxis);
                const defaultScaleOptions = chartDefaults.scales || {};
                firstIDs[axis] = firstIDs[axis] || id;
                scales[id] = mergeIf(Object.create(null), [ {
                    axis
                }, scaleConf, defaultScaleOptions[axis], defaultScaleOptions[defaultId] ]);
            }));
            config.data.datasets.forEach((dataset => {
                const type = dataset.type || config.type;
                const indexAxis = dataset.indexAxis || getIndexAxis(type, options);
                const datasetDefaults = overrides[type] || {};
                const defaultScaleOptions = datasetDefaults.scales || {};
                Object.keys(defaultScaleOptions).forEach((defaultID => {
                    const axis = getAxisFromDefaultScaleID(defaultID, indexAxis);
                    const id = dataset[axis + "AxisID"] || firstIDs[axis] || axis;
                    scales[id] = scales[id] || Object.create(null);
                    mergeIf(scales[id], [ {
                        axis
                    }, configScales[id], defaultScaleOptions[defaultID] ]);
                }));
            }));
            Object.keys(scales).forEach((key => {
                const scale = scales[key];
                mergeIf(scale, [ defaults.scales[scale.type], defaults.scale ]);
            }));
            return scales;
        }
        function initOptions(config) {
            const options = config.options || (config.options = {});
            options.plugins = valueOrDefault(options.plugins, {});
            options.scales = mergeScaleConfig(config, options);
        }
        function initData(data) {
            data = data || {};
            data.datasets = data.datasets || [];
            data.labels = data.labels || [];
            return data;
        }
        function initConfig(config) {
            config = config || {};
            config.data = initData(config.data);
            initOptions(config);
            return config;
        }
        const keyCache = new Map;
        const keysCached = new Set;
        function cachedKeys(cacheKey, generate) {
            let keys = keyCache.get(cacheKey);
            if (!keys) {
                keys = generate();
                keyCache.set(cacheKey, keys);
                keysCached.add(keys);
            }
            return keys;
        }
        const addIfFound = (set, obj, key) => {
            const opts = resolveObjectKey(obj, key);
            if (void 0 !== opts) set.add(opts);
        };
        class Config {
            constructor(config) {
                this._config = initConfig(config);
                this._scopeCache = new Map;
                this._resolverCache = new Map;
            }
            get platform() {
                return this._config.platform;
            }
            get type() {
                return this._config.type;
            }
            set type(type) {
                this._config.type = type;
            }
            get data() {
                return this._config.data;
            }
            set data(data) {
                this._config.data = initData(data);
            }
            get options() {
                return this._config.options;
            }
            set options(options) {
                this._config.options = options;
            }
            get plugins() {
                return this._config.plugins;
            }
            update() {
                const config = this._config;
                this.clearCache();
                initOptions(config);
            }
            clearCache() {
                this._scopeCache.clear();
                this._resolverCache.clear();
            }
            datasetScopeKeys(datasetType) {
                return cachedKeys(datasetType, (() => [ [ `datasets.${datasetType}`, "" ] ]));
            }
            datasetAnimationScopeKeys(datasetType, transition) {
                return cachedKeys(`${datasetType}.transition.${transition}`, (() => [ [ `datasets.${datasetType}.transitions.${transition}`, `transitions.${transition}` ], [ `datasets.${datasetType}`, "" ] ]));
            }
            datasetElementScopeKeys(datasetType, elementType) {
                return cachedKeys(`${datasetType}-${elementType}`, (() => [ [ `datasets.${datasetType}.elements.${elementType}`, `datasets.${datasetType}`, `elements.${elementType}`, "" ] ]));
            }
            pluginScopeKeys(plugin) {
                const id = plugin.id;
                const type = this.type;
                return cachedKeys(`${type}-plugin-${id}`, (() => [ [ `plugins.${id}`, ...plugin.additionalOptionScopes || [] ] ]));
            }
            _cachedScopes(mainScope, resetCache) {
                const _scopeCache = this._scopeCache;
                let cache = _scopeCache.get(mainScope);
                if (!cache || resetCache) {
                    cache = new Map;
                    _scopeCache.set(mainScope, cache);
                }
                return cache;
            }
            getOptionScopes(mainScope, keyLists, resetCache) {
                const {options, type} = this;
                const cache = this._cachedScopes(mainScope, resetCache);
                const cached = cache.get(keyLists);
                if (cached) return cached;
                const scopes = new Set;
                keyLists.forEach((keys => {
                    if (mainScope) {
                        scopes.add(mainScope);
                        keys.forEach((key => addIfFound(scopes, mainScope, key)));
                    }
                    keys.forEach((key => addIfFound(scopes, options, key)));
                    keys.forEach((key => addIfFound(scopes, overrides[type] || {}, key)));
                    keys.forEach((key => addIfFound(scopes, defaults, key)));
                    keys.forEach((key => addIfFound(scopes, descriptors, key)));
                }));
                const array = Array.from(scopes);
                if (0 === array.length) array.push(Object.create(null));
                if (keysCached.has(keyLists)) cache.set(keyLists, array);
                return array;
            }
            chartOptionScopes() {
                const {options, type} = this;
                return [ options, overrides[type] || {}, defaults.datasets[type] || {}, {
                    type
                }, defaults, descriptors ];
            }
            resolveNamedOptions(scopes, names, context, prefixes = [ "" ]) {
                const result = {
                    $shared: true
                };
                const {resolver, subPrefixes} = getResolver(this._resolverCache, scopes, prefixes);
                let options = resolver;
                if (needContext(resolver, names)) {
                    result.$shared = false;
                    context = isFunction(context) ? context() : context;
                    const subResolver = this.createResolver(scopes, context, subPrefixes);
                    options = _attachContext(resolver, context, subResolver);
                }
                for (const prop of names) result[prop] = options[prop];
                return result;
            }
            createResolver(scopes, context, prefixes = [ "" ], descriptorDefaults) {
                const {resolver} = getResolver(this._resolverCache, scopes, prefixes);
                return isObject(context) ? _attachContext(resolver, context, void 0, descriptorDefaults) : resolver;
            }
        }
        function getResolver(resolverCache, scopes, prefixes) {
            let cache = resolverCache.get(scopes);
            if (!cache) {
                cache = new Map;
                resolverCache.set(scopes, cache);
            }
            const cacheKey = prefixes.join();
            let cached = cache.get(cacheKey);
            if (!cached) {
                const resolver = _createResolver(scopes, prefixes);
                cached = {
                    resolver,
                    subPrefixes: prefixes.filter((p => !p.toLowerCase().includes("hover")))
                };
                cache.set(cacheKey, cached);
            }
            return cached;
        }
        const hasFunction = value => isObject(value) && Object.getOwnPropertyNames(value).reduce(((acc, key) => acc || isFunction(value[key])), false);
        function needContext(proxy, names) {
            const {isScriptable, isIndexable} = _descriptors(proxy);
            for (const prop of names) {
                const scriptable = isScriptable(prop);
                const indexable = isIndexable(prop);
                const value = (indexable || scriptable) && proxy[prop];
                if (scriptable && (isFunction(value) || hasFunction(value)) || indexable && isArray(value)) return true;
            }
            return false;
        }
        var version = "3.7.0";
        const KNOWN_POSITIONS = [ "top", "bottom", "left", "right", "chartArea" ];
        function positionIsHorizontal(position, axis) {
            return "top" === position || "bottom" === position || -1 === KNOWN_POSITIONS.indexOf(position) && "x" === axis;
        }
        function compare2Level(l1, l2) {
            return function(a, b) {
                return a[l1] === b[l1] ? a[l2] - b[l2] : a[l1] - b[l1];
            };
        }
        function onAnimationsComplete(context) {
            const chart = context.chart;
            const animationOptions = chart.options.animation;
            chart.notifyPlugins("afterRender");
            callback(animationOptions && animationOptions.onComplete, [ context ], chart);
        }
        function onAnimationProgress(context) {
            const chart = context.chart;
            const animationOptions = chart.options.animation;
            callback(animationOptions && animationOptions.onProgress, [ context ], chart);
        }
        function getCanvas(item) {
            if (_isDomSupported() && "string" === typeof item) item = document.getElementById(item); else if (item && item.length) item = item[0];
            if (item && item.canvas) item = item.canvas;
            return item;
        }
        const instances = {};
        const getChart = key => {
            const canvas = getCanvas(key);
            return Object.values(instances).filter((c => c.canvas === canvas)).pop();
        };
        function moveNumericKeys(obj, start, move) {
            const keys = Object.keys(obj);
            for (const key of keys) {
                const intKey = +key;
                if (intKey >= start) {
                    const value = obj[key];
                    delete obj[key];
                    if (move > 0 || intKey > start) obj[intKey + move] = value;
                }
            }
        }
        function determineLastEvent(e, lastEvent, inChartArea, isClick) {
            if (!inChartArea || "mouseout" === e.type) return null;
            if (isClick) return lastEvent;
            return e;
        }
        class chart_esm_Chart {
            constructor(item, userConfig) {
                const config = this.config = new Config(userConfig);
                const initialCanvas = getCanvas(item);
                const existingChart = getChart(initialCanvas);
                if (existingChart) throw new Error("Canvas is already in use. Chart with ID '" + existingChart.id + "'" + " must be destroyed before the canvas can be reused.");
                const options = config.createResolver(config.chartOptionScopes(), this.getContext());
                this.platform = new (config.platform || _detectPlatform(initialCanvas));
                this.platform.updateConfig(config);
                const context = this.platform.acquireContext(initialCanvas, options.aspectRatio);
                const canvas = context && context.canvas;
                const height = canvas && canvas.height;
                const width = canvas && canvas.width;
                this.id = uid();
                this.ctx = context;
                this.canvas = canvas;
                this.width = width;
                this.height = height;
                this._options = options;
                this._aspectRatio = this.aspectRatio;
                this._layers = [];
                this._metasets = [];
                this._stacks = void 0;
                this.boxes = [];
                this.currentDevicePixelRatio = void 0;
                this.chartArea = void 0;
                this._active = [];
                this._lastEvent = void 0;
                this._listeners = {};
                this._responsiveListeners = void 0;
                this._sortedMetasets = [];
                this.scales = {};
                this._plugins = new PluginService;
                this.$proxies = {};
                this._hiddenIndices = {};
                this.attached = false;
                this._animationsDisabled = void 0;
                this.$context = void 0;
                this._doResize = debounce((mode => this.update(mode)), options.resizeDelay || 0);
                this._dataChanges = [];
                instances[this.id] = this;
                if (!context || !canvas) {
                    console.error("Failed to create chart: can't acquire context from the given item");
                    return;
                }
                animator.listen(this, "complete", onAnimationsComplete);
                animator.listen(this, "progress", onAnimationProgress);
                this._initialize();
                if (this.attached) this.update();
            }
            get aspectRatio() {
                const {options: {aspectRatio, maintainAspectRatio}, width, height, _aspectRatio} = this;
                if (!isNullOrUndef(aspectRatio)) return aspectRatio;
                if (maintainAspectRatio && _aspectRatio) return _aspectRatio;
                return height ? width / height : null;
            }
            get data() {
                return this.config.data;
            }
            set data(data) {
                this.config.data = data;
            }
            get options() {
                return this._options;
            }
            set options(options) {
                this.config.options = options;
            }
            _initialize() {
                this.notifyPlugins("beforeInit");
                if (this.options.responsive) this.resize(); else retinaScale(this, this.options.devicePixelRatio);
                this.bindEvents();
                this.notifyPlugins("afterInit");
                return this;
            }
            clear() {
                clearCanvas(this.canvas, this.ctx);
                return this;
            }
            stop() {
                animator.stop(this);
                return this;
            }
            resize(width, height) {
                if (!animator.running(this)) this._resize(width, height); else this._resizeBeforeDraw = {
                    width,
                    height
                };
            }
            _resize(width, height) {
                const options = this.options;
                const canvas = this.canvas;
                const aspectRatio = options.maintainAspectRatio && this.aspectRatio;
                const newSize = this.platform.getMaximumSize(canvas, width, height, aspectRatio);
                const newRatio = options.devicePixelRatio || this.platform.getDevicePixelRatio();
                const mode = this.width ? "resize" : "attach";
                this.width = newSize.width;
                this.height = newSize.height;
                this._aspectRatio = this.aspectRatio;
                if (!retinaScale(this, newRatio, true)) return;
                this.notifyPlugins("resize", {
                    size: newSize
                });
                callback(options.onResize, [ this, newSize ], this);
                if (this.attached) if (this._doResize(mode)) this.render();
            }
            ensureScalesHaveIDs() {
                const options = this.options;
                const scalesOptions = options.scales || {};
                each(scalesOptions, ((axisOptions, axisID) => {
                    axisOptions.id = axisID;
                }));
            }
            buildOrUpdateScales() {
                const options = this.options;
                const scaleOpts = options.scales;
                const scales = this.scales;
                const updated = Object.keys(scales).reduce(((obj, id) => {
                    obj[id] = false;
                    return obj;
                }), {});
                let items = [];
                if (scaleOpts) items = items.concat(Object.keys(scaleOpts).map((id => {
                    const scaleOptions = scaleOpts[id];
                    const axis = determineAxis(id, scaleOptions);
                    const isRadial = "r" === axis;
                    const isHorizontal = "x" === axis;
                    return {
                        options: scaleOptions,
                        dposition: isRadial ? "chartArea" : isHorizontal ? "bottom" : "left",
                        dtype: isRadial ? "radialLinear" : isHorizontal ? "category" : "linear"
                    };
                })));
                each(items, (item => {
                    const scaleOptions = item.options;
                    const id = scaleOptions.id;
                    const axis = determineAxis(id, scaleOptions);
                    const scaleType = valueOrDefault(scaleOptions.type, item.dtype);
                    if (void 0 === scaleOptions.position || positionIsHorizontal(scaleOptions.position, axis) !== positionIsHorizontal(item.dposition)) scaleOptions.position = item.dposition;
                    updated[id] = true;
                    let scale = null;
                    if (id in scales && scales[id].type === scaleType) scale = scales[id]; else {
                        const scaleClass = registry.getScale(scaleType);
                        scale = new scaleClass({
                            id,
                            type: scaleType,
                            ctx: this.ctx,
                            chart: this
                        });
                        scales[scale.id] = scale;
                    }
                    scale.init(scaleOptions, options);
                }));
                each(updated, ((hasUpdated, id) => {
                    if (!hasUpdated) delete scales[id];
                }));
                each(scales, (scale => {
                    layouts.configure(this, scale, scale.options);
                    layouts.addBox(this, scale);
                }));
            }
            _updateMetasets() {
                const metasets = this._metasets;
                const numData = this.data.datasets.length;
                const numMeta = metasets.length;
                metasets.sort(((a, b) => a.index - b.index));
                if (numMeta > numData) {
                    for (let i = numData; i < numMeta; ++i) this._destroyDatasetMeta(i);
                    metasets.splice(numData, numMeta - numData);
                }
                this._sortedMetasets = metasets.slice(0).sort(compare2Level("order", "index"));
            }
            _removeUnreferencedMetasets() {
                const {_metasets: metasets, data: {datasets}} = this;
                if (metasets.length > datasets.length) delete this._stacks;
                metasets.forEach(((meta, index) => {
                    if (0 === datasets.filter((x => x === meta._dataset)).length) this._destroyDatasetMeta(index);
                }));
            }
            buildOrUpdateControllers() {
                const newControllers = [];
                const datasets = this.data.datasets;
                let i, ilen;
                this._removeUnreferencedMetasets();
                for (i = 0, ilen = datasets.length; i < ilen; i++) {
                    const dataset = datasets[i];
                    let meta = this.getDatasetMeta(i);
                    const type = dataset.type || this.config.type;
                    if (meta.type && meta.type !== type) {
                        this._destroyDatasetMeta(i);
                        meta = this.getDatasetMeta(i);
                    }
                    meta.type = type;
                    meta.indexAxis = dataset.indexAxis || getIndexAxis(type, this.options);
                    meta.order = dataset.order || 0;
                    meta.index = i;
                    meta.label = "" + dataset.label;
                    meta.visible = this.isDatasetVisible(i);
                    if (meta.controller) {
                        meta.controller.updateIndex(i);
                        meta.controller.linkScales();
                    } else {
                        const ControllerClass = registry.getController(type);
                        const {datasetElementType, dataElementType} = defaults.datasets[type];
                        Object.assign(ControllerClass.prototype, {
                            dataElementType: registry.getElement(dataElementType),
                            datasetElementType: datasetElementType && registry.getElement(datasetElementType)
                        });
                        meta.controller = new ControllerClass(this, i);
                        newControllers.push(meta.controller);
                    }
                }
                this._updateMetasets();
                return newControllers;
            }
            _resetElements() {
                each(this.data.datasets, ((dataset, datasetIndex) => {
                    this.getDatasetMeta(datasetIndex).controller.reset();
                }), this);
            }
            reset() {
                this._resetElements();
                this.notifyPlugins("reset");
            }
            update(mode) {
                const config = this.config;
                config.update();
                const options = this._options = config.createResolver(config.chartOptionScopes(), this.getContext());
                const animsDisabled = this._animationsDisabled = !options.animation;
                this._updateScales();
                this._checkEventBindings();
                this._updateHiddenIndices();
                this._plugins.invalidate();
                if (false === this.notifyPlugins("beforeUpdate", {
                    mode,
                    cancelable: true
                })) return;
                const newControllers = this.buildOrUpdateControllers();
                this.notifyPlugins("beforeElementsUpdate");
                let minPadding = 0;
                for (let i = 0, ilen = this.data.datasets.length; i < ilen; i++) {
                    const {controller} = this.getDatasetMeta(i);
                    const reset = !animsDisabled && -1 === newControllers.indexOf(controller);
                    controller.buildOrUpdateElements(reset);
                    minPadding = Math.max(+controller.getMaxOverflow(), minPadding);
                }
                minPadding = this._minPadding = options.layout.autoPadding ? minPadding : 0;
                this._updateLayout(minPadding);
                if (!animsDisabled) each(newControllers, (controller => {
                    controller.reset();
                }));
                this._updateDatasets(mode);
                this.notifyPlugins("afterUpdate", {
                    mode
                });
                this._layers.sort(compare2Level("z", "_idx"));
                const {_active, _lastEvent} = this;
                if (_lastEvent) this._eventHandler(_lastEvent, true); else if (_active.length) this._updateHoverStyles(_active, _active, true);
                this.render();
            }
            _updateScales() {
                each(this.scales, (scale => {
                    layouts.removeBox(this, scale);
                }));
                this.ensureScalesHaveIDs();
                this.buildOrUpdateScales();
            }
            _checkEventBindings() {
                const options = this.options;
                const existingEvents = new Set(Object.keys(this._listeners));
                const newEvents = new Set(options.events);
                if (!setsEqual(existingEvents, newEvents) || !!this._responsiveListeners !== options.responsive) {
                    this.unbindEvents();
                    this.bindEvents();
                }
            }
            _updateHiddenIndices() {
                const {_hiddenIndices} = this;
                const changes = this._getUniformDataChanges() || [];
                for (const {method, start, count} of changes) {
                    const move = "_removeElements" === method ? -count : count;
                    moveNumericKeys(_hiddenIndices, start, move);
                }
            }
            _getUniformDataChanges() {
                const _dataChanges = this._dataChanges;
                if (!_dataChanges || !_dataChanges.length) return;
                this._dataChanges = [];
                const datasetCount = this.data.datasets.length;
                const makeSet = idx => new Set(_dataChanges.filter((c => c[0] === idx)).map(((c, i) => i + "," + c.splice(1).join(","))));
                const changeSet = makeSet(0);
                for (let i = 1; i < datasetCount; i++) if (!setsEqual(changeSet, makeSet(i))) return;
                return Array.from(changeSet).map((c => c.split(","))).map((a => ({
                    method: a[1],
                    start: +a[2],
                    count: +a[3]
                })));
            }
            _updateLayout(minPadding) {
                if (false === this.notifyPlugins("beforeLayout", {
                    cancelable: true
                })) return;
                layouts.update(this, this.width, this.height, minPadding);
                const area = this.chartArea;
                const noArea = area.width <= 0 || area.height <= 0;
                this._layers = [];
                each(this.boxes, (box => {
                    if (noArea && "chartArea" === box.position) return;
                    if (box.configure) box.configure();
                    this._layers.push(...box._layers());
                }), this);
                this._layers.forEach(((item, index) => {
                    item._idx = index;
                }));
                this.notifyPlugins("afterLayout");
            }
            _updateDatasets(mode) {
                if (false === this.notifyPlugins("beforeDatasetsUpdate", {
                    mode,
                    cancelable: true
                })) return;
                for (let i = 0, ilen = this.data.datasets.length; i < ilen; ++i) this.getDatasetMeta(i).controller.configure();
                for (let i = 0, ilen = this.data.datasets.length; i < ilen; ++i) this._updateDataset(i, isFunction(mode) ? mode({
                    datasetIndex: i
                }) : mode);
                this.notifyPlugins("afterDatasetsUpdate", {
                    mode
                });
            }
            _updateDataset(index, mode) {
                const meta = this.getDatasetMeta(index);
                const args = {
                    meta,
                    index,
                    mode,
                    cancelable: true
                };
                if (false === this.notifyPlugins("beforeDatasetUpdate", args)) return;
                meta.controller._update(mode);
                args.cancelable = false;
                this.notifyPlugins("afterDatasetUpdate", args);
            }
            render() {
                if (false === this.notifyPlugins("beforeRender", {
                    cancelable: true
                })) return;
                if (animator.has(this)) {
                    if (this.attached && !animator.running(this)) animator.start(this);
                } else {
                    this.draw();
                    onAnimationsComplete({
                        chart: this
                    });
                }
            }
            draw() {
                let i;
                if (this._resizeBeforeDraw) {
                    const {width, height} = this._resizeBeforeDraw;
                    this._resize(width, height);
                    this._resizeBeforeDraw = null;
                }
                this.clear();
                if (this.width <= 0 || this.height <= 0) return;
                if (false === this.notifyPlugins("beforeDraw", {
                    cancelable: true
                })) return;
                const layers = this._layers;
                for (i = 0; i < layers.length && layers[i].z <= 0; ++i) layers[i].draw(this.chartArea);
                this._drawDatasets();
                for (;i < layers.length; ++i) layers[i].draw(this.chartArea);
                this.notifyPlugins("afterDraw");
            }
            _getSortedDatasetMetas(filterVisible) {
                const metasets = this._sortedMetasets;
                const result = [];
                let i, ilen;
                for (i = 0, ilen = metasets.length; i < ilen; ++i) {
                    const meta = metasets[i];
                    if (!filterVisible || meta.visible) result.push(meta);
                }
                return result;
            }
            getSortedVisibleDatasetMetas() {
                return this._getSortedDatasetMetas(true);
            }
            _drawDatasets() {
                if (false === this.notifyPlugins("beforeDatasetsDraw", {
                    cancelable: true
                })) return;
                const metasets = this.getSortedVisibleDatasetMetas();
                for (let i = metasets.length - 1; i >= 0; --i) this._drawDataset(metasets[i]);
                this.notifyPlugins("afterDatasetsDraw");
            }
            _drawDataset(meta) {
                const ctx = this.ctx;
                const clip = meta._clip;
                const useClip = !clip.disabled;
                const area = this.chartArea;
                const args = {
                    meta,
                    index: meta.index,
                    cancelable: true
                };
                if (false === this.notifyPlugins("beforeDatasetDraw", args)) return;
                if (useClip) clipArea(ctx, {
                    left: false === clip.left ? 0 : area.left - clip.left,
                    right: false === clip.right ? this.width : area.right + clip.right,
                    top: false === clip.top ? 0 : area.top - clip.top,
                    bottom: false === clip.bottom ? this.height : area.bottom + clip.bottom
                });
                meta.controller.draw();
                if (useClip) unclipArea(ctx);
                args.cancelable = false;
                this.notifyPlugins("afterDatasetDraw", args);
            }
            getElementsAtEventForMode(e, mode, options, useFinalPosition) {
                const method = Interaction.modes[mode];
                if ("function" === typeof method) return method(this, e, options, useFinalPosition);
                return [];
            }
            getDatasetMeta(datasetIndex) {
                const dataset = this.data.datasets[datasetIndex];
                const metasets = this._metasets;
                let meta = metasets.filter((x => x && x._dataset === dataset)).pop();
                if (!meta) {
                    meta = {
                        type: null,
                        data: [],
                        dataset: null,
                        controller: null,
                        hidden: null,
                        xAxisID: null,
                        yAxisID: null,
                        order: dataset && dataset.order || 0,
                        index: datasetIndex,
                        _dataset: dataset,
                        _parsed: [],
                        _sorted: false
                    };
                    metasets.push(meta);
                }
                return meta;
            }
            getContext() {
                return this.$context || (this.$context = createContext(null, {
                    chart: this,
                    type: "chart"
                }));
            }
            getVisibleDatasetCount() {
                return this.getSortedVisibleDatasetMetas().length;
            }
            isDatasetVisible(datasetIndex) {
                const dataset = this.data.datasets[datasetIndex];
                if (!dataset) return false;
                const meta = this.getDatasetMeta(datasetIndex);
                return "boolean" === typeof meta.hidden ? !meta.hidden : !dataset.hidden;
            }
            setDatasetVisibility(datasetIndex, visible) {
                const meta = this.getDatasetMeta(datasetIndex);
                meta.hidden = !visible;
            }
            toggleDataVisibility(index) {
                this._hiddenIndices[index] = !this._hiddenIndices[index];
            }
            getDataVisibility(index) {
                return !this._hiddenIndices[index];
            }
            _updateVisibility(datasetIndex, dataIndex, visible) {
                const mode = visible ? "show" : "hide";
                const meta = this.getDatasetMeta(datasetIndex);
                const anims = meta.controller._resolveAnimations(void 0, mode);
                if (defined(dataIndex)) {
                    meta.data[dataIndex].hidden = !visible;
                    this.update();
                } else {
                    this.setDatasetVisibility(datasetIndex, visible);
                    anims.update(meta, {
                        visible
                    });
                    this.update((ctx => ctx.datasetIndex === datasetIndex ? mode : void 0));
                }
            }
            hide(datasetIndex, dataIndex) {
                this._updateVisibility(datasetIndex, dataIndex, false);
            }
            show(datasetIndex, dataIndex) {
                this._updateVisibility(datasetIndex, dataIndex, true);
            }
            _destroyDatasetMeta(datasetIndex) {
                const meta = this._metasets[datasetIndex];
                if (meta && meta.controller) meta.controller._destroy();
                delete this._metasets[datasetIndex];
            }
            _stop() {
                let i, ilen;
                this.stop();
                animator.remove(this);
                for (i = 0, ilen = this.data.datasets.length; i < ilen; ++i) this._destroyDatasetMeta(i);
            }
            destroy() {
                this.notifyPlugins("beforeDestroy");
                const {canvas, ctx} = this;
                this._stop();
                this.config.clearCache();
                if (canvas) {
                    this.unbindEvents();
                    clearCanvas(canvas, ctx);
                    this.platform.releaseContext(ctx);
                    this.canvas = null;
                    this.ctx = null;
                }
                this.notifyPlugins("destroy");
                delete instances[this.id];
                this.notifyPlugins("afterDestroy");
            }
            toBase64Image(...args) {
                return this.canvas.toDataURL(...args);
            }
            bindEvents() {
                this.bindUserEvents();
                if (this.options.responsive) this.bindResponsiveEvents(); else this.attached = true;
            }
            bindUserEvents() {
                const listeners = this._listeners;
                const platform = this.platform;
                const _add = (type, listener) => {
                    platform.addEventListener(this, type, listener);
                    listeners[type] = listener;
                };
                const listener = (e, x, y) => {
                    e.offsetX = x;
                    e.offsetY = y;
                    this._eventHandler(e);
                };
                each(this.options.events, (type => _add(type, listener)));
            }
            bindResponsiveEvents() {
                if (!this._responsiveListeners) this._responsiveListeners = {};
                const listeners = this._responsiveListeners;
                const platform = this.platform;
                const _add = (type, listener) => {
                    platform.addEventListener(this, type, listener);
                    listeners[type] = listener;
                };
                const _remove = (type, listener) => {
                    if (listeners[type]) {
                        platform.removeEventListener(this, type, listener);
                        delete listeners[type];
                    }
                };
                const listener = (width, height) => {
                    if (this.canvas) this.resize(width, height);
                };
                let detached;
                const attached = () => {
                    _remove("attach", attached);
                    this.attached = true;
                    this.resize();
                    _add("resize", listener);
                    _add("detach", detached);
                };
                detached = () => {
                    this.attached = false;
                    _remove("resize", listener);
                    this._stop();
                    this._resize(0, 0);
                    _add("attach", attached);
                };
                if (platform.isAttached(this.canvas)) attached(); else detached();
            }
            unbindEvents() {
                each(this._listeners, ((listener, type) => {
                    this.platform.removeEventListener(this, type, listener);
                }));
                this._listeners = {};
                each(this._responsiveListeners, ((listener, type) => {
                    this.platform.removeEventListener(this, type, listener);
                }));
                this._responsiveListeners = void 0;
            }
            updateHoverStyle(items, mode, enabled) {
                const prefix = enabled ? "set" : "remove";
                let meta, item, i, ilen;
                if ("dataset" === mode) {
                    meta = this.getDatasetMeta(items[0].datasetIndex);
                    meta.controller["_" + prefix + "DatasetHoverStyle"]();
                }
                for (i = 0, ilen = items.length; i < ilen; ++i) {
                    item = items[i];
                    const controller = item && this.getDatasetMeta(item.datasetIndex).controller;
                    if (controller) controller[prefix + "HoverStyle"](item.element, item.datasetIndex, item.index);
                }
            }
            getActiveElements() {
                return this._active || [];
            }
            setActiveElements(activeElements) {
                const lastActive = this._active || [];
                const active = activeElements.map((({datasetIndex, index}) => {
                    const meta = this.getDatasetMeta(datasetIndex);
                    if (!meta) throw new Error("No dataset found at index " + datasetIndex);
                    return {
                        datasetIndex,
                        element: meta.data[index],
                        index
                    };
                }));
                const changed = !_elementsEqual(active, lastActive);
                if (changed) {
                    this._active = active;
                    this._lastEvent = null;
                    this._updateHoverStyles(active, lastActive);
                }
            }
            notifyPlugins(hook, args, filter) {
                return this._plugins.notify(this, hook, args, filter);
            }
            _updateHoverStyles(active, lastActive, replay) {
                const hoverOptions = this.options.hover;
                const diff = (a, b) => a.filter((x => !b.some((y => x.datasetIndex === y.datasetIndex && x.index === y.index))));
                const deactivated = diff(lastActive, active);
                const activated = replay ? active : diff(active, lastActive);
                if (deactivated.length) this.updateHoverStyle(deactivated, hoverOptions.mode, false);
                if (activated.length && hoverOptions.mode) this.updateHoverStyle(activated, hoverOptions.mode, true);
            }
            _eventHandler(e, replay) {
                const args = {
                    event: e,
                    replay,
                    cancelable: true,
                    inChartArea: _isPointInArea(e, this.chartArea, this._minPadding)
                };
                const eventFilter = plugin => (plugin.options.events || this.options.events).includes(e.native.type);
                if (false === this.notifyPlugins("beforeEvent", args, eventFilter)) return;
                const changed = this._handleEvent(e, replay, args.inChartArea);
                args.cancelable = false;
                this.notifyPlugins("afterEvent", args, eventFilter);
                if (changed || args.changed) this.render();
                return this;
            }
            _handleEvent(e, replay, inChartArea) {
                const {_active: lastActive = [], options} = this;
                const useFinalPosition = replay;
                const active = this._getActiveElements(e, lastActive, inChartArea, useFinalPosition);
                const isClick = _isClickEvent(e);
                const lastEvent = determineLastEvent(e, this._lastEvent, inChartArea, isClick);
                if (inChartArea) {
                    this._lastEvent = null;
                    callback(options.onHover, [ e, active, this ], this);
                    if (isClick) callback(options.onClick, [ e, active, this ], this);
                }
                const changed = !_elementsEqual(active, lastActive);
                if (changed || replay) {
                    this._active = active;
                    this._updateHoverStyles(active, lastActive, replay);
                }
                this._lastEvent = lastEvent;
                return changed;
            }
            _getActiveElements(e, lastActive, inChartArea, useFinalPosition) {
                if ("mouseout" === e.type) return [];
                if (!inChartArea) return lastActive;
                const hoverOptions = this.options.hover;
                return this.getElementsAtEventForMode(e, hoverOptions.mode, hoverOptions, useFinalPosition);
            }
        }
        const invalidatePlugins = () => each(chart_esm_Chart.instances, (chart => chart._plugins.invalidate()));
        const enumerable = true;
        Object.defineProperties(chart_esm_Chart, {
            defaults: {
                enumerable,
                value: defaults
            },
            instances: {
                enumerable,
                value: instances
            },
            overrides: {
                enumerable,
                value: overrides
            },
            registry: {
                enumerable,
                value: registry
            },
            version: {
                enumerable,
                value: version
            },
            getChart: {
                enumerable,
                value: getChart
            },
            register: {
                enumerable,
                value: (...items) => {
                    registry.add(...items);
                    invalidatePlugins();
                }
            },
            unregister: {
                enumerable,
                value: (...items) => {
                    registry.remove(...items);
                    invalidatePlugins();
                }
            }
        });
        function clipArc(ctx, element, endAngle) {
            const {startAngle, pixelMargin, x, y, outerRadius, innerRadius} = element;
            let angleMargin = pixelMargin / outerRadius;
            ctx.beginPath();
            ctx.arc(x, y, outerRadius, startAngle - angleMargin, endAngle + angleMargin);
            if (innerRadius > pixelMargin) {
                angleMargin = pixelMargin / innerRadius;
                ctx.arc(x, y, innerRadius, endAngle + angleMargin, startAngle - angleMargin, true);
            } else ctx.arc(x, y, pixelMargin, endAngle + HALF_PI, startAngle - HALF_PI);
            ctx.closePath();
            ctx.clip();
        }
        function toRadiusCorners(value) {
            return _readValueToProps(value, [ "outerStart", "outerEnd", "innerStart", "innerEnd" ]);
        }
        function parseBorderRadius$1(arc, innerRadius, outerRadius, angleDelta) {
            const o = toRadiusCorners(arc.options.borderRadius);
            const halfThickness = (outerRadius - innerRadius) / 2;
            const innerLimit = Math.min(halfThickness, angleDelta * innerRadius / 2);
            const computeOuterLimit = val => {
                const outerArcLimit = (outerRadius - Math.min(halfThickness, val)) * angleDelta / 2;
                return _limitValue(val, 0, Math.min(halfThickness, outerArcLimit));
            };
            return {
                outerStart: computeOuterLimit(o.outerStart),
                outerEnd: computeOuterLimit(o.outerEnd),
                innerStart: _limitValue(o.innerStart, 0, innerLimit),
                innerEnd: _limitValue(o.innerEnd, 0, innerLimit)
            };
        }
        function rThetaToXY(r, theta, x, y) {
            return {
                x: x + r * Math.cos(theta),
                y: y + r * Math.sin(theta)
            };
        }
        function pathArc(ctx, element, offset, spacing, end) {
            const {x, y, startAngle: start, pixelMargin, innerRadius: innerR} = element;
            const outerRadius = Math.max(element.outerRadius + spacing + offset - pixelMargin, 0);
            const innerRadius = innerR > 0 ? innerR + spacing + offset + pixelMargin : 0;
            let spacingOffset = 0;
            const alpha = end - start;
            if (spacing) {
                const noSpacingInnerRadius = innerR > 0 ? innerR - spacing : 0;
                const noSpacingOuterRadius = outerRadius > 0 ? outerRadius - spacing : 0;
                const avNogSpacingRadius = (noSpacingInnerRadius + noSpacingOuterRadius) / 2;
                const adjustedAngle = 0 !== avNogSpacingRadius ? alpha * avNogSpacingRadius / (avNogSpacingRadius + spacing) : alpha;
                spacingOffset = (alpha - adjustedAngle) / 2;
            }
            const beta = Math.max(.001, alpha * outerRadius - offset / PI) / outerRadius;
            const angleOffset = (alpha - beta) / 2;
            const startAngle = start + angleOffset + spacingOffset;
            const endAngle = end - angleOffset - spacingOffset;
            const {outerStart, outerEnd, innerStart, innerEnd} = parseBorderRadius$1(element, innerRadius, outerRadius, endAngle - startAngle);
            const outerStartAdjustedRadius = outerRadius - outerStart;
            const outerEndAdjustedRadius = outerRadius - outerEnd;
            const outerStartAdjustedAngle = startAngle + outerStart / outerStartAdjustedRadius;
            const outerEndAdjustedAngle = endAngle - outerEnd / outerEndAdjustedRadius;
            const innerStartAdjustedRadius = innerRadius + innerStart;
            const innerEndAdjustedRadius = innerRadius + innerEnd;
            const innerStartAdjustedAngle = startAngle + innerStart / innerStartAdjustedRadius;
            const innerEndAdjustedAngle = endAngle - innerEnd / innerEndAdjustedRadius;
            ctx.beginPath();
            ctx.arc(x, y, outerRadius, outerStartAdjustedAngle, outerEndAdjustedAngle);
            if (outerEnd > 0) {
                const pCenter = rThetaToXY(outerEndAdjustedRadius, outerEndAdjustedAngle, x, y);
                ctx.arc(pCenter.x, pCenter.y, outerEnd, outerEndAdjustedAngle, endAngle + HALF_PI);
            }
            const p4 = rThetaToXY(innerEndAdjustedRadius, endAngle, x, y);
            ctx.lineTo(p4.x, p4.y);
            if (innerEnd > 0) {
                const pCenter = rThetaToXY(innerEndAdjustedRadius, innerEndAdjustedAngle, x, y);
                ctx.arc(pCenter.x, pCenter.y, innerEnd, endAngle + HALF_PI, innerEndAdjustedAngle + Math.PI);
            }
            ctx.arc(x, y, innerRadius, endAngle - innerEnd / innerRadius, startAngle + innerStart / innerRadius, true);
            if (innerStart > 0) {
                const pCenter = rThetaToXY(innerStartAdjustedRadius, innerStartAdjustedAngle, x, y);
                ctx.arc(pCenter.x, pCenter.y, innerStart, innerStartAdjustedAngle + Math.PI, startAngle - HALF_PI);
            }
            const p8 = rThetaToXY(outerStartAdjustedRadius, startAngle, x, y);
            ctx.lineTo(p8.x, p8.y);
            if (outerStart > 0) {
                const pCenter = rThetaToXY(outerStartAdjustedRadius, outerStartAdjustedAngle, x, y);
                ctx.arc(pCenter.x, pCenter.y, outerStart, startAngle - HALF_PI, outerStartAdjustedAngle);
            }
            ctx.closePath();
        }
        function drawArc(ctx, element, offset, spacing) {
            const {fullCircles, startAngle, circumference} = element;
            let endAngle = element.endAngle;
            if (fullCircles) {
                pathArc(ctx, element, offset, spacing, startAngle + TAU);
                for (let i = 0; i < fullCircles; ++i) ctx.fill();
                if (!isNaN(circumference)) {
                    endAngle = startAngle + circumference % TAU;
                    if (circumference % TAU === 0) endAngle += TAU;
                }
            }
            pathArc(ctx, element, offset, spacing, endAngle);
            ctx.fill();
            return endAngle;
        }
        function drawFullCircleBorders(ctx, element, inner) {
            const {x, y, startAngle, pixelMargin, fullCircles} = element;
            const outerRadius = Math.max(element.outerRadius - pixelMargin, 0);
            const innerRadius = element.innerRadius + pixelMargin;
            let i;
            if (inner) clipArc(ctx, element, startAngle + TAU);
            ctx.beginPath();
            ctx.arc(x, y, innerRadius, startAngle + TAU, startAngle, true);
            for (i = 0; i < fullCircles; ++i) ctx.stroke();
            ctx.beginPath();
            ctx.arc(x, y, outerRadius, startAngle, startAngle + TAU);
            for (i = 0; i < fullCircles; ++i) ctx.stroke();
        }
        function drawBorder(ctx, element, offset, spacing, endAngle) {
            const {options} = element;
            const {borderWidth, borderJoinStyle} = options;
            const inner = "inner" === options.borderAlign;
            if (!borderWidth) return;
            if (inner) {
                ctx.lineWidth = 2 * borderWidth;
                ctx.lineJoin = borderJoinStyle || "round";
            } else {
                ctx.lineWidth = borderWidth;
                ctx.lineJoin = borderJoinStyle || "bevel";
            }
            if (element.fullCircles) drawFullCircleBorders(ctx, element, inner);
            if (inner) clipArc(ctx, element, endAngle);
            pathArc(ctx, element, offset, spacing, endAngle);
            ctx.stroke();
        }
        class ArcElement extends chart_esm_Element {
            constructor(cfg) {
                super();
                this.options = void 0;
                this.circumference = void 0;
                this.startAngle = void 0;
                this.endAngle = void 0;
                this.innerRadius = void 0;
                this.outerRadius = void 0;
                this.pixelMargin = 0;
                this.fullCircles = 0;
                if (cfg) Object.assign(this, cfg);
            }
            inRange(chartX, chartY, useFinalPosition) {
                const point = this.getProps([ "x", "y" ], useFinalPosition);
                const {angle, distance} = getAngleFromPoint(point, {
                    x: chartX,
                    y: chartY
                });
                const {startAngle, endAngle, innerRadius, outerRadius, circumference} = this.getProps([ "startAngle", "endAngle", "innerRadius", "outerRadius", "circumference" ], useFinalPosition);
                const rAdjust = this.options.spacing / 2;
                const _circumference = valueOrDefault(circumference, endAngle - startAngle);
                const betweenAngles = _circumference >= TAU || _angleBetween(angle, startAngle, endAngle);
                const withinRadius = _isBetween(distance, innerRadius + rAdjust, outerRadius + rAdjust);
                return betweenAngles && withinRadius;
            }
            getCenterPoint(useFinalPosition) {
                const {x, y, startAngle, endAngle, innerRadius, outerRadius} = this.getProps([ "x", "y", "startAngle", "endAngle", "innerRadius", "outerRadius", "circumference" ], useFinalPosition);
                const {offset, spacing} = this.options;
                const halfAngle = (startAngle + endAngle) / 2;
                const halfRadius = (innerRadius + outerRadius + spacing + offset) / 2;
                return {
                    x: x + Math.cos(halfAngle) * halfRadius,
                    y: y + Math.sin(halfAngle) * halfRadius
                };
            }
            tooltipPosition(useFinalPosition) {
                return this.getCenterPoint(useFinalPosition);
            }
            draw(ctx) {
                const {options, circumference} = this;
                const offset = (options.offset || 0) / 2;
                const spacing = (options.spacing || 0) / 2;
                this.pixelMargin = "inner" === options.borderAlign ? .33 : 0;
                this.fullCircles = circumference > TAU ? Math.floor(circumference / TAU) : 0;
                if (0 === circumference || this.innerRadius < 0 || this.outerRadius < 0) return;
                ctx.save();
                let radiusOffset = 0;
                if (offset) {
                    radiusOffset = offset / 2;
                    const halfAngle = (this.startAngle + this.endAngle) / 2;
                    ctx.translate(Math.cos(halfAngle) * radiusOffset, Math.sin(halfAngle) * radiusOffset);
                    if (this.circumference >= PI) radiusOffset = offset;
                }
                ctx.fillStyle = options.backgroundColor;
                ctx.strokeStyle = options.borderColor;
                const endAngle = drawArc(ctx, this, radiusOffset, spacing);
                drawBorder(ctx, this, radiusOffset, spacing, endAngle);
                ctx.restore();
            }
        }
        ArcElement.id = "arc";
        ArcElement.defaults = {
            borderAlign: "center",
            borderColor: "#fff",
            borderJoinStyle: void 0,
            borderRadius: 0,
            borderWidth: 2,
            offset: 0,
            spacing: 0,
            angle: void 0
        };
        ArcElement.defaultRoutes = {
            backgroundColor: "backgroundColor"
        };
        function setStyle(ctx, options, style = options) {
            ctx.lineCap = valueOrDefault(style.borderCapStyle, options.borderCapStyle);
            ctx.setLineDash(valueOrDefault(style.borderDash, options.borderDash));
            ctx.lineDashOffset = valueOrDefault(style.borderDashOffset, options.borderDashOffset);
            ctx.lineJoin = valueOrDefault(style.borderJoinStyle, options.borderJoinStyle);
            ctx.lineWidth = valueOrDefault(style.borderWidth, options.borderWidth);
            ctx.strokeStyle = valueOrDefault(style.borderColor, options.borderColor);
        }
        function lineTo(ctx, previous, target) {
            ctx.lineTo(target.x, target.y);
        }
        function getLineMethod(options) {
            if (options.stepped) return _steppedLineTo;
            if (options.tension || "monotone" === options.cubicInterpolationMode) return _bezierCurveTo;
            return lineTo;
        }
        function pathVars(points, segment, params = {}) {
            const count = points.length;
            const {start: paramsStart = 0, end: paramsEnd = count - 1} = params;
            const {start: segmentStart, end: segmentEnd} = segment;
            const start = Math.max(paramsStart, segmentStart);
            const end = Math.min(paramsEnd, segmentEnd);
            const outside = paramsStart < segmentStart && paramsEnd < segmentStart || paramsStart > segmentEnd && paramsEnd > segmentEnd;
            return {
                count,
                start,
                loop: segment.loop,
                ilen: end < start && !outside ? count + end - start : end - start
            };
        }
        function pathSegment(ctx, line, segment, params) {
            const {points, options} = line;
            const {count, start, loop, ilen} = pathVars(points, segment, params);
            const lineMethod = getLineMethod(options);
            let {move = true, reverse} = params || {};
            let i, point, prev;
            for (i = 0; i <= ilen; ++i) {
                point = points[(start + (reverse ? ilen - i : i)) % count];
                if (point.skip) continue; else if (move) {
                    ctx.moveTo(point.x, point.y);
                    move = false;
                } else lineMethod(ctx, prev, point, reverse, options.stepped);
                prev = point;
            }
            if (loop) {
                point = points[(start + (reverse ? ilen : 0)) % count];
                lineMethod(ctx, prev, point, reverse, options.stepped);
            }
            return !!loop;
        }
        function fastPathSegment(ctx, line, segment, params) {
            const points = line.points;
            const {count, start, ilen} = pathVars(points, segment, params);
            const {move = true, reverse} = params || {};
            let avgX = 0;
            let countX = 0;
            let i, point, prevX, minY, maxY, lastY;
            const pointIndex = index => (start + (reverse ? ilen - index : index)) % count;
            const drawX = () => {
                if (minY !== maxY) {
                    ctx.lineTo(avgX, maxY);
                    ctx.lineTo(avgX, minY);
                    ctx.lineTo(avgX, lastY);
                }
            };
            if (move) {
                point = points[pointIndex(0)];
                ctx.moveTo(point.x, point.y);
            }
            for (i = 0; i <= ilen; ++i) {
                point = points[pointIndex(i)];
                if (point.skip) continue;
                const x = point.x;
                const y = point.y;
                const truncX = 0 | x;
                if (truncX === prevX) {
                    if (y < minY) minY = y; else if (y > maxY) maxY = y;
                    avgX = (countX * avgX + x) / ++countX;
                } else {
                    drawX();
                    ctx.lineTo(x, y);
                    prevX = truncX;
                    countX = 0;
                    minY = maxY = y;
                }
                lastY = y;
            }
            drawX();
        }
        function _getSegmentMethod(line) {
            const opts = line.options;
            const borderDash = opts.borderDash && opts.borderDash.length;
            const useFastPath = !line._decimated && !line._loop && !opts.tension && "monotone" !== opts.cubicInterpolationMode && !opts.stepped && !borderDash;
            return useFastPath ? fastPathSegment : pathSegment;
        }
        function _getInterpolationMethod(options) {
            if (options.stepped) return _steppedInterpolation;
            if (options.tension || "monotone" === options.cubicInterpolationMode) return _bezierInterpolation;
            return _pointInLine;
        }
        function strokePathWithCache(ctx, line, start, count) {
            let path = line._path;
            if (!path) {
                path = line._path = new Path2D;
                if (line.path(path, start, count)) path.closePath();
            }
            setStyle(ctx, line.options);
            ctx.stroke(path);
        }
        function strokePathDirect(ctx, line, start, count) {
            const {segments, options} = line;
            const segmentMethod = _getSegmentMethod(line);
            for (const segment of segments) {
                setStyle(ctx, options, segment.style);
                ctx.beginPath();
                if (segmentMethod(ctx, line, segment, {
                    start,
                    end: start + count - 1
                })) ctx.closePath();
                ctx.stroke();
            }
        }
        const usePath2D = "function" === typeof Path2D;
        function draw(ctx, line, start, count) {
            if (usePath2D && !line.options.segment) strokePathWithCache(ctx, line, start, count); else strokePathDirect(ctx, line, start, count);
        }
        class LineElement extends chart_esm_Element {
            constructor(cfg) {
                super();
                this.animated = true;
                this.options = void 0;
                this._chart = void 0;
                this._loop = void 0;
                this._fullLoop = void 0;
                this._path = void 0;
                this._points = void 0;
                this._segments = void 0;
                this._decimated = false;
                this._pointsUpdated = false;
                this._datasetIndex = void 0;
                if (cfg) Object.assign(this, cfg);
            }
            updateControlPoints(chartArea, indexAxis) {
                const options = this.options;
                if ((options.tension || "monotone" === options.cubicInterpolationMode) && !options.stepped && !this._pointsUpdated) {
                    const loop = options.spanGaps ? this._loop : this._fullLoop;
                    _updateBezierControlPoints(this._points, options, chartArea, loop, indexAxis);
                    this._pointsUpdated = true;
                }
            }
            set points(points) {
                this._points = points;
                delete this._segments;
                delete this._path;
                this._pointsUpdated = false;
            }
            get points() {
                return this._points;
            }
            get segments() {
                return this._segments || (this._segments = _computeSegments(this, this.options.segment));
            }
            first() {
                const segments = this.segments;
                const points = this.points;
                return segments.length && points[segments[0].start];
            }
            last() {
                const segments = this.segments;
                const points = this.points;
                const count = segments.length;
                return count && points[segments[count - 1].end];
            }
            interpolate(point, property) {
                const options = this.options;
                const value = point[property];
                const points = this.points;
                const segments = _boundSegments(this, {
                    property,
                    start: value,
                    end: value
                });
                if (!segments.length) return;
                const result = [];
                const _interpolate = _getInterpolationMethod(options);
                let i, ilen;
                for (i = 0, ilen = segments.length; i < ilen; ++i) {
                    const {start, end} = segments[i];
                    const p1 = points[start];
                    const p2 = points[end];
                    if (p1 === p2) {
                        result.push(p1);
                        continue;
                    }
                    const t = Math.abs((value - p1[property]) / (p2[property] - p1[property]));
                    const interpolated = _interpolate(p1, p2, t, options.stepped);
                    interpolated[property] = point[property];
                    result.push(interpolated);
                }
                return 1 === result.length ? result[0] : result;
            }
            pathSegment(ctx, segment, params) {
                const segmentMethod = _getSegmentMethod(this);
                return segmentMethod(ctx, this, segment, params);
            }
            path(ctx, start, count) {
                const segments = this.segments;
                const segmentMethod = _getSegmentMethod(this);
                let loop = this._loop;
                start = start || 0;
                count = count || this.points.length - start;
                for (const segment of segments) loop &= segmentMethod(ctx, this, segment, {
                    start,
                    end: start + count - 1
                });
                return !!loop;
            }
            draw(ctx, chartArea, start, count) {
                const options = this.options || {};
                const points = this.points || [];
                if (points.length && options.borderWidth) {
                    ctx.save();
                    draw(ctx, this, start, count);
                    ctx.restore();
                }
                if (this.animated) {
                    this._pointsUpdated = false;
                    this._path = void 0;
                }
            }
        }
        LineElement.id = "line";
        LineElement.defaults = {
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0,
            borderJoinStyle: "miter",
            borderWidth: 3,
            capBezierPoints: true,
            cubicInterpolationMode: "default",
            fill: false,
            spanGaps: false,
            stepped: false,
            tension: 0
        };
        LineElement.defaultRoutes = {
            backgroundColor: "backgroundColor",
            borderColor: "borderColor"
        };
        LineElement.descriptors = {
            _scriptable: true,
            _indexable: name => "borderDash" !== name && "fill" !== name
        };
        function inRange$1(el, pos, axis, useFinalPosition) {
            const options = el.options;
            const {[axis]: value} = el.getProps([ axis ], useFinalPosition);
            return Math.abs(pos - value) < options.radius + options.hitRadius;
        }
        class PointElement extends chart_esm_Element {
            constructor(cfg) {
                super();
                this.options = void 0;
                this.parsed = void 0;
                this.skip = void 0;
                this.stop = void 0;
                if (cfg) Object.assign(this, cfg);
            }
            inRange(mouseX, mouseY, useFinalPosition) {
                const options = this.options;
                const {x, y} = this.getProps([ "x", "y" ], useFinalPosition);
                return Math.pow(mouseX - x, 2) + Math.pow(mouseY - y, 2) < Math.pow(options.hitRadius + options.radius, 2);
            }
            inXRange(mouseX, useFinalPosition) {
                return inRange$1(this, mouseX, "x", useFinalPosition);
            }
            inYRange(mouseY, useFinalPosition) {
                return inRange$1(this, mouseY, "y", useFinalPosition);
            }
            getCenterPoint(useFinalPosition) {
                const {x, y} = this.getProps([ "x", "y" ], useFinalPosition);
                return {
                    x,
                    y
                };
            }
            size(options) {
                options = options || this.options || {};
                let radius = options.radius || 0;
                radius = Math.max(radius, radius && options.hoverRadius || 0);
                const borderWidth = radius && options.borderWidth || 0;
                return 2 * (radius + borderWidth);
            }
            draw(ctx, area) {
                const options = this.options;
                if (this.skip || options.radius < .1 || !_isPointInArea(this, area, this.size(options) / 2)) return;
                ctx.strokeStyle = options.borderColor;
                ctx.lineWidth = options.borderWidth;
                ctx.fillStyle = options.backgroundColor;
                drawPoint(ctx, options, this.x, this.y);
            }
            getRange() {
                const options = this.options || {};
                return options.radius + options.hitRadius;
            }
        }
        PointElement.id = "point";
        PointElement.defaults = {
            borderWidth: 1,
            hitRadius: 1,
            hoverBorderWidth: 1,
            hoverRadius: 4,
            pointStyle: "circle",
            radius: 3,
            rotation: 0
        };
        PointElement.defaultRoutes = {
            backgroundColor: "backgroundColor",
            borderColor: "borderColor"
        };
        function getBarBounds(bar, useFinalPosition) {
            const {x, y, base, width, height} = bar.getProps([ "x", "y", "base", "width", "height" ], useFinalPosition);
            let left, right, top, bottom, half;
            if (bar.horizontal) {
                half = height / 2;
                left = Math.min(x, base);
                right = Math.max(x, base);
                top = y - half;
                bottom = y + half;
            } else {
                half = width / 2;
                left = x - half;
                right = x + half;
                top = Math.min(y, base);
                bottom = Math.max(y, base);
            }
            return {
                left,
                top,
                right,
                bottom
            };
        }
        function skipOrLimit(skip, value, min, max) {
            return skip ? 0 : _limitValue(value, min, max);
        }
        function parseBorderWidth(bar, maxW, maxH) {
            const value = bar.options.borderWidth;
            const skip = bar.borderSkipped;
            const o = toTRBL(value);
            return {
                t: skipOrLimit(skip.top, o.top, 0, maxH),
                r: skipOrLimit(skip.right, o.right, 0, maxW),
                b: skipOrLimit(skip.bottom, o.bottom, 0, maxH),
                l: skipOrLimit(skip.left, o.left, 0, maxW)
            };
        }
        function parseBorderRadius(bar, maxW, maxH) {
            const {enableBorderRadius} = bar.getProps([ "enableBorderRadius" ]);
            const value = bar.options.borderRadius;
            const o = toTRBLCorners(value);
            const maxR = Math.min(maxW, maxH);
            const skip = bar.borderSkipped;
            const enableBorder = enableBorderRadius || isObject(value);
            return {
                topLeft: skipOrLimit(!enableBorder || skip.top || skip.left, o.topLeft, 0, maxR),
                topRight: skipOrLimit(!enableBorder || skip.top || skip.right, o.topRight, 0, maxR),
                bottomLeft: skipOrLimit(!enableBorder || skip.bottom || skip.left, o.bottomLeft, 0, maxR),
                bottomRight: skipOrLimit(!enableBorder || skip.bottom || skip.right, o.bottomRight, 0, maxR)
            };
        }
        function boundingRects(bar) {
            const bounds = getBarBounds(bar);
            const width = bounds.right - bounds.left;
            const height = bounds.bottom - bounds.top;
            const border = parseBorderWidth(bar, width / 2, height / 2);
            const radius = parseBorderRadius(bar, width / 2, height / 2);
            return {
                outer: {
                    x: bounds.left,
                    y: bounds.top,
                    w: width,
                    h: height,
                    radius
                },
                inner: {
                    x: bounds.left + border.l,
                    y: bounds.top + border.t,
                    w: width - border.l - border.r,
                    h: height - border.t - border.b,
                    radius: {
                        topLeft: Math.max(0, radius.topLeft - Math.max(border.t, border.l)),
                        topRight: Math.max(0, radius.topRight - Math.max(border.t, border.r)),
                        bottomLeft: Math.max(0, radius.bottomLeft - Math.max(border.b, border.l)),
                        bottomRight: Math.max(0, radius.bottomRight - Math.max(border.b, border.r))
                    }
                }
            };
        }
        function inRange(bar, x, y, useFinalPosition) {
            const skipX = null === x;
            const skipY = null === y;
            const skipBoth = skipX && skipY;
            const bounds = bar && !skipBoth && getBarBounds(bar, useFinalPosition);
            return bounds && (skipX || _isBetween(x, bounds.left, bounds.right)) && (skipY || _isBetween(y, bounds.top, bounds.bottom));
        }
        function hasRadius(radius) {
            return radius.topLeft || radius.topRight || radius.bottomLeft || radius.bottomRight;
        }
        function addNormalRectPath(ctx, rect) {
            ctx.rect(rect.x, rect.y, rect.w, rect.h);
        }
        function inflateRect(rect, amount, refRect = {}) {
            const x = rect.x !== refRect.x ? -amount : 0;
            const y = rect.y !== refRect.y ? -amount : 0;
            const w = (rect.x + rect.w !== refRect.x + refRect.w ? amount : 0) - x;
            const h = (rect.y + rect.h !== refRect.y + refRect.h ? amount : 0) - y;
            return {
                x: rect.x + x,
                y: rect.y + y,
                w: rect.w + w,
                h: rect.h + h,
                radius: rect.radius
            };
        }
        class BarElement extends chart_esm_Element {
            constructor(cfg) {
                super();
                this.options = void 0;
                this.horizontal = void 0;
                this.base = void 0;
                this.width = void 0;
                this.height = void 0;
                this.inflateAmount = void 0;
                if (cfg) Object.assign(this, cfg);
            }
            draw(ctx) {
                const {inflateAmount, options: {borderColor, backgroundColor}} = this;
                const {inner, outer} = boundingRects(this);
                const addRectPath = hasRadius(outer.radius) ? addRoundedRectPath : addNormalRectPath;
                ctx.save();
                if (outer.w !== inner.w || outer.h !== inner.h) {
                    ctx.beginPath();
                    addRectPath(ctx, inflateRect(outer, inflateAmount, inner));
                    ctx.clip();
                    addRectPath(ctx, inflateRect(inner, -inflateAmount, outer));
                    ctx.fillStyle = borderColor;
                    ctx.fill("evenodd");
                }
                ctx.beginPath();
                addRectPath(ctx, inflateRect(inner, inflateAmount));
                ctx.fillStyle = backgroundColor;
                ctx.fill();
                ctx.restore();
            }
            inRange(mouseX, mouseY, useFinalPosition) {
                return inRange(this, mouseX, mouseY, useFinalPosition);
            }
            inXRange(mouseX, useFinalPosition) {
                return inRange(this, mouseX, null, useFinalPosition);
            }
            inYRange(mouseY, useFinalPosition) {
                return inRange(this, null, mouseY, useFinalPosition);
            }
            getCenterPoint(useFinalPosition) {
                const {x, y, base, horizontal} = this.getProps([ "x", "y", "base", "horizontal" ], useFinalPosition);
                return {
                    x: horizontal ? (x + base) / 2 : x,
                    y: horizontal ? y : (y + base) / 2
                };
            }
            getRange(axis) {
                return "x" === axis ? this.width / 2 : this.height / 2;
            }
        }
        BarElement.id = "bar";
        BarElement.defaults = {
            borderSkipped: "start",
            borderWidth: 0,
            borderRadius: 0,
            inflateAmount: "auto",
            pointStyle: void 0
        };
        BarElement.defaultRoutes = {
            backgroundColor: "backgroundColor",
            borderColor: "borderColor"
        };
        function lttbDecimation(data, start, count, availableWidth, options) {
            const samples = options.samples || availableWidth;
            if (samples >= count) return data.slice(start, start + count);
            const decimated = [];
            const bucketWidth = (count - 2) / (samples - 2);
            let sampledIndex = 0;
            const endIndex = start + count - 1;
            let a = start;
            let i, maxAreaPoint, maxArea, area, nextA;
            decimated[sampledIndex++] = data[a];
            for (i = 0; i < samples - 2; i++) {
                let avgX = 0;
                let avgY = 0;
                let j;
                const avgRangeStart = Math.floor((i + 1) * bucketWidth) + 1 + start;
                const avgRangeEnd = Math.min(Math.floor((i + 2) * bucketWidth) + 1, count) + start;
                const avgRangeLength = avgRangeEnd - avgRangeStart;
                for (j = avgRangeStart; j < avgRangeEnd; j++) {
                    avgX += data[j].x;
                    avgY += data[j].y;
                }
                avgX /= avgRangeLength;
                avgY /= avgRangeLength;
                const rangeOffs = Math.floor(i * bucketWidth) + 1 + start;
                const rangeTo = Math.min(Math.floor((i + 1) * bucketWidth) + 1, count) + start;
                const {x: pointAx, y: pointAy} = data[a];
                maxArea = area = -1;
                for (j = rangeOffs; j < rangeTo; j++) {
                    area = .5 * Math.abs((pointAx - avgX) * (data[j].y - pointAy) - (pointAx - data[j].x) * (avgY - pointAy));
                    if (area > maxArea) {
                        maxArea = area;
                        maxAreaPoint = data[j];
                        nextA = j;
                    }
                }
                decimated[sampledIndex++] = maxAreaPoint;
                a = nextA;
            }
            decimated[sampledIndex++] = data[endIndex];
            return decimated;
        }
        function minMaxDecimation(data, start, count, availableWidth) {
            let avgX = 0;
            let countX = 0;
            let i, point, x, y, prevX, minIndex, maxIndex, startIndex, minY, maxY;
            const decimated = [];
            const endIndex = start + count - 1;
            const xMin = data[start].x;
            const xMax = data[endIndex].x;
            const dx = xMax - xMin;
            for (i = start; i < start + count; ++i) {
                point = data[i];
                x = (point.x - xMin) / dx * availableWidth;
                y = point.y;
                const truncX = 0 | x;
                if (truncX === prevX) {
                    if (y < minY) {
                        minY = y;
                        minIndex = i;
                    } else if (y > maxY) {
                        maxY = y;
                        maxIndex = i;
                    }
                    avgX = (countX * avgX + point.x) / ++countX;
                } else {
                    const lastIndex = i - 1;
                    if (!isNullOrUndef(minIndex) && !isNullOrUndef(maxIndex)) {
                        const intermediateIndex1 = Math.min(minIndex, maxIndex);
                        const intermediateIndex2 = Math.max(minIndex, maxIndex);
                        if (intermediateIndex1 !== startIndex && intermediateIndex1 !== lastIndex) decimated.push({
                            ...data[intermediateIndex1],
                            x: avgX
                        });
                        if (intermediateIndex2 !== startIndex && intermediateIndex2 !== lastIndex) decimated.push({
                            ...data[intermediateIndex2],
                            x: avgX
                        });
                    }
                    if (i > 0 && lastIndex !== startIndex) decimated.push(data[lastIndex]);
                    decimated.push(point);
                    prevX = truncX;
                    countX = 0;
                    minY = maxY = y;
                    minIndex = maxIndex = startIndex = i;
                }
            }
            return decimated;
        }
        function cleanDecimatedDataset(dataset) {
            if (dataset._decimated) {
                const data = dataset._data;
                delete dataset._decimated;
                delete dataset._data;
                Object.defineProperty(dataset, "data", {
                    value: data
                });
            }
        }
        function cleanDecimatedData(chart) {
            chart.data.datasets.forEach((dataset => {
                cleanDecimatedDataset(dataset);
            }));
        }
        function getStartAndCountOfVisiblePointsSimplified(meta, points) {
            const pointCount = points.length;
            let start = 0;
            let count;
            const {iScale} = meta;
            const {min, max, minDefined, maxDefined} = iScale.getUserBounds();
            if (minDefined) start = _limitValue(_lookupByKey(points, iScale.axis, min).lo, 0, pointCount - 1);
            if (maxDefined) count = _limitValue(_lookupByKey(points, iScale.axis, max).hi + 1, start, pointCount) - start; else count = pointCount - start;
            return {
                start,
                count
            };
        }
        var plugin_decimation = {
            id: "decimation",
            defaults: {
                algorithm: "min-max",
                enabled: false
            },
            beforeElementsUpdate: (chart, args, options) => {
                if (!options.enabled) {
                    cleanDecimatedData(chart);
                    return;
                }
                const availableWidth = chart.width;
                chart.data.datasets.forEach(((dataset, datasetIndex) => {
                    const {_data, indexAxis} = dataset;
                    const meta = chart.getDatasetMeta(datasetIndex);
                    const data = _data || dataset.data;
                    if ("y" === resolve([ indexAxis, chart.options.indexAxis ])) return;
                    if ("line" !== meta.type) return;
                    const xAxis = chart.scales[meta.xAxisID];
                    if ("linear" !== xAxis.type && "time" !== xAxis.type) return;
                    if (chart.options.parsing) return;
                    let {start, count} = getStartAndCountOfVisiblePointsSimplified(meta, data);
                    const threshold = options.threshold || 4 * availableWidth;
                    if (count <= threshold) {
                        cleanDecimatedDataset(dataset);
                        return;
                    }
                    if (isNullOrUndef(_data)) {
                        dataset._data = data;
                        delete dataset.data;
                        Object.defineProperty(dataset, "data", {
                            configurable: true,
                            enumerable: true,
                            get: function() {
                                return this._decimated;
                            },
                            set: function(d) {
                                this._data = d;
                            }
                        });
                    }
                    let decimated;
                    switch (options.algorithm) {
                      case "lttb":
                        decimated = lttbDecimation(data, start, count, availableWidth, options);
                        break;

                      case "min-max":
                        decimated = minMaxDecimation(data, start, count, availableWidth);
                        break;

                      default:
                        throw new Error(`Unsupported decimation algorithm '${options.algorithm}'`);
                    }
                    dataset._decimated = decimated;
                }));
            },
            destroy(chart) {
                cleanDecimatedData(chart);
            }
        };
        function getLineByIndex(chart, index) {
            const meta = chart.getDatasetMeta(index);
            const visible = meta && chart.isDatasetVisible(index);
            return visible ? meta.dataset : null;
        }
        function parseFillOption(line) {
            const options = line.options;
            const fillOption = options.fill;
            let fill = valueOrDefault(fillOption && fillOption.target, fillOption);
            if (void 0 === fill) fill = !!options.backgroundColor;
            if (false === fill || null === fill) return false;
            if (true === fill) return "origin";
            return fill;
        }
        function decodeFill(line, index, count) {
            const fill = parseFillOption(line);
            if (isObject(fill)) return isNaN(fill.value) ? false : fill;
            let target = parseFloat(fill);
            if (isNumberFinite(target) && Math.floor(target) === target) {
                if ("-" === fill[0] || "+" === fill[0]) target = index + target;
                if (target === index || target < 0 || target >= count) return false;
                return target;
            }
            return [ "origin", "start", "end", "stack", "shape" ].indexOf(fill) >= 0 && fill;
        }
        function computeLinearBoundary(source) {
            const {scale = {}, fill} = source;
            let target = null;
            let horizontal;
            if ("start" === fill) target = scale.bottom; else if ("end" === fill) target = scale.top; else if (isObject(fill)) target = scale.getPixelForValue(fill.value); else if (scale.getBasePixel) target = scale.getBasePixel();
            if (isNumberFinite(target)) {
                horizontal = scale.isHorizontal();
                return {
                    x: horizontal ? target : null,
                    y: horizontal ? null : target
                };
            }
            return null;
        }
        class simpleArc {
            constructor(opts) {
                this.x = opts.x;
                this.y = opts.y;
                this.radius = opts.radius;
            }
            pathSegment(ctx, bounds, opts) {
                const {x, y, radius} = this;
                bounds = bounds || {
                    start: 0,
                    end: TAU
                };
                ctx.arc(x, y, radius, bounds.end, bounds.start, true);
                return !opts.bounds;
            }
            interpolate(point) {
                const {x, y, radius} = this;
                const angle = point.angle;
                return {
                    x: x + Math.cos(angle) * radius,
                    y: y + Math.sin(angle) * radius,
                    angle
                };
            }
        }
        function computeCircularBoundary(source) {
            const {scale, fill} = source;
            const options = scale.options;
            const length = scale.getLabels().length;
            const target = [];
            const start = options.reverse ? scale.max : scale.min;
            const end = options.reverse ? scale.min : scale.max;
            let i, center, value;
            if ("start" === fill) value = start; else if ("end" === fill) value = end; else if (isObject(fill)) value = fill.value; else value = scale.getBaseValue();
            if (options.grid.circular) {
                center = scale.getPointPositionForValue(0, start);
                return new simpleArc({
                    x: center.x,
                    y: center.y,
                    radius: scale.getDistanceFromCenterForValue(value)
                });
            }
            for (i = 0; i < length; ++i) target.push(scale.getPointPositionForValue(i, value));
            return target;
        }
        function computeBoundary(source) {
            const scale = source.scale || {};
            if (scale.getPointPositionForValue) return computeCircularBoundary(source);
            return computeLinearBoundary(source);
        }
        function findSegmentEnd(start, end, points) {
            for (;end > start; end--) {
                const point = points[end];
                if (!isNaN(point.x) && !isNaN(point.y)) break;
            }
            return end;
        }
        function pointsFromSegments(boundary, line) {
            const {x = null, y = null} = boundary || {};
            const linePoints = line.points;
            const points = [];
            line.segments.forEach((({start, end}) => {
                end = findSegmentEnd(start, end, linePoints);
                const first = linePoints[start];
                const last = linePoints[end];
                if (null !== y) {
                    points.push({
                        x: first.x,
                        y
                    });
                    points.push({
                        x: last.x,
                        y
                    });
                } else if (null !== x) {
                    points.push({
                        x,
                        y: first.y
                    });
                    points.push({
                        x,
                        y: last.y
                    });
                }
            }));
            return points;
        }
        function buildStackLine(source) {
            const {scale, index, line} = source;
            const points = [];
            const segments = line.segments;
            const sourcePoints = line.points;
            const linesBelow = getLinesBelow(scale, index);
            linesBelow.push(createBoundaryLine({
                x: null,
                y: scale.bottom
            }, line));
            for (let i = 0; i < segments.length; i++) {
                const segment = segments[i];
                for (let j = segment.start; j <= segment.end; j++) addPointsBelow(points, sourcePoints[j], linesBelow);
            }
            return new LineElement({
                points,
                options: {}
            });
        }
        function getLinesBelow(scale, index) {
            const below = [];
            const metas = scale.getMatchingVisibleMetas("line");
            for (let i = 0; i < metas.length; i++) {
                const meta = metas[i];
                if (meta.index === index) break;
                if (!meta.hidden) below.unshift(meta.dataset);
            }
            return below;
        }
        function addPointsBelow(points, sourcePoint, linesBelow) {
            const postponed = [];
            for (let j = 0; j < linesBelow.length; j++) {
                const line = linesBelow[j];
                const {first, last, point} = findPoint(line, sourcePoint, "x");
                if (!point || first && last) continue;
                if (first) postponed.unshift(point); else {
                    points.push(point);
                    if (!last) break;
                }
            }
            points.push(...postponed);
        }
        function findPoint(line, sourcePoint, property) {
            const point = line.interpolate(sourcePoint, property);
            if (!point) return {};
            const pointValue = point[property];
            const segments = line.segments;
            const linePoints = line.points;
            let first = false;
            let last = false;
            for (let i = 0; i < segments.length; i++) {
                const segment = segments[i];
                const firstValue = linePoints[segment.start][property];
                const lastValue = linePoints[segment.end][property];
                if (_isBetween(pointValue, firstValue, lastValue)) {
                    first = pointValue === firstValue;
                    last = pointValue === lastValue;
                    break;
                }
            }
            return {
                first,
                last,
                point
            };
        }
        function getTarget(source) {
            const {chart, fill, line} = source;
            if (isNumberFinite(fill)) return getLineByIndex(chart, fill);
            if ("stack" === fill) return buildStackLine(source);
            if ("shape" === fill) return true;
            const boundary = computeBoundary(source);
            if (boundary instanceof simpleArc) return boundary;
            return createBoundaryLine(boundary, line);
        }
        function createBoundaryLine(boundary, line) {
            let points = [];
            let _loop = false;
            if (isArray(boundary)) {
                _loop = true;
                points = boundary;
            } else points = pointsFromSegments(boundary, line);
            return points.length ? new LineElement({
                points,
                options: {
                    tension: 0
                },
                _loop,
                _fullLoop: _loop
            }) : null;
        }
        function resolveTarget(sources, index, propagate) {
            const source = sources[index];
            let fill = source.fill;
            const visited = [ index ];
            let target;
            if (!propagate) return fill;
            while (false !== fill && -1 === visited.indexOf(fill)) {
                if (!isNumberFinite(fill)) return fill;
                target = sources[fill];
                if (!target) return false;
                if (target.visible) return fill;
                visited.push(fill);
                fill = target.fill;
            }
            return false;
        }
        function _clip(ctx, target, clipY) {
            ctx.beginPath();
            target.path(ctx);
            ctx.lineTo(target.last().x, clipY);
            ctx.lineTo(target.first().x, clipY);
            ctx.closePath();
            ctx.clip();
        }
        function getBounds(property, first, last, loop) {
            if (loop) return;
            let start = first[property];
            let end = last[property];
            if ("angle" === property) {
                start = _normalizeAngle(start);
                end = _normalizeAngle(end);
            }
            return {
                property,
                start,
                end
            };
        }
        function _getEdge(a, b, prop, fn) {
            if (a && b) return fn(a[prop], b[prop]);
            return a ? a[prop] : b ? b[prop] : 0;
        }
        function _segments(line, target, property) {
            const segments = line.segments;
            const points = line.points;
            const tpoints = target.points;
            const parts = [];
            for (const segment of segments) {
                let {start, end} = segment;
                end = findSegmentEnd(start, end, points);
                const bounds = getBounds(property, points[start], points[end], segment.loop);
                if (!target.segments) {
                    parts.push({
                        source: segment,
                        target: bounds,
                        start: points[start],
                        end: points[end]
                    });
                    continue;
                }
                const targetSegments = _boundSegments(target, bounds);
                for (const tgt of targetSegments) {
                    const subBounds = getBounds(property, tpoints[tgt.start], tpoints[tgt.end], tgt.loop);
                    const fillSources = _boundSegment(segment, points, subBounds);
                    for (const fillSource of fillSources) parts.push({
                        source: fillSource,
                        target: tgt,
                        start: {
                            [property]: _getEdge(bounds, subBounds, "start", Math.max)
                        },
                        end: {
                            [property]: _getEdge(bounds, subBounds, "end", Math.min)
                        }
                    });
                }
            }
            return parts;
        }
        function clipBounds(ctx, scale, bounds) {
            const {top, bottom} = scale.chart.chartArea;
            const {property, start, end} = bounds || {};
            if ("x" === property) {
                ctx.beginPath();
                ctx.rect(start, top, end - start, bottom - top);
                ctx.clip();
            }
        }
        function interpolatedLineTo(ctx, target, point, property) {
            const interpolatedPoint = target.interpolate(point, property);
            if (interpolatedPoint) ctx.lineTo(interpolatedPoint.x, interpolatedPoint.y);
        }
        function _fill(ctx, cfg) {
            const {line, target, property, color, scale} = cfg;
            const segments = _segments(line, target, property);
            for (const {source: src, target: tgt, start, end} of segments) {
                const {style: {backgroundColor = color} = {}} = src;
                const notShape = true !== target;
                ctx.save();
                ctx.fillStyle = backgroundColor;
                clipBounds(ctx, scale, notShape && getBounds(property, start, end));
                ctx.beginPath();
                const lineLoop = !!line.pathSegment(ctx, src);
                let loop;
                if (notShape) {
                    if (lineLoop) ctx.closePath(); else interpolatedLineTo(ctx, target, end, property);
                    const targetLoop = !!target.pathSegment(ctx, tgt, {
                        move: lineLoop,
                        reverse: true
                    });
                    loop = lineLoop && targetLoop;
                    if (!loop) interpolatedLineTo(ctx, target, start, property);
                }
                ctx.closePath();
                ctx.fill(loop ? "evenodd" : "nonzero");
                ctx.restore();
            }
        }
        function doFill(ctx, cfg) {
            const {line, target, above, below, area, scale} = cfg;
            const property = line._loop ? "angle" : cfg.axis;
            ctx.save();
            if ("x" === property && below !== above) {
                _clip(ctx, target, area.top);
                _fill(ctx, {
                    line,
                    target,
                    color: above,
                    scale,
                    property
                });
                ctx.restore();
                ctx.save();
                _clip(ctx, target, area.bottom);
            }
            _fill(ctx, {
                line,
                target,
                color: below,
                scale,
                property
            });
            ctx.restore();
        }
        function drawfill(ctx, source, area) {
            const target = getTarget(source);
            const {line, scale, axis} = source;
            const lineOpts = line.options;
            const fillOption = lineOpts.fill;
            const color = lineOpts.backgroundColor;
            const {above = color, below = color} = fillOption || {};
            if (target && line.points.length) {
                clipArea(ctx, area);
                doFill(ctx, {
                    line,
                    target,
                    above,
                    below,
                    area,
                    scale,
                    axis
                });
                unclipArea(ctx);
            }
        }
        var plugin_filler = {
            id: "filler",
            afterDatasetsUpdate(chart, _args, options) {
                const count = (chart.data.datasets || []).length;
                const sources = [];
                let meta, i, line, source;
                for (i = 0; i < count; ++i) {
                    meta = chart.getDatasetMeta(i);
                    line = meta.dataset;
                    source = null;
                    if (line && line.options && line instanceof LineElement) source = {
                        visible: chart.isDatasetVisible(i),
                        index: i,
                        fill: decodeFill(line, i, count),
                        chart,
                        axis: meta.controller.options.indexAxis,
                        scale: meta.vScale,
                        line
                    };
                    meta.$filler = source;
                    sources.push(source);
                }
                for (i = 0; i < count; ++i) {
                    source = sources[i];
                    if (!source || false === source.fill) continue;
                    source.fill = resolveTarget(sources, i, options.propagate);
                }
            },
            beforeDraw(chart, _args, options) {
                const draw = "beforeDraw" === options.drawTime;
                const metasets = chart.getSortedVisibleDatasetMetas();
                const area = chart.chartArea;
                for (let i = metasets.length - 1; i >= 0; --i) {
                    const source = metasets[i].$filler;
                    if (!source) continue;
                    source.line.updateControlPoints(area, source.axis);
                    if (draw) drawfill(chart.ctx, source, area);
                }
            },
            beforeDatasetsDraw(chart, _args, options) {
                if ("beforeDatasetsDraw" !== options.drawTime) return;
                const metasets = chart.getSortedVisibleDatasetMetas();
                for (let i = metasets.length - 1; i >= 0; --i) {
                    const source = metasets[i].$filler;
                    if (source) drawfill(chart.ctx, source, chart.chartArea);
                }
            },
            beforeDatasetDraw(chart, args, options) {
                const source = args.meta.$filler;
                if (!source || false === source.fill || "beforeDatasetDraw" !== options.drawTime) return;
                drawfill(chart.ctx, source, chart.chartArea);
            },
            defaults: {
                propagate: true,
                drawTime: "beforeDatasetDraw"
            }
        };
        const getBoxSize = (labelOpts, fontSize) => {
            let {boxHeight = fontSize, boxWidth = fontSize} = labelOpts;
            if (labelOpts.usePointStyle) {
                boxHeight = Math.min(boxHeight, fontSize);
                boxWidth = Math.min(boxWidth, fontSize);
            }
            return {
                boxWidth,
                boxHeight,
                itemHeight: Math.max(fontSize, boxHeight)
            };
        };
        const itemsEqual = (a, b) => null !== a && null !== b && a.datasetIndex === b.datasetIndex && a.index === b.index;
        class Legend extends chart_esm_Element {
            constructor(config) {
                super();
                this._added = false;
                this.legendHitBoxes = [];
                this._hoveredItem = null;
                this.doughnutMode = false;
                this.chart = config.chart;
                this.options = config.options;
                this.ctx = config.ctx;
                this.legendItems = void 0;
                this.columnSizes = void 0;
                this.lineWidths = void 0;
                this.maxHeight = void 0;
                this.maxWidth = void 0;
                this.top = void 0;
                this.bottom = void 0;
                this.left = void 0;
                this.right = void 0;
                this.height = void 0;
                this.width = void 0;
                this._margins = void 0;
                this.position = void 0;
                this.weight = void 0;
                this.fullSize = void 0;
            }
            update(maxWidth, maxHeight, margins) {
                this.maxWidth = maxWidth;
                this.maxHeight = maxHeight;
                this._margins = margins;
                this.setDimensions();
                this.buildLabels();
                this.fit();
            }
            setDimensions() {
                if (this.isHorizontal()) {
                    this.width = this.maxWidth;
                    this.left = this._margins.left;
                    this.right = this.width;
                } else {
                    this.height = this.maxHeight;
                    this.top = this._margins.top;
                    this.bottom = this.height;
                }
            }
            buildLabels() {
                const labelOpts = this.options.labels || {};
                let legendItems = callback(labelOpts.generateLabels, [ this.chart ], this) || [];
                if (labelOpts.filter) legendItems = legendItems.filter((item => labelOpts.filter(item, this.chart.data)));
                if (labelOpts.sort) legendItems = legendItems.sort(((a, b) => labelOpts.sort(a, b, this.chart.data)));
                if (this.options.reverse) legendItems.reverse();
                this.legendItems = legendItems;
            }
            fit() {
                const {options, ctx} = this;
                if (!options.display) {
                    this.width = this.height = 0;
                    return;
                }
                const labelOpts = options.labels;
                const labelFont = toFont(labelOpts.font);
                const fontSize = labelFont.size;
                const titleHeight = this._computeTitleHeight();
                const {boxWidth, itemHeight} = getBoxSize(labelOpts, fontSize);
                let width, height;
                ctx.font = labelFont.string;
                if (this.isHorizontal()) {
                    width = this.maxWidth;
                    height = this._fitRows(titleHeight, fontSize, boxWidth, itemHeight) + 10;
                } else {
                    height = this.maxHeight;
                    width = this._fitCols(titleHeight, fontSize, boxWidth, itemHeight) + 10;
                }
                this.width = Math.min(width, options.maxWidth || this.maxWidth);
                this.height = Math.min(height, options.maxHeight || this.maxHeight);
            }
            _fitRows(titleHeight, fontSize, boxWidth, itemHeight) {
                const {ctx, maxWidth, options: {labels: {padding}}} = this;
                const hitboxes = this.legendHitBoxes = [];
                const lineWidths = this.lineWidths = [ 0 ];
                const lineHeight = itemHeight + padding;
                let totalHeight = titleHeight;
                ctx.textAlign = "left";
                ctx.textBaseline = "middle";
                let row = -1;
                let top = -lineHeight;
                this.legendItems.forEach(((legendItem, i) => {
                    const itemWidth = boxWidth + fontSize / 2 + ctx.measureText(legendItem.text).width;
                    if (0 === i || lineWidths[lineWidths.length - 1] + itemWidth + 2 * padding > maxWidth) {
                        totalHeight += lineHeight;
                        lineWidths[lineWidths.length - (i > 0 ? 0 : 1)] = 0;
                        top += lineHeight;
                        row++;
                    }
                    hitboxes[i] = {
                        left: 0,
                        top,
                        row,
                        width: itemWidth,
                        height: itemHeight
                    };
                    lineWidths[lineWidths.length - 1] += itemWidth + padding;
                }));
                return totalHeight;
            }
            _fitCols(titleHeight, fontSize, boxWidth, itemHeight) {
                const {ctx, maxHeight, options: {labels: {padding}}} = this;
                const hitboxes = this.legendHitBoxes = [];
                const columnSizes = this.columnSizes = [];
                const heightLimit = maxHeight - titleHeight;
                let totalWidth = padding;
                let currentColWidth = 0;
                let currentColHeight = 0;
                let left = 0;
                let col = 0;
                this.legendItems.forEach(((legendItem, i) => {
                    const itemWidth = boxWidth + fontSize / 2 + ctx.measureText(legendItem.text).width;
                    if (i > 0 && currentColHeight + itemHeight + 2 * padding > heightLimit) {
                        totalWidth += currentColWidth + padding;
                        columnSizes.push({
                            width: currentColWidth,
                            height: currentColHeight
                        });
                        left += currentColWidth + padding;
                        col++;
                        currentColWidth = currentColHeight = 0;
                    }
                    hitboxes[i] = {
                        left,
                        top: currentColHeight,
                        col,
                        width: itemWidth,
                        height: itemHeight
                    };
                    currentColWidth = Math.max(currentColWidth, itemWidth);
                    currentColHeight += itemHeight + padding;
                }));
                totalWidth += currentColWidth;
                columnSizes.push({
                    width: currentColWidth,
                    height: currentColHeight
                });
                return totalWidth;
            }
            adjustHitBoxes() {
                if (!this.options.display) return;
                const titleHeight = this._computeTitleHeight();
                const {legendHitBoxes: hitboxes, options: {align, labels: {padding}, rtl}} = this;
                const rtlHelper = getRtlAdapter(rtl, this.left, this.width);
                if (this.isHorizontal()) {
                    let row = 0;
                    let left = _alignStartEnd(align, this.left + padding, this.right - this.lineWidths[row]);
                    for (const hitbox of hitboxes) {
                        if (row !== hitbox.row) {
                            row = hitbox.row;
                            left = _alignStartEnd(align, this.left + padding, this.right - this.lineWidths[row]);
                        }
                        hitbox.top += this.top + titleHeight + padding;
                        hitbox.left = rtlHelper.leftForLtr(rtlHelper.x(left), hitbox.width);
                        left += hitbox.width + padding;
                    }
                } else {
                    let col = 0;
                    let top = _alignStartEnd(align, this.top + titleHeight + padding, this.bottom - this.columnSizes[col].height);
                    for (const hitbox of hitboxes) {
                        if (hitbox.col !== col) {
                            col = hitbox.col;
                            top = _alignStartEnd(align, this.top + titleHeight + padding, this.bottom - this.columnSizes[col].height);
                        }
                        hitbox.top = top;
                        hitbox.left += this.left + padding;
                        hitbox.left = rtlHelper.leftForLtr(rtlHelper.x(hitbox.left), hitbox.width);
                        top += hitbox.height + padding;
                    }
                }
            }
            isHorizontal() {
                return "top" === this.options.position || "bottom" === this.options.position;
            }
            draw() {
                if (this.options.display) {
                    const ctx = this.ctx;
                    clipArea(ctx, this);
                    this._draw();
                    unclipArea(ctx);
                }
            }
            _draw() {
                const {options: opts, columnSizes, lineWidths, ctx} = this;
                const {align, labels: labelOpts} = opts;
                const defaultColor = defaults.color;
                const rtlHelper = getRtlAdapter(opts.rtl, this.left, this.width);
                const labelFont = toFont(labelOpts.font);
                const {color: fontColor, padding} = labelOpts;
                const fontSize = labelFont.size;
                const halfFontSize = fontSize / 2;
                let cursor;
                this.drawTitle();
                ctx.textAlign = rtlHelper.textAlign("left");
                ctx.textBaseline = "middle";
                ctx.lineWidth = .5;
                ctx.font = labelFont.string;
                const {boxWidth, boxHeight, itemHeight} = getBoxSize(labelOpts, fontSize);
                const drawLegendBox = function(x, y, legendItem) {
                    if (isNaN(boxWidth) || boxWidth <= 0 || isNaN(boxHeight) || boxHeight < 0) return;
                    ctx.save();
                    const lineWidth = valueOrDefault(legendItem.lineWidth, 1);
                    ctx.fillStyle = valueOrDefault(legendItem.fillStyle, defaultColor);
                    ctx.lineCap = valueOrDefault(legendItem.lineCap, "butt");
                    ctx.lineDashOffset = valueOrDefault(legendItem.lineDashOffset, 0);
                    ctx.lineJoin = valueOrDefault(legendItem.lineJoin, "miter");
                    ctx.lineWidth = lineWidth;
                    ctx.strokeStyle = valueOrDefault(legendItem.strokeStyle, defaultColor);
                    ctx.setLineDash(valueOrDefault(legendItem.lineDash, []));
                    if (labelOpts.usePointStyle) {
                        const drawOptions = {
                            radius: boxWidth * Math.SQRT2 / 2,
                            pointStyle: legendItem.pointStyle,
                            rotation: legendItem.rotation,
                            borderWidth: lineWidth
                        };
                        const centerX = rtlHelper.xPlus(x, boxWidth / 2);
                        const centerY = y + halfFontSize;
                        drawPoint(ctx, drawOptions, centerX, centerY);
                    } else {
                        const yBoxTop = y + Math.max((fontSize - boxHeight) / 2, 0);
                        const xBoxLeft = rtlHelper.leftForLtr(x, boxWidth);
                        const borderRadius = toTRBLCorners(legendItem.borderRadius);
                        ctx.beginPath();
                        if (Object.values(borderRadius).some((v => 0 !== v))) addRoundedRectPath(ctx, {
                            x: xBoxLeft,
                            y: yBoxTop,
                            w: boxWidth,
                            h: boxHeight,
                            radius: borderRadius
                        }); else ctx.rect(xBoxLeft, yBoxTop, boxWidth, boxHeight);
                        ctx.fill();
                        if (0 !== lineWidth) ctx.stroke();
                    }
                    ctx.restore();
                };
                const fillText = function(x, y, legendItem) {
                    renderText(ctx, legendItem.text, x, y + itemHeight / 2, labelFont, {
                        strikethrough: legendItem.hidden,
                        textAlign: rtlHelper.textAlign(legendItem.textAlign)
                    });
                };
                const isHorizontal = this.isHorizontal();
                const titleHeight = this._computeTitleHeight();
                if (isHorizontal) cursor = {
                    x: _alignStartEnd(align, this.left + padding, this.right - lineWidths[0]),
                    y: this.top + padding + titleHeight,
                    line: 0
                }; else cursor = {
                    x: this.left + padding,
                    y: _alignStartEnd(align, this.top + titleHeight + padding, this.bottom - columnSizes[0].height),
                    line: 0
                };
                overrideTextDirection(this.ctx, opts.textDirection);
                const lineHeight = itemHeight + padding;
                this.legendItems.forEach(((legendItem, i) => {
                    ctx.strokeStyle = legendItem.fontColor || fontColor;
                    ctx.fillStyle = legendItem.fontColor || fontColor;
                    const textWidth = ctx.measureText(legendItem.text).width;
                    const textAlign = rtlHelper.textAlign(legendItem.textAlign || (legendItem.textAlign = labelOpts.textAlign));
                    const width = boxWidth + halfFontSize + textWidth;
                    let x = cursor.x;
                    let y = cursor.y;
                    rtlHelper.setWidth(this.width);
                    if (isHorizontal) {
                        if (i > 0 && x + width + padding > this.right) {
                            y = cursor.y += lineHeight;
                            cursor.line++;
                            x = cursor.x = _alignStartEnd(align, this.left + padding, this.right - lineWidths[cursor.line]);
                        }
                    } else if (i > 0 && y + lineHeight > this.bottom) {
                        x = cursor.x = x + columnSizes[cursor.line].width + padding;
                        cursor.line++;
                        y = cursor.y = _alignStartEnd(align, this.top + titleHeight + padding, this.bottom - columnSizes[cursor.line].height);
                    }
                    const realX = rtlHelper.x(x);
                    drawLegendBox(realX, y, legendItem);
                    x = _textX(textAlign, x + boxWidth + halfFontSize, isHorizontal ? x + width : this.right, opts.rtl);
                    fillText(rtlHelper.x(x), y, legendItem);
                    if (isHorizontal) cursor.x += width + padding; else cursor.y += lineHeight;
                }));
                restoreTextDirection(this.ctx, opts.textDirection);
            }
            drawTitle() {
                const opts = this.options;
                const titleOpts = opts.title;
                const titleFont = toFont(titleOpts.font);
                const titlePadding = toPadding(titleOpts.padding);
                if (!titleOpts.display) return;
                const rtlHelper = getRtlAdapter(opts.rtl, this.left, this.width);
                const ctx = this.ctx;
                const position = titleOpts.position;
                const halfFontSize = titleFont.size / 2;
                const topPaddingPlusHalfFontSize = titlePadding.top + halfFontSize;
                let y;
                let left = this.left;
                let maxWidth = this.width;
                if (this.isHorizontal()) {
                    maxWidth = Math.max(...this.lineWidths);
                    y = this.top + topPaddingPlusHalfFontSize;
                    left = _alignStartEnd(opts.align, left, this.right - maxWidth);
                } else {
                    const maxHeight = this.columnSizes.reduce(((acc, size) => Math.max(acc, size.height)), 0);
                    y = topPaddingPlusHalfFontSize + _alignStartEnd(opts.align, this.top, this.bottom - maxHeight - opts.labels.padding - this._computeTitleHeight());
                }
                const x = _alignStartEnd(position, left, left + maxWidth);
                ctx.textAlign = rtlHelper.textAlign(_toLeftRightCenter(position));
                ctx.textBaseline = "middle";
                ctx.strokeStyle = titleOpts.color;
                ctx.fillStyle = titleOpts.color;
                ctx.font = titleFont.string;
                renderText(ctx, titleOpts.text, x, y, titleFont);
            }
            _computeTitleHeight() {
                const titleOpts = this.options.title;
                const titleFont = toFont(titleOpts.font);
                const titlePadding = toPadding(titleOpts.padding);
                return titleOpts.display ? titleFont.lineHeight + titlePadding.height : 0;
            }
            _getLegendItemAt(x, y) {
                let i, hitBox, lh;
                if (_isBetween(x, this.left, this.right) && _isBetween(y, this.top, this.bottom)) {
                    lh = this.legendHitBoxes;
                    for (i = 0; i < lh.length; ++i) {
                        hitBox = lh[i];
                        if (_isBetween(x, hitBox.left, hitBox.left + hitBox.width) && _isBetween(y, hitBox.top, hitBox.top + hitBox.height)) return this.legendItems[i];
                    }
                }
                return null;
            }
            handleEvent(e) {
                const opts = this.options;
                if (!isListened(e.type, opts)) return;
                const hoveredItem = this._getLegendItemAt(e.x, e.y);
                if ("mousemove" === e.type) {
                    const previous = this._hoveredItem;
                    const sameItem = itemsEqual(previous, hoveredItem);
                    if (previous && !sameItem) callback(opts.onLeave, [ e, previous, this ], this);
                    this._hoveredItem = hoveredItem;
                    if (hoveredItem && !sameItem) callback(opts.onHover, [ e, hoveredItem, this ], this);
                } else if (hoveredItem) callback(opts.onClick, [ e, hoveredItem, this ], this);
            }
        }
        function isListened(type, opts) {
            if ("mousemove" === type && (opts.onHover || opts.onLeave)) return true;
            if (opts.onClick && ("click" === type || "mouseup" === type)) return true;
            return false;
        }
        var plugin_legend = {
            id: "legend",
            _element: Legend,
            start(chart, _args, options) {
                const legend = chart.legend = new Legend({
                    ctx: chart.ctx,
                    options,
                    chart
                });
                layouts.configure(chart, legend, options);
                layouts.addBox(chart, legend);
            },
            stop(chart) {
                layouts.removeBox(chart, chart.legend);
                delete chart.legend;
            },
            beforeUpdate(chart, _args, options) {
                const legend = chart.legend;
                layouts.configure(chart, legend, options);
                legend.options = options;
            },
            afterUpdate(chart) {
                const legend = chart.legend;
                legend.buildLabels();
                legend.adjustHitBoxes();
            },
            afterEvent(chart, args) {
                if (!args.replay) chart.legend.handleEvent(args.event);
            },
            defaults: {
                display: true,
                position: "top",
                align: "center",
                fullSize: true,
                reverse: false,
                weight: 1e3,
                onClick(e, legendItem, legend) {
                    const index = legendItem.datasetIndex;
                    const ci = legend.chart;
                    if (ci.isDatasetVisible(index)) {
                        ci.hide(index);
                        legendItem.hidden = true;
                    } else {
                        ci.show(index);
                        legendItem.hidden = false;
                    }
                },
                onHover: null,
                onLeave: null,
                labels: {
                    color: ctx => ctx.chart.options.color,
                    boxWidth: 40,
                    padding: 10,
                    generateLabels(chart) {
                        const datasets = chart.data.datasets;
                        const {labels: {usePointStyle, pointStyle, textAlign, color}} = chart.legend.options;
                        return chart._getSortedDatasetMetas().map((meta => {
                            const style = meta.controller.getStyle(usePointStyle ? 0 : void 0);
                            const borderWidth = toPadding(style.borderWidth);
                            return {
                                text: datasets[meta.index].label,
                                fillStyle: style.backgroundColor,
                                fontColor: color,
                                hidden: !meta.visible,
                                lineCap: style.borderCapStyle,
                                lineDash: style.borderDash,
                                lineDashOffset: style.borderDashOffset,
                                lineJoin: style.borderJoinStyle,
                                lineWidth: (borderWidth.width + borderWidth.height) / 4,
                                strokeStyle: style.borderColor,
                                pointStyle: pointStyle || style.pointStyle,
                                rotation: style.rotation,
                                textAlign: textAlign || style.textAlign,
                                borderRadius: 0,
                                datasetIndex: meta.index
                            };
                        }), this);
                    }
                },
                title: {
                    color: ctx => ctx.chart.options.color,
                    display: false,
                    position: "center",
                    text: ""
                }
            },
            descriptors: {
                _scriptable: name => !name.startsWith("on"),
                labels: {
                    _scriptable: name => ![ "generateLabels", "filter", "sort" ].includes(name)
                }
            }
        };
        class Title extends chart_esm_Element {
            constructor(config) {
                super();
                this.chart = config.chart;
                this.options = config.options;
                this.ctx = config.ctx;
                this._padding = void 0;
                this.top = void 0;
                this.bottom = void 0;
                this.left = void 0;
                this.right = void 0;
                this.width = void 0;
                this.height = void 0;
                this.position = void 0;
                this.weight = void 0;
                this.fullSize = void 0;
            }
            update(maxWidth, maxHeight) {
                const opts = this.options;
                this.left = 0;
                this.top = 0;
                if (!opts.display) {
                    this.width = this.height = this.right = this.bottom = 0;
                    return;
                }
                this.width = this.right = maxWidth;
                this.height = this.bottom = maxHeight;
                const lineCount = isArray(opts.text) ? opts.text.length : 1;
                this._padding = toPadding(opts.padding);
                const textSize = lineCount * toFont(opts.font).lineHeight + this._padding.height;
                if (this.isHorizontal()) this.height = textSize; else this.width = textSize;
            }
            isHorizontal() {
                const pos = this.options.position;
                return "top" === pos || "bottom" === pos;
            }
            _drawArgs(offset) {
                const {top, left, bottom, right, options} = this;
                const align = options.align;
                let rotation = 0;
                let maxWidth, titleX, titleY;
                if (this.isHorizontal()) {
                    titleX = _alignStartEnd(align, left, right);
                    titleY = top + offset;
                    maxWidth = right - left;
                } else {
                    if ("left" === options.position) {
                        titleX = left + offset;
                        titleY = _alignStartEnd(align, bottom, top);
                        rotation = -.5 * PI;
                    } else {
                        titleX = right - offset;
                        titleY = _alignStartEnd(align, top, bottom);
                        rotation = .5 * PI;
                    }
                    maxWidth = bottom - top;
                }
                return {
                    titleX,
                    titleY,
                    maxWidth,
                    rotation
                };
            }
            draw() {
                const ctx = this.ctx;
                const opts = this.options;
                if (!opts.display) return;
                const fontOpts = toFont(opts.font);
                const lineHeight = fontOpts.lineHeight;
                const offset = lineHeight / 2 + this._padding.top;
                const {titleX, titleY, maxWidth, rotation} = this._drawArgs(offset);
                renderText(ctx, opts.text, 0, 0, fontOpts, {
                    color: opts.color,
                    maxWidth,
                    rotation,
                    textAlign: _toLeftRightCenter(opts.align),
                    textBaseline: "middle",
                    translation: [ titleX, titleY ]
                });
            }
        }
        function createTitle(chart, titleOpts) {
            const title = new Title({
                ctx: chart.ctx,
                options: titleOpts,
                chart
            });
            layouts.configure(chart, title, titleOpts);
            layouts.addBox(chart, title);
            chart.titleBlock = title;
        }
        var plugin_title = {
            id: "title",
            _element: Title,
            start(chart, _args, options) {
                createTitle(chart, options);
            },
            stop(chart) {
                const titleBlock = chart.titleBlock;
                layouts.removeBox(chart, titleBlock);
                delete chart.titleBlock;
            },
            beforeUpdate(chart, _args, options) {
                const title = chart.titleBlock;
                layouts.configure(chart, title, options);
                title.options = options;
            },
            defaults: {
                align: "center",
                display: false,
                font: {
                    weight: "bold"
                },
                fullSize: true,
                padding: 10,
                position: "top",
                text: "",
                weight: 2e3
            },
            defaultRoutes: {
                color: "color"
            },
            descriptors: {
                _scriptable: true,
                _indexable: false
            }
        };
        const chart_esm_map = new WeakMap;
        var plugin_subtitle = {
            id: "subtitle",
            start(chart, _args, options) {
                const title = new Title({
                    ctx: chart.ctx,
                    options,
                    chart
                });
                layouts.configure(chart, title, options);
                layouts.addBox(chart, title);
                chart_esm_map.set(chart, title);
            },
            stop(chart) {
                layouts.removeBox(chart, chart_esm_map.get(chart));
                chart_esm_map.delete(chart);
            },
            beforeUpdate(chart, _args, options) {
                const title = chart_esm_map.get(chart);
                layouts.configure(chart, title, options);
                title.options = options;
            },
            defaults: {
                align: "center",
                display: false,
                font: {
                    weight: "normal"
                },
                fullSize: true,
                padding: 0,
                position: "top",
                text: "",
                weight: 1500
            },
            defaultRoutes: {
                color: "color"
            },
            descriptors: {
                _scriptable: true,
                _indexable: false
            }
        };
        const positioners = {
            average(items) {
                if (!items.length) return false;
                let i, len;
                let x = 0;
                let y = 0;
                let count = 0;
                for (i = 0, len = items.length; i < len; ++i) {
                    const el = items[i].element;
                    if (el && el.hasValue()) {
                        const pos = el.tooltipPosition();
                        x += pos.x;
                        y += pos.y;
                        ++count;
                    }
                }
                return {
                    x: x / count,
                    y: y / count
                };
            },
            nearest(items, eventPosition) {
                if (!items.length) return false;
                let x = eventPosition.x;
                let y = eventPosition.y;
                let minDistance = Number.POSITIVE_INFINITY;
                let i, len, nearestElement;
                for (i = 0, len = items.length; i < len; ++i) {
                    const el = items[i].element;
                    if (el && el.hasValue()) {
                        const center = el.getCenterPoint();
                        const d = distanceBetweenPoints(eventPosition, center);
                        if (d < minDistance) {
                            minDistance = d;
                            nearestElement = el;
                        }
                    }
                }
                if (nearestElement) {
                    const tp = nearestElement.tooltipPosition();
                    x = tp.x;
                    y = tp.y;
                }
                return {
                    x,
                    y
                };
            }
        };
        function pushOrConcat(base, toPush) {
            if (toPush) if (isArray(toPush)) Array.prototype.push.apply(base, toPush); else base.push(toPush);
            return base;
        }
        function splitNewlines(str) {
            if (("string" === typeof str || str instanceof String) && str.indexOf("\n") > -1) return str.split("\n");
            return str;
        }
        function createTooltipItem(chart, item) {
            const {element, datasetIndex, index} = item;
            const controller = chart.getDatasetMeta(datasetIndex).controller;
            const {label, value} = controller.getLabelAndValue(index);
            return {
                chart,
                label,
                parsed: controller.getParsed(index),
                raw: chart.data.datasets[datasetIndex].data[index],
                formattedValue: value,
                dataset: controller.getDataset(),
                dataIndex: index,
                datasetIndex,
                element
            };
        }
        function getTooltipSize(tooltip, options) {
            const ctx = tooltip.chart.ctx;
            const {body, footer, title} = tooltip;
            const {boxWidth, boxHeight} = options;
            const bodyFont = toFont(options.bodyFont);
            const titleFont = toFont(options.titleFont);
            const footerFont = toFont(options.footerFont);
            const titleLineCount = title.length;
            const footerLineCount = footer.length;
            const bodyLineItemCount = body.length;
            const padding = toPadding(options.padding);
            let height = padding.height;
            let width = 0;
            let combinedBodyLength = body.reduce(((count, bodyItem) => count + bodyItem.before.length + bodyItem.lines.length + bodyItem.after.length), 0);
            combinedBodyLength += tooltip.beforeBody.length + tooltip.afterBody.length;
            if (titleLineCount) height += titleLineCount * titleFont.lineHeight + (titleLineCount - 1) * options.titleSpacing + options.titleMarginBottom;
            if (combinedBodyLength) {
                const bodyLineHeight = options.displayColors ? Math.max(boxHeight, bodyFont.lineHeight) : bodyFont.lineHeight;
                height += bodyLineItemCount * bodyLineHeight + (combinedBodyLength - bodyLineItemCount) * bodyFont.lineHeight + (combinedBodyLength - 1) * options.bodySpacing;
            }
            if (footerLineCount) height += options.footerMarginTop + footerLineCount * footerFont.lineHeight + (footerLineCount - 1) * options.footerSpacing;
            let widthPadding = 0;
            const maxLineWidth = function(line) {
                width = Math.max(width, ctx.measureText(line).width + widthPadding);
            };
            ctx.save();
            ctx.font = titleFont.string;
            each(tooltip.title, maxLineWidth);
            ctx.font = bodyFont.string;
            each(tooltip.beforeBody.concat(tooltip.afterBody), maxLineWidth);
            widthPadding = options.displayColors ? boxWidth + 2 + options.boxPadding : 0;
            each(body, (bodyItem => {
                each(bodyItem.before, maxLineWidth);
                each(bodyItem.lines, maxLineWidth);
                each(bodyItem.after, maxLineWidth);
            }));
            widthPadding = 0;
            ctx.font = footerFont.string;
            each(tooltip.footer, maxLineWidth);
            ctx.restore();
            width += padding.width;
            return {
                width,
                height
            };
        }
        function determineYAlign(chart, size) {
            const {y, height} = size;
            if (y < height / 2) return "top"; else if (y > chart.height - height / 2) return "bottom";
            return "center";
        }
        function doesNotFitWithAlign(xAlign, chart, options, size) {
            const {x, width} = size;
            const caret = options.caretSize + options.caretPadding;
            if ("left" === xAlign && x + width + caret > chart.width) return true;
            if ("right" === xAlign && x - width - caret < 0) return true;
        }
        function determineXAlign(chart, options, size, yAlign) {
            const {x, width} = size;
            const {width: chartWidth, chartArea: {left, right}} = chart;
            let xAlign = "center";
            if ("center" === yAlign) xAlign = x <= (left + right) / 2 ? "left" : "right"; else if (x <= width / 2) xAlign = "left"; else if (x >= chartWidth - width / 2) xAlign = "right";
            if (doesNotFitWithAlign(xAlign, chart, options, size)) xAlign = "center";
            return xAlign;
        }
        function determineAlignment(chart, options, size) {
            const yAlign = size.yAlign || options.yAlign || determineYAlign(chart, size);
            return {
                xAlign: size.xAlign || options.xAlign || determineXAlign(chart, options, size, yAlign),
                yAlign
            };
        }
        function alignX(size, xAlign) {
            let {x, width} = size;
            if ("right" === xAlign) x -= width; else if ("center" === xAlign) x -= width / 2;
            return x;
        }
        function alignY(size, yAlign, paddingAndSize) {
            let {y, height} = size;
            if ("top" === yAlign) y += paddingAndSize; else if ("bottom" === yAlign) y -= height + paddingAndSize; else y -= height / 2;
            return y;
        }
        function getBackgroundPoint(options, size, alignment, chart) {
            const {caretSize, caretPadding, cornerRadius} = options;
            const {xAlign, yAlign} = alignment;
            const paddingAndSize = caretSize + caretPadding;
            const {topLeft, topRight, bottomLeft, bottomRight} = toTRBLCorners(cornerRadius);
            let x = alignX(size, xAlign);
            const y = alignY(size, yAlign, paddingAndSize);
            if ("center" === yAlign) {
                if ("left" === xAlign) x += paddingAndSize; else if ("right" === xAlign) x -= paddingAndSize;
            } else if ("left" === xAlign) x -= Math.max(topLeft, bottomLeft) + caretSize; else if ("right" === xAlign) x += Math.max(topRight, bottomRight) + caretSize;
            return {
                x: _limitValue(x, 0, chart.width - size.width),
                y: _limitValue(y, 0, chart.height - size.height)
            };
        }
        function getAlignedX(tooltip, align, options) {
            const padding = toPadding(options.padding);
            return "center" === align ? tooltip.x + tooltip.width / 2 : "right" === align ? tooltip.x + tooltip.width - padding.right : tooltip.x + padding.left;
        }
        function getBeforeAfterBodyLines(callback) {
            return pushOrConcat([], splitNewlines(callback));
        }
        function createTooltipContext(parent, tooltip, tooltipItems) {
            return createContext(parent, {
                tooltip,
                tooltipItems,
                type: "tooltip"
            });
        }
        function overrideCallbacks(callbacks, context) {
            const override = context && context.dataset && context.dataset.tooltip && context.dataset.tooltip.callbacks;
            return override ? callbacks.override(override) : callbacks;
        }
        class Tooltip extends chart_esm_Element {
            constructor(config) {
                super();
                this.opacity = 0;
                this._active = [];
                this._eventPosition = void 0;
                this._size = void 0;
                this._cachedAnimations = void 0;
                this._tooltipItems = [];
                this.$animations = void 0;
                this.$context = void 0;
                this.chart = config.chart || config._chart;
                this._chart = this.chart;
                this.options = config.options;
                this.dataPoints = void 0;
                this.title = void 0;
                this.beforeBody = void 0;
                this.body = void 0;
                this.afterBody = void 0;
                this.footer = void 0;
                this.xAlign = void 0;
                this.yAlign = void 0;
                this.x = void 0;
                this.y = void 0;
                this.height = void 0;
                this.width = void 0;
                this.caretX = void 0;
                this.caretY = void 0;
                this.labelColors = void 0;
                this.labelPointStyles = void 0;
                this.labelTextColors = void 0;
            }
            initialize(options) {
                this.options = options;
                this._cachedAnimations = void 0;
                this.$context = void 0;
            }
            _resolveAnimations() {
                const cached = this._cachedAnimations;
                if (cached) return cached;
                const chart = this.chart;
                const options = this.options.setContext(this.getContext());
                const opts = options.enabled && chart.options.animation && options.animations;
                const animations = new Animations(this.chart, opts);
                if (opts._cacheable) this._cachedAnimations = Object.freeze(animations);
                return animations;
            }
            getContext() {
                return this.$context || (this.$context = createTooltipContext(this.chart.getContext(), this, this._tooltipItems));
            }
            getTitle(context, options) {
                const {callbacks} = options;
                const beforeTitle = callbacks.beforeTitle.apply(this, [ context ]);
                const title = callbacks.title.apply(this, [ context ]);
                const afterTitle = callbacks.afterTitle.apply(this, [ context ]);
                let lines = [];
                lines = pushOrConcat(lines, splitNewlines(beforeTitle));
                lines = pushOrConcat(lines, splitNewlines(title));
                lines = pushOrConcat(lines, splitNewlines(afterTitle));
                return lines;
            }
            getBeforeBody(tooltipItems, options) {
                return getBeforeAfterBodyLines(options.callbacks.beforeBody.apply(this, [ tooltipItems ]));
            }
            getBody(tooltipItems, options) {
                const {callbacks} = options;
                const bodyItems = [];
                each(tooltipItems, (context => {
                    const bodyItem = {
                        before: [],
                        lines: [],
                        after: []
                    };
                    const scoped = overrideCallbacks(callbacks, context);
                    pushOrConcat(bodyItem.before, splitNewlines(scoped.beforeLabel.call(this, context)));
                    pushOrConcat(bodyItem.lines, scoped.label.call(this, context));
                    pushOrConcat(bodyItem.after, splitNewlines(scoped.afterLabel.call(this, context)));
                    bodyItems.push(bodyItem);
                }));
                return bodyItems;
            }
            getAfterBody(tooltipItems, options) {
                return getBeforeAfterBodyLines(options.callbacks.afterBody.apply(this, [ tooltipItems ]));
            }
            getFooter(tooltipItems, options) {
                const {callbacks} = options;
                const beforeFooter = callbacks.beforeFooter.apply(this, [ tooltipItems ]);
                const footer = callbacks.footer.apply(this, [ tooltipItems ]);
                const afterFooter = callbacks.afterFooter.apply(this, [ tooltipItems ]);
                let lines = [];
                lines = pushOrConcat(lines, splitNewlines(beforeFooter));
                lines = pushOrConcat(lines, splitNewlines(footer));
                lines = pushOrConcat(lines, splitNewlines(afterFooter));
                return lines;
            }
            _createItems(options) {
                const active = this._active;
                const data = this.chart.data;
                const labelColors = [];
                const labelPointStyles = [];
                const labelTextColors = [];
                let tooltipItems = [];
                let i, len;
                for (i = 0, len = active.length; i < len; ++i) tooltipItems.push(createTooltipItem(this.chart, active[i]));
                if (options.filter) tooltipItems = tooltipItems.filter(((element, index, array) => options.filter(element, index, array, data)));
                if (options.itemSort) tooltipItems = tooltipItems.sort(((a, b) => options.itemSort(a, b, data)));
                each(tooltipItems, (context => {
                    const scoped = overrideCallbacks(options.callbacks, context);
                    labelColors.push(scoped.labelColor.call(this, context));
                    labelPointStyles.push(scoped.labelPointStyle.call(this, context));
                    labelTextColors.push(scoped.labelTextColor.call(this, context));
                }));
                this.labelColors = labelColors;
                this.labelPointStyles = labelPointStyles;
                this.labelTextColors = labelTextColors;
                this.dataPoints = tooltipItems;
                return tooltipItems;
            }
            update(changed, replay) {
                const options = this.options.setContext(this.getContext());
                const active = this._active;
                let properties;
                let tooltipItems = [];
                if (!active.length) {
                    if (0 !== this.opacity) properties = {
                        opacity: 0
                    };
                } else {
                    const position = positioners[options.position].call(this, active, this._eventPosition);
                    tooltipItems = this._createItems(options);
                    this.title = this.getTitle(tooltipItems, options);
                    this.beforeBody = this.getBeforeBody(tooltipItems, options);
                    this.body = this.getBody(tooltipItems, options);
                    this.afterBody = this.getAfterBody(tooltipItems, options);
                    this.footer = this.getFooter(tooltipItems, options);
                    const size = this._size = getTooltipSize(this, options);
                    const positionAndSize = Object.assign({}, position, size);
                    const alignment = determineAlignment(this.chart, options, positionAndSize);
                    const backgroundPoint = getBackgroundPoint(options, positionAndSize, alignment, this.chart);
                    this.xAlign = alignment.xAlign;
                    this.yAlign = alignment.yAlign;
                    properties = {
                        opacity: 1,
                        x: backgroundPoint.x,
                        y: backgroundPoint.y,
                        width: size.width,
                        height: size.height,
                        caretX: position.x,
                        caretY: position.y
                    };
                }
                this._tooltipItems = tooltipItems;
                this.$context = void 0;
                if (properties) this._resolveAnimations().update(this, properties);
                if (changed && options.external) options.external.call(this, {
                    chart: this.chart,
                    tooltip: this,
                    replay
                });
            }
            drawCaret(tooltipPoint, ctx, size, options) {
                const caretPosition = this.getCaretPosition(tooltipPoint, size, options);
                ctx.lineTo(caretPosition.x1, caretPosition.y1);
                ctx.lineTo(caretPosition.x2, caretPosition.y2);
                ctx.lineTo(caretPosition.x3, caretPosition.y3);
            }
            getCaretPosition(tooltipPoint, size, options) {
                const {xAlign, yAlign} = this;
                const {caretSize, cornerRadius} = options;
                const {topLeft, topRight, bottomLeft, bottomRight} = toTRBLCorners(cornerRadius);
                const {x: ptX, y: ptY} = tooltipPoint;
                const {width, height} = size;
                let x1, x2, x3, y1, y2, y3;
                if ("center" === yAlign) {
                    y2 = ptY + height / 2;
                    if ("left" === xAlign) {
                        x1 = ptX;
                        x2 = x1 - caretSize;
                        y1 = y2 + caretSize;
                        y3 = y2 - caretSize;
                    } else {
                        x1 = ptX + width;
                        x2 = x1 + caretSize;
                        y1 = y2 - caretSize;
                        y3 = y2 + caretSize;
                    }
                    x3 = x1;
                } else {
                    if ("left" === xAlign) x2 = ptX + Math.max(topLeft, bottomLeft) + caretSize; else if ("right" === xAlign) x2 = ptX + width - Math.max(topRight, bottomRight) - caretSize; else x2 = this.caretX;
                    if ("top" === yAlign) {
                        y1 = ptY;
                        y2 = y1 - caretSize;
                        x1 = x2 - caretSize;
                        x3 = x2 + caretSize;
                    } else {
                        y1 = ptY + height;
                        y2 = y1 + caretSize;
                        x1 = x2 + caretSize;
                        x3 = x2 - caretSize;
                    }
                    y3 = y1;
                }
                return {
                    x1,
                    x2,
                    x3,
                    y1,
                    y2,
                    y3
                };
            }
            drawTitle(pt, ctx, options) {
                const title = this.title;
                const length = title.length;
                let titleFont, titleSpacing, i;
                if (length) {
                    const rtlHelper = getRtlAdapter(options.rtl, this.x, this.width);
                    pt.x = getAlignedX(this, options.titleAlign, options);
                    ctx.textAlign = rtlHelper.textAlign(options.titleAlign);
                    ctx.textBaseline = "middle";
                    titleFont = toFont(options.titleFont);
                    titleSpacing = options.titleSpacing;
                    ctx.fillStyle = options.titleColor;
                    ctx.font = titleFont.string;
                    for (i = 0; i < length; ++i) {
                        ctx.fillText(title[i], rtlHelper.x(pt.x), pt.y + titleFont.lineHeight / 2);
                        pt.y += titleFont.lineHeight + titleSpacing;
                        if (i + 1 === length) pt.y += options.titleMarginBottom - titleSpacing;
                    }
                }
            }
            _drawColorBox(ctx, pt, i, rtlHelper, options) {
                const labelColors = this.labelColors[i];
                const labelPointStyle = this.labelPointStyles[i];
                const {boxHeight, boxWidth, boxPadding} = options;
                const bodyFont = toFont(options.bodyFont);
                const colorX = getAlignedX(this, "left", options);
                const rtlColorX = rtlHelper.x(colorX);
                const yOffSet = boxHeight < bodyFont.lineHeight ? (bodyFont.lineHeight - boxHeight) / 2 : 0;
                const colorY = pt.y + yOffSet;
                if (options.usePointStyle) {
                    const drawOptions = {
                        radius: Math.min(boxWidth, boxHeight) / 2,
                        pointStyle: labelPointStyle.pointStyle,
                        rotation: labelPointStyle.rotation,
                        borderWidth: 1
                    };
                    const centerX = rtlHelper.leftForLtr(rtlColorX, boxWidth) + boxWidth / 2;
                    const centerY = colorY + boxHeight / 2;
                    ctx.strokeStyle = options.multiKeyBackground;
                    ctx.fillStyle = options.multiKeyBackground;
                    drawPoint(ctx, drawOptions, centerX, centerY);
                    ctx.strokeStyle = labelColors.borderColor;
                    ctx.fillStyle = labelColors.backgroundColor;
                    drawPoint(ctx, drawOptions, centerX, centerY);
                } else {
                    ctx.lineWidth = labelColors.borderWidth || 1;
                    ctx.strokeStyle = labelColors.borderColor;
                    ctx.setLineDash(labelColors.borderDash || []);
                    ctx.lineDashOffset = labelColors.borderDashOffset || 0;
                    const outerX = rtlHelper.leftForLtr(rtlColorX, boxWidth - boxPadding);
                    const innerX = rtlHelper.leftForLtr(rtlHelper.xPlus(rtlColorX, 1), boxWidth - boxPadding - 2);
                    const borderRadius = toTRBLCorners(labelColors.borderRadius);
                    if (Object.values(borderRadius).some((v => 0 !== v))) {
                        ctx.beginPath();
                        ctx.fillStyle = options.multiKeyBackground;
                        addRoundedRectPath(ctx, {
                            x: outerX,
                            y: colorY,
                            w: boxWidth,
                            h: boxHeight,
                            radius: borderRadius
                        });
                        ctx.fill();
                        ctx.stroke();
                        ctx.fillStyle = labelColors.backgroundColor;
                        ctx.beginPath();
                        addRoundedRectPath(ctx, {
                            x: innerX,
                            y: colorY + 1,
                            w: boxWidth - 2,
                            h: boxHeight - 2,
                            radius: borderRadius
                        });
                        ctx.fill();
                    } else {
                        ctx.fillStyle = options.multiKeyBackground;
                        ctx.fillRect(outerX, colorY, boxWidth, boxHeight);
                        ctx.strokeRect(outerX, colorY, boxWidth, boxHeight);
                        ctx.fillStyle = labelColors.backgroundColor;
                        ctx.fillRect(innerX, colorY + 1, boxWidth - 2, boxHeight - 2);
                    }
                }
                ctx.fillStyle = this.labelTextColors[i];
            }
            drawBody(pt, ctx, options) {
                const {body} = this;
                const {bodySpacing, bodyAlign, displayColors, boxHeight, boxWidth, boxPadding} = options;
                const bodyFont = toFont(options.bodyFont);
                let bodyLineHeight = bodyFont.lineHeight;
                let xLinePadding = 0;
                const rtlHelper = getRtlAdapter(options.rtl, this.x, this.width);
                const fillLineOfText = function(line) {
                    ctx.fillText(line, rtlHelper.x(pt.x + xLinePadding), pt.y + bodyLineHeight / 2);
                    pt.y += bodyLineHeight + bodySpacing;
                };
                const bodyAlignForCalculation = rtlHelper.textAlign(bodyAlign);
                let bodyItem, textColor, lines, i, j, ilen, jlen;
                ctx.textAlign = bodyAlign;
                ctx.textBaseline = "middle";
                ctx.font = bodyFont.string;
                pt.x = getAlignedX(this, bodyAlignForCalculation, options);
                ctx.fillStyle = options.bodyColor;
                each(this.beforeBody, fillLineOfText);
                xLinePadding = displayColors && "right" !== bodyAlignForCalculation ? "center" === bodyAlign ? boxWidth / 2 + boxPadding : boxWidth + 2 + boxPadding : 0;
                for (i = 0, ilen = body.length; i < ilen; ++i) {
                    bodyItem = body[i];
                    textColor = this.labelTextColors[i];
                    ctx.fillStyle = textColor;
                    each(bodyItem.before, fillLineOfText);
                    lines = bodyItem.lines;
                    if (displayColors && lines.length) {
                        this._drawColorBox(ctx, pt, i, rtlHelper, options);
                        bodyLineHeight = Math.max(bodyFont.lineHeight, boxHeight);
                    }
                    for (j = 0, jlen = lines.length; j < jlen; ++j) {
                        fillLineOfText(lines[j]);
                        bodyLineHeight = bodyFont.lineHeight;
                    }
                    each(bodyItem.after, fillLineOfText);
                }
                xLinePadding = 0;
                bodyLineHeight = bodyFont.lineHeight;
                each(this.afterBody, fillLineOfText);
                pt.y -= bodySpacing;
            }
            drawFooter(pt, ctx, options) {
                const footer = this.footer;
                const length = footer.length;
                let footerFont, i;
                if (length) {
                    const rtlHelper = getRtlAdapter(options.rtl, this.x, this.width);
                    pt.x = getAlignedX(this, options.footerAlign, options);
                    pt.y += options.footerMarginTop;
                    ctx.textAlign = rtlHelper.textAlign(options.footerAlign);
                    ctx.textBaseline = "middle";
                    footerFont = toFont(options.footerFont);
                    ctx.fillStyle = options.footerColor;
                    ctx.font = footerFont.string;
                    for (i = 0; i < length; ++i) {
                        ctx.fillText(footer[i], rtlHelper.x(pt.x), pt.y + footerFont.lineHeight / 2);
                        pt.y += footerFont.lineHeight + options.footerSpacing;
                    }
                }
            }
            drawBackground(pt, ctx, tooltipSize, options) {
                const {xAlign, yAlign} = this;
                const {x, y} = pt;
                const {width, height} = tooltipSize;
                const {topLeft, topRight, bottomLeft, bottomRight} = toTRBLCorners(options.cornerRadius);
                ctx.fillStyle = options.backgroundColor;
                ctx.strokeStyle = options.borderColor;
                ctx.lineWidth = options.borderWidth;
                ctx.beginPath();
                ctx.moveTo(x + topLeft, y);
                if ("top" === yAlign) this.drawCaret(pt, ctx, tooltipSize, options);
                ctx.lineTo(x + width - topRight, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + topRight);
                if ("center" === yAlign && "right" === xAlign) this.drawCaret(pt, ctx, tooltipSize, options);
                ctx.lineTo(x + width, y + height - bottomRight);
                ctx.quadraticCurveTo(x + width, y + height, x + width - bottomRight, y + height);
                if ("bottom" === yAlign) this.drawCaret(pt, ctx, tooltipSize, options);
                ctx.lineTo(x + bottomLeft, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - bottomLeft);
                if ("center" === yAlign && "left" === xAlign) this.drawCaret(pt, ctx, tooltipSize, options);
                ctx.lineTo(x, y + topLeft);
                ctx.quadraticCurveTo(x, y, x + topLeft, y);
                ctx.closePath();
                ctx.fill();
                if (options.borderWidth > 0) ctx.stroke();
            }
            _updateAnimationTarget(options) {
                const chart = this.chart;
                const anims = this.$animations;
                const animX = anims && anims.x;
                const animY = anims && anims.y;
                if (animX || animY) {
                    const position = positioners[options.position].call(this, this._active, this._eventPosition);
                    if (!position) return;
                    const size = this._size = getTooltipSize(this, options);
                    const positionAndSize = Object.assign({}, position, this._size);
                    const alignment = determineAlignment(chart, options, positionAndSize);
                    const point = getBackgroundPoint(options, positionAndSize, alignment, chart);
                    if (animX._to !== point.x || animY._to !== point.y) {
                        this.xAlign = alignment.xAlign;
                        this.yAlign = alignment.yAlign;
                        this.width = size.width;
                        this.height = size.height;
                        this.caretX = position.x;
                        this.caretY = position.y;
                        this._resolveAnimations().update(this, point);
                    }
                }
            }
            draw(ctx) {
                const options = this.options.setContext(this.getContext());
                let opacity = this.opacity;
                if (!opacity) return;
                this._updateAnimationTarget(options);
                const tooltipSize = {
                    width: this.width,
                    height: this.height
                };
                const pt = {
                    x: this.x,
                    y: this.y
                };
                opacity = Math.abs(opacity) < .001 ? 0 : opacity;
                const padding = toPadding(options.padding);
                const hasTooltipContent = this.title.length || this.beforeBody.length || this.body.length || this.afterBody.length || this.footer.length;
                if (options.enabled && hasTooltipContent) {
                    ctx.save();
                    ctx.globalAlpha = opacity;
                    this.drawBackground(pt, ctx, tooltipSize, options);
                    overrideTextDirection(ctx, options.textDirection);
                    pt.y += padding.top;
                    this.drawTitle(pt, ctx, options);
                    this.drawBody(pt, ctx, options);
                    this.drawFooter(pt, ctx, options);
                    restoreTextDirection(ctx, options.textDirection);
                    ctx.restore();
                }
            }
            getActiveElements() {
                return this._active || [];
            }
            setActiveElements(activeElements, eventPosition) {
                const lastActive = this._active;
                const active = activeElements.map((({datasetIndex, index}) => {
                    const meta = this.chart.getDatasetMeta(datasetIndex);
                    if (!meta) throw new Error("Cannot find a dataset at index " + datasetIndex);
                    return {
                        datasetIndex,
                        element: meta.data[index],
                        index
                    };
                }));
                const changed = !_elementsEqual(lastActive, active);
                const positionChanged = this._positionChanged(active, eventPosition);
                if (changed || positionChanged) {
                    this._active = active;
                    this._eventPosition = eventPosition;
                    this._ignoreReplayEvents = true;
                    this.update(true);
                }
            }
            handleEvent(e, replay, inChartArea = true) {
                if (replay && this._ignoreReplayEvents) return false;
                this._ignoreReplayEvents = false;
                const options = this.options;
                const lastActive = this._active || [];
                const active = this._getActiveElements(e, lastActive, replay, inChartArea);
                const positionChanged = this._positionChanged(active, e);
                const changed = replay || !_elementsEqual(active, lastActive) || positionChanged;
                if (changed) {
                    this._active = active;
                    if (options.enabled || options.external) {
                        this._eventPosition = {
                            x: e.x,
                            y: e.y
                        };
                        this.update(true, replay);
                    }
                }
                return changed;
            }
            _getActiveElements(e, lastActive, replay, inChartArea) {
                const options = this.options;
                if ("mouseout" === e.type) return [];
                if (!inChartArea) return lastActive;
                const active = this.chart.getElementsAtEventForMode(e, options.mode, options, replay);
                if (options.reverse) active.reverse();
                return active;
            }
            _positionChanged(active, e) {
                const {caretX, caretY, options} = this;
                const position = positioners[options.position].call(this, active, e);
                return false !== position && (caretX !== position.x || caretY !== position.y);
            }
        }
        Tooltip.positioners = positioners;
        var plugin_tooltip = {
            id: "tooltip",
            _element: Tooltip,
            positioners,
            afterInit(chart, _args, options) {
                if (options) chart.tooltip = new Tooltip({
                    chart,
                    options
                });
            },
            beforeUpdate(chart, _args, options) {
                if (chart.tooltip) chart.tooltip.initialize(options);
            },
            reset(chart, _args, options) {
                if (chart.tooltip) chart.tooltip.initialize(options);
            },
            afterDraw(chart) {
                const tooltip = chart.tooltip;
                const args = {
                    tooltip
                };
                if (false === chart.notifyPlugins("beforeTooltipDraw", args)) return;
                if (tooltip) tooltip.draw(chart.ctx);
                chart.notifyPlugins("afterTooltipDraw", args);
            },
            afterEvent(chart, args) {
                if (chart.tooltip) {
                    const useFinalPosition = args.replay;
                    if (chart.tooltip.handleEvent(args.event, useFinalPosition, args.inChartArea)) args.changed = true;
                }
            },
            defaults: {
                enabled: true,
                external: null,
                position: "average",
                backgroundColor: "rgba(0,0,0,0.8)",
                titleColor: "#fff",
                titleFont: {
                    weight: "bold"
                },
                titleSpacing: 2,
                titleMarginBottom: 6,
                titleAlign: "left",
                bodyColor: "#fff",
                bodySpacing: 2,
                bodyFont: {},
                bodyAlign: "left",
                footerColor: "#fff",
                footerSpacing: 2,
                footerMarginTop: 6,
                footerFont: {
                    weight: "bold"
                },
                footerAlign: "left",
                padding: 6,
                caretPadding: 2,
                caretSize: 5,
                cornerRadius: 6,
                boxHeight: (ctx, opts) => opts.bodyFont.size,
                boxWidth: (ctx, opts) => opts.bodyFont.size,
                multiKeyBackground: "#fff",
                displayColors: true,
                boxPadding: 0,
                borderColor: "rgba(0,0,0,0)",
                borderWidth: 0,
                animation: {
                    duration: 400,
                    easing: "easeOutQuart"
                },
                animations: {
                    numbers: {
                        type: "number",
                        properties: [ "x", "y", "width", "height", "caretX", "caretY" ]
                    },
                    opacity: {
                        easing: "linear",
                        duration: 200
                    }
                },
                callbacks: {
                    beforeTitle: noop,
                    title(tooltipItems) {
                        if (tooltipItems.length > 0) {
                            const item = tooltipItems[0];
                            const labels = item.chart.data.labels;
                            const labelCount = labels ? labels.length : 0;
                            if (this && this.options && "dataset" === this.options.mode) return item.dataset.label || ""; else if (item.label) return item.label; else if (labelCount > 0 && item.dataIndex < labelCount) return labels[item.dataIndex];
                        }
                        return "";
                    },
                    afterTitle: noop,
                    beforeBody: noop,
                    beforeLabel: noop,
                    label(tooltipItem) {
                        if (this && this.options && "dataset" === this.options.mode) return tooltipItem.label + ": " + tooltipItem.formattedValue || tooltipItem.formattedValue;
                        let label = tooltipItem.dataset.label || "";
                        if (label) label += ": ";
                        const value = tooltipItem.formattedValue;
                        if (!isNullOrUndef(value)) label += value;
                        return label;
                    },
                    labelColor(tooltipItem) {
                        const meta = tooltipItem.chart.getDatasetMeta(tooltipItem.datasetIndex);
                        const options = meta.controller.getStyle(tooltipItem.dataIndex);
                        return {
                            borderColor: options.borderColor,
                            backgroundColor: options.backgroundColor,
                            borderWidth: options.borderWidth,
                            borderDash: options.borderDash,
                            borderDashOffset: options.borderDashOffset,
                            borderRadius: 0
                        };
                    },
                    labelTextColor() {
                        return this.options.bodyColor;
                    },
                    labelPointStyle(tooltipItem) {
                        const meta = tooltipItem.chart.getDatasetMeta(tooltipItem.datasetIndex);
                        const options = meta.controller.getStyle(tooltipItem.dataIndex);
                        return {
                            pointStyle: options.pointStyle,
                            rotation: options.rotation
                        };
                    },
                    afterLabel: noop,
                    afterBody: noop,
                    beforeFooter: noop,
                    footer: noop,
                    afterFooter: noop
                }
            },
            defaultRoutes: {
                bodyFont: "font",
                footerFont: "font",
                titleFont: "font"
            },
            descriptors: {
                _scriptable: name => "filter" !== name && "itemSort" !== name && "external" !== name,
                _indexable: false,
                callbacks: {
                    _scriptable: false,
                    _indexable: false
                },
                animation: {
                    _fallback: false
                },
                animations: {
                    _fallback: "animation"
                }
            },
            additionalOptionScopes: [ "interaction" ]
        };
        const addIfString = (labels, raw, index, addedLabels) => {
            if ("string" === typeof raw) {
                index = labels.push(raw) - 1;
                addedLabels.unshift({
                    index,
                    label: raw
                });
            } else if (isNaN(raw)) index = null;
            return index;
        };
        function findOrAddLabel(labels, raw, index, addedLabels) {
            const first = labels.indexOf(raw);
            if (-1 === first) return addIfString(labels, raw, index, addedLabels);
            const last = labels.lastIndexOf(raw);
            return first !== last ? index : first;
        }
        const validIndex = (index, max) => null === index ? null : _limitValue(Math.round(index), 0, max);
        class CategoryScale extends Scale {
            constructor(cfg) {
                super(cfg);
                this._startValue = void 0;
                this._valueRange = 0;
                this._addedLabels = [];
            }
            init(scaleOptions) {
                const added = this._addedLabels;
                if (added.length) {
                    const labels = this.getLabels();
                    for (const {index, label} of added) if (labels[index] === label) labels.splice(index, 1);
                    this._addedLabels = [];
                }
                super.init(scaleOptions);
            }
            parse(raw, index) {
                if (isNullOrUndef(raw)) return null;
                const labels = this.getLabels();
                index = isFinite(index) && labels[index] === raw ? index : findOrAddLabel(labels, raw, valueOrDefault(index, raw), this._addedLabels);
                return validIndex(index, labels.length - 1);
            }
            determineDataLimits() {
                const {minDefined, maxDefined} = this.getUserBounds();
                let {min, max} = this.getMinMax(true);
                if ("ticks" === this.options.bounds) {
                    if (!minDefined) min = 0;
                    if (!maxDefined) max = this.getLabels().length - 1;
                }
                this.min = min;
                this.max = max;
            }
            buildTicks() {
                const min = this.min;
                const max = this.max;
                const offset = this.options.offset;
                const ticks = [];
                let labels = this.getLabels();
                labels = 0 === min && max === labels.length - 1 ? labels : labels.slice(min, max + 1);
                this._valueRange = Math.max(labels.length - (offset ? 0 : 1), 1);
                this._startValue = this.min - (offset ? .5 : 0);
                for (let value = min; value <= max; value++) ticks.push({
                    value
                });
                return ticks;
            }
            getLabelForValue(value) {
                const labels = this.getLabels();
                if (value >= 0 && value < labels.length) return labels[value];
                return value;
            }
            configure() {
                super.configure();
                if (!this.isHorizontal()) this._reversePixels = !this._reversePixels;
            }
            getPixelForValue(value) {
                if ("number" !== typeof value) value = this.parse(value);
                return null === value ? NaN : this.getPixelForDecimal((value - this._startValue) / this._valueRange);
            }
            getPixelForTick(index) {
                const ticks = this.ticks;
                if (index < 0 || index > ticks.length - 1) return null;
                return this.getPixelForValue(ticks[index].value);
            }
            getValueForPixel(pixel) {
                return Math.round(this._startValue + this.getDecimalForPixel(pixel) * this._valueRange);
            }
            getBasePixel() {
                return this.bottom;
            }
        }
        CategoryScale.id = "category";
        CategoryScale.defaults = {
            ticks: {
                callback: CategoryScale.prototype.getLabelForValue
            }
        };
        function generateTicks$1(generationOptions, dataRange) {
            const ticks = [];
            const MIN_SPACING = 1e-14;
            const {bounds, step, min, max, precision, count, maxTicks, maxDigits, includeBounds} = generationOptions;
            const unit = step || 1;
            const maxSpaces = maxTicks - 1;
            const {min: rmin, max: rmax} = dataRange;
            const minDefined = !isNullOrUndef(min);
            const maxDefined = !isNullOrUndef(max);
            const countDefined = !isNullOrUndef(count);
            const minSpacing = (rmax - rmin) / (maxDigits + 1);
            let spacing = niceNum((rmax - rmin) / maxSpaces / unit) * unit;
            let factor, niceMin, niceMax, numSpaces;
            if (spacing < MIN_SPACING && !minDefined && !maxDefined) return [ {
                value: rmin
            }, {
                value: rmax
            } ];
            numSpaces = Math.ceil(rmax / spacing) - Math.floor(rmin / spacing);
            if (numSpaces > maxSpaces) spacing = niceNum(numSpaces * spacing / maxSpaces / unit) * unit;
            if (!isNullOrUndef(precision)) {
                factor = Math.pow(10, precision);
                spacing = Math.ceil(spacing * factor) / factor;
            }
            if ("ticks" === bounds) {
                niceMin = Math.floor(rmin / spacing) * spacing;
                niceMax = Math.ceil(rmax / spacing) * spacing;
            } else {
                niceMin = rmin;
                niceMax = rmax;
            }
            if (minDefined && maxDefined && step && almostWhole((max - min) / step, spacing / 1e3)) {
                numSpaces = Math.round(Math.min((max - min) / spacing, maxTicks));
                spacing = (max - min) / numSpaces;
                niceMin = min;
                niceMax = max;
            } else if (countDefined) {
                niceMin = minDefined ? min : niceMin;
                niceMax = maxDefined ? max : niceMax;
                numSpaces = count - 1;
                spacing = (niceMax - niceMin) / numSpaces;
            } else {
                numSpaces = (niceMax - niceMin) / spacing;
                if (almostEquals(numSpaces, Math.round(numSpaces), spacing / 1e3)) numSpaces = Math.round(numSpaces); else numSpaces = Math.ceil(numSpaces);
            }
            const decimalPlaces = Math.max(_decimalPlaces(spacing), _decimalPlaces(niceMin));
            factor = Math.pow(10, isNullOrUndef(precision) ? decimalPlaces : precision);
            niceMin = Math.round(niceMin * factor) / factor;
            niceMax = Math.round(niceMax * factor) / factor;
            let j = 0;
            if (minDefined) if (includeBounds && niceMin !== min) {
                ticks.push({
                    value: min
                });
                if (niceMin < min) j++;
                if (almostEquals(Math.round((niceMin + j * spacing) * factor) / factor, min, relativeLabelSize(min, minSpacing, generationOptions))) j++;
            } else if (niceMin < min) j++;
            for (;j < numSpaces; ++j) ticks.push({
                value: Math.round((niceMin + j * spacing) * factor) / factor
            });
            if (maxDefined && includeBounds && niceMax !== max) if (ticks.length && almostEquals(ticks[ticks.length - 1].value, max, relativeLabelSize(max, minSpacing, generationOptions))) ticks[ticks.length - 1].value = max; else ticks.push({
                value: max
            }); else if (!maxDefined || niceMax === max) ticks.push({
                value: niceMax
            });
            return ticks;
        }
        function relativeLabelSize(value, minSpacing, {horizontal, minRotation}) {
            const rad = toRadians(minRotation);
            const ratio = (horizontal ? Math.sin(rad) : Math.cos(rad)) || .001;
            const length = .75 * minSpacing * ("" + value).length;
            return Math.min(minSpacing / ratio, length);
        }
        class LinearScaleBase extends Scale {
            constructor(cfg) {
                super(cfg);
                this.start = void 0;
                this.end = void 0;
                this._startValue = void 0;
                this._endValue = void 0;
                this._valueRange = 0;
            }
            parse(raw, index) {
                if (isNullOrUndef(raw)) return null;
                if (("number" === typeof raw || raw instanceof Number) && !isFinite(+raw)) return null;
                return +raw;
            }
            handleTickRangeOptions() {
                const {beginAtZero} = this.options;
                const {minDefined, maxDefined} = this.getUserBounds();
                let {min, max} = this;
                const setMin = v => min = minDefined ? min : v;
                const setMax = v => max = maxDefined ? max : v;
                if (beginAtZero) {
                    const minSign = sign(min);
                    const maxSign = sign(max);
                    if (minSign < 0 && maxSign < 0) setMax(0); else if (minSign > 0 && maxSign > 0) setMin(0);
                }
                if (min === max) {
                    let offset = 1;
                    if (max >= Number.MAX_SAFE_INTEGER || min <= Number.MIN_SAFE_INTEGER) offset = Math.abs(.05 * max);
                    setMax(max + offset);
                    if (!beginAtZero) setMin(min - offset);
                }
                this.min = min;
                this.max = max;
            }
            getTickLimit() {
                const tickOpts = this.options.ticks;
                let {maxTicksLimit, stepSize} = tickOpts;
                let maxTicks;
                if (stepSize) {
                    maxTicks = Math.ceil(this.max / stepSize) - Math.floor(this.min / stepSize) + 1;
                    if (maxTicks > 1e3) {
                        console.warn(`scales.${this.id}.ticks.stepSize: ${stepSize} would result generating up to ${maxTicks} ticks. Limiting to 1000.`);
                        maxTicks = 1e3;
                    }
                } else {
                    maxTicks = this.computeTickLimit();
                    maxTicksLimit = maxTicksLimit || 11;
                }
                if (maxTicksLimit) maxTicks = Math.min(maxTicksLimit, maxTicks);
                return maxTicks;
            }
            computeTickLimit() {
                return Number.POSITIVE_INFINITY;
            }
            buildTicks() {
                const opts = this.options;
                const tickOpts = opts.ticks;
                let maxTicks = this.getTickLimit();
                maxTicks = Math.max(2, maxTicks);
                const numericGeneratorOptions = {
                    maxTicks,
                    bounds: opts.bounds,
                    min: opts.min,
                    max: opts.max,
                    precision: tickOpts.precision,
                    step: tickOpts.stepSize,
                    count: tickOpts.count,
                    maxDigits: this._maxDigits(),
                    horizontal: this.isHorizontal(),
                    minRotation: tickOpts.minRotation || 0,
                    includeBounds: false !== tickOpts.includeBounds
                };
                const dataRange = this._range || this;
                const ticks = generateTicks$1(numericGeneratorOptions, dataRange);
                if ("ticks" === opts.bounds) _setMinAndMaxByKey(ticks, this, "value");
                if (opts.reverse) {
                    ticks.reverse();
                    this.start = this.max;
                    this.end = this.min;
                } else {
                    this.start = this.min;
                    this.end = this.max;
                }
                return ticks;
            }
            configure() {
                const ticks = this.ticks;
                let start = this.min;
                let end = this.max;
                super.configure();
                if (this.options.offset && ticks.length) {
                    const offset = (end - start) / Math.max(ticks.length - 1, 1) / 2;
                    start -= offset;
                    end += offset;
                }
                this._startValue = start;
                this._endValue = end;
                this._valueRange = end - start;
            }
            getLabelForValue(value) {
                return formatNumber(value, this.chart.options.locale, this.options.ticks.format);
            }
        }
        class LinearScale extends LinearScaleBase {
            determineDataLimits() {
                const {min, max} = this.getMinMax(true);
                this.min = isNumberFinite(min) ? min : 0;
                this.max = isNumberFinite(max) ? max : 1;
                this.handleTickRangeOptions();
            }
            computeTickLimit() {
                const horizontal = this.isHorizontal();
                const length = horizontal ? this.width : this.height;
                const minRotation = toRadians(this.options.ticks.minRotation);
                const ratio = (horizontal ? Math.sin(minRotation) : Math.cos(minRotation)) || .001;
                const tickFont = this._resolveTickFontOptions(0);
                return Math.ceil(length / Math.min(40, tickFont.lineHeight / ratio));
            }
            getPixelForValue(value) {
                return null === value ? NaN : this.getPixelForDecimal((value - this._startValue) / this._valueRange);
            }
            getValueForPixel(pixel) {
                return this._startValue + this.getDecimalForPixel(pixel) * this._valueRange;
            }
        }
        LinearScale.id = "linear";
        LinearScale.defaults = {
            ticks: {
                callback: Ticks.formatters.numeric
            }
        };
        function isMajor(tickVal) {
            const remain = tickVal / Math.pow(10, Math.floor(log10(tickVal)));
            return 1 === remain;
        }
        function generateTicks(generationOptions, dataRange) {
            const endExp = Math.floor(log10(dataRange.max));
            const endSignificand = Math.ceil(dataRange.max / Math.pow(10, endExp));
            const ticks = [];
            let tickVal = finiteOrDefault(generationOptions.min, Math.pow(10, Math.floor(log10(dataRange.min))));
            let exp = Math.floor(log10(tickVal));
            let significand = Math.floor(tickVal / Math.pow(10, exp));
            let precision = exp < 0 ? Math.pow(10, Math.abs(exp)) : 1;
            do {
                ticks.push({
                    value: tickVal,
                    major: isMajor(tickVal)
                });
                ++significand;
                if (10 === significand) {
                    significand = 1;
                    ++exp;
                    precision = exp >= 0 ? 1 : precision;
                }
                tickVal = Math.round(significand * Math.pow(10, exp) * precision) / precision;
            } while (exp < endExp || exp === endExp && significand < endSignificand);
            const lastTick = finiteOrDefault(generationOptions.max, tickVal);
            ticks.push({
                value: lastTick,
                major: isMajor(tickVal)
            });
            return ticks;
        }
        class LogarithmicScale extends Scale {
            constructor(cfg) {
                super(cfg);
                this.start = void 0;
                this.end = void 0;
                this._startValue = void 0;
                this._valueRange = 0;
            }
            parse(raw, index) {
                const value = LinearScaleBase.prototype.parse.apply(this, [ raw, index ]);
                if (0 === value) {
                    this._zero = true;
                    return;
                }
                return isNumberFinite(value) && value > 0 ? value : null;
            }
            determineDataLimits() {
                const {min, max} = this.getMinMax(true);
                this.min = isNumberFinite(min) ? Math.max(0, min) : null;
                this.max = isNumberFinite(max) ? Math.max(0, max) : null;
                if (this.options.beginAtZero) this._zero = true;
                this.handleTickRangeOptions();
            }
            handleTickRangeOptions() {
                const {minDefined, maxDefined} = this.getUserBounds();
                let min = this.min;
                let max = this.max;
                const setMin = v => min = minDefined ? min : v;
                const setMax = v => max = maxDefined ? max : v;
                const exp = (v, m) => Math.pow(10, Math.floor(log10(v)) + m);
                if (min === max) if (min <= 0) {
                    setMin(1);
                    setMax(10);
                } else {
                    setMin(exp(min, -1));
                    setMax(exp(max, +1));
                }
                if (min <= 0) setMin(exp(max, -1));
                if (max <= 0) setMax(exp(min, +1));
                if (this._zero && this.min !== this._suggestedMin && min === exp(this.min, 0)) setMin(exp(min, -1));
                this.min = min;
                this.max = max;
            }
            buildTicks() {
                const opts = this.options;
                const generationOptions = {
                    min: this._userMin,
                    max: this._userMax
                };
                const ticks = generateTicks(generationOptions, this);
                if ("ticks" === opts.bounds) _setMinAndMaxByKey(ticks, this, "value");
                if (opts.reverse) {
                    ticks.reverse();
                    this.start = this.max;
                    this.end = this.min;
                } else {
                    this.start = this.min;
                    this.end = this.max;
                }
                return ticks;
            }
            getLabelForValue(value) {
                return void 0 === value ? "0" : formatNumber(value, this.chart.options.locale, this.options.ticks.format);
            }
            configure() {
                const start = this.min;
                super.configure();
                this._startValue = log10(start);
                this._valueRange = log10(this.max) - log10(start);
            }
            getPixelForValue(value) {
                if (void 0 === value || 0 === value) value = this.min;
                if (null === value || isNaN(value)) return NaN;
                return this.getPixelForDecimal(value === this.min ? 0 : (log10(value) - this._startValue) / this._valueRange);
            }
            getValueForPixel(pixel) {
                const decimal = this.getDecimalForPixel(pixel);
                return Math.pow(10, this._startValue + decimal * this._valueRange);
            }
        }
        LogarithmicScale.id = "logarithmic";
        LogarithmicScale.defaults = {
            ticks: {
                callback: Ticks.formatters.logarithmic,
                major: {
                    enabled: true
                }
            }
        };
        function getTickBackdropHeight(opts) {
            const tickOpts = opts.ticks;
            if (tickOpts.display && opts.display) {
                const padding = toPadding(tickOpts.backdropPadding);
                return valueOrDefault(tickOpts.font && tickOpts.font.size, defaults.font.size) + padding.height;
            }
            return 0;
        }
        function measureLabelSize(ctx, font, label) {
            label = isArray(label) ? label : [ label ];
            return {
                w: _longestText(ctx, font.string, label),
                h: label.length * font.lineHeight
            };
        }
        function determineLimits(angle, pos, size, min, max) {
            if (angle === min || angle === max) return {
                start: pos - size / 2,
                end: pos + size / 2
            }; else if (angle < min || angle > max) return {
                start: pos - size,
                end: pos
            };
            return {
                start: pos,
                end: pos + size
            };
        }
        function fitWithPointLabels(scale) {
            const orig = {
                l: scale.left + scale._padding.left,
                r: scale.right - scale._padding.right,
                t: scale.top + scale._padding.top,
                b: scale.bottom - scale._padding.bottom
            };
            const limits = Object.assign({}, orig);
            const labelSizes = [];
            const padding = [];
            const valueCount = scale._pointLabels.length;
            const pointLabelOpts = scale.options.pointLabels;
            const additionalAngle = pointLabelOpts.centerPointLabels ? PI / valueCount : 0;
            for (let i = 0; i < valueCount; i++) {
                const opts = pointLabelOpts.setContext(scale.getPointLabelContext(i));
                padding[i] = opts.padding;
                const pointPosition = scale.getPointPosition(i, scale.drawingArea + padding[i], additionalAngle);
                const plFont = toFont(opts.font);
                const textSize = measureLabelSize(scale.ctx, plFont, scale._pointLabels[i]);
                labelSizes[i] = textSize;
                const angleRadians = _normalizeAngle(scale.getIndexAngle(i) + additionalAngle);
                const angle = Math.round(toDegrees(angleRadians));
                const hLimits = determineLimits(angle, pointPosition.x, textSize.w, 0, 180);
                const vLimits = determineLimits(angle, pointPosition.y, textSize.h, 90, 270);
                updateLimits(limits, orig, angleRadians, hLimits, vLimits);
            }
            scale.setCenterPoint(orig.l - limits.l, limits.r - orig.r, orig.t - limits.t, limits.b - orig.b);
            scale._pointLabelItems = buildPointLabelItems(scale, labelSizes, padding);
        }
        function updateLimits(limits, orig, angle, hLimits, vLimits) {
            const sin = Math.abs(Math.sin(angle));
            const cos = Math.abs(Math.cos(angle));
            let x = 0;
            let y = 0;
            if (hLimits.start < orig.l) {
                x = (orig.l - hLimits.start) / sin;
                limits.l = Math.min(limits.l, orig.l - x);
            } else if (hLimits.end > orig.r) {
                x = (hLimits.end - orig.r) / sin;
                limits.r = Math.max(limits.r, orig.r + x);
            }
            if (vLimits.start < orig.t) {
                y = (orig.t - vLimits.start) / cos;
                limits.t = Math.min(limits.t, orig.t - y);
            } else if (vLimits.end > orig.b) {
                y = (vLimits.end - orig.b) / cos;
                limits.b = Math.max(limits.b, orig.b + y);
            }
        }
        function buildPointLabelItems(scale, labelSizes, padding) {
            const items = [];
            const valueCount = scale._pointLabels.length;
            const opts = scale.options;
            const extra = getTickBackdropHeight(opts) / 2;
            const outerDistance = scale.drawingArea;
            const additionalAngle = opts.pointLabels.centerPointLabels ? PI / valueCount : 0;
            for (let i = 0; i < valueCount; i++) {
                const pointLabelPosition = scale.getPointPosition(i, outerDistance + extra + padding[i], additionalAngle);
                const angle = Math.round(toDegrees(_normalizeAngle(pointLabelPosition.angle + HALF_PI)));
                const size = labelSizes[i];
                const y = yForAngle(pointLabelPosition.y, size.h, angle);
                const textAlign = getTextAlignForAngle(angle);
                const left = leftForTextAlign(pointLabelPosition.x, size.w, textAlign);
                items.push({
                    x: pointLabelPosition.x,
                    y,
                    textAlign,
                    left,
                    top: y,
                    right: left + size.w,
                    bottom: y + size.h
                });
            }
            return items;
        }
        function getTextAlignForAngle(angle) {
            if (0 === angle || 180 === angle) return "center"; else if (angle < 180) return "left";
            return "right";
        }
        function leftForTextAlign(x, w, align) {
            if ("right" === align) x -= w; else if ("center" === align) x -= w / 2;
            return x;
        }
        function yForAngle(y, h, angle) {
            if (90 === angle || 270 === angle) y -= h / 2; else if (angle > 270 || angle < 90) y -= h;
            return y;
        }
        function drawPointLabels(scale, labelCount) {
            const {ctx, options: {pointLabels}} = scale;
            for (let i = labelCount - 1; i >= 0; i--) {
                const optsAtIndex = pointLabels.setContext(scale.getPointLabelContext(i));
                const plFont = toFont(optsAtIndex.font);
                const {x, y, textAlign, left, top, right, bottom} = scale._pointLabelItems[i];
                const {backdropColor} = optsAtIndex;
                if (!isNullOrUndef(backdropColor)) {
                    const padding = toPadding(optsAtIndex.backdropPadding);
                    ctx.fillStyle = backdropColor;
                    ctx.fillRect(left - padding.left, top - padding.top, right - left + padding.width, bottom - top + padding.height);
                }
                renderText(ctx, scale._pointLabels[i], x, y + plFont.lineHeight / 2, plFont, {
                    color: optsAtIndex.color,
                    textAlign,
                    textBaseline: "middle"
                });
            }
        }
        function pathRadiusLine(scale, radius, circular, labelCount) {
            const {ctx} = scale;
            if (circular) ctx.arc(scale.xCenter, scale.yCenter, radius, 0, TAU); else {
                let pointPosition = scale.getPointPosition(0, radius);
                ctx.moveTo(pointPosition.x, pointPosition.y);
                for (let i = 1; i < labelCount; i++) {
                    pointPosition = scale.getPointPosition(i, radius);
                    ctx.lineTo(pointPosition.x, pointPosition.y);
                }
            }
        }
        function drawRadiusLine(scale, gridLineOpts, radius, labelCount) {
            const ctx = scale.ctx;
            const circular = gridLineOpts.circular;
            const {color, lineWidth} = gridLineOpts;
            if (!circular && !labelCount || !color || !lineWidth || radius < 0) return;
            ctx.save();
            ctx.strokeStyle = color;
            ctx.lineWidth = lineWidth;
            ctx.setLineDash(gridLineOpts.borderDash);
            ctx.lineDashOffset = gridLineOpts.borderDashOffset;
            ctx.beginPath();
            pathRadiusLine(scale, radius, circular, labelCount);
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        }
        function createPointLabelContext(parent, index, label) {
            return createContext(parent, {
                label,
                index,
                type: "pointLabel"
            });
        }
        class RadialLinearScale extends LinearScaleBase {
            constructor(cfg) {
                super(cfg);
                this.xCenter = void 0;
                this.yCenter = void 0;
                this.drawingArea = void 0;
                this._pointLabels = [];
                this._pointLabelItems = [];
            }
            setDimensions() {
                const padding = this._padding = toPadding(getTickBackdropHeight(this.options) / 2);
                const w = this.width = this.maxWidth - padding.width;
                const h = this.height = this.maxHeight - padding.height;
                this.xCenter = Math.floor(this.left + w / 2 + padding.left);
                this.yCenter = Math.floor(this.top + h / 2 + padding.top);
                this.drawingArea = Math.floor(Math.min(w, h) / 2);
            }
            determineDataLimits() {
                const {min, max} = this.getMinMax(false);
                this.min = isNumberFinite(min) && !isNaN(min) ? min : 0;
                this.max = isNumberFinite(max) && !isNaN(max) ? max : 0;
                this.handleTickRangeOptions();
            }
            computeTickLimit() {
                return Math.ceil(this.drawingArea / getTickBackdropHeight(this.options));
            }
            generateTickLabels(ticks) {
                LinearScaleBase.prototype.generateTickLabels.call(this, ticks);
                this._pointLabels = this.getLabels().map(((value, index) => {
                    const label = callback(this.options.pointLabels.callback, [ value, index ], this);
                    return label || 0 === label ? label : "";
                })).filter(((v, i) => this.chart.getDataVisibility(i)));
            }
            fit() {
                const opts = this.options;
                if (opts.display && opts.pointLabels.display) fitWithPointLabels(this); else this.setCenterPoint(0, 0, 0, 0);
            }
            setCenterPoint(leftMovement, rightMovement, topMovement, bottomMovement) {
                this.xCenter += Math.floor((leftMovement - rightMovement) / 2);
                this.yCenter += Math.floor((topMovement - bottomMovement) / 2);
                this.drawingArea -= Math.min(this.drawingArea / 2, Math.max(leftMovement, rightMovement, topMovement, bottomMovement));
            }
            getIndexAngle(index) {
                const angleMultiplier = TAU / (this._pointLabels.length || 1);
                const startAngle = this.options.startAngle || 0;
                return _normalizeAngle(index * angleMultiplier + toRadians(startAngle));
            }
            getDistanceFromCenterForValue(value) {
                if (isNullOrUndef(value)) return NaN;
                const scalingFactor = this.drawingArea / (this.max - this.min);
                if (this.options.reverse) return (this.max - value) * scalingFactor;
                return (value - this.min) * scalingFactor;
            }
            getValueForDistanceFromCenter(distance) {
                if (isNullOrUndef(distance)) return NaN;
                const scaledDistance = distance / (this.drawingArea / (this.max - this.min));
                return this.options.reverse ? this.max - scaledDistance : this.min + scaledDistance;
            }
            getPointLabelContext(index) {
                const pointLabels = this._pointLabels || [];
                if (index >= 0 && index < pointLabels.length) {
                    const pointLabel = pointLabels[index];
                    return createPointLabelContext(this.getContext(), index, pointLabel);
                }
            }
            getPointPosition(index, distanceFromCenter, additionalAngle = 0) {
                const angle = this.getIndexAngle(index) - HALF_PI + additionalAngle;
                return {
                    x: Math.cos(angle) * distanceFromCenter + this.xCenter,
                    y: Math.sin(angle) * distanceFromCenter + this.yCenter,
                    angle
                };
            }
            getPointPositionForValue(index, value) {
                return this.getPointPosition(index, this.getDistanceFromCenterForValue(value));
            }
            getBasePosition(index) {
                return this.getPointPositionForValue(index || 0, this.getBaseValue());
            }
            getPointLabelPosition(index) {
                const {left, top, right, bottom} = this._pointLabelItems[index];
                return {
                    left,
                    top,
                    right,
                    bottom
                };
            }
            drawBackground() {
                const {backgroundColor, grid: {circular}} = this.options;
                if (backgroundColor) {
                    const ctx = this.ctx;
                    ctx.save();
                    ctx.beginPath();
                    pathRadiusLine(this, this.getDistanceFromCenterForValue(this._endValue), circular, this._pointLabels.length);
                    ctx.closePath();
                    ctx.fillStyle = backgroundColor;
                    ctx.fill();
                    ctx.restore();
                }
            }
            drawGrid() {
                const ctx = this.ctx;
                const opts = this.options;
                const {angleLines, grid} = opts;
                const labelCount = this._pointLabels.length;
                let i, offset, position;
                if (opts.pointLabels.display) drawPointLabels(this, labelCount);
                if (grid.display) this.ticks.forEach(((tick, index) => {
                    if (0 !== index) {
                        offset = this.getDistanceFromCenterForValue(tick.value);
                        const optsAtIndex = grid.setContext(this.getContext(index - 1));
                        drawRadiusLine(this, optsAtIndex, offset, labelCount);
                    }
                }));
                if (angleLines.display) {
                    ctx.save();
                    for (i = labelCount - 1; i >= 0; i--) {
                        const optsAtIndex = angleLines.setContext(this.getPointLabelContext(i));
                        const {color, lineWidth} = optsAtIndex;
                        if (!lineWidth || !color) continue;
                        ctx.lineWidth = lineWidth;
                        ctx.strokeStyle = color;
                        ctx.setLineDash(optsAtIndex.borderDash);
                        ctx.lineDashOffset = optsAtIndex.borderDashOffset;
                        offset = this.getDistanceFromCenterForValue(opts.ticks.reverse ? this.min : this.max);
                        position = this.getPointPosition(i, offset);
                        ctx.beginPath();
                        ctx.moveTo(this.xCenter, this.yCenter);
                        ctx.lineTo(position.x, position.y);
                        ctx.stroke();
                    }
                    ctx.restore();
                }
            }
            drawBorder() {}
            drawLabels() {
                const ctx = this.ctx;
                const opts = this.options;
                const tickOpts = opts.ticks;
                if (!tickOpts.display) return;
                const startAngle = this.getIndexAngle(0);
                let offset, width;
                ctx.save();
                ctx.translate(this.xCenter, this.yCenter);
                ctx.rotate(startAngle);
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                this.ticks.forEach(((tick, index) => {
                    if (0 === index && !opts.reverse) return;
                    const optsAtIndex = tickOpts.setContext(this.getContext(index));
                    const tickFont = toFont(optsAtIndex.font);
                    offset = this.getDistanceFromCenterForValue(this.ticks[index].value);
                    if (optsAtIndex.showLabelBackdrop) {
                        ctx.font = tickFont.string;
                        width = ctx.measureText(tick.label).width;
                        ctx.fillStyle = optsAtIndex.backdropColor;
                        const padding = toPadding(optsAtIndex.backdropPadding);
                        ctx.fillRect(-width / 2 - padding.left, -offset - tickFont.size / 2 - padding.top, width + padding.width, tickFont.size + padding.height);
                    }
                    renderText(ctx, tick.label, 0, -offset, tickFont, {
                        color: optsAtIndex.color
                    });
                }));
                ctx.restore();
            }
            drawTitle() {}
        }
        RadialLinearScale.id = "radialLinear";
        RadialLinearScale.defaults = {
            display: true,
            animate: true,
            position: "chartArea",
            angleLines: {
                display: true,
                lineWidth: 1,
                borderDash: [],
                borderDashOffset: 0
            },
            grid: {
                circular: false
            },
            startAngle: 0,
            ticks: {
                showLabelBackdrop: true,
                callback: Ticks.formatters.numeric
            },
            pointLabels: {
                backdropColor: void 0,
                backdropPadding: 2,
                display: true,
                font: {
                    size: 10
                },
                callback(label) {
                    return label;
                },
                padding: 5,
                centerPointLabels: false
            }
        };
        RadialLinearScale.defaultRoutes = {
            "angleLines.color": "borderColor",
            "pointLabels.color": "color",
            "ticks.color": "color"
        };
        RadialLinearScale.descriptors = {
            angleLines: {
                _fallback: "grid"
            }
        };
        const INTERVALS = {
            millisecond: {
                common: true,
                size: 1,
                steps: 1e3
            },
            second: {
                common: true,
                size: 1e3,
                steps: 60
            },
            minute: {
                common: true,
                size: 6e4,
                steps: 60
            },
            hour: {
                common: true,
                size: 36e5,
                steps: 24
            },
            day: {
                common: true,
                size: 864e5,
                steps: 30
            },
            week: {
                common: false,
                size: 6048e5,
                steps: 4
            },
            month: {
                common: true,
                size: 2628e6,
                steps: 12
            },
            quarter: {
                common: false,
                size: 7884e6,
                steps: 4
            },
            year: {
                common: true,
                size: 3154e7
            }
        };
        const UNITS = Object.keys(INTERVALS);
        function sorter(a, b) {
            return a - b;
        }
        function parse(scale, input) {
            if (isNullOrUndef(input)) return null;
            const adapter = scale._adapter;
            const {parser, round, isoWeekday} = scale._parseOpts;
            let value = input;
            if ("function" === typeof parser) value = parser(value);
            if (!isNumberFinite(value)) value = "string" === typeof parser ? adapter.parse(value, parser) : adapter.parse(value);
            if (null === value) return null;
            if (round) value = "week" === round && (isNumber(isoWeekday) || true === isoWeekday) ? adapter.startOf(value, "isoWeek", isoWeekday) : adapter.startOf(value, round);
            return +value;
        }
        function determineUnitForAutoTicks(minUnit, min, max, capacity) {
            const ilen = UNITS.length;
            for (let i = UNITS.indexOf(minUnit); i < ilen - 1; ++i) {
                const interval = INTERVALS[UNITS[i]];
                const factor = interval.steps ? interval.steps : Number.MAX_SAFE_INTEGER;
                if (interval.common && Math.ceil((max - min) / (factor * interval.size)) <= capacity) return UNITS[i];
            }
            return UNITS[ilen - 1];
        }
        function determineUnitForFormatting(scale, numTicks, minUnit, min, max) {
            for (let i = UNITS.length - 1; i >= UNITS.indexOf(minUnit); i--) {
                const unit = UNITS[i];
                if (INTERVALS[unit].common && scale._adapter.diff(max, min, unit) >= numTicks - 1) return unit;
            }
            return UNITS[minUnit ? UNITS.indexOf(minUnit) : 0];
        }
        function determineMajorUnit(unit) {
            for (let i = UNITS.indexOf(unit) + 1, ilen = UNITS.length; i < ilen; ++i) if (INTERVALS[UNITS[i]].common) return UNITS[i];
        }
        function addTick(ticks, time, timestamps) {
            if (!timestamps) ticks[time] = true; else if (timestamps.length) {
                const {lo, hi} = _lookup(timestamps, time);
                const timestamp = timestamps[lo] >= time ? timestamps[lo] : timestamps[hi];
                ticks[timestamp] = true;
            }
        }
        function setMajorTicks(scale, ticks, map, majorUnit) {
            const adapter = scale._adapter;
            const first = +adapter.startOf(ticks[0].value, majorUnit);
            const last = ticks[ticks.length - 1].value;
            let major, index;
            for (major = first; major <= last; major = +adapter.add(major, 1, majorUnit)) {
                index = map[major];
                if (index >= 0) ticks[index].major = true;
            }
            return ticks;
        }
        function ticksFromTimestamps(scale, values, majorUnit) {
            const ticks = [];
            const map = {};
            const ilen = values.length;
            let i, value;
            for (i = 0; i < ilen; ++i) {
                value = values[i];
                map[value] = i;
                ticks.push({
                    value,
                    major: false
                });
            }
            return 0 === ilen || !majorUnit ? ticks : setMajorTicks(scale, ticks, map, majorUnit);
        }
        class TimeScale extends Scale {
            constructor(props) {
                super(props);
                this._cache = {
                    data: [],
                    labels: [],
                    all: []
                };
                this._unit = "day";
                this._majorUnit = void 0;
                this._offsets = {};
                this._normalized = false;
                this._parseOpts = void 0;
            }
            init(scaleOpts, opts) {
                const time = scaleOpts.time || (scaleOpts.time = {});
                const adapter = this._adapter = new adapters._date(scaleOpts.adapters.date);
                mergeIf(time.displayFormats, adapter.formats());
                this._parseOpts = {
                    parser: time.parser,
                    round: time.round,
                    isoWeekday: time.isoWeekday
                };
                super.init(scaleOpts);
                this._normalized = opts.normalized;
            }
            parse(raw, index) {
                if (void 0 === raw) return null;
                return parse(this, raw);
            }
            beforeLayout() {
                super.beforeLayout();
                this._cache = {
                    data: [],
                    labels: [],
                    all: []
                };
            }
            determineDataLimits() {
                const options = this.options;
                const adapter = this._adapter;
                const unit = options.time.unit || "day";
                let {min, max, minDefined, maxDefined} = this.getUserBounds();
                function _applyBounds(bounds) {
                    if (!minDefined && !isNaN(bounds.min)) min = Math.min(min, bounds.min);
                    if (!maxDefined && !isNaN(bounds.max)) max = Math.max(max, bounds.max);
                }
                if (!minDefined || !maxDefined) {
                    _applyBounds(this._getLabelBounds());
                    if ("ticks" !== options.bounds || "labels" !== options.ticks.source) _applyBounds(this.getMinMax(false));
                }
                min = isNumberFinite(min) && !isNaN(min) ? min : +adapter.startOf(Date.now(), unit);
                max = isNumberFinite(max) && !isNaN(max) ? max : +adapter.endOf(Date.now(), unit) + 1;
                this.min = Math.min(min, max - 1);
                this.max = Math.max(min + 1, max);
            }
            _getLabelBounds() {
                const arr = this.getLabelTimestamps();
                let min = Number.POSITIVE_INFINITY;
                let max = Number.NEGATIVE_INFINITY;
                if (arr.length) {
                    min = arr[0];
                    max = arr[arr.length - 1];
                }
                return {
                    min,
                    max
                };
            }
            buildTicks() {
                const options = this.options;
                const timeOpts = options.time;
                const tickOpts = options.ticks;
                const timestamps = "labels" === tickOpts.source ? this.getLabelTimestamps() : this._generate();
                if ("ticks" === options.bounds && timestamps.length) {
                    this.min = this._userMin || timestamps[0];
                    this.max = this._userMax || timestamps[timestamps.length - 1];
                }
                const min = this.min;
                const max = this.max;
                const ticks = _filterBetween(timestamps, min, max);
                this._unit = timeOpts.unit || (tickOpts.autoSkip ? determineUnitForAutoTicks(timeOpts.minUnit, this.min, this.max, this._getLabelCapacity(min)) : determineUnitForFormatting(this, ticks.length, timeOpts.minUnit, this.min, this.max));
                this._majorUnit = !tickOpts.major.enabled || "year" === this._unit ? void 0 : determineMajorUnit(this._unit);
                this.initOffsets(timestamps);
                if (options.reverse) ticks.reverse();
                return ticksFromTimestamps(this, ticks, this._majorUnit);
            }
            initOffsets(timestamps) {
                let start = 0;
                let end = 0;
                let first, last;
                if (this.options.offset && timestamps.length) {
                    first = this.getDecimalForValue(timestamps[0]);
                    if (1 === timestamps.length) start = 1 - first; else start = (this.getDecimalForValue(timestamps[1]) - first) / 2;
                    last = this.getDecimalForValue(timestamps[timestamps.length - 1]);
                    if (1 === timestamps.length) end = last; else end = (last - this.getDecimalForValue(timestamps[timestamps.length - 2])) / 2;
                }
                const limit = timestamps.length < 3 ? .5 : .25;
                start = _limitValue(start, 0, limit);
                end = _limitValue(end, 0, limit);
                this._offsets = {
                    start,
                    end,
                    factor: 1 / (start + 1 + end)
                };
            }
            _generate() {
                const adapter = this._adapter;
                const min = this.min;
                const max = this.max;
                const options = this.options;
                const timeOpts = options.time;
                const minor = timeOpts.unit || determineUnitForAutoTicks(timeOpts.minUnit, min, max, this._getLabelCapacity(min));
                const stepSize = valueOrDefault(timeOpts.stepSize, 1);
                const weekday = "week" === minor ? timeOpts.isoWeekday : false;
                const hasWeekday = isNumber(weekday) || true === weekday;
                const ticks = {};
                let first = min;
                let time, count;
                if (hasWeekday) first = +adapter.startOf(first, "isoWeek", weekday);
                first = +adapter.startOf(first, hasWeekday ? "day" : minor);
                if (adapter.diff(max, min, minor) > 1e5 * stepSize) throw new Error(min + " and " + max + " are too far apart with stepSize of " + stepSize + " " + minor);
                const timestamps = "data" === options.ticks.source && this.getDataTimestamps();
                for (time = first, count = 0; time < max; time = +adapter.add(time, stepSize, minor), 
                count++) addTick(ticks, time, timestamps);
                if (time === max || "ticks" === options.bounds || 1 === count) addTick(ticks, time, timestamps);
                return Object.keys(ticks).sort(((a, b) => a - b)).map((x => +x));
            }
            getLabelForValue(value) {
                const adapter = this._adapter;
                const timeOpts = this.options.time;
                if (timeOpts.tooltipFormat) return adapter.format(value, timeOpts.tooltipFormat);
                return adapter.format(value, timeOpts.displayFormats.datetime);
            }
            _tickFormatFunction(time, index, ticks, format) {
                const options = this.options;
                const formats = options.time.displayFormats;
                const unit = this._unit;
                const majorUnit = this._majorUnit;
                const minorFormat = unit && formats[unit];
                const majorFormat = majorUnit && formats[majorUnit];
                const tick = ticks[index];
                const major = majorUnit && majorFormat && tick && tick.major;
                const label = this._adapter.format(time, format || (major ? majorFormat : minorFormat));
                const formatter = options.ticks.callback;
                return formatter ? callback(formatter, [ label, index, ticks ], this) : label;
            }
            generateTickLabels(ticks) {
                let i, ilen, tick;
                for (i = 0, ilen = ticks.length; i < ilen; ++i) {
                    tick = ticks[i];
                    tick.label = this._tickFormatFunction(tick.value, i, ticks);
                }
            }
            getDecimalForValue(value) {
                return null === value ? NaN : (value - this.min) / (this.max - this.min);
            }
            getPixelForValue(value) {
                const offsets = this._offsets;
                const pos = this.getDecimalForValue(value);
                return this.getPixelForDecimal((offsets.start + pos) * offsets.factor);
            }
            getValueForPixel(pixel) {
                const offsets = this._offsets;
                const pos = this.getDecimalForPixel(pixel) / offsets.factor - offsets.end;
                return this.min + pos * (this.max - this.min);
            }
            _getLabelSize(label) {
                const ticksOpts = this.options.ticks;
                const tickLabelWidth = this.ctx.measureText(label).width;
                const angle = toRadians(this.isHorizontal() ? ticksOpts.maxRotation : ticksOpts.minRotation);
                const cosRotation = Math.cos(angle);
                const sinRotation = Math.sin(angle);
                const tickFontSize = this._resolveTickFontOptions(0).size;
                return {
                    w: tickLabelWidth * cosRotation + tickFontSize * sinRotation,
                    h: tickLabelWidth * sinRotation + tickFontSize * cosRotation
                };
            }
            _getLabelCapacity(exampleTime) {
                const timeOpts = this.options.time;
                const displayFormats = timeOpts.displayFormats;
                const format = displayFormats[timeOpts.unit] || displayFormats.millisecond;
                const exampleLabel = this._tickFormatFunction(exampleTime, 0, ticksFromTimestamps(this, [ exampleTime ], this._majorUnit), format);
                const size = this._getLabelSize(exampleLabel);
                const capacity = Math.floor(this.isHorizontal() ? this.width / size.w : this.height / size.h) - 1;
                return capacity > 0 ? capacity : 1;
            }
            getDataTimestamps() {
                let timestamps = this._cache.data || [];
                let i, ilen;
                if (timestamps.length) return timestamps;
                const metas = this.getMatchingVisibleMetas();
                if (this._normalized && metas.length) return this._cache.data = metas[0].controller.getAllParsedValues(this);
                for (i = 0, ilen = metas.length; i < ilen; ++i) timestamps = timestamps.concat(metas[i].controller.getAllParsedValues(this));
                return this._cache.data = this.normalize(timestamps);
            }
            getLabelTimestamps() {
                const timestamps = this._cache.labels || [];
                let i, ilen;
                if (timestamps.length) return timestamps;
                const labels = this.getLabels();
                for (i = 0, ilen = labels.length; i < ilen; ++i) timestamps.push(parse(this, labels[i]));
                return this._cache.labels = this._normalized ? timestamps : this.normalize(timestamps);
            }
            normalize(values) {
                return _arrayUnique(values.sort(sorter));
            }
        }
        TimeScale.id = "time";
        TimeScale.defaults = {
            bounds: "data",
            adapters: {},
            time: {
                parser: false,
                unit: false,
                round: false,
                isoWeekday: false,
                minUnit: "millisecond",
                displayFormats: {}
            },
            ticks: {
                source: "auto",
                major: {
                    enabled: false
                }
            }
        };
        function interpolate(table, val, reverse) {
            let lo = 0;
            let hi = table.length - 1;
            let prevSource, nextSource, prevTarget, nextTarget;
            if (reverse) {
                if (val >= table[lo].pos && val <= table[hi].pos) ({lo, hi} = _lookupByKey(table, "pos", val));
                ({pos: prevSource, time: prevTarget} = table[lo]);
                ({pos: nextSource, time: nextTarget} = table[hi]);
            } else {
                if (val >= table[lo].time && val <= table[hi].time) ({lo, hi} = _lookupByKey(table, "time", val));
                ({time: prevSource, pos: prevTarget} = table[lo]);
                ({time: nextSource, pos: nextTarget} = table[hi]);
            }
            const span = nextSource - prevSource;
            return span ? prevTarget + (nextTarget - prevTarget) * (val - prevSource) / span : prevTarget;
        }
        class TimeSeriesScale extends TimeScale {
            constructor(props) {
                super(props);
                this._table = [];
                this._minPos = void 0;
                this._tableRange = void 0;
            }
            initOffsets() {
                const timestamps = this._getTimestampsForTable();
                const table = this._table = this.buildLookupTable(timestamps);
                this._minPos = interpolate(table, this.min);
                this._tableRange = interpolate(table, this.max) - this._minPos;
                super.initOffsets(timestamps);
            }
            buildLookupTable(timestamps) {
                const {min, max} = this;
                const items = [];
                const table = [];
                let i, ilen, prev, curr, next;
                for (i = 0, ilen = timestamps.length; i < ilen; ++i) {
                    curr = timestamps[i];
                    if (curr >= min && curr <= max) items.push(curr);
                }
                if (items.length < 2) return [ {
                    time: min,
                    pos: 0
                }, {
                    time: max,
                    pos: 1
                } ];
                for (i = 0, ilen = items.length; i < ilen; ++i) {
                    next = items[i + 1];
                    prev = items[i - 1];
                    curr = items[i];
                    if (Math.round((next + prev) / 2) !== curr) table.push({
                        time: curr,
                        pos: i / (ilen - 1)
                    });
                }
                return table;
            }
            _getTimestampsForTable() {
                let timestamps = this._cache.all || [];
                if (timestamps.length) return timestamps;
                const data = this.getDataTimestamps();
                const label = this.getLabelTimestamps();
                if (data.length && label.length) timestamps = this.normalize(data.concat(label)); else timestamps = data.length ? data : label;
                timestamps = this._cache.all = timestamps;
                return timestamps;
            }
            getDecimalForValue(value) {
                return (interpolate(this._table, value) - this._minPos) / this._tableRange;
            }
            getValueForPixel(pixel) {
                const offsets = this._offsets;
                const decimal = this.getDecimalForPixel(pixel) / offsets.factor - offsets.end;
                return interpolate(this._table, decimal * this._tableRange + this._minPos, true);
            }
        }
        TimeSeriesScale.id = "timeseries";
        TimeSeriesScale.defaults = TimeScale.defaults;
        class Popup {
            constructor(options) {
                let config = {
                    logging: true,
                    init: true,
                    attributeOpenButton: "data-popup",
                    attributeCloseButton: "data-close",
                    fixElementSelector: "[data-lp]",
                    youtubeAttribute: "data-youtube",
                    youtubePlaceAttribute: "data-youtube-place",
                    setAutoplayYoutube: true,
                    classes: {
                        popup: "popup",
                        popupContent: "popup__content",
                        popupActive: "popup_show",
                        bodyActive: "popup-show"
                    },
                    focusCatch: true,
                    closeEsc: true,
                    bodyLock: true,
                    hashSettings: {
                        location: true,
                        goHash: true
                    },
                    on: {
                        beforeOpen: function() {},
                        afterOpen: function() {},
                        beforeClose: function() {},
                        afterClose: function() {}
                    }
                };
                this.isOpen = false;
                this.targetOpen = {
                    selector: false,
                    element: false
                };
                this.previousOpen = {
                    selector: false,
                    element: false
                };
                this.lastClosed = {
                    selector: false,
                    element: false
                };
                this._dataValue = false;
                this.hash = false;
                this._reopen = false;
                this._selectorOpen = false;
                this.lastFocusEl = false;
                this._focusEl = [ "a[href]", 'input:not([disabled]):not([type="hidden"]):not([aria-hidden])', "button:not([disabled]):not([aria-hidden])", "select:not([disabled]):not([aria-hidden])", "textarea:not([disabled]):not([aria-hidden])", "area[href]", "iframe", "object", "embed", "[contenteditable]", '[tabindex]:not([tabindex^="-"])' ];
                this.options = {
                    ...config,
                    ...options,
                    classes: {
                        ...config.classes,
                        ...options?.classes
                    },
                    hashSettings: {
                        ...config.hashSettings,
                        ...options?.hashSettings
                    },
                    on: {
                        ...config.on,
                        ...options?.on
                    }
                };
                this.bodyLock = false;
                this.options.init ? this.initPopups() : null;
            }
            initPopups() {
                this.popupLogging(`Проснулся`);
                this.eventsPopup();
            }
            eventsPopup() {
                document.addEventListener("click", function(e) {
                    const buttonOpen = e.target.closest(`[${this.options.attributeOpenButton}]`);
                    if (buttonOpen) {
                        e.preventDefault();
                        this._dataValue = buttonOpen.getAttribute(this.options.attributeOpenButton) ? buttonOpen.getAttribute(this.options.attributeOpenButton) : "error";
                        if ("error" !== this._dataValue) {
                            if (!this.isOpen) this.lastFocusEl = buttonOpen;
                            this.targetOpen.selector = `${this._dataValue}`;
                            this._selectorOpen = true;
                            this.open();
                            return;
                        } else this.popupLogging(`Ой ой, не заполнен атрибут у ${buttonOpen.classList}`);
                        return;
                    }
                    const buttonClose = e.target.closest(`[${this.options.attributeCloseButton}]`);
                    if (buttonClose || !e.target.closest(`.${this.options.classes.popupContent}`) && this.isOpen) {
                        e.preventDefault();
                        this.close();
                        return;
                    }
                }.bind(this));
                document.addEventListener("keydown", function(e) {
                    if (this.options.closeEsc && 27 == e.which && "Escape" === e.code && this.isOpen) {
                        e.preventDefault();
                        this.close();
                        return;
                    }
                    if (this.options.focusCatch && 9 == e.which && this.isOpen) {
                        this._focusCatch(e);
                        return;
                    }
                }.bind(this));
                if (this.options.hashSettings.goHash) {
                    window.addEventListener("hashchange", function() {
                        if (window.location.hash) this._openToHash(); else this.close(this.targetOpen.selector);
                    }.bind(this));
                    window.addEventListener("load", function() {
                        if (window.location.hash) this._openToHash();
                    }.bind(this));
                }
            }
            open(selectorValue) {
                if (bodyLockStatus) {
                    this.bodyLock = document.documentElement.classList.contains("lock") ? true : false;
                    if (selectorValue && "string" === typeof selectorValue && "" !== selectorValue.trim()) {
                        this.targetOpen.selector = selectorValue;
                        this._selectorOpen = true;
                    }
                    if (this.isOpen) {
                        this._reopen = true;
                        this.close();
                    }
                    if (!this._selectorOpen) this.targetOpen.selector = this.lastClosed.selector;
                    if (!this._reopen) this.previousActiveElement = document.activeElement;
                    this.targetOpen.element = document.querySelector(this.targetOpen.selector);
                    if (this.targetOpen.element) {
                        if (this.targetOpen.element.hasAttribute(this.options.youtubeAttribute)) {
                            const codeVideo = this.targetOpen.element.getAttribute(this.options.youtubeAttribute);
                            const urlVideo = `https://www.youtube.com/embed/${codeVideo}?rel=0&showinfo=0&autoplay=1`;
                            const iframe = document.createElement("iframe");
                            iframe.setAttribute("allowfullscreen", "");
                            const autoplay = this.options.setAutoplayYoutube ? "autoplay;" : "";
                            iframe.setAttribute("allow", `${autoplay}; encrypted-media`);
                            iframe.setAttribute("src", urlVideo);
                            if (this.targetOpen.element.querySelector(`[${this.options.youtubePlaceAttribute}]`)) this.targetOpen.element.querySelector(`[${this.options.youtubePlaceAttribute}]`).appendChild(iframe);
                        }
                        if (this.options.hashSettings.location) {
                            this._getHash();
                            this._setHash();
                        }
                        this.options.on.beforeOpen(this);
                        document.dispatchEvent(new CustomEvent("beforePopupOpen", {
                            detail: {
                                popup: this
                            }
                        }));
                        this.targetOpen.element.classList.add(this.options.classes.popupActive);
                        document.documentElement.classList.add(this.options.classes.bodyActive);
                        if (!this._reopen) !this.bodyLock ? bodyLock() : null; else this._reopen = false;
                        this.targetOpen.element.setAttribute("aria-hidden", "false");
                        this.previousOpen.selector = this.targetOpen.selector;
                        this.previousOpen.element = this.targetOpen.element;
                        this._selectorOpen = false;
                        this.isOpen = true;
                        setTimeout((() => {
                            this._focusTrap();
                        }), 50);
                        this.options.on.afterOpen(this);
                        document.dispatchEvent(new CustomEvent("afterPopupOpen", {
                            detail: {
                                popup: this
                            }
                        }));
                        this.popupLogging(`Открыл попап`);
                    } else this.popupLogging(`Ой ой, такого попапа нет. Проверьте корректность ввода. `);
                }
            }
            close(selectorValue) {
                if (selectorValue && "string" === typeof selectorValue && "" !== selectorValue.trim()) this.previousOpen.selector = selectorValue;
                if (!this.isOpen || !bodyLockStatus) return;
                this.options.on.beforeClose(this);
                document.dispatchEvent(new CustomEvent("beforePopupClose", {
                    detail: {
                        popup: this
                    }
                }));
                if (this.targetOpen.element.hasAttribute(this.options.youtubeAttribute)) if (this.targetOpen.element.querySelector(`[${this.options.youtubePlaceAttribute}]`)) this.targetOpen.element.querySelector(`[${this.options.youtubePlaceAttribute}]`).innerHTML = "";
                this.previousOpen.element.classList.remove(this.options.classes.popupActive);
                this.previousOpen.element.setAttribute("aria-hidden", "true");
                if (!this._reopen) {
                    document.documentElement.classList.remove(this.options.classes.bodyActive);
                    !this.bodyLock ? bodyUnlock() : null;
                    this.isOpen = false;
                }
                this._removeHash();
                if (this._selectorOpen) {
                    this.lastClosed.selector = this.previousOpen.selector;
                    this.lastClosed.element = this.previousOpen.element;
                }
                this.options.on.afterClose(this);
                document.dispatchEvent(new CustomEvent("afterPopupClose", {
                    detail: {
                        popup: this
                    }
                }));
                setTimeout((() => {
                    this._focusTrap();
                }), 50);
                this.popupLogging(`Закрыл попап`);
            }
            _getHash() {
                if (this.options.hashSettings.location) this.hash = this.targetOpen.selector.includes("#") ? this.targetOpen.selector : this.targetOpen.selector.replace(".", "#");
            }
            _openToHash() {
                let classInHash = document.querySelector(`.${window.location.hash.replace("#", "")}`) ? `.${window.location.hash.replace("#", "")}` : document.querySelector(`${window.location.hash}`) ? `${window.location.hash}` : null;
                const buttons = document.querySelector(`[${this.options.attributeOpenButton}="${classInHash}"]`) ? document.querySelector(`[${this.options.attributeOpenButton}="${classInHash}"]`) : document.querySelector(`[${this.options.attributeOpenButton}="${classInHash.replace(".", "#")}"]`);
                if (buttons && classInHash) this.open(classInHash);
            }
            _setHash() {
                history.pushState("", "", this.hash);
            }
            _removeHash() {
                history.pushState("", "", window.location.href.split("#")[0]);
            }
            _focusCatch(e) {
                const focusable = this.targetOpen.element.querySelectorAll(this._focusEl);
                const focusArray = Array.prototype.slice.call(focusable);
                const focusedIndex = focusArray.indexOf(document.activeElement);
                if (e.shiftKey && 0 === focusedIndex) {
                    focusArray[focusArray.length - 1].focus();
                    e.preventDefault();
                }
                if (!e.shiftKey && focusedIndex === focusArray.length - 1) {
                    focusArray[0].focus();
                    e.preventDefault();
                }
            }
            _focusTrap() {
                const focusable = this.previousOpen.element.querySelectorAll(this._focusEl);
                if (!this.isOpen && this.lastFocusEl) this.lastFocusEl.focus(); else focusable[0].focus();
            }
            popupLogging(message) {
                this.options.logging ? FLS(`[Попапос]: ${message}`) : null;
            }
        }
        flsModules.popup = new Popup({});
        let gotoblock_gotoBlock = (targetBlock, noHeader = false, speed = 500, offsetTop = 0) => {
            const targetBlockElement = document.querySelector(targetBlock);
            if (targetBlockElement) {
                let headerItem = "";
                let headerItemHeight = 0;
                if (noHeader) {
                    headerItem = "header.header";
                    headerItemHeight = document.querySelector(headerItem).offsetHeight;
                }
                let options = {
                    speedAsDuration: true,
                    speed,
                    header: headerItem,
                    offset: offsetTop,
                    easing: "easeOutQuad"
                };
                document.documentElement.classList.contains("menu-open") ? menuClose() : null;
                if ("undefined" !== typeof SmoothScroll) (new SmoothScroll).animateScroll(targetBlockElement, "", options); else {
                    let targetBlockElementPosition = targetBlockElement.getBoundingClientRect().top + scrollY;
                    targetBlockElementPosition = headerItemHeight ? targetBlockElementPosition - headerItemHeight : targetBlockElementPosition;
                    targetBlockElementPosition = offsetTop ? targetBlockElementPosition - offsetTop : targetBlockElementPosition;
                    window.scrollTo({
                        top: targetBlockElementPosition,
                        behavior: "smooth"
                    });
                }
                FLS(`[gotoBlock]: Юхуу...едем к ${targetBlock}`);
            } else FLS(`[gotoBlock]: Ой ой..Такого блока нет на странице: ${targetBlock}`);
        };
        function formFieldsInit(options = {
            viewPass: false
        }) {
            const formFields = document.querySelectorAll("input[placeholder],textarea[placeholder]");
            if (formFields.length) formFields.forEach((formField => {
                if (!formField.hasAttribute("data-placeholder-nohide")) formField.dataset.placeholder = formField.placeholder;
            }));
            document.body.addEventListener("focusin", (function(e) {
                const targetElement = e.target;
                if ("INPUT" === targetElement.tagName || "TEXTAREA" === targetElement.tagName) {
                    if (targetElement.dataset.placeholder) targetElement.placeholder = "";
                    if (!targetElement.hasAttribute("data-no-focus-classes")) {
                        targetElement.classList.add("_form-focus");
                        targetElement.parentElement.classList.add("_form-focus");
                    }
                    formValidate.removeError(targetElement);
                }
            }));
            document.body.addEventListener("focusout", (function(e) {
                const targetElement = e.target;
                if ("INPUT" === targetElement.tagName || "TEXTAREA" === targetElement.tagName) {
                    if (targetElement.dataset.placeholder) targetElement.placeholder = targetElement.dataset.placeholder;
                    if (!targetElement.hasAttribute("data-no-focus-classes")) {
                        targetElement.classList.remove("_form-focus");
                        targetElement.parentElement.classList.remove("_form-focus");
                    }
                    if (targetElement.hasAttribute("data-validate")) formValidate.validateInput(targetElement);
                }
            }));
            if (options.viewPass) document.addEventListener("click", (function(e) {
                let targetElement = e.target;
                if (targetElement.closest('[class*="__viewpass"]')) {
                    let inputType = targetElement.classList.contains("_viewpass-active") ? "password" : "text";
                    targetElement.parentElement.querySelector("input").setAttribute("type", inputType);
                    targetElement.classList.toggle("_viewpass-active");
                }
            }));
        }
        let formValidate = {
            getErrors(form) {
                let error = 0;
                let formRequiredItems = form.querySelectorAll("*[data-required]");
                if (formRequiredItems.length) formRequiredItems.forEach((formRequiredItem => {
                    if ((null !== formRequiredItem.offsetParent || "SELECT" === formRequiredItem.tagName) && !formRequiredItem.disabled) error += this.validateInput(formRequiredItem);
                }));
                return error;
            },
            validateInput(formRequiredItem) {
                let error = 0;
                if ("email" === formRequiredItem.dataset.required) {
                    formRequiredItem.value = formRequiredItem.value.replace(" ", "");
                    if (this.emailTest(formRequiredItem)) {
                        this.addError(formRequiredItem);
                        error++;
                    } else this.removeError(formRequiredItem);
                } else if ("checkbox" === formRequiredItem.type && !formRequiredItem.checked) {
                    this.addError(formRequiredItem);
                    error++;
                } else if (!formRequiredItem.value) {
                    this.addError(formRequiredItem);
                    error++;
                } else this.removeError(formRequiredItem);
                return error;
            },
            addError(formRequiredItem) {
                formRequiredItem.classList.add("_form-error");
                formRequiredItem.parentElement.classList.add("_form-error");
                let inputError = formRequiredItem.parentElement.querySelector(".form__error");
                if (inputError) formRequiredItem.parentElement.removeChild(inputError);
                if (formRequiredItem.dataset.error) formRequiredItem.parentElement.insertAdjacentHTML("beforeend", `<div class="form__error">${formRequiredItem.dataset.error}</div>`);
            },
            removeError(formRequiredItem) {
                formRequiredItem.classList.remove("_form-error");
                formRequiredItem.parentElement.classList.remove("_form-error");
                if (formRequiredItem.parentElement.querySelector(".form__error")) formRequiredItem.parentElement.removeChild(formRequiredItem.parentElement.querySelector(".form__error"));
            },
            formClean(form) {
                form.reset();
                setTimeout((() => {
                    let inputs = form.querySelectorAll("input,textarea");
                    for (let index = 0; index < inputs.length; index++) {
                        const el = inputs[index];
                        el.parentElement.classList.remove("_form-focus");
                        el.classList.remove("_form-focus");
                        formValidate.removeError(el);
                    }
                    let checkboxes = form.querySelectorAll(".checkbox__input");
                    if (checkboxes.length > 0) for (let index = 0; index < checkboxes.length; index++) {
                        const checkbox = checkboxes[index];
                        checkbox.checked = false;
                    }
                    if (flsModules.select) {
                        let selects = form.querySelectorAll(".select");
                        if (selects.length) for (let index = 0; index < selects.length; index++) {
                            const select = selects[index].querySelector("select");
                            flsModules.select.selectBuild(select);
                        }
                    }
                }), 0);
            },
            emailTest(formRequiredItem) {
                return !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(formRequiredItem.value);
            }
        };
        function formSubmit(options = {
            validate: true
        }) {
            const forms = document.forms;
            if (forms.length) for (const form of forms) {
                form.addEventListener("submit", (function(e) {
                    const form = e.target;
                    formSubmitAction(form, e);
                }));
                form.addEventListener("reset", (function(e) {
                    const form = e.target;
                    formValidate.formClean(form);
                }));
            }
            async function formSubmitAction(form, e) {
                const error = !form.hasAttribute("data-no-validate") ? formValidate.getErrors(form) : 0;
                if (0 === error) {
                    const ajax = form.hasAttribute("data-ajax");
                    if (ajax) {
                        e.preventDefault();
                        const formAction = form.getAttribute("action") ? form.getAttribute("action").trim() : "#";
                        const formMethod = form.getAttribute("method") ? form.getAttribute("method").trim() : "GET";
                        const formData = new FormData(form);
                        form.classList.add("_sending");
                        const response = await fetch(formAction, {
                            method: formMethod,
                            body: formData
                        });
                        if (response.ok) {
                            let responseResult = await response.json();
                            form.classList.remove("_sending");
                            formSent(form);
                            formResult(responseResult);
                        } else {
                            alert("Ошибка");
                            form.classList.remove("_sending");
                        }
                    } else if (form.hasAttribute("data-dev")) {
                        e.preventDefault();
                        formSent(form);
                    }
                } else {
                    e.preventDefault();
                    const formError = form.querySelector("._form-error");
                    if (formError && form.hasAttribute("data-goto-error")) gotoblock_gotoBlock(formError, true, 1e3);
                }
            }
            function formSent(form) {
                document.dispatchEvent(new CustomEvent("formSent", {
                    detail: {
                        form
                    }
                }));
                setTimeout((() => {
                    if (flsModules.popup) {
                        const popup = form.dataset.popupMessage;
                        popup ? flsModules.popup.open(popup) : null;
                    }
                }), 0);
                formValidate.formClean(form);
                formLogging(`Форма отправлена!`);
            }
            function formLogging(message) {
                FLS(`[Формы]: ${message}`);
            }
        }
        class SelectConstructor {
            constructor(props, data = null) {
                let defaultConfig = {
                    init: true,
                    logging: true
                };
                this.config = Object.assign(defaultConfig, props);
                this.selectClasses = {
                    classSelect: "select",
                    classSelectBody: "select__body",
                    classSelectTitle: "select__title",
                    classSelectValue: "select__value",
                    classSelectLabel: "select__label",
                    classSelectInput: "select__input",
                    classSelectText: "select__text",
                    classSelectLink: "select__link",
                    classSelectOptions: "select__options",
                    classSelectOptionsScroll: "select__scroll",
                    classSelectOption: "select__option",
                    classSelectContent: "select__content",
                    classSelectRow: "select__row",
                    classSelectData: "select__asset",
                    classSelectDisabled: "_select-disabled",
                    classSelectTag: "_select-tag",
                    classSelectOpen: "_select-open",
                    classSelectActive: "_select-active",
                    classSelectFocus: "_select-focus",
                    classSelectMultiple: "_select-multiple",
                    classSelectCheckBox: "_select-checkbox",
                    classSelectOptionSelected: "_select-selected",
                    classSelectPseudoLabel: "_select-pseudo-label"
                };
                this._this = this;
                if (this.config.init) {
                    const selectItems = data ? document.querySelectorAll(data) : document.querySelectorAll("select");
                    if (selectItems.length) {
                        this.selectsInit(selectItems);
                        this.setLogging(`Проснулся, построил селектов: (${selectItems.length})`);
                    } else this.setLogging("Сплю, нет ни одного select zzZZZzZZz");
                }
            }
            getSelectClass(className) {
                return `.${className}`;
            }
            getSelectElement(selectItem, className) {
                return {
                    originalSelect: selectItem.querySelector("select"),
                    selectElement: selectItem.querySelector(this.getSelectClass(className))
                };
            }
            selectsInit(selectItems) {
                selectItems.forEach(((originalSelect, index) => {
                    this.selectInit(originalSelect, index + 1);
                }));
                document.addEventListener("click", function(e) {
                    this.selectsActions(e);
                }.bind(this));
                document.addEventListener("keydown", function(e) {
                    this.selectsActions(e);
                }.bind(this));
                document.addEventListener("focusin", function(e) {
                    this.selectsActions(e);
                }.bind(this));
                document.addEventListener("focusout", function(e) {
                    this.selectsActions(e);
                }.bind(this));
            }
            selectInit(originalSelect, index) {
                const _this = this;
                let selectItem = document.createElement("div");
                selectItem.classList.add(this.selectClasses.classSelect);
                originalSelect.parentNode.insertBefore(selectItem, originalSelect);
                selectItem.appendChild(originalSelect);
                originalSelect.hidden = true;
                index ? originalSelect.dataset.id = index : null;
                if (this.getSelectPlaceholder(originalSelect)) {
                    originalSelect.dataset.placeholder = this.getSelectPlaceholder(originalSelect).value;
                    if (this.getSelectPlaceholder(originalSelect).label.show) {
                        const selectItemTitle = this.getSelectElement(selectItem, this.selectClasses.classSelectTitle).selectElement;
                        selectItemTitle.insertAdjacentHTML("afterbegin", `<span class="${this.selectClasses.classSelectLabel}">${this.getSelectPlaceholder(originalSelect).label.text ? this.getSelectPlaceholder(originalSelect).label.text : this.getSelectPlaceholder(originalSelect).value}</span>`);
                    }
                }
                selectItem.insertAdjacentHTML("beforeend", `<div class="${this.selectClasses.classSelectBody}"><div hidden class="${this.selectClasses.classSelectOptions}"></div></div>`);
                this.selectBuild(originalSelect);
                originalSelect.dataset.speed = originalSelect.dataset.speed ? originalSelect.dataset.speed : "150";
                originalSelect.addEventListener("change", (function(e) {
                    _this.selectChange(e);
                }));
            }
            selectBuild(originalSelect) {
                const selectItem = originalSelect.parentElement;
                selectItem.dataset.id = originalSelect.dataset.id;
                selectItem.classList.add(originalSelect.getAttribute("class") ? `select_${originalSelect.getAttribute("class")}` : "");
                originalSelect.multiple ? selectItem.classList.add(this.selectClasses.classSelectMultiple) : selectItem.classList.remove(this.selectClasses.classSelectMultiple);
                originalSelect.hasAttribute("data-checkbox") && originalSelect.multiple ? selectItem.classList.add(this.selectClasses.classSelectCheckBox) : selectItem.classList.remove(this.selectClasses.classSelectCheckBox);
                this.setSelectTitleValue(selectItem, originalSelect);
                this.setOptions(selectItem, originalSelect);
                originalSelect.hasAttribute("data-search") ? this.searchActions(selectItem) : null;
                originalSelect.hasAttribute("data-open") ? this.selectAction(selectItem) : null;
                this.selectDisabled(selectItem, originalSelect);
            }
            selectsActions(e) {
                const targetElement = e.target;
                const targetType = e.type;
                if (targetElement.closest(this.getSelectClass(this.selectClasses.classSelect)) || targetElement.closest(this.getSelectClass(this.selectClasses.classSelectTag))) {
                    const selectItem = targetElement.closest(".select") ? targetElement.closest(".select") : document.querySelector(`.${this.selectClasses.classSelect}[data-id="${targetElement.closest(this.getSelectClass(this.selectClasses.classSelectTag)).dataset.selectId}"]`);
                    const originalSelect = this.getSelectElement(selectItem).originalSelect;
                    if ("click" === targetType) {
                        if (!originalSelect.disabled) if (targetElement.closest(this.getSelectClass(this.selectClasses.classSelectTag))) {
                            const targetTag = targetElement.closest(this.getSelectClass(this.selectClasses.classSelectTag));
                            const optionItem = document.querySelector(`.${this.selectClasses.classSelect}[data-id="${targetTag.dataset.selectId}"] .select__option[data-value="${targetTag.dataset.value}"]`);
                            this.optionAction(selectItem, originalSelect, optionItem);
                        } else if (targetElement.closest(this.getSelectClass(this.selectClasses.classSelectTitle))) this.selectAction(selectItem); else if (targetElement.closest(this.getSelectClass(this.selectClasses.classSelectOption))) {
                            const optionItem = targetElement.closest(this.getSelectClass(this.selectClasses.classSelectOption));
                            this.optionAction(selectItem, originalSelect, optionItem);
                        }
                    } else if ("focusin" === targetType || "focusout" === targetType) {
                        if (targetElement.closest(this.getSelectClass(this.selectClasses.classSelect))) "focusin" === targetType ? selectItem.classList.add(this.selectClasses.classSelectFocus) : selectItem.classList.remove(this.selectClasses.classSelectFocus);
                    } else if ("keydown" === targetType && "Escape" === e.code) this.selectsСlose();
                } else this.selectsСlose();
            }
            selectsСlose(selectOneGroup) {
                const selectsGroup = selectOneGroup ? selectOneGroup : document;
                const selectActiveItems = selectsGroup.querySelectorAll(`${this.getSelectClass(this.selectClasses.classSelect)}${this.getSelectClass(this.selectClasses.classSelectOpen)}`);
                if (selectActiveItems.length) selectActiveItems.forEach((selectActiveItem => {
                    this.selectСlose(selectActiveItem);
                }));
            }
            selectСlose(selectItem) {
                const originalSelect = this.getSelectElement(selectItem).originalSelect;
                const selectOptions = this.getSelectElement(selectItem, this.selectClasses.classSelectOptions).selectElement;
                if (!selectOptions.classList.contains("_slide")) {
                    selectItem.classList.remove(this.selectClasses.classSelectOpen);
                    _slideUp(selectOptions, originalSelect.dataset.speed);
                }
            }
            selectAction(selectItem) {
                const originalSelect = this.getSelectElement(selectItem).originalSelect;
                const selectOptions = this.getSelectElement(selectItem, this.selectClasses.classSelectOptions).selectElement;
                if (originalSelect.closest("[data-one-select]")) {
                    const selectOneGroup = originalSelect.closest("[data-one-select]");
                    this.selectsСlose(selectOneGroup);
                }
                if (!selectOptions.classList.contains("_slide")) {
                    selectItem.classList.toggle(this.selectClasses.classSelectOpen);
                    _slideToggle(selectOptions, originalSelect.dataset.speed);
                }
            }
            setSelectTitleValue(selectItem, originalSelect) {
                const selectItemBody = this.getSelectElement(selectItem, this.selectClasses.classSelectBody).selectElement;
                const selectItemTitle = this.getSelectElement(selectItem, this.selectClasses.classSelectTitle).selectElement;
                if (selectItemTitle) selectItemTitle.remove();
                selectItemBody.insertAdjacentHTML("afterbegin", this.getSelectTitleValue(selectItem, originalSelect));
            }
            getSelectTitleValue(selectItem, originalSelect) {
                let selectTitleValue = this.getSelectedOptionsData(originalSelect, 2).html;
                if (originalSelect.multiple && originalSelect.hasAttribute("data-tags")) {
                    selectTitleValue = this.getSelectedOptionsData(originalSelect).elements.map((option => `<span role="button" data-select-id="${selectItem.dataset.id}" data-value="${option.value}" class="_select-tag">${this.getSelectElementContent(option)}</span>`)).join("");
                    if (originalSelect.dataset.tags && document.querySelector(originalSelect.dataset.tags)) {
                        document.querySelector(originalSelect.dataset.tags).innerHTML = selectTitleValue;
                        if (originalSelect.hasAttribute("data-search")) selectTitleValue = false;
                    }
                }
                selectTitleValue = selectTitleValue.length ? selectTitleValue : originalSelect.dataset.placeholder ? originalSelect.dataset.placeholder : "";
                let pseudoAttribute = "";
                let pseudoAttributeClass = "";
                if (originalSelect.hasAttribute("data-pseudo-label")) {
                    pseudoAttribute = originalSelect.dataset.pseudoLabel ? ` data-pseudo-label="${originalSelect.dataset.pseudoLabel}"` : ` data-pseudo-label="Заполните атрибут"`;
                    pseudoAttributeClass = ` ${this.selectClasses.classSelectPseudoLabel}`;
                }
                this.getSelectedOptionsData(originalSelect).values.length ? selectItem.classList.add(this.selectClasses.classSelectActive) : selectItem.classList.remove(this.selectClasses.classSelectActive);
                if (originalSelect.hasAttribute("data-search")) return `<div class="${this.selectClasses.classSelectTitle}"><span${pseudoAttribute} class="${this.selectClasses.classSelectValue}"><input autocomplete="off" type="text" placeholder="${selectTitleValue}" data-placeholder="${selectTitleValue}" class="${this.selectClasses.classSelectInput}"></span></div>`; else {
                    const customClass = this.getSelectedOptionsData(originalSelect).elements.length && this.getSelectedOptionsData(originalSelect).elements[0].dataset.class ? ` ${this.getSelectedOptionsData(originalSelect).elements[0].dataset.class}` : "";
                    return `<button type="button" class="${this.selectClasses.classSelectTitle}"><span${pseudoAttribute} class="${this.selectClasses.classSelectValue}${pseudoAttributeClass}"><span class="${this.selectClasses.classSelectContent}${customClass}">${selectTitleValue}</span></span></button>`;
                }
            }
            getSelectElementContent(selectOption) {
                const selectOptionData = selectOption.dataset.asset ? `${selectOption.dataset.asset}` : "";
                const selectOptionDataHTML = selectOptionData.indexOf("img") >= 0 ? `<img src="${selectOptionData}" alt="">` : selectOptionData;
                let selectOptionContentHTML = ``;
                selectOptionContentHTML += selectOptionData ? `<span class="${this.selectClasses.classSelectRow}">` : "";
                selectOptionContentHTML += selectOptionData ? `<span class="${this.selectClasses.classSelectData}">` : "";
                selectOptionContentHTML += selectOptionData ? selectOptionDataHTML : "";
                selectOptionContentHTML += selectOptionData ? `</span>` : "";
                selectOptionContentHTML += selectOptionData ? `<span class="${this.selectClasses.classSelectText}">` : "";
                selectOptionContentHTML += selectOption.textContent;
                selectOptionContentHTML += selectOptionData ? `</span>` : "";
                selectOptionContentHTML += selectOptionData ? `</span>` : "";
                return selectOptionContentHTML;
            }
            getSelectPlaceholder(originalSelect) {
                const selectPlaceholder = Array.from(originalSelect.options).find((option => !option.value));
                if (selectPlaceholder) return {
                    value: selectPlaceholder.textContent,
                    show: selectPlaceholder.hasAttribute("data-show"),
                    label: {
                        show: selectPlaceholder.hasAttribute("data-label"),
                        text: selectPlaceholder.dataset.label
                    }
                };
            }
            getSelectedOptionsData(originalSelect, type) {
                let selectedOptions = [];
                if (originalSelect.multiple) selectedOptions = Array.from(originalSelect.options).filter((option => option.value)).filter((option => option.selected)); else selectedOptions.push(originalSelect.options[originalSelect.selectedIndex]);
                return {
                    elements: selectedOptions.map((option => option)),
                    values: selectedOptions.filter((option => option.value)).map((option => option.value)),
                    html: selectedOptions.map((option => this.getSelectElementContent(option)))
                };
            }
            getOptions(originalSelect) {
                let selectOptionsScroll = originalSelect.hasAttribute("data-scroll") ? `data-simplebar` : "";
                let selectOptionsScrollHeight = originalSelect.dataset.scroll ? `style="max-height:${originalSelect.dataset.scroll}px"` : "";
                let selectOptions = Array.from(originalSelect.options);
                if (selectOptions.length > 0) {
                    let selectOptionsHTML = ``;
                    if (this.getSelectPlaceholder(originalSelect) && !this.getSelectPlaceholder(originalSelect).show || originalSelect.multiple) selectOptions = selectOptions.filter((option => option.value));
                    selectOptionsHTML += selectOptionsScroll ? `<div ${selectOptionsScroll} ${selectOptionsScrollHeight} class="${this.selectClasses.classSelectOptionsScroll}">` : "";
                    selectOptions.forEach((selectOption => {
                        selectOptionsHTML += this.getOption(selectOption, originalSelect);
                    }));
                    selectOptionsHTML += selectOptionsScroll ? `</div>` : "";
                    return selectOptionsHTML;
                }
            }
            getOption(selectOption, originalSelect) {
                const selectOptionSelected = selectOption.selected && originalSelect.multiple ? ` ${this.selectClasses.classSelectOptionSelected}` : "";
                const selectOptionHide = selectOption.selected && !originalSelect.hasAttribute("data-show-selected") && !originalSelect.multiple ? `hidden` : ``;
                const selectOptionClass = selectOption.dataset.class ? ` ${selectOption.dataset.class}` : "";
                const selectOptionLink = selectOption.dataset.href ? selectOption.dataset.href : false;
                const selectOptionLinkTarget = selectOption.hasAttribute("data-href-blank") ? `target="_blank"` : "";
                let selectOptionHTML = ``;
                selectOptionHTML += selectOptionLink ? `<a ${selectOptionLinkTarget} ${selectOptionHide} href="${selectOptionLink}" data-value="${selectOption.value}" class="${this.selectClasses.classSelectOption}${selectOptionClass}${selectOptionSelected}">` : `<button ${selectOptionHide} class="${this.selectClasses.classSelectOption}${selectOptionClass}${selectOptionSelected}" data-value="${selectOption.value}" type="button">`;
                selectOptionHTML += this.getSelectElementContent(selectOption);
                selectOptionHTML += selectOptionLink ? `</a>` : `</button>`;
                return selectOptionHTML;
            }
            setOptions(selectItem, originalSelect) {
                const selectItemOptions = this.getSelectElement(selectItem, this.selectClasses.classSelectOptions).selectElement;
                selectItemOptions.innerHTML = this.getOptions(originalSelect);
            }
            optionAction(selectItem, originalSelect, optionItem) {
                if (originalSelect.multiple) {
                    optionItem.classList.toggle(this.selectClasses.classSelectOptionSelected);
                    const originalSelectSelectedItems = this.getSelectedOptionsData(originalSelect).elements;
                    originalSelectSelectedItems.forEach((originalSelectSelectedItem => {
                        originalSelectSelectedItem.removeAttribute("selected");
                    }));
                    const selectSelectedItems = selectItem.querySelectorAll(this.getSelectClass(this.selectClasses.classSelectOptionSelected));
                    selectSelectedItems.forEach((selectSelectedItems => {
                        originalSelect.querySelector(`option[value="${selectSelectedItems.dataset.value}"]`).setAttribute("selected", "selected");
                    }));
                } else {
                    if (!originalSelect.hasAttribute("data-show-selected")) {
                        if (selectItem.querySelector(`${this.getSelectClass(this.selectClasses.classSelectOption)}[hidden]`)) selectItem.querySelector(`${this.getSelectClass(this.selectClasses.classSelectOption)}[hidden]`).hidden = false;
                        optionItem.hidden = true;
                    }
                    originalSelect.value = optionItem.hasAttribute("data-value") ? optionItem.dataset.value : optionItem.textContent;
                    this.selectAction(selectItem);
                }
                this.setSelectTitleValue(selectItem, originalSelect);
                this.setSelectChange(originalSelect);
            }
            selectChange(e) {
                const originalSelect = e.target;
                this.selectBuild(originalSelect);
                this.setSelectChange(originalSelect);
            }
            setSelectChange(originalSelect) {
                if (originalSelect.hasAttribute("data-validate")) formValidate.validateInput(originalSelect);
                if (originalSelect.hasAttribute("data-submit") && originalSelect.value) {
                    let tempButton = document.createElement("button");
                    tempButton.type = "submit";
                    originalSelect.closest("form").append(tempButton);
                    tempButton.click();
                    tempButton.remove();
                }
                const selectItem = originalSelect.parentElement;
                this.selectCallback(selectItem, originalSelect);
            }
            selectDisabled(selectItem, originalSelect) {
                if (originalSelect.disabled) {
                    selectItem.classList.add(this.selectClasses.classSelectDisabled);
                    this.getSelectElement(selectItem, this.selectClasses.classSelectTitle).selectElement.disabled = true;
                } else {
                    selectItem.classList.remove(this.selectClasses.classSelectDisabled);
                    this.getSelectElement(selectItem, this.selectClasses.classSelectTitle).selectElement.disabled = false;
                }
            }
            searchActions(selectItem) {
                this.getSelectElement(selectItem).originalSelect;
                const selectInput = this.getSelectElement(selectItem, this.selectClasses.classSelectInput).selectElement;
                const selectOptions = this.getSelectElement(selectItem, this.selectClasses.classSelectOptions).selectElement;
                const selectOptionsItems = selectOptions.querySelectorAll(`.${this.selectClasses.classSelectOption}`);
                const _this = this;
                selectInput.addEventListener("input", (function() {
                    selectOptionsItems.forEach((selectOptionsItem => {
                        if (selectOptionsItem.textContent.toUpperCase().indexOf(selectInput.value.toUpperCase()) >= 0) selectOptionsItem.hidden = false; else selectOptionsItem.hidden = true;
                    }));
                    true === selectOptions.hidden ? _this.selectAction(selectItem) : null;
                }));
            }
            selectCallback(selectItem, originalSelect) {
                document.dispatchEvent(new CustomEvent("selectCallback", {
                    detail: {
                        select: originalSelect
                    }
                }));
            }
            setLogging(message) {
                this.config.logging ? FLS(`[select]: ${message}`) : null;
            }
        }
        flsModules.select = new SelectConstructor({});
        __webpack_require__(9399);
        __webpack_require__(3542);
        var can_use_dom = __webpack_require__(1807);
        var can_use_dom_default = __webpack_require__.n(can_use_dom);
        __webpack_require__(8165);
        __webpack_require__(7543);
        __webpack_require__(7692);
        __webpack_require__(2352);
        __webpack_require__(4249);
        __webpack_require__(3344);
        __webpack_require__(7323);
        __webpack_require__(4079);
        var lodash_throttle = __webpack_require__(3096);
        var lodash_throttle_default = __webpack_require__.n(lodash_throttle);
        var lodash_debounce = __webpack_require__(1296);
        var lodash_debounce_default = __webpack_require__.n(lodash_debounce);
        var lodash_memoize = __webpack_require__(773);
        var lodash_memoize_default = __webpack_require__.n(lodash_memoize);
        var resizeObservers = [];
        var hasActiveObservations = function() {
            return resizeObservers.some((function(ro) {
                return ro.activeTargets.length > 0;
            }));
        };
        var hasSkippedObservations = function() {
            return resizeObservers.some((function(ro) {
                return ro.skippedTargets.length > 0;
            }));
        };
        var msg = "ResizeObserver loop completed with undelivered notifications.";
        var deliverResizeLoopError = function() {
            var event;
            if ("function" === typeof ErrorEvent) event = new ErrorEvent("error", {
                message: msg
            }); else {
                event = document.createEvent("Event");
                event.initEvent("error", false, false);
                event.message = msg;
            }
            window.dispatchEvent(event);
        };
        var ResizeObserverBoxOptions;
        (function(ResizeObserverBoxOptions) {
            ResizeObserverBoxOptions["BORDER_BOX"] = "border-box";
            ResizeObserverBoxOptions["CONTENT_BOX"] = "content-box";
            ResizeObserverBoxOptions["DEVICE_PIXEL_CONTENT_BOX"] = "device-pixel-content-box";
        })(ResizeObserverBoxOptions || (ResizeObserverBoxOptions = {}));
        var freeze = function(obj) {
            return Object.freeze(obj);
        };
        var ResizeObserverSize = function() {
            function ResizeObserverSize(inlineSize, blockSize) {
                this.inlineSize = inlineSize;
                this.blockSize = blockSize;
                freeze(this);
            }
            return ResizeObserverSize;
        }();
        var DOMRectReadOnly = function() {
            function DOMRectReadOnly(x, y, width, height) {
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
                this.top = this.y;
                this.left = this.x;
                this.bottom = this.top + this.height;
                this.right = this.left + this.width;
                return freeze(this);
            }
            DOMRectReadOnly.prototype.toJSON = function() {
                var _a = this, x = _a.x, y = _a.y, top = _a.top, right = _a.right, bottom = _a.bottom, left = _a.left, width = _a.width, height = _a.height;
                return {
                    x,
                    y,
                    top,
                    right,
                    bottom,
                    left,
                    width,
                    height
                };
            };
            DOMRectReadOnly.fromRect = function(rectangle) {
                return new DOMRectReadOnly(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            };
            return DOMRectReadOnly;
        }();
        var isSVG = function(target) {
            return target instanceof SVGElement && "getBBox" in target;
        };
        var isHidden = function(target) {
            if (isSVG(target)) {
                var _a = target.getBBox(), width = _a.width, height = _a.height;
                return !width && !height;
            }
            var _b = target, offsetWidth = _b.offsetWidth, offsetHeight = _b.offsetHeight;
            return !(offsetWidth || offsetHeight || target.getClientRects().length);
        };
        var isElement = function(obj) {
            var _a, _b;
            if (obj instanceof Element) return true;
            var scope = null === (_b = null === (_a = obj) || void 0 === _a ? void 0 : _a.ownerDocument) || void 0 === _b ? void 0 : _b.defaultView;
            return !!(scope && obj instanceof scope.Element);
        };
        var isReplacedElement = function(target) {
            switch (target.tagName) {
              case "INPUT":
                if ("image" !== target.type) break;

              case "VIDEO":
              case "AUDIO":
              case "EMBED":
              case "OBJECT":
              case "CANVAS":
              case "IFRAME":
              case "IMG":
                return true;
            }
            return false;
        };
        var global = "undefined" !== typeof window ? window : {};
        var cache = new WeakMap;
        var scrollRegexp = /auto|scroll/;
        var verticalRegexp = /^tb|vertical/;
        var IE = /msie|trident/i.test(global.navigator && global.navigator.userAgent);
        var parseDimension = function(pixel) {
            return parseFloat(pixel || "0");
        };
        var size = function(inlineSize, blockSize, switchSizes) {
            if (void 0 === inlineSize) inlineSize = 0;
            if (void 0 === blockSize) blockSize = 0;
            if (void 0 === switchSizes) switchSizes = false;
            return new ResizeObserverSize((switchSizes ? blockSize : inlineSize) || 0, (switchSizes ? inlineSize : blockSize) || 0);
        };
        var zeroBoxes = freeze({
            devicePixelContentBoxSize: size(),
            borderBoxSize: size(),
            contentBoxSize: size(),
            contentRect: new DOMRectReadOnly(0, 0, 0, 0)
        });
        var calculateBoxSizes = function(target, forceRecalculation) {
            if (void 0 === forceRecalculation) forceRecalculation = false;
            if (cache.has(target) && !forceRecalculation) return cache.get(target);
            if (isHidden(target)) {
                cache.set(target, zeroBoxes);
                return zeroBoxes;
            }
            var cs = getComputedStyle(target);
            var svg = isSVG(target) && target.ownerSVGElement && target.getBBox();
            var removePadding = !IE && "border-box" === cs.boxSizing;
            var switchSizes = verticalRegexp.test(cs.writingMode || "");
            var canScrollVertically = !svg && scrollRegexp.test(cs.overflowY || "");
            var canScrollHorizontally = !svg && scrollRegexp.test(cs.overflowX || "");
            var paddingTop = svg ? 0 : parseDimension(cs.paddingTop);
            var paddingRight = svg ? 0 : parseDimension(cs.paddingRight);
            var paddingBottom = svg ? 0 : parseDimension(cs.paddingBottom);
            var paddingLeft = svg ? 0 : parseDimension(cs.paddingLeft);
            var borderTop = svg ? 0 : parseDimension(cs.borderTopWidth);
            var borderRight = svg ? 0 : parseDimension(cs.borderRightWidth);
            var borderBottom = svg ? 0 : parseDimension(cs.borderBottomWidth);
            var borderLeft = svg ? 0 : parseDimension(cs.borderLeftWidth);
            var horizontalPadding = paddingLeft + paddingRight;
            var verticalPadding = paddingTop + paddingBottom;
            var horizontalBorderArea = borderLeft + borderRight;
            var verticalBorderArea = borderTop + borderBottom;
            var horizontalScrollbarThickness = !canScrollHorizontally ? 0 : target.offsetHeight - verticalBorderArea - target.clientHeight;
            var verticalScrollbarThickness = !canScrollVertically ? 0 : target.offsetWidth - horizontalBorderArea - target.clientWidth;
            var widthReduction = removePadding ? horizontalPadding + horizontalBorderArea : 0;
            var heightReduction = removePadding ? verticalPadding + verticalBorderArea : 0;
            var contentWidth = svg ? svg.width : parseDimension(cs.width) - widthReduction - verticalScrollbarThickness;
            var contentHeight = svg ? svg.height : parseDimension(cs.height) - heightReduction - horizontalScrollbarThickness;
            var borderBoxWidth = contentWidth + horizontalPadding + verticalScrollbarThickness + horizontalBorderArea;
            var borderBoxHeight = contentHeight + verticalPadding + horizontalScrollbarThickness + verticalBorderArea;
            var boxes = freeze({
                devicePixelContentBoxSize: size(Math.round(contentWidth * devicePixelRatio), Math.round(contentHeight * devicePixelRatio), switchSizes),
                borderBoxSize: size(borderBoxWidth, borderBoxHeight, switchSizes),
                contentBoxSize: size(contentWidth, contentHeight, switchSizes),
                contentRect: new DOMRectReadOnly(paddingLeft, paddingTop, contentWidth, contentHeight)
            });
            cache.set(target, boxes);
            return boxes;
        };
        var calculateBoxSize = function(target, observedBox, forceRecalculation) {
            var _a = calculateBoxSizes(target, forceRecalculation), borderBoxSize = _a.borderBoxSize, contentBoxSize = _a.contentBoxSize, devicePixelContentBoxSize = _a.devicePixelContentBoxSize;
            switch (observedBox) {
              case ResizeObserverBoxOptions.DEVICE_PIXEL_CONTENT_BOX:
                return devicePixelContentBoxSize;

              case ResizeObserverBoxOptions.BORDER_BOX:
                return borderBoxSize;

              default:
                return contentBoxSize;
            }
        };
        var ResizeObserverEntry = function() {
            function ResizeObserverEntry(target) {
                var boxes = calculateBoxSizes(target);
                this.target = target;
                this.contentRect = boxes.contentRect;
                this.borderBoxSize = freeze([ boxes.borderBoxSize ]);
                this.contentBoxSize = freeze([ boxes.contentBoxSize ]);
                this.devicePixelContentBoxSize = freeze([ boxes.devicePixelContentBoxSize ]);
            }
            return ResizeObserverEntry;
        }();
        var calculateDepthForNode = function(node) {
            if (isHidden(node)) return 1 / 0;
            var depth = 0;
            var parent = node.parentNode;
            while (parent) {
                depth += 1;
                parent = parent.parentNode;
            }
            return depth;
        };
        var broadcastActiveObservations = function() {
            var shallowestDepth = 1 / 0;
            var callbacks = [];
            resizeObservers.forEach((function processObserver(ro) {
                if (0 === ro.activeTargets.length) return;
                var entries = [];
                ro.activeTargets.forEach((function processTarget(ot) {
                    var entry = new ResizeObserverEntry(ot.target);
                    var targetDepth = calculateDepthForNode(ot.target);
                    entries.push(entry);
                    ot.lastReportedSize = calculateBoxSize(ot.target, ot.observedBox);
                    if (targetDepth < shallowestDepth) shallowestDepth = targetDepth;
                }));
                callbacks.push((function resizeObserverCallback() {
                    ro.callback.call(ro.observer, entries, ro.observer);
                }));
                ro.activeTargets.splice(0, ro.activeTargets.length);
            }));
            for (var _i = 0, callbacks_1 = callbacks; _i < callbacks_1.length; _i++) {
                var callback = callbacks_1[_i];
                callback();
            }
            return shallowestDepth;
        };
        var gatherActiveObservationsAtDepth = function(depth) {
            resizeObservers.forEach((function processObserver(ro) {
                ro.activeTargets.splice(0, ro.activeTargets.length);
                ro.skippedTargets.splice(0, ro.skippedTargets.length);
                ro.observationTargets.forEach((function processTarget(ot) {
                    if (ot.isActive()) if (calculateDepthForNode(ot.target) > depth) ro.activeTargets.push(ot); else ro.skippedTargets.push(ot);
                }));
            }));
        };
        var process = function() {
            var depth = 0;
            gatherActiveObservationsAtDepth(depth);
            while (hasActiveObservations()) {
                depth = broadcastActiveObservations();
                gatherActiveObservationsAtDepth(depth);
            }
            if (hasSkippedObservations()) deliverResizeLoopError();
            return depth > 0;
        };
        var trigger;
        var callbacks = [];
        var notify = function() {
            return callbacks.splice(0).forEach((function(cb) {
                return cb();
            }));
        };
        var queueMicroTask = function(callback) {
            if (!trigger) {
                var toggle_1 = 0;
                var el_1 = document.createTextNode("");
                var config = {
                    characterData: true
                };
                new MutationObserver((function() {
                    return notify();
                })).observe(el_1, config);
                trigger = function() {
                    el_1.textContent = "" + (toggle_1 ? toggle_1-- : toggle_1++);
                };
            }
            callbacks.push(callback);
            trigger();
        };
        var queueResizeObserver = function(cb) {
            queueMicroTask((function ResizeObserver() {
                requestAnimationFrame(cb);
            }));
        };
        var watching = 0;
        var isWatching = function() {
            return !!watching;
        };
        var CATCH_PERIOD = 250;
        var observerConfig = {
            attributes: true,
            characterData: true,
            childList: true,
            subtree: true
        };
        var events = [ "resize", "load", "transitionend", "animationend", "animationstart", "animationiteration", "keyup", "keydown", "mouseup", "mousedown", "mouseover", "mouseout", "blur", "focus" ];
        var time = function(timeout) {
            if (void 0 === timeout) timeout = 0;
            return Date.now() + timeout;
        };
        var scheduled = false;
        var Scheduler = function() {
            function Scheduler() {
                var _this = this;
                this.stopped = true;
                this.listener = function() {
                    return _this.schedule();
                };
            }
            Scheduler.prototype.run = function(timeout) {
                var _this = this;
                if (void 0 === timeout) timeout = CATCH_PERIOD;
                if (scheduled) return;
                scheduled = true;
                var until = time(timeout);
                queueResizeObserver((function() {
                    var elementsHaveResized = false;
                    try {
                        elementsHaveResized = process();
                    } finally {
                        scheduled = false;
                        timeout = until - time();
                        if (!isWatching()) return;
                        if (elementsHaveResized) _this.run(1e3); else if (timeout > 0) _this.run(timeout); else _this.start();
                    }
                }));
            };
            Scheduler.prototype.schedule = function() {
                this.stop();
                this.run();
            };
            Scheduler.prototype.observe = function() {
                var _this = this;
                var cb = function() {
                    return _this.observer && _this.observer.observe(document.body, observerConfig);
                };
                document.body ? cb() : global.addEventListener("DOMContentLoaded", cb);
            };
            Scheduler.prototype.start = function() {
                var _this = this;
                if (this.stopped) {
                    this.stopped = false;
                    this.observer = new MutationObserver(this.listener);
                    this.observe();
                    events.forEach((function(name) {
                        return global.addEventListener(name, _this.listener, true);
                    }));
                }
            };
            Scheduler.prototype.stop = function() {
                var _this = this;
                if (!this.stopped) {
                    this.observer && this.observer.disconnect();
                    events.forEach((function(name) {
                        return global.removeEventListener(name, _this.listener, true);
                    }));
                    this.stopped = true;
                }
            };
            return Scheduler;
        }();
        var scheduler = new Scheduler;
        var updateCount = function(n) {
            !watching && n > 0 && scheduler.start();
            watching += n;
            !watching && scheduler.stop();
        };
        var skipNotifyOnElement = function(target) {
            return !isSVG(target) && !isReplacedElement(target) && "inline" === getComputedStyle(target).display;
        };
        var ResizeObservation = function() {
            function ResizeObservation(target, observedBox) {
                this.target = target;
                this.observedBox = observedBox || ResizeObserverBoxOptions.CONTENT_BOX;
                this.lastReportedSize = {
                    inlineSize: 0,
                    blockSize: 0
                };
            }
            ResizeObservation.prototype.isActive = function() {
                var size = calculateBoxSize(this.target, this.observedBox, true);
                if (skipNotifyOnElement(this.target)) this.lastReportedSize = size;
                if (this.lastReportedSize.inlineSize !== size.inlineSize || this.lastReportedSize.blockSize !== size.blockSize) return true;
                return false;
            };
            return ResizeObservation;
        }();
        var ResizeObserverDetail = function() {
            function ResizeObserverDetail(resizeObserver, callback) {
                this.activeTargets = [];
                this.skippedTargets = [];
                this.observationTargets = [];
                this.observer = resizeObserver;
                this.callback = callback;
            }
            return ResizeObserverDetail;
        }();
        var observerMap = new WeakMap;
        var getObservationIndex = function(observationTargets, target) {
            for (var i = 0; i < observationTargets.length; i += 1) if (observationTargets[i].target === target) return i;
            return -1;
        };
        var ResizeObserverController = function() {
            function ResizeObserverController() {}
            ResizeObserverController.connect = function(resizeObserver, callback) {
                var detail = new ResizeObserverDetail(resizeObserver, callback);
                observerMap.set(resizeObserver, detail);
            };
            ResizeObserverController.observe = function(resizeObserver, target, options) {
                var detail = observerMap.get(resizeObserver);
                var firstObservation = 0 === detail.observationTargets.length;
                if (getObservationIndex(detail.observationTargets, target) < 0) {
                    firstObservation && resizeObservers.push(detail);
                    detail.observationTargets.push(new ResizeObservation(target, options && options.box));
                    updateCount(1);
                    scheduler.schedule();
                }
            };
            ResizeObserverController.unobserve = function(resizeObserver, target) {
                var detail = observerMap.get(resizeObserver);
                var index = getObservationIndex(detail.observationTargets, target);
                var lastObservation = 1 === detail.observationTargets.length;
                if (index >= 0) {
                    lastObservation && resizeObservers.splice(resizeObservers.indexOf(detail), 1);
                    detail.observationTargets.splice(index, 1);
                    updateCount(-1);
                }
            };
            ResizeObserverController.disconnect = function(resizeObserver) {
                var _this = this;
                var detail = observerMap.get(resizeObserver);
                detail.observationTargets.slice().forEach((function(ot) {
                    return _this.unobserve(resizeObserver, ot.target);
                }));
                detail.activeTargets.splice(0, detail.activeTargets.length);
            };
            return ResizeObserverController;
        }();
        var ResizeObserver_ResizeObserver = function() {
            function ResizeObserver(callback) {
                if (0 === arguments.length) throw new TypeError("Failed to construct 'ResizeObserver': 1 argument required, but only 0 present.");
                if ("function" !== typeof callback) throw new TypeError("Failed to construct 'ResizeObserver': The callback provided as parameter 1 is not a function.");
                ResizeObserverController.connect(this, callback);
            }
            ResizeObserver.prototype.observe = function(target, options) {
                if (0 === arguments.length) throw new TypeError("Failed to execute 'observe' on 'ResizeObserver': 1 argument required, but only 0 present.");
                if (!isElement(target)) throw new TypeError("Failed to execute 'observe' on 'ResizeObserver': parameter 1 is not of type 'Element");
                ResizeObserverController.observe(this, target, options);
            };
            ResizeObserver.prototype.unobserve = function(target) {
                if (0 === arguments.length) throw new TypeError("Failed to execute 'unobserve' on 'ResizeObserver': 1 argument required, but only 0 present.");
                if (!isElement(target)) throw new TypeError("Failed to execute 'unobserve' on 'ResizeObserver': parameter 1 is not of type 'Element");
                ResizeObserverController.unobserve(this, target);
            };
            ResizeObserver.prototype.disconnect = function() {
                ResizeObserverController.disconnect(this);
            };
            ResizeObserver.toString = function() {
                return "function ResizeObserver () { [polyfill code] }";
            };
            return ResizeObserver;
        }();
        __webpack_require__(7985);
        __webpack_require__(6618);
        __webpack_require__(9989);
        __webpack_require__(8307);
        __webpack_require__(4390);
        var getOptions = function getOptions(obj) {
            var options = Array.prototype.reduce.call(obj, (function(acc, attribute) {
                var option = attribute.name.match(/data-simplebar-(.+)/);
                if (option) {
                    var key = option[1].replace(/\W+(.)/g, (function(x, chr) {
                        return chr.toUpperCase();
                    }));
                    switch (attribute.value) {
                      case "true":
                        acc[key] = true;
                        break;

                      case "false":
                        acc[key] = false;
                        break;

                      case void 0:
                        acc[key] = true;
                        break;

                      default:
                        acc[key] = attribute.value;
                    }
                }
                return acc;
            }), {});
            return options;
        };
        function getElementWindow(element) {
            if (!element || !element.ownerDocument || !element.ownerDocument.defaultView) return window;
            return element.ownerDocument.defaultView;
        }
        function getElementDocument(element) {
            if (!element || !element.ownerDocument) return document;
            return element.ownerDocument;
        }
        var cachedScrollbarWidth = null;
        var cachedDevicePixelRatio = null;
        if (can_use_dom_default()) window.addEventListener("resize", (function() {
            if (cachedDevicePixelRatio !== window.devicePixelRatio) {
                cachedDevicePixelRatio = window.devicePixelRatio;
                cachedScrollbarWidth = null;
            }
        }));
        function scrollbarWidth(el) {
            if (null === cachedScrollbarWidth) {
                var document = getElementDocument(el);
                if ("undefined" === typeof document) {
                    cachedScrollbarWidth = 0;
                    return cachedScrollbarWidth;
                }
                var body = document.body;
                var box = document.createElement("div");
                box.classList.add("simplebar-hide-scrollbar");
                body.appendChild(box);
                var width = box.getBoundingClientRect().right;
                body.removeChild(box);
                cachedScrollbarWidth = width;
            }
            return cachedScrollbarWidth;
        }
        var SimpleBar = function() {
            function SimpleBar(element, options) {
                var _this = this;
                this.onScroll = function() {
                    var elWindow = getElementWindow(_this.el);
                    if (!_this.scrollXTicking) {
                        elWindow.requestAnimationFrame(_this.scrollX);
                        _this.scrollXTicking = true;
                    }
                    if (!_this.scrollYTicking) {
                        elWindow.requestAnimationFrame(_this.scrollY);
                        _this.scrollYTicking = true;
                    }
                };
                this.scrollX = function() {
                    if (_this.axis.x.isOverflowing) {
                        _this.showScrollbar("x");
                        _this.positionScrollbar("x");
                    }
                    _this.scrollXTicking = false;
                };
                this.scrollY = function() {
                    if (_this.axis.y.isOverflowing) {
                        _this.showScrollbar("y");
                        _this.positionScrollbar("y");
                    }
                    _this.scrollYTicking = false;
                };
                this.onMouseEnter = function() {
                    _this.showScrollbar("x");
                    _this.showScrollbar("y");
                };
                this.onMouseMove = function(e) {
                    _this.mouseX = e.clientX;
                    _this.mouseY = e.clientY;
                    if (_this.axis.x.isOverflowing || _this.axis.x.forceVisible) _this.onMouseMoveForAxis("x");
                    if (_this.axis.y.isOverflowing || _this.axis.y.forceVisible) _this.onMouseMoveForAxis("y");
                };
                this.onMouseLeave = function() {
                    _this.onMouseMove.cancel();
                    if (_this.axis.x.isOverflowing || _this.axis.x.forceVisible) _this.onMouseLeaveForAxis("x");
                    if (_this.axis.y.isOverflowing || _this.axis.y.forceVisible) _this.onMouseLeaveForAxis("y");
                    _this.mouseX = -1;
                    _this.mouseY = -1;
                };
                this.onWindowResize = function() {
                    _this.scrollbarWidth = _this.getScrollbarWidth();
                    _this.hideNativeScrollbar();
                };
                this.hideScrollbars = function() {
                    _this.axis.x.track.rect = _this.axis.x.track.el.getBoundingClientRect();
                    _this.axis.y.track.rect = _this.axis.y.track.el.getBoundingClientRect();
                    if (!_this.isWithinBounds(_this.axis.y.track.rect)) {
                        _this.axis.y.scrollbar.el.classList.remove(_this.classNames.visible);
                        _this.axis.y.isVisible = false;
                    }
                    if (!_this.isWithinBounds(_this.axis.x.track.rect)) {
                        _this.axis.x.scrollbar.el.classList.remove(_this.classNames.visible);
                        _this.axis.x.isVisible = false;
                    }
                };
                this.onPointerEvent = function(e) {
                    var isWithinTrackXBounds, isWithinTrackYBounds;
                    _this.axis.x.track.rect = _this.axis.x.track.el.getBoundingClientRect();
                    _this.axis.y.track.rect = _this.axis.y.track.el.getBoundingClientRect();
                    if (_this.axis.x.isOverflowing || _this.axis.x.forceVisible) isWithinTrackXBounds = _this.isWithinBounds(_this.axis.x.track.rect);
                    if (_this.axis.y.isOverflowing || _this.axis.y.forceVisible) isWithinTrackYBounds = _this.isWithinBounds(_this.axis.y.track.rect);
                    if (isWithinTrackXBounds || isWithinTrackYBounds) {
                        e.preventDefault();
                        e.stopPropagation();
                        if ("mousedown" === e.type) {
                            if (isWithinTrackXBounds) {
                                _this.axis.x.scrollbar.rect = _this.axis.x.scrollbar.el.getBoundingClientRect();
                                if (_this.isWithinBounds(_this.axis.x.scrollbar.rect)) _this.onDragStart(e, "x"); else _this.onTrackClick(e, "x");
                            }
                            if (isWithinTrackYBounds) {
                                _this.axis.y.scrollbar.rect = _this.axis.y.scrollbar.el.getBoundingClientRect();
                                if (_this.isWithinBounds(_this.axis.y.scrollbar.rect)) _this.onDragStart(e, "y"); else _this.onTrackClick(e, "y");
                            }
                        }
                    }
                };
                this.drag = function(e) {
                    var eventOffset;
                    var track = _this.axis[_this.draggedAxis].track;
                    var trackSize = track.rect[_this.axis[_this.draggedAxis].sizeAttr];
                    var scrollbar = _this.axis[_this.draggedAxis].scrollbar;
                    var contentSize = _this.contentWrapperEl[_this.axis[_this.draggedAxis].scrollSizeAttr];
                    var hostSize = parseInt(_this.elStyles[_this.axis[_this.draggedAxis].sizeAttr], 10);
                    e.preventDefault();
                    e.stopPropagation();
                    if ("y" === _this.draggedAxis) eventOffset = e.pageY; else eventOffset = e.pageX;
                    var dragPos = eventOffset - track.rect[_this.axis[_this.draggedAxis].offsetAttr] - _this.axis[_this.draggedAxis].dragOffset;
                    var dragPerc = dragPos / (trackSize - scrollbar.size);
                    var scrollPos = dragPerc * (contentSize - hostSize);
                    if ("x" === _this.draggedAxis) {
                        scrollPos = _this.isRtl && SimpleBar.getRtlHelpers().isRtlScrollbarInverted ? scrollPos - (trackSize + scrollbar.size) : scrollPos;
                        scrollPos = _this.isRtl && SimpleBar.getRtlHelpers().isRtlScrollingInverted ? -scrollPos : scrollPos;
                    }
                    _this.contentWrapperEl[_this.axis[_this.draggedAxis].scrollOffsetAttr] = scrollPos;
                };
                this.onEndDrag = function(e) {
                    var elDocument = getElementDocument(_this.el);
                    var elWindow = getElementWindow(_this.el);
                    e.preventDefault();
                    e.stopPropagation();
                    _this.el.classList.remove(_this.classNames.dragging);
                    elDocument.removeEventListener("mousemove", _this.drag, true);
                    elDocument.removeEventListener("mouseup", _this.onEndDrag, true);
                    _this.removePreventClickId = elWindow.setTimeout((function() {
                        elDocument.removeEventListener("click", _this.preventClick, true);
                        elDocument.removeEventListener("dblclick", _this.preventClick, true);
                        _this.removePreventClickId = null;
                    }));
                };
                this.preventClick = function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                };
                this.el = element;
                this.minScrollbarWidth = 20;
                this.options = Object.assign({}, SimpleBar.defaultOptions, {}, options);
                this.classNames = Object.assign({}, SimpleBar.defaultOptions.classNames, {}, this.options.classNames);
                this.axis = {
                    x: {
                        scrollOffsetAttr: "scrollLeft",
                        sizeAttr: "width",
                        scrollSizeAttr: "scrollWidth",
                        offsetSizeAttr: "offsetWidth",
                        offsetAttr: "left",
                        overflowAttr: "overflowX",
                        dragOffset: 0,
                        isOverflowing: true,
                        isVisible: false,
                        forceVisible: false,
                        track: {},
                        scrollbar: {}
                    },
                    y: {
                        scrollOffsetAttr: "scrollTop",
                        sizeAttr: "height",
                        scrollSizeAttr: "scrollHeight",
                        offsetSizeAttr: "offsetHeight",
                        offsetAttr: "top",
                        overflowAttr: "overflowY",
                        dragOffset: 0,
                        isOverflowing: true,
                        isVisible: false,
                        forceVisible: false,
                        track: {},
                        scrollbar: {}
                    }
                };
                this.removePreventClickId = null;
                if (SimpleBar.instances.has(this.el)) return;
                this.recalculate = lodash_throttle_default()(this.recalculate.bind(this), 64);
                this.onMouseMove = lodash_throttle_default()(this.onMouseMove.bind(this), 64);
                this.hideScrollbars = lodash_debounce_default()(this.hideScrollbars.bind(this), this.options.timeout);
                this.onWindowResize = lodash_debounce_default()(this.onWindowResize.bind(this), 64, {
                    leading: true
                });
                SimpleBar.getRtlHelpers = lodash_memoize_default()(SimpleBar.getRtlHelpers);
                this.init();
            }
            SimpleBar.getRtlHelpers = function getRtlHelpers() {
                var dummyDiv = document.createElement("div");
                dummyDiv.innerHTML = '<div class="hs-dummy-scrollbar-size"><div style="height: 200%; width: 200%; margin: 10px 0;"></div></div>';
                var scrollbarDummyEl = dummyDiv.firstElementChild;
                document.body.appendChild(scrollbarDummyEl);
                var dummyContainerChild = scrollbarDummyEl.firstElementChild;
                scrollbarDummyEl.scrollLeft = 0;
                var dummyContainerOffset = SimpleBar.getOffset(scrollbarDummyEl);
                var dummyContainerChildOffset = SimpleBar.getOffset(dummyContainerChild);
                scrollbarDummyEl.scrollLeft = 999;
                var dummyContainerScrollOffsetAfterScroll = SimpleBar.getOffset(dummyContainerChild);
                return {
                    isRtlScrollingInverted: dummyContainerOffset.left !== dummyContainerChildOffset.left && dummyContainerChildOffset.left - dummyContainerScrollOffsetAfterScroll.left !== 0,
                    isRtlScrollbarInverted: dummyContainerOffset.left !== dummyContainerChildOffset.left
                };
            };
            SimpleBar.getOffset = function getOffset(el) {
                var rect = el.getBoundingClientRect();
                var elDocument = getElementDocument(el);
                var elWindow = getElementWindow(el);
                return {
                    top: rect.top + (elWindow.pageYOffset || elDocument.documentElement.scrollTop),
                    left: rect.left + (elWindow.pageXOffset || elDocument.documentElement.scrollLeft)
                };
            };
            var _proto = SimpleBar.prototype;
            _proto.init = function init() {
                SimpleBar.instances.set(this.el, this);
                if (can_use_dom_default()) {
                    this.initDOM();
                    this.setAccessibilityAttributes();
                    this.scrollbarWidth = this.getScrollbarWidth();
                    this.recalculate();
                    this.initListeners();
                }
            };
            _proto.initDOM = function initDOM() {
                var _this2 = this;
                if (Array.prototype.filter.call(this.el.children, (function(child) {
                    return child.classList.contains(_this2.classNames.wrapper);
                })).length) {
                    this.wrapperEl = this.el.querySelector("." + this.classNames.wrapper);
                    this.contentWrapperEl = this.options.scrollableNode || this.el.querySelector("." + this.classNames.contentWrapper);
                    this.contentEl = this.options.contentNode || this.el.querySelector("." + this.classNames.contentEl);
                    this.offsetEl = this.el.querySelector("." + this.classNames.offset);
                    this.maskEl = this.el.querySelector("." + this.classNames.mask);
                    this.placeholderEl = this.findChild(this.wrapperEl, "." + this.classNames.placeholder);
                    this.heightAutoObserverWrapperEl = this.el.querySelector("." + this.classNames.heightAutoObserverWrapperEl);
                    this.heightAutoObserverEl = this.el.querySelector("." + this.classNames.heightAutoObserverEl);
                    this.axis.x.track.el = this.findChild(this.el, "." + this.classNames.track + "." + this.classNames.horizontal);
                    this.axis.y.track.el = this.findChild(this.el, "." + this.classNames.track + "." + this.classNames.vertical);
                } else {
                    this.wrapperEl = document.createElement("div");
                    this.contentWrapperEl = document.createElement("div");
                    this.offsetEl = document.createElement("div");
                    this.maskEl = document.createElement("div");
                    this.contentEl = document.createElement("div");
                    this.placeholderEl = document.createElement("div");
                    this.heightAutoObserverWrapperEl = document.createElement("div");
                    this.heightAutoObserverEl = document.createElement("div");
                    this.wrapperEl.classList.add(this.classNames.wrapper);
                    this.contentWrapperEl.classList.add(this.classNames.contentWrapper);
                    this.offsetEl.classList.add(this.classNames.offset);
                    this.maskEl.classList.add(this.classNames.mask);
                    this.contentEl.classList.add(this.classNames.contentEl);
                    this.placeholderEl.classList.add(this.classNames.placeholder);
                    this.heightAutoObserverWrapperEl.classList.add(this.classNames.heightAutoObserverWrapperEl);
                    this.heightAutoObserverEl.classList.add(this.classNames.heightAutoObserverEl);
                    while (this.el.firstChild) this.contentEl.appendChild(this.el.firstChild);
                    this.contentWrapperEl.appendChild(this.contentEl);
                    this.offsetEl.appendChild(this.contentWrapperEl);
                    this.maskEl.appendChild(this.offsetEl);
                    this.heightAutoObserverWrapperEl.appendChild(this.heightAutoObserverEl);
                    this.wrapperEl.appendChild(this.heightAutoObserverWrapperEl);
                    this.wrapperEl.appendChild(this.maskEl);
                    this.wrapperEl.appendChild(this.placeholderEl);
                    this.el.appendChild(this.wrapperEl);
                }
                if (!this.axis.x.track.el || !this.axis.y.track.el) {
                    var track = document.createElement("div");
                    var scrollbar = document.createElement("div");
                    track.classList.add(this.classNames.track);
                    scrollbar.classList.add(this.classNames.scrollbar);
                    track.appendChild(scrollbar);
                    this.axis.x.track.el = track.cloneNode(true);
                    this.axis.x.track.el.classList.add(this.classNames.horizontal);
                    this.axis.y.track.el = track.cloneNode(true);
                    this.axis.y.track.el.classList.add(this.classNames.vertical);
                    this.el.appendChild(this.axis.x.track.el);
                    this.el.appendChild(this.axis.y.track.el);
                }
                this.axis.x.scrollbar.el = this.axis.x.track.el.querySelector("." + this.classNames.scrollbar);
                this.axis.y.scrollbar.el = this.axis.y.track.el.querySelector("." + this.classNames.scrollbar);
                if (!this.options.autoHide) {
                    this.axis.x.scrollbar.el.classList.add(this.classNames.visible);
                    this.axis.y.scrollbar.el.classList.add(this.classNames.visible);
                }
                this.el.setAttribute("data-simplebar", "init");
            };
            _proto.setAccessibilityAttributes = function setAccessibilityAttributes() {
                var ariaLabel = this.options.ariaLabel || "scrollable content";
                this.contentWrapperEl.setAttribute("tabindex", "0");
                this.contentWrapperEl.setAttribute("role", "region");
                this.contentWrapperEl.setAttribute("aria-label", ariaLabel);
            };
            _proto.initListeners = function initListeners() {
                var _this3 = this;
                var elWindow = getElementWindow(this.el);
                if (this.options.autoHide) this.el.addEventListener("mouseenter", this.onMouseEnter);
                [ "mousedown", "click", "dblclick" ].forEach((function(e) {
                    _this3.el.addEventListener(e, _this3.onPointerEvent, true);
                }));
                [ "touchstart", "touchend", "touchmove" ].forEach((function(e) {
                    _this3.el.addEventListener(e, _this3.onPointerEvent, {
                        capture: true,
                        passive: true
                    });
                }));
                this.el.addEventListener("mousemove", this.onMouseMove);
                this.el.addEventListener("mouseleave", this.onMouseLeave);
                this.contentWrapperEl.addEventListener("scroll", this.onScroll);
                elWindow.addEventListener("resize", this.onWindowResize);
                var resizeObserverStarted = false;
                var resizeObserver = elWindow.ResizeObserver || ResizeObserver_ResizeObserver;
                this.resizeObserver = new resizeObserver((function() {
                    if (!resizeObserverStarted) return;
                    _this3.recalculate();
                }));
                this.resizeObserver.observe(this.el);
                this.resizeObserver.observe(this.contentEl);
                elWindow.requestAnimationFrame((function() {
                    resizeObserverStarted = true;
                }));
                this.mutationObserver = new elWindow.MutationObserver(this.recalculate);
                this.mutationObserver.observe(this.contentEl, {
                    childList: true,
                    subtree: true,
                    characterData: true
                });
            };
            _proto.recalculate = function recalculate() {
                var elWindow = getElementWindow(this.el);
                this.elStyles = elWindow.getComputedStyle(this.el);
                this.isRtl = "rtl" === this.elStyles.direction;
                var isHeightAuto = this.heightAutoObserverEl.offsetHeight <= 1;
                var isWidthAuto = this.heightAutoObserverEl.offsetWidth <= 1;
                var contentElOffsetWidth = this.contentEl.offsetWidth;
                var contentWrapperElOffsetWidth = this.contentWrapperEl.offsetWidth;
                var elOverflowX = this.elStyles.overflowX;
                var elOverflowY = this.elStyles.overflowY;
                this.contentEl.style.padding = this.elStyles.paddingTop + " " + this.elStyles.paddingRight + " " + this.elStyles.paddingBottom + " " + this.elStyles.paddingLeft;
                this.wrapperEl.style.margin = "-" + this.elStyles.paddingTop + " -" + this.elStyles.paddingRight + " -" + this.elStyles.paddingBottom + " -" + this.elStyles.paddingLeft;
                var contentElScrollHeight = this.contentEl.scrollHeight;
                var contentElScrollWidth = this.contentEl.scrollWidth;
                this.contentWrapperEl.style.height = isHeightAuto ? "auto" : "100%";
                this.placeholderEl.style.width = isWidthAuto ? contentElOffsetWidth + "px" : "auto";
                this.placeholderEl.style.height = contentElScrollHeight + "px";
                var contentWrapperElOffsetHeight = this.contentWrapperEl.offsetHeight;
                this.axis.x.isOverflowing = contentElScrollWidth > contentElOffsetWidth;
                this.axis.y.isOverflowing = contentElScrollHeight > contentWrapperElOffsetHeight;
                this.axis.x.isOverflowing = "hidden" === elOverflowX ? false : this.axis.x.isOverflowing;
                this.axis.y.isOverflowing = "hidden" === elOverflowY ? false : this.axis.y.isOverflowing;
                this.axis.x.forceVisible = "x" === this.options.forceVisible || true === this.options.forceVisible;
                this.axis.y.forceVisible = "y" === this.options.forceVisible || true === this.options.forceVisible;
                this.hideNativeScrollbar();
                var offsetForXScrollbar = this.axis.x.isOverflowing ? this.scrollbarWidth : 0;
                var offsetForYScrollbar = this.axis.y.isOverflowing ? this.scrollbarWidth : 0;
                this.axis.x.isOverflowing = this.axis.x.isOverflowing && contentElScrollWidth > contentWrapperElOffsetWidth - offsetForYScrollbar;
                this.axis.y.isOverflowing = this.axis.y.isOverflowing && contentElScrollHeight > contentWrapperElOffsetHeight - offsetForXScrollbar;
                this.axis.x.scrollbar.size = this.getScrollbarSize("x");
                this.axis.y.scrollbar.size = this.getScrollbarSize("y");
                this.axis.x.scrollbar.el.style.width = this.axis.x.scrollbar.size + "px";
                this.axis.y.scrollbar.el.style.height = this.axis.y.scrollbar.size + "px";
                this.positionScrollbar("x");
                this.positionScrollbar("y");
                this.toggleTrackVisibility("x");
                this.toggleTrackVisibility("y");
            };
            _proto.getScrollbarSize = function getScrollbarSize(axis) {
                if (void 0 === axis) axis = "y";
                if (!this.axis[axis].isOverflowing) return 0;
                var contentSize = this.contentEl[this.axis[axis].scrollSizeAttr];
                var trackSize = this.axis[axis].track.el[this.axis[axis].offsetSizeAttr];
                var scrollbarSize;
                var scrollbarRatio = trackSize / contentSize;
                scrollbarSize = Math.max(~~(scrollbarRatio * trackSize), this.options.scrollbarMinSize);
                if (this.options.scrollbarMaxSize) scrollbarSize = Math.min(scrollbarSize, this.options.scrollbarMaxSize);
                return scrollbarSize;
            };
            _proto.positionScrollbar = function positionScrollbar(axis) {
                if (void 0 === axis) axis = "y";
                if (!this.axis[axis].isOverflowing) return;
                var contentSize = this.contentWrapperEl[this.axis[axis].scrollSizeAttr];
                var trackSize = this.axis[axis].track.el[this.axis[axis].offsetSizeAttr];
                var hostSize = parseInt(this.elStyles[this.axis[axis].sizeAttr], 10);
                var scrollbar = this.axis[axis].scrollbar;
                var scrollOffset = this.contentWrapperEl[this.axis[axis].scrollOffsetAttr];
                scrollOffset = "x" === axis && this.isRtl && SimpleBar.getRtlHelpers().isRtlScrollingInverted ? -scrollOffset : scrollOffset;
                var scrollPourcent = scrollOffset / (contentSize - hostSize);
                var handleOffset = ~~((trackSize - scrollbar.size) * scrollPourcent);
                handleOffset = "x" === axis && this.isRtl && SimpleBar.getRtlHelpers().isRtlScrollbarInverted ? handleOffset + (trackSize - scrollbar.size) : handleOffset;
                scrollbar.el.style.transform = "x" === axis ? "translate3d(" + handleOffset + "px, 0, 0)" : "translate3d(0, " + handleOffset + "px, 0)";
            };
            _proto.toggleTrackVisibility = function toggleTrackVisibility(axis) {
                if (void 0 === axis) axis = "y";
                var track = this.axis[axis].track.el;
                var scrollbar = this.axis[axis].scrollbar.el;
                if (this.axis[axis].isOverflowing || this.axis[axis].forceVisible) {
                    track.style.visibility = "visible";
                    this.contentWrapperEl.style[this.axis[axis].overflowAttr] = "scroll";
                } else {
                    track.style.visibility = "hidden";
                    this.contentWrapperEl.style[this.axis[axis].overflowAttr] = "hidden";
                }
                if (this.axis[axis].isOverflowing) scrollbar.style.display = "block"; else scrollbar.style.display = "none";
            };
            _proto.hideNativeScrollbar = function hideNativeScrollbar() {
                this.offsetEl.style[this.isRtl ? "left" : "right"] = this.axis.y.isOverflowing || this.axis.y.forceVisible ? "-" + this.scrollbarWidth + "px" : 0;
                this.offsetEl.style.bottom = this.axis.x.isOverflowing || this.axis.x.forceVisible ? "-" + this.scrollbarWidth + "px" : 0;
            };
            _proto.onMouseMoveForAxis = function onMouseMoveForAxis(axis) {
                if (void 0 === axis) axis = "y";
                this.axis[axis].track.rect = this.axis[axis].track.el.getBoundingClientRect();
                this.axis[axis].scrollbar.rect = this.axis[axis].scrollbar.el.getBoundingClientRect();
                var isWithinScrollbarBoundsX = this.isWithinBounds(this.axis[axis].scrollbar.rect);
                if (isWithinScrollbarBoundsX) this.axis[axis].scrollbar.el.classList.add(this.classNames.hover); else this.axis[axis].scrollbar.el.classList.remove(this.classNames.hover);
                if (this.isWithinBounds(this.axis[axis].track.rect)) {
                    this.showScrollbar(axis);
                    this.axis[axis].track.el.classList.add(this.classNames.hover);
                } else this.axis[axis].track.el.classList.remove(this.classNames.hover);
            };
            _proto.onMouseLeaveForAxis = function onMouseLeaveForAxis(axis) {
                if (void 0 === axis) axis = "y";
                this.axis[axis].track.el.classList.remove(this.classNames.hover);
                this.axis[axis].scrollbar.el.classList.remove(this.classNames.hover);
            };
            _proto.showScrollbar = function showScrollbar(axis) {
                if (void 0 === axis) axis = "y";
                var scrollbar = this.axis[axis].scrollbar.el;
                if (!this.axis[axis].isVisible) {
                    scrollbar.classList.add(this.classNames.visible);
                    this.axis[axis].isVisible = true;
                }
                if (this.options.autoHide) this.hideScrollbars();
            };
            _proto.onDragStart = function onDragStart(e, axis) {
                if (void 0 === axis) axis = "y";
                var elDocument = getElementDocument(this.el);
                var elWindow = getElementWindow(this.el);
                var scrollbar = this.axis[axis].scrollbar;
                var eventOffset = "y" === axis ? e.pageY : e.pageX;
                this.axis[axis].dragOffset = eventOffset - scrollbar.rect[this.axis[axis].offsetAttr];
                this.draggedAxis = axis;
                this.el.classList.add(this.classNames.dragging);
                elDocument.addEventListener("mousemove", this.drag, true);
                elDocument.addEventListener("mouseup", this.onEndDrag, true);
                if (null === this.removePreventClickId) {
                    elDocument.addEventListener("click", this.preventClick, true);
                    elDocument.addEventListener("dblclick", this.preventClick, true);
                } else {
                    elWindow.clearTimeout(this.removePreventClickId);
                    this.removePreventClickId = null;
                }
            };
            _proto.onTrackClick = function onTrackClick(e, axis) {
                var _this4 = this;
                if (void 0 === axis) axis = "y";
                if (!this.options.clickOnTrack) return;
                var elWindow = getElementWindow(this.el);
                this.axis[axis].scrollbar.rect = this.axis[axis].scrollbar.el.getBoundingClientRect();
                var scrollbar = this.axis[axis].scrollbar;
                var scrollbarOffset = scrollbar.rect[this.axis[axis].offsetAttr];
                var hostSize = parseInt(this.elStyles[this.axis[axis].sizeAttr], 10);
                var scrolled = this.contentWrapperEl[this.axis[axis].scrollOffsetAttr];
                var t = "y" === axis ? this.mouseY - scrollbarOffset : this.mouseX - scrollbarOffset;
                var dir = t < 0 ? -1 : 1;
                var scrollSize = -1 === dir ? scrolled - hostSize : scrolled + hostSize;
                var scrollTo = function scrollTo() {
                    if (-1 === dir) {
                        if (scrolled > scrollSize) {
                            var _this4$contentWrapper;
                            scrolled -= _this4.options.clickOnTrackSpeed;
                            _this4.contentWrapperEl.scrollTo((_this4$contentWrapper = {}, _this4$contentWrapper[_this4.axis[axis].offsetAttr] = scrolled, 
                            _this4$contentWrapper));
                            elWindow.requestAnimationFrame(scrollTo);
                        }
                    } else if (scrolled < scrollSize) {
                        var _this4$contentWrapper2;
                        scrolled += _this4.options.clickOnTrackSpeed;
                        _this4.contentWrapperEl.scrollTo((_this4$contentWrapper2 = {}, _this4$contentWrapper2[_this4.axis[axis].offsetAttr] = scrolled, 
                        _this4$contentWrapper2));
                        elWindow.requestAnimationFrame(scrollTo);
                    }
                };
                scrollTo();
            };
            _proto.getContentElement = function getContentElement() {
                return this.contentEl;
            };
            _proto.getScrollElement = function getScrollElement() {
                return this.contentWrapperEl;
            };
            _proto.getScrollbarWidth = function getScrollbarWidth() {
                try {
                    if ("none" === getComputedStyle(this.contentWrapperEl, "::-webkit-scrollbar").display || "scrollbarWidth" in document.documentElement.style || "-ms-overflow-style" in document.documentElement.style) return 0; else return scrollbarWidth(this.el);
                } catch (e) {
                    return scrollbarWidth(this.el);
                }
            };
            _proto.removeListeners = function removeListeners() {
                var _this5 = this;
                var elWindow = getElementWindow(this.el);
                if (this.options.autoHide) this.el.removeEventListener("mouseenter", this.onMouseEnter);
                [ "mousedown", "click", "dblclick" ].forEach((function(e) {
                    _this5.el.removeEventListener(e, _this5.onPointerEvent, true);
                }));
                [ "touchstart", "touchend", "touchmove" ].forEach((function(e) {
                    _this5.el.removeEventListener(e, _this5.onPointerEvent, {
                        capture: true,
                        passive: true
                    });
                }));
                this.el.removeEventListener("mousemove", this.onMouseMove);
                this.el.removeEventListener("mouseleave", this.onMouseLeave);
                if (this.contentWrapperEl) this.contentWrapperEl.removeEventListener("scroll", this.onScroll);
                elWindow.removeEventListener("resize", this.onWindowResize);
                if (this.mutationObserver) this.mutationObserver.disconnect();
                if (this.resizeObserver) this.resizeObserver.disconnect();
                this.recalculate.cancel();
                this.onMouseMove.cancel();
                this.hideScrollbars.cancel();
                this.onWindowResize.cancel();
            };
            _proto.unMount = function unMount() {
                this.removeListeners();
                SimpleBar.instances.delete(this.el);
            };
            _proto.isWithinBounds = function isWithinBounds(bbox) {
                return this.mouseX >= bbox.left && this.mouseX <= bbox.left + bbox.width && this.mouseY >= bbox.top && this.mouseY <= bbox.top + bbox.height;
            };
            _proto.findChild = function findChild(el, query) {
                var matches = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;
                return Array.prototype.filter.call(el.children, (function(child) {
                    return matches.call(child, query);
                }))[0];
            };
            return SimpleBar;
        }();
        SimpleBar.defaultOptions = {
            autoHide: true,
            forceVisible: false,
            clickOnTrack: true,
            clickOnTrackSpeed: 40,
            classNames: {
                contentEl: "simplebar-content",
                contentWrapper: "simplebar-content-wrapper",
                offset: "simplebar-offset",
                mask: "simplebar-mask",
                wrapper: "simplebar-wrapper",
                placeholder: "simplebar-placeholder",
                scrollbar: "simplebar-scrollbar",
                track: "simplebar-track",
                heightAutoObserverWrapperEl: "simplebar-height-auto-observer-wrapper",
                heightAutoObserverEl: "simplebar-height-auto-observer",
                visible: "simplebar-visible",
                horizontal: "simplebar-horizontal",
                vertical: "simplebar-vertical",
                hover: "simplebar-hover",
                dragging: "simplebar-dragging"
            },
            scrollbarMinSize: 25,
            scrollbarMaxSize: 0,
            timeout: 1e3
        };
        SimpleBar.instances = new WeakMap;
        SimpleBar.initDOMLoadedElements = function() {
            document.removeEventListener("DOMContentLoaded", this.initDOMLoadedElements);
            window.removeEventListener("load", this.initDOMLoadedElements);
            Array.prototype.forEach.call(document.querySelectorAll("[data-simplebar]"), (function(el) {
                if ("init" !== el.getAttribute("data-simplebar") && !SimpleBar.instances.has(el)) new SimpleBar(el, getOptions(el.attributes));
            }));
        };
        SimpleBar.removeObserver = function() {
            this.globalObserver.disconnect();
        };
        SimpleBar.initHtmlApi = function() {
            this.initDOMLoadedElements = this.initDOMLoadedElements.bind(this);
            if ("undefined" !== typeof MutationObserver) {
                this.globalObserver = new MutationObserver(SimpleBar.handleMutations);
                this.globalObserver.observe(document, {
                    childList: true,
                    subtree: true
                });
            }
            if ("complete" === document.readyState || "loading" !== document.readyState && !document.documentElement.doScroll) window.setTimeout(this.initDOMLoadedElements); else {
                document.addEventListener("DOMContentLoaded", this.initDOMLoadedElements);
                window.addEventListener("load", this.initDOMLoadedElements);
            }
        };
        SimpleBar.handleMutations = function(mutations) {
            mutations.forEach((function(mutation) {
                Array.prototype.forEach.call(mutation.addedNodes, (function(addedNode) {
                    if (1 === addedNode.nodeType) if (addedNode.hasAttribute("data-simplebar")) !SimpleBar.instances.has(addedNode) && document.documentElement.contains(addedNode) && new SimpleBar(addedNode, getOptions(addedNode.attributes)); else Array.prototype.forEach.call(addedNode.querySelectorAll("[data-simplebar]"), (function(el) {
                        if ("init" !== el.getAttribute("data-simplebar") && !SimpleBar.instances.has(el) && document.documentElement.contains(el)) new SimpleBar(el, getOptions(el.attributes));
                    }));
                }));
                Array.prototype.forEach.call(mutation.removedNodes, (function(removedNode) {
                    if (1 === removedNode.nodeType) if ("init" === removedNode.getAttribute("data-simplebar")) SimpleBar.instances.has(removedNode) && !document.documentElement.contains(removedNode) && SimpleBar.instances.get(removedNode).unMount(); else Array.prototype.forEach.call(removedNode.querySelectorAll('[data-simplebar="init"]'), (function(el) {
                        SimpleBar.instances.has(el) && !document.documentElement.contains(el) && SimpleBar.instances.get(el).unMount();
                    }));
                }));
            }));
        };
        SimpleBar.getOptions = getOptions;
        if (can_use_dom_default()) SimpleBar.initHtmlApi();
        let addWindowScrollEvent = false;
        setTimeout((() => {
            if (addWindowScrollEvent) {
                let windowScroll = new Event("windowScroll");
                window.addEventListener("scroll", (function(e) {
                    document.dispatchEvent(windowScroll);
                }));
            }
        }), 0);
        function DynamicAdapt(type) {
            this.type = type;
        }
        DynamicAdapt.prototype.init = function() {
            const _this = this;
            this.оbjects = [];
            this.daClassname = "_dynamic_adapt_";
            this.nodes = document.querySelectorAll("[data-da]");
            for (let i = 0; i < this.nodes.length; i++) {
                const node = this.nodes[i];
                const data = node.dataset.da.trim();
                const dataArray = data.split(",");
                const оbject = {};
                оbject.element = node;
                оbject.parent = node.parentNode;
                оbject.destination = document.querySelector(dataArray[0].trim());
                оbject.breakpoint = dataArray[1] ? dataArray[1].trim() : "767";
                оbject.place = dataArray[2] ? dataArray[2].trim() : "last";
                оbject.index = this.indexInParent(оbject.parent, оbject.element);
                this.оbjects.push(оbject);
            }
            this.arraySort(this.оbjects);
            this.mediaQueries = Array.prototype.map.call(this.оbjects, (function(item) {
                return "(" + this.type + "-width: " + item.breakpoint + "px)," + item.breakpoint;
            }), this);
            this.mediaQueries = Array.prototype.filter.call(this.mediaQueries, (function(item, index, self) {
                return Array.prototype.indexOf.call(self, item) === index;
            }));
            for (let i = 0; i < this.mediaQueries.length; i++) {
                const media = this.mediaQueries[i];
                const mediaSplit = String.prototype.split.call(media, ",");
                const matchMedia = window.matchMedia(mediaSplit[0]);
                const mediaBreakpoint = mediaSplit[1];
                const оbjectsFilter = Array.prototype.filter.call(this.оbjects, (function(item) {
                    return item.breakpoint === mediaBreakpoint;
                }));
                matchMedia.addListener((function() {
                    _this.mediaHandler(matchMedia, оbjectsFilter);
                }));
                this.mediaHandler(matchMedia, оbjectsFilter);
            }
        };
        DynamicAdapt.prototype.mediaHandler = function(matchMedia, оbjects) {
            if (matchMedia.matches) for (let i = 0; i < оbjects.length; i++) {
                const оbject = оbjects[i];
                оbject.index = this.indexInParent(оbject.parent, оbject.element);
                this.moveTo(оbject.place, оbject.element, оbject.destination);
            } else for (let i = оbjects.length - 1; i >= 0; i--) {
                const оbject = оbjects[i];
                if (оbject.element.classList.contains(this.daClassname)) this.moveBack(оbject.parent, оbject.element, оbject.index);
            }
        };
        DynamicAdapt.prototype.moveTo = function(place, element, destination) {
            element.classList.add(this.daClassname);
            if ("last" === place || place >= destination.children.length) {
                destination.insertAdjacentElement("beforeend", element);
                return;
            }
            if ("first" === place) {
                destination.insertAdjacentElement("afterbegin", element);
                return;
            }
            destination.children[place].insertAdjacentElement("beforebegin", element);
        };
        DynamicAdapt.prototype.moveBack = function(parent, element, index) {
            element.classList.remove(this.daClassname);
            if (void 0 !== parent.children[index]) parent.children[index].insertAdjacentElement("beforebegin", element); else parent.insertAdjacentElement("beforeend", element);
        };
        DynamicAdapt.prototype.indexInParent = function(parent, element) {
            const array = Array.prototype.slice.call(parent.children);
            return Array.prototype.indexOf.call(array, element);
        };
        DynamicAdapt.prototype.arraySort = function(arr) {
            if ("min" === this.type) Array.prototype.sort.call(arr, (function(a, b) {
                if (a.breakpoint === b.breakpoint) {
                    if (a.place === b.place) return 0;
                    if ("first" === a.place || "last" === b.place) return -1;
                    if ("last" === a.place || "first" === b.place) return 1;
                    return a.place - b.place;
                }
                return a.breakpoint - b.breakpoint;
            })); else {
                Array.prototype.sort.call(arr, (function(a, b) {
                    if (a.breakpoint === b.breakpoint) {
                        if (a.place === b.place) return 0;
                        if ("first" === a.place || "last" === b.place) return 1;
                        if ("last" === a.place || "first" === b.place) return -1;
                        return b.place - a.place;
                    }
                    return b.breakpoint - a.breakpoint;
                }));
                return;
            }
        };
        const da = new DynamicAdapt("max");
        da.init();
        document.addEventListener("DOMContentLoaded", (() => {
            const listButton = document.querySelector(".list__button");
            const listClose = document.querySelector(".list__close");
            function renderPreview(fileName, formPreview) {
                if ("" !== fileName) formPreview.insertAdjacentHTML("beforeend", `\n      <a href="#" class="formPreview__line">\n        <span class="formPreview__icon _icon-file"></span>\n        <div class="formPreview__text">${fileName} (Скачать)</div>\n        <div class="formPreview__pencil _icon-pencil"></div>\n      </a>`); else formPreview.insertAdjacentHTML("beforeend", `\n      <a href="#" class="formPreview__line">\n        <span class="formPreview__icon _icon-file"></span>\n        <div class="formPreview__text">${formFile.files[0].name} (Скачать)</div>\n        <div class="formPreview__pencil _icon-pencil"></div>\n      </a>`);
            }
            function renderQues(quesAll, quesWrap) {
                const quesQuan = quesAll.length + 1;
                quesWrap.insertAdjacentHTML("beforeend", `<div class="info-addtest__question question-addtest">\n    <div class="question-addtest__title">Вопрос #${quesQuan}<a href="#" class="question-addtest__del _icon-del_elem _red"></a></div>\n    <textarea name="" id="" placeholder="Для чего нужна солнечная гравитация" data-required class="question-addtest__name _input"></textarea>\n    <div class="question-addtest__answers">\n    </div>\n    <div class="info-addtest__link info-addtest__link_answ"> \n      <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">\n        <path d="M22 11C22 13.9174 20.8411 16.7153 18.7782 18.7782C16.7153 20.8411 13.9174 22 11 22C8.08262 22 5.28473 20.8411 3.22183 18.7782C1.15893 16.7153 0 13.9174 0 11C0 8.08262 1.15893 5.28473 3.22183 3.22183C5.28473 1.15893 8.08262 0 11 0C13.9174 0 16.7153 1.15893 18.7782 3.22183C20.8411 5.28473 22 8.08262 22 11ZM11.6875 6.1875C11.6875 6.00516 11.6151 5.8303 11.4861 5.70136C11.3572 5.57243 11.1823 5.5 11 5.5C10.8177 5.5 10.6428 5.57243 10.5139 5.70136C10.3849 5.8303 10.3125 6.00516 10.3125 6.1875V10.3125H6.1875C6.00516 10.3125 5.8303 10.3849 5.70136 10.5139C5.57243 10.6428 5.5 10.8177 5.5 11C5.5 11.1823 5.57243 11.3572 5.70136 11.4861C5.8303 11.6151 6.00516 11.6875 6.1875 11.6875H10.3125V15.8125C10.3125 15.9948 10.3849 16.1697 10.5139 16.2986C10.6428 16.4276 10.8177 16.5 11 16.5C11.1823 16.5 11.3572 16.4276 11.4861 16.2986C11.6151 16.1697 11.6875 15.9948 11.6875 15.8125V11.6875H15.8125C15.9948 11.6875 16.1697 11.6151 16.2986 11.4861C16.4276 11.3572 16.5 11.1823 16.5 11C16.5 10.8177 16.4276 10.6428 16.2986 10.5139C16.1697 10.3849 15.9948 10.3125 15.8125 10.3125H11.6875V6.1875Z" fill="#82C43C"/>\n      </svg>\n      Добавить ответ\n    </div>\n  </div>`);
            }
            function renderAnswer(thisAnswers, answQuan, thisQues) {
                const answAllQuan = document.querySelectorAll(".answer-addtest").length;
                thisAnswers.insertAdjacentHTML("beforeend", `<div class="question-addtest__answer answer-addtest">\n    <div class="answer-addtest__title">Ответ #${answQuan}</div>\n    <div class="answer-addtest__line">\n      <input type="radio" name="addtest-answer${thisQues}" id="answer${answAllQuan + 1}" class="answer-addtest__radio">\n      <label for="answer${answAllQuan + 1}" class="answer-addtest__label">\n        <input type="text" placeholder="Для чего нужна солнечная гравитация" class="answer-addtest__input">\n      </label>\n      <a href="#" class="answer-addtest__del _icon-del_elem _red"></a>\n    </div>\n  </div>`);
            }
            
            if (listButton) {
                listButton.addEventListener("click", (() => {
                    document.documentElement.classList.toggle("lock");
                    document.documentElement.classList.toggle("list-open");
                }));
                listClose.addEventListener("click", (() => {
                    document.documentElement.classList.remove("lock");
                    document.documentElement.classList.remove("list-open");
                }));
            }
            document.addEventListener("click", (e => {
                const targetElement = e.target;
                if (targetElement.classList.contains("info-addtest__link_ques")) {
                    const quesAll = document.querySelectorAll(".question-addtest");
                    const quesWrap = document.querySelector(".info-addtest__questions");
                    renderQues(quesAll, quesWrap);
                }
                if (targetElement.classList.contains("info-addtest__link_answ")) {
                    document.querySelectorAll(".question-addtest");
                    const thisAnswers = targetElement.parentNode.querySelector(".question-addtest__answers");
                    const answQuan = thisAnswers.querySelectorAll(".answer-addtest").length + 1;
                    const thisQuestion = targetElement.parentNode.querySelector(".question-addtest__title").innerText.split("#");
                    const thisQues = thisQuestion[thisQuestion.length - 1];
                    renderAnswer(thisAnswers, answQuan, thisQues);
                }
                if (targetElement.classList.contains("select__title") || targetElement.closest(".select__title")) {
                    const openSels = document.querySelectorAll("._select-open");
                    if (openSels.length > 0) {
                        const sels = document.querySelector('[class*="__sels"]');
                        const selsForm = sels.parentNode.parentNode.parentNode;
                        if (selsForm.getElementsByTagName("form").length > 0) {
                            if (null === selsForm.getElementsByTagName("form")[0].nextElementSibling) selsForm.getElementsByTagName("form")[0].style = "padding-bottom: 250px";
                        } else if (null === selsForm.nextElementSibling) selsForm.style = "padding-bottom: 250px";
                    } else document.getElementsByTagName("form")[0].style = "";
                }
            }));
            
            const formFile = document.getElementById("formFile");
            const formPreview = document.getElementById("formPreview");
            const nameInput = document.querySelector(".form-addfile__input");
            if (nameInput) formFile.addEventListener("change", (() => {
                const fileName = nameInput.value;
                renderPreview(fileName, formPreview);
            }));
            const notification = document.querySelector(".notification");
            const notificationClose = document.querySelector(".notification__close");
            if (notification) {
                notification.classList.remove("_dn");
                setTimeout((function() {
                    notification.classList.remove("_hide");
                }), 100);
                notificationClose.addEventListener("click", (() => {
                    notification.classList.add("_hide");
                    setTimeout((function() {
                        notification.classList.add("_dn");
                    }), 100);
                }));
            }
            const videoLesson = document.getElementById("videoLesson");
            const progress = document.getElementById("progress");
            if (videoLesson) {
                const playBtnImage = document.getElementById("playBtn");
                const playBtn = document.querySelector(".controls-videolesson__play");
                const stopBtn = document.getElementById("stopBtn");
                document.getElementById("fullBtn");
                const fullBtnWrap = document.querySelector(".controls-videolesson__fullscreen");
                const time = document.querySelector(".controls-videolesson__timer");
                function toggleVideoStatus() {
                    if (videoLesson.paused) {
                        videoLesson.play();
                        playBtnImage.src = "./img/icons/video_pause.svg";
                    } else {
                        videoLesson.pause();
                        playBtnImage.src = "./img/icons/video_play.svg";
                    }
                }
                function stopVideoLesson() {
                    videoLesson.currentTime = 0;
                    videoLesson.pause();
                    playBtnImage.src = "./img/icons/video_play.svg";
                }
                videoLesson.addEventListener("timeupdate", (function() {
                    time.innerHTML = secondsToTime(videoLesson.currentTime);
                    progress.value = videoLesson.currentTime / videoLesson.duration * 100;
                }), false);
                progress.addEventListener("input", (() => {
                    videoLesson.currentTime = progress.value * videoLesson.duration / 100;
                }));
                function secondsToTime(time) {
                    let fulltime;
                    var h = Math.floor(time / (60 * 60)), dm = time % (60 * 60), m = Math.floor(dm / 60), ds = dm % 60, s = Math.ceil(ds);
                    if (60 === s) {
                        s = 0;
                        m += 1;
                    }
                    if (s < 10) s = "0" + s;
                    if (60 === m) {
                        m = 0;
                        h += 1;
                    }
                    if (m < 10) m = "0" + m;
                    if (0 === h) fulltime = m + ":" + s; else fulltime = h + ":" + m + ":" + s;
                    return fulltime;
                }
                playBtn.addEventListener("click", toggleVideoStatus);
                videoLesson.addEventListener("click", toggleVideoStatus);
                stopBtn.addEventListener("click", stopVideoLesson);
                fullBtnWrap.addEventListener("click", (() => {
                    videoLesson.requestFullscreen();
                }));
            }
            const newPass2 = document.getElementById("newPass2");
            if (newPass2) {
                const settingsSubmitButton = document.querySelector(".settings-form__button");
                newPass2.addEventListener("input", (function(e) {
                    const newPass1 = document.getElementById("newPass1");
                    if (newPass1.value !== newPass2.value) {
                        newPass1.classList.add("_form-error");
                        newPass2.classList.add("_form-error");
                        settingsSubmitButton.disabled = true;
                    } else {
                        newPass1.classList.remove("_form-error");
                        newPass2.classList.remove("_form-error");
                        settingsSubmitButton.disabled = false;
                    }
                }));
            }
            const base = document.querySelector(".base");
            if (base) {
                const baseSettings = document.querySelector(".settings-base");
                const baseOpen = document.querySelector(".base-header__link_redact");
                const baseSave = document.querySelector(".base-header__link_save");
                const baseContainer = document.querySelector(".page__wrapper_base");
                const baseContainerHeight = baseContainer.offsetHeight;
                if (isMobile.any()) base.style = `height: ${baseContainerHeight}px`;
                baseOpen.addEventListener("click", (() => {
                    baseOpen.classList.add("_dn");
                    baseSave.classList.remove("_dn");
                    document.documentElement.classList.add("base-settings-open");
                    const baseSettingsHeight = baseSettings.offsetHeight;
                    if (isMobile.any()) base.style = `height: ${baseContainerHeight}px; transform: translateY(-${baseSettingsHeight}px);`; else base.style = `transform: translateY(-${baseSettingsHeight}px);`;
                }));
                const settingsRadio = document.querySelectorAll(".settings-base__radio");
                settingsRadio.forEach((e => {
                    e.addEventListener("change", (() => {
                        const value = e.value;
                        base.classList.remove("base_mars", "base_moon", "base_neon", "base_green");
                        base.classList.add(value);
                    }));
                }));
            }
            const baseProgress = document.querySelectorAll(".base__progress");
            if (baseProgress) baseProgress.forEach((e => {
                const baseProgressSpan = e.querySelector(".base_range_span");
                const baseProgressPercent = parseInt(e.querySelector(".base_percent").innerText);
                if (baseProgressPercent < 50) baseProgressSpan.style = `width: ${baseProgressPercent / 100 * 100}%; background-color: #FC5A5A;`; else if (baseProgressPercent < 75 && 50 <= baseProgressPercent) baseProgressSpan.style = `width:${baseProgressPercent / 100 * 100}%; background-color: #FFC542;`; else if (baseProgressPercent >= 75) baseProgressSpan.style = `width:${baseProgressPercent / 100 * 100}%; background-color: #82C43C;`;
            }));
            const settingsbase = document.querySelector(".settingsbase");
            if (settingsbase) {
                const settingsOn = document.querySelector(".base-header__link_redact");
                const settingsSave = document.querySelector(".base-header__link_save");
                settingsOn.addEventListener("click", (() => {
                    settingsOn.classList.add("_dn");
                    settingsSave.classList.remove("_dn");
                    document.documentElement.classList.add("settingsbase-on");
                    const stantionSettings = document.querySelector(".stantion-settingsbase__popup");
                    stantionSettings.classList.remove("_dn");
                    const settingsModules = document.querySelectorAll(".items-settingsbase__item");
                    for (let i = 0; i < settingsModules.length; i++) {
                        let e = settingsModules[i];
                        e.setAttribute("data-popup", "#module-settings");
                        e.classList.add("_change" + i);
                        if (document.documentElement.classList.contains("settingsbase-on")) e.addEventListener("click", (ev => {
                            document.documentElement.classList.add(`settings-change-${i}`);
                            if (!document.querySelector(`input[name="module-${i}-new-icon"]`)) document.querySelector("form").insertAdjacentHTML("beforeend", `<input type="hidden" name="module-${i}-new-icon" value="">`);
                            if (!document.querySelector(`input[name="module-${i}-new-color"]`)) document.querySelector("form").insertAdjacentHTML("beforeend", `<input type="hidden" name="module-${i}-new-color" value="">`);
                        }));
                    }
                    const settingsIconsRadios = document.querySelectorAll(".icons-module-settings__radio");
                    const settingsColorsRadios = document.querySelectorAll(".color-module-settings__radio");
                    settingsIconsRadios.forEach((i => {
                        i.addEventListener("change", (() => {
                            document.documentElement.classList.forEach((e => {
                                if (e.includes("change")) {
                                    let numberOfChanges = parseInt(e[e.length - 1]);
                                    let changedImg = document.querySelectorAll(`.items-settingsbase__item img`)[numberOfChanges];
                                    let itemIdSpan = document.querySelectorAll(`.items-settingsbase__item .item_id`)[numberOfChanges];
                                    let itemID = "test_sign_"+itemIdSpan.value;
                                    //console.log(itemID);
                                    const value = i.value.split("***")[0];
                                    document.getElementById(itemID).value = i.value.split("***")[1];
                                    document.getElementById('changed_test_signs').value = document.getElementById('changed_test_signs').value+itemIdSpan.value+",";
                                    changedImg.src = `${value}`;
                                    document.querySelector(`input[name="module-${numberOfChanges}-new-icon"]`).value = value;
                                }
                            }));
                        }));
                    }));
                    settingsColorsRadios.forEach((i => {
                        i.parentNode.querySelector(".color-module-settings__label").style = `background-color: #${i.value.split("***")[0]}`;
                        i.addEventListener("change", (() => {
                            document.documentElement.classList.forEach((e => {
                                if (e.includes("change")) {
                                    let numberOfChanges = parseInt(e[e.length - 1]);
                                    let changedImg = document.querySelectorAll(`.items-settingsbase__name`)[numberOfChanges];
                                    let itemIdSpan = document.querySelectorAll(`.items-settingsbase__item .item_id`)[numberOfChanges];
                                    let itemID = "test_color_"+itemIdSpan.value;
                                    //console.log(itemID);
                                    const value = i.value.split("***")[0];
                                    changedImg.style = `background-color: #${value}`;
                                    document.getElementById(itemID).value = i.value.split("***")[1];
                                    document.getElementById('changed_test_colors').value = document.getElementById('changed_test_colors').value+itemIdSpan.value+",";
                                    document.querySelector(`input[name="module-${numberOfChanges}-new-color"]`).value = value;
                                }
                            }));
                        }));
                    }));
                    document.addEventListener("afterPopupClose", (function(e) {
                        if (document.documentElement.classList.contains("settingsbase-on")) setTimeout((() => {
                            document.documentElement.className = "settingsbase-on";
                            isWebp();
                        }), 700);
                    }));
                }));
                const stantionSettingsRadios = document.querySelectorAll(".stantion-settings__radio");
                stantionSettingsRadios.forEach((e => {
                    e.addEventListener("change", (() => {
                        const stantionSettingsRadioValue = e.value.split("***")[1];
                        if (document.documentElement.classList.contains("settingsbase-on")) {
                            const stantionImage = document.querySelector(".stantion-settingsbase__image");
                            stantionImage.src = `${stantionSettingsRadioValue}`;
                            document.getElementById("selected_station_id").value  = e.value.split("***")[0];
                            //stantionImage.parentNode.querySelector("source").srcset = `./img/base/${stantionSettingsRadioValue}.webp`;
                        }
                    }));
                }));
            }
            if (document.querySelector("#editor")) ClassicEditor.create(document.querySelector("#editor"), {}).catch((error => {
                console.error(error);
            }));
        }));
        window["FLS"] = true;
        isWebp();
        menuInit();
        chart_esm_Chart.register(ArcElement, LineElement, BarElement, PointElement, BarController, BubbleController, DoughnutController, LineController, PieController, PolarAreaController, RadarController, ScatterController, CategoryScale, LinearScale, LogarithmicScale, RadialLinearScale, TimeScale, TimeSeriesScale, plugin_decimation, plugin_filler, plugin_legend, plugin_title, plugin_tooltip, plugin_subtitle);
        spollers();
        tabs();
        showMore();
        formFieldsInit({
            viewPass: true
        });
        formSubmit();
    })();
})();